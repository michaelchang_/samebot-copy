#ifndef __MODULE_H
#define __MODULE_H

#include "SynchronizedQueueProcessor.h"
#include "intelligence/UserIntelligenceClient.h"
#include <string>
#include <thread>

namespace samebot {

class Configuration;
class IRCMessage;
class ModuleClient;

enum class MessageType {
    Incoming,
    Outgoing,
};

class Module : private SynchronizedQueueProcessor<std::pair<std::shared_ptr<IRCMessage>, MessageType>>, private UserIntelligenceClientMessageSender {
public:
    Module();
    virtual ~Module();

    void setConfiguration(const Configuration* configuration);
    const Configuration* configuration() const { return m_configuration; }

    void setClient(std::shared_ptr<ModuleClient> client) { m_client = client; }
    ModuleClient& client() const;

    // These are safe to call from any thread.
    // When a module is disabled, it simply stops enqueueing messages or responding to inactivity. If it has messages queued up,
    // it will continue to process them until its queue is empty, then enter a dormant state. This is a very lightweight toggle.
    bool isEnabled() const;
    void setEnabled(bool);

    void start();
    void stop();
    void processRemainingMessagesAndStop();

    void handleMessage(std::shared_ptr<IRCMessage>, MessageType);

    virtual std::string moduleIdentifier() const = 0;

    virtual double getActiveScore(std::shared_ptr<IRCMessage> message) = 0;
protected:
    virtual void didSetConfiguration(const Configuration*) {}
    virtual void didReceiveMessage(std::shared_ptr<IRCMessage>) = 0;

    // Called whenever any message is sent, including those from other modules.
    virtual void messageWasSent(std::shared_ptr<IRCMessage>) {}

    virtual bool wantsUserMessages() const { return true; }
    virtual bool wantsServerMessages() const { return false; }
    virtual bool wantsChannelMessages() const { return true; }
    virtual bool wantsPrivateMessages() const { return false; }

    // Overrides all of the above.
    virtual bool wantsMessageFiltering() const { return true; }

    bool isMention(const IRCMessage&) const;
    std::string messageBodyWithMentionStripped(const IRCMessage&) const;

    bool isPrivateMessage(const IRCMessage&) const;
    void sendMessage(std::string command, std::initializer_list<std::string> parameters = {});
    void sendMessage(std::string userOrChannel, std::string message);
    void replyToMessage(const IRCMessage&, std::string);

    // Support for modules that wake up periodically in addition to waking up on user feedback:
    // If this returns a positive value, the module will be woken up after this number of seconds, even if there were no messages.
    virtual int secondsOfInactivityBeforeModuleWakesUp() const { return 0; }
    // In that case, this handler will be executed.
    virtual void didWakeUpDueToInactivity() {}

    UserIntelligenceClient& userIntelligenceClient();

private:
    void sendMessage(std::shared_ptr<IRCMessage>);

    // SynchronizedQueueProcessor overrides.
    virtual void process(std::pair<std::shared_ptr<IRCMessage>, MessageType>) override;
    virtual int queuePopTimeoutInSeconds() const override { return secondsOfInactivityBeforeModuleWakesUp(); }
    virtual void handleTimeout() override;

    // UserIntelligenceClientMessageSender overrides.
    virtual void sendPublicMessageForUserIntelligenceClient(UserIntelligenceClient&, const std::string& message) override;
    virtual void sendPrivateMessageForUserIntelligenceClient(UserIntelligenceClient&, const std::string& recipient, const std::string& message) override;

    const Configuration* m_configuration = nullptr;
    std::weak_ptr<ModuleClient> m_client;
    std::thread m_messageProcessorThread;
    std::atomic_bool m_messageQueueFlushPending;
    std::unique_ptr<UserIntelligenceClient> m_userIntelligenceClient;
};

class ModuleClient {
public:
    virtual void sendMessage(std::shared_ptr<IRCMessage>) = 0;
    virtual std::vector<Module*> modulesWithIdentifier(std::string) = 0;
};

} // namespace

#endif // __MODULE_H

