#include "EmoticonModule.h"
#include "network/irc/IRCMessage.h"
#include "EmoticonList.h"
#include "basics/Assertions.h"
#include "basics/StringUtilities.h"

#include <string>
#include <vector>
#include <map>
#include <algorithm>

using namespace samebot;

static auto trimSpaces(std::string s) {
	if (s.empty()) return s;
	while (s.front() == ' ') s.erase(0, 1);
	while (s.back() == ' ')  s.pop_back();
    return s;
}

EmoticonModule::EmoticonModule()
    : SQLiteDataStore("emoticonList.db")
{
}

bool EmoticonModule::createSchema(SQLiteDatabase& database) {
	if (!database.executeSQL("CREATE TABLE emoticons (id VARCHAR(255) NOT NULL, emote VARCHAR, PRIMARY KEY(id))")) {
		return false;
	}

	for (const auto& emote : baseEmoticonList) {
		if (!insertEmoticon(emote.first, emote.second, &database)) {
			LOG_ERROR("Could not insert into emoticon list");
			return false;
		}
	}

	return true;
}

bool EmoticonModule::getEmoticonList(std::vector<std::pair<std::string, std::string>>& emoticonList) {
    if (!setUpSchemaIfNeeded()) {
        return false;
    }

	return database().executeSQLQuery("SELECT * FROM emoticons;", [&emoticonList](const SQLiteResultRow& row) {
		emoticonList.push_back({ row.get<std::string>(0), row.get<std::string>(1) });
	});
}

bool EmoticonModule::updateEmoticon(std::string id, std::string emote) {
    if (!setUpSchemaIfNeeded()) {
        return false;
    }

	return database().executeSQL("UPDATE emoticons SET emote = ? WHERE id = ?", emote, id);
}

bool EmoticonModule::insertEmoticon(std::string id, std::string emote, SQLiteDatabase* database) {
    // We intentionally don't call setUpSchemaIfNeeded() because this can be called as part of schema initialization.
    if (!database) {
        database = &this->database();
    }

	return database->executeSQL("INSERT INTO emoticons VALUES (?, ?)", id, emote);
}

bool EmoticonModule::deleteEmoticon(std::string id) {
	if (!setUpSchemaIfNeeded()) {
		return false;
	}

	return database().executeSQL("DELETE FROM emoticons WHERE id = ?", id);
}

bool EmoticonModule::getEmoticon(std::string id, std::string& emote) {
    if (setUpSchemaIfNeeded()) {
        return false;
    }

	bool status;
	status = database().executeSQLQuery("SELECT emote FROM emoticons WHERE id = '" + id + "'", [&emote](const SQLiteResultRow& row) {
		emote = row.get<std::string>(0);
	});

	return status;
}

bool EmoticonModule::findId(std::string s, std::string& id) {
	auto startPos = s.find("::");
    if (startPos == std::string::npos) {
        return false;
    }

    // Skip over the initial ::.
    startPos += 2;

	auto endPos = s.find("::", startPos);
    if (endPos == std::string::npos) {
        return false;
    }

	id = s.substr(startPos, endPos - startPos);
	return true;
}

void EmoticonModule::didReceiveMessage(std::shared_ptr<IRCMessage> message) {
    if (!message->prefix() || message->prefix()->type() != IRCMessagePrefix::Type::User) {
		LOG_DEBUG("Received message from server, not user; ignoring");
		return;
	}

	auto recipient = message->parameters().front();
	auto body = message->parameters().back();


	if (!recipient.length() || !body.length()) {
		LOG_ERROR("Received malformed PRIVMSG (missing recipient or length); ignoring");
		return;
	}

	if (!(message->command() == "PRIVMSG" && recipient[0] != '#')) {
		return;
	}


	auto sender = message->prefix()->user().nickname;

	if (body.find("emoticon list") != std::string::npos) {
		int count = 0;
		std::vector<std::pair<std::string, std::string> > emoticonList;
		bool status = getEmoticonList(emoticonList);
		if (status) {
			for (auto& emote : emoticonList) {
				count++;
				if (count >= 5) {
					count = 0;
					std::this_thread::sleep_for(std::chrono::seconds(1));

				}
				std::string s;
				s.append(emote.first);
				s.append(" ");
				s.append(emote.second);
				sendMessage(sender, s);
			}
		}
		else {
			sendMessage(sender, "Couldn't retrieve list. Sorry!");
		}
		std::this_thread::sleep_for(std::chrono::seconds(1));
		sendMessage(sender, "Emoticon List commands:");
		sendMessage(sender, "DELETE/INSERT/UPDATE ::emoteId:: emote");
		sendMessage(sender, "GET/DELETE ::emoteId::");
		return;
	}

	if (body.find("DELETE") != std::string::npos) {
		std::string id;
		if (findId(body, id)) {
			bool status = deleteEmoticon(id);
			if (status) {
				sendMessage(sender, "Deleted emote "+id+".");
			}
			else {
				sendMessage(sender, "Failed to delete emote"+id+".");
			}
		}
		else {
			sendMessage(sender, "Error with that command. Usage: [delete ::emoteId::].");
		}
		return;
	}

	if (body.find("INSERT") != std::string::npos) {
		std::string id;
		if (findId(body, id)) {

			std::string emote;
			emote = trimSpaces(body.substr(body.find(id)+id.length()+2));

			bool status = insertEmoticon(id, emote);
			if (status) {
				sendMessage(sender, "Inserted emote "+id+".");
			}
			else {
				sendMessage(sender, "Failed to insert emote ["+id+" "+emote+"].");
			}
		}
		else {
			sendMessage(sender, "Error with that command. Usage: [insert ::emoteId:: emote].");
		}
		return;
	}

	if (body.find("UPDATE") != std::string::npos) {
		std::string id;
		if (findId(body, id)) {
			std::string emote;
			emote = trimSpaces(body.substr(body.find(id)+id.length()+2));

			bool status = updateEmoticon(id, emote);
			if (status) {
				sendMessage(sender, "Updated emote "+id+".");
			}
			else {
				sendMessage(sender, "Failed to update emote ["+id+" "+emote+"].");
			}
		}
		else {
			sendMessage(sender, "Error with that command. Usage: [update ::emoteId:: emote].");
		}
		return;
	}

	if (body.find("GET") != std::string::npos) {
		std::string id;
		if (findId(body, id)) {
			std::string emote;
			bool status = getEmoticon(id, emote);
			if (status && !emote.empty()) {
				sendMessage(sender, emote);
			}
			else {
				sendMessage(sender, "Failed to get emote "+id+".");
			}

		}
		else {
			sendMessage(sender, "Error with that command. Usage: [get ::emote::].");
		}
		return;
	}

	if (body.find("-RESET LIST-") != std::string::npos) {
        destroyDatabase();
		if (setUpSchemaIfNeeded()) {
			sendMessage(sender, "Emoticon list reset and initialized.");
		}
		else {
			sendMessage(sender, "Error initializing emoticon list.");
		}
		return;
	}

	bool FIRSTCOLON = 0;
	bool SECONDCOLON = 0;
	bool ENDFIRSTCOLON = 0;

	unsigned int startPos = 0;
	unsigned int endPos = 0;
	std::string id;

	bool sendMsg = 0;


	for (unsigned int currPos = 0; currPos < body.length(); currPos++) {

		if (body[currPos] == ':') {
			if (FIRSTCOLON && SECONDCOLON && ENDFIRSTCOLON) {

				id.erase(std::remove(id.begin(), id.end(), ' '), id.end());
                id = lowercaseString(id);

				endPos = currPos+1;
				FIRSTCOLON = SECONDCOLON = ENDFIRSTCOLON = 0;
				//The id has ended. Search list for the emoticon.

				std::string emote;
				bool status = getEmoticon(id, emote);
				if (status && emote.length() > 0) {
					sendMsg = 1;
					body.replace(startPos, endPos-startPos, emote);
				}

				id.erase();
			}
			else if (FIRSTCOLON && SECONDCOLON) {
				ENDFIRSTCOLON = 1;
			}
			else if (FIRSTCOLON) {
				SECONDCOLON = 1;
			}
			else {
				startPos = currPos;
				FIRSTCOLON = 1;
			}
		}
		else {
			//The current character isn't a colon. If we are between colons, add it to the id
			if (FIRSTCOLON && SECONDCOLON && ENDFIRSTCOLON) {
				//There is a character between the ending colons '::id:a:'
				FIRSTCOLON = SECONDCOLON = ENDFIRSTCOLON = 0;
			}
			else if (FIRSTCOLON && SECONDCOLON) {
				id.push_back(body[currPos]);
			}
			else if (FIRSTCOLON) {
				FIRSTCOLON = 0;
			}
		}

	}
	if (sendMsg) {
		// sendMessage("#samebot-test", body);
		sendMessage("#general", body);
	}
}

double EmoticonModule::getActiveScore(std::shared_ptr<IRCMessage> message) {
    //Since we only want to respond to private messages, not "samebot:" prompted messages,
    //this should always be 0.
    return 0;
}
