#ifndef __EMOTICON_MODULE_H
#define __EMOTICON_MODULE_H

#include "modules/Module.h"
#include "database/SQLiteDataStore.h"

namespace samebot {

class EmoticonModule : public Module, private SQLiteDataStore {
public:
    virtual std::string moduleIdentifier() const override { return "emoticonModule"; }
    EmoticonModule();

    virtual double getActiveScore(std::shared_ptr<IRCMessage> message) override;
private:
    virtual void didReceiveMessage(std::shared_ptr<IRCMessage>) override;
    virtual bool wantsPrivateMessages() const override { return true; }

    bool findId(std::string s, std::string& id);

    bool getEmoticonList(std::vector<std::pair<std::string, std::string> >& emoticonList);
    bool updateEmoticon(std::string id, std::string emote);
    bool insertEmoticon(std::string id, std::string emote, SQLiteDatabase* = nullptr);
    bool deleteEmoticon(std::string id);
    bool getEmoticon   (std::string id, std::string& emote);

    virtual int currentSchemaVersion() const override { return 1; }
    virtual bool createSchema(SQLiteDatabase&) override;
};

} // namespace

#endif //__EMOTICON_MODULE_H
