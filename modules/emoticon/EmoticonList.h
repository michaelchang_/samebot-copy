#ifndef __EMOTICON_LIST_H
#define __EMOTICON_LIST_H

#include <map>

namespace samebot {


struct emoticon {
    std::string name;
    std::string emote;
};

std::map<std::string, std::string> baseEmoticonList = {

//bear emotes
{"bear", "ᶘ ᵒᴥᵒᶅ"},
{"beartableflip", "ʕノ•ᴥ•ʔノ ︵ ┻━┻"},
{"bearx", "٩ʕ•×•ʔ۶"},
{"bearlips", "ʕʽɞʼʔ"},
{"bearcute", "ʕ•ᴥ•ʔ"},
{"bearstoic", "ʕ ·(エ)· ʔ"},
{"bearright", "ʕっ•ᴥ•ʔっ"},
{"beardoit", "ʕง•ᴥ•ʔง"},
{"bearnotamused", "ʕ　·㉨·ʔ"},
{"bearsmall", "ˁ˙˟˙ˀ"},

//positive emotes
{ "idiot", "ᕕ( ᐛ )ᕗ"},
{ "excited", "(*≧▽≦)" },
{ "happy", "(•ᴗ•)" },
{ "happy2", "ﾍ(=￣∇￣)ﾉ" },
{ "happy3", "(๑>◡<๑)" },
{ "happy4", "(＊◕ᴗ◕＊)" },
{ "best", "٩(`(エ)´ )و" },
{ "danceright", "〜(^∇^〜）" },
{ "danceleft", "（〜^∇^)〜" },
{ "love", "(◍•ᴗ•◍)❤" },
{ "love2", "♡〜٩( ╹▿╹ )۶〜♡" },
{ "obnoxiouslove", "✿*ﾟ¨ﾟ✎･ ✿.｡.:* *.:｡✿*ﾟ¨ﾟ✎･✿.｡.:* ♡LOVE♡LOVE♡ ✿*ﾟ¨ﾟ✎･ ✿.｡.:*" },
{ "wedding", "*♡೫̥͙*:・ℋɑppყ Ϣәԁԁıɲɠﾟ･:* ೫̥͙♡*" },
{ "success", "٩(｡•ω•｡)و" },
{ "success2", "٩(｡•ㅅ•｡)و" },
{ "kiss", "( ˘ ³˘)❤" },

//negative emotes
{ "middlefinger", "凸(｀△´＋）" },
{ "doublemiddlefinger", "凸(｀⌒´)凸" },
{ "angry", " （╬ಠ益ಠ)" },
{ "disapproval", "ಠ_ಠ" },
{ "thatsmyfetish", "(ಠ◡ಠ)" },
{ "tableflip", "(╯°□°）╯︵ ┻━┻" },
{ "sad", "(๑◕︵◕๑)" },
{ "upset", "( ≧Д≦)" },
{ "cry", "｡：ﾟ(｡ﾉω＼｡)ﾟ･｡" },
{ "tears", "(。┰ω┰。)" },
{ "comfort", "(´._.`)\\(•ω•)" },

//other animals
{ "dog", "₍ᐢ•ﻌ•ᐢ₎*･ﾟ｡" },
{ "dogpaws", "ฅ^•ﻌ•^ฅ" },
{ "dog2", "◖⚆ᴥ⚆◗" },

//misc
{ "highfive", "( ⌒o⌒)人(⌒-⌒ )v" },
{ "obnoxioushello", "♡հҽӀӀօ♡* ૂི•̮͡• ૂ ྀෆ⃛﹡೫٭ॢ*⋆♡⁎೨ ♡⃛ෆ͙⃛ ॢ٭ॢ*⋆♡⁎೨" },
{ "obnoxiousbye", "★☆。.:*:･”ﾟ★βyёヾ(*ﾟ∇ﾟ*)ﾉβyё★｡.:*:･”☆★" },
{ "goodmorning", "☆❋──❁ɢ∞פ ʍօ®ɴɪɴɢ❃──❋" },
{ "happybirthday", ".*･ﾟ☆Happyヾ(*ε*)ﾉBirthday☆ﾟ･*." },
{ "thankyou", "(人❛ᴗ❛)♪тнайк　чоц♪(❛ᴗ❛*人)" },
{ "yeah", "( ✪ワ✪)ノʸᵉᵃʰᵎ" },


};


} // namespace

#endif //__EMOTICON_LIST_H
