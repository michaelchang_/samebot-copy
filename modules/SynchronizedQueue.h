#ifndef __SYNCHRONIZED_QUEUE_H
#define __SYNCHRONIZED_QUEUE_H

#include <atomic>
#include <condition_variable>
#include <memory>
#include <mutex>
#include <queue>

namespace samebot {

template <typename T>
class SynchronizedQueue {
public:
    SynchronizedQueue()
        : m_stopWaiting(false)
    {
    }

    void push(T t) {
        bool wasEmpty;
        {
            std::lock_guard<std::mutex> lock(m_accessMutex);
            wasEmpty = m_queue.empty();
            m_queue.push(t);
        }

        if (wasEmpty) {
            m_nonEmptyCondition.notify_one();
        }
    }

    bool popWhenAvailable(T& t, int timeoutInSeconds) {
        {
            std::unique_lock<std::mutex> lock(m_accessMutex);
            auto predicate = [this] {
                return !m_queue.empty() || m_stopWaiting;
            };

            bool waitCompleted = false;
            if (timeoutInSeconds > 0) {
                waitCompleted = m_nonEmptyCondition.wait_for(lock, std::chrono::seconds(timeoutInSeconds), predicate);
            } else {
                m_nonEmptyCondition.wait(lock, predicate);
                waitCompleted = !m_stopWaiting;
            }

            if (!waitCompleted) {
                return false;
            }

            t = m_queue.front();
            m_queue.pop();
        }
        return true;
    }

    bool isEmpty() {
        std::lock_guard<std::mutex> lock(m_accessMutex);
        return m_queue.empty();
    }

    void stopWaiting() {
        if (!m_stopWaiting) {
            m_stopWaiting = true;
            m_nonEmptyCondition.notify_one();
        }
    }

private:
    std::atomic_bool m_stopWaiting;
    std::queue<T> m_queue;
    std::mutex m_accessMutex;
    std::condition_variable m_nonEmptyCondition;
};

} // namespace

#endif // __SYNCHRONIZED_QUEUE_H

