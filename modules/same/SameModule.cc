#include "SameModule.h"

#include "basics/Assertions.h"
#include "basics/StringUtilities.h"
#include "network/irc/IRCMessage.h"
#include "modules/ModuleActiveResponseHandler.h"
using namespace samebot;

SameModule::SameModule()
    : m_dataStore("same_bag_of_words.db")
    , m_stringProcessor(m_dataStore)
{
}

bool SameModule::isSelfSame(const IRCMessage& message) {
    if (!m_lastNonSameMessage) {
        return false;
    }

    if (auto* prefix = message.prefix()) {
        if (auto* lastNonSameMessagePrefix = m_lastNonSameMessage->prefix()) {
            return prefix->user().username == lastNonSameMessagePrefix->user().username;
        }
    }

    return false;
}

static auto confidenceThatMessageIsSame(const IRCMessage& message) {
    const auto& body = message.parameters().back();
    if (isFuzzyMatch(body, "same tbh")) {
        return 2;
    }

    if (isFuzzyMatch(body, "same")) {
        return 2;
    }

    if (splitString(body).size() <= 3 && findFirstFuzzyWordMatch(body, "same") != std::string::npos) {
        return 1;
    }

    return 0;
}

void SameModule::didReceiveMessage(std::shared_ptr<IRCMessage> message) {
    if (tryToIncorporateFeedback(*message)) {
        return;
    }

    // Only feedback is supported via private message for now.
    if (isPrivateMessage(*message)) {
        return;
    }

    auto sameConfidence = confidenceThatMessageIsSame(*message);
    if (!sameConfidence) {
        m_lastNonSameMessage = message;
        respondWithSameIfAppropriate();
        return;
    }

    if (!m_lastNonSameMessage) {
        LOG_DEBUG("SameModule: Interpreted '%s' as same (confidence %d) but don't know which message it's related to", message->parameters().back().c_str(), sameConfidence);
        return;
    }

    if (isSelfSame(*message)) {
        LOG_DEBUG("SameModule: Interpreted '%s' as same (confidence %d) in response to '%s' but ignoring as it's by the same user", message->parameters().back().c_str(), sameConfidence, m_lastNonSameMessage->parameters().back().c_str());
        return;
    }

    LOG_DEBUG("SameModule: Interpreted '%s' as same (confidence %d) in response to '%s'", message->parameters().back().c_str(), sameConfidence, m_lastNonSameMessage->parameters().back().c_str());
    auto body = m_lastNonSameMessage->parameters().back();
    for (int i = 0; i < sameConfidence; ++i) {
        m_stringProcessor.noteStringWasInteresting(body);
    }
}

std::string SameModule::randomResponse() {
    struct Response {
        std::string text;
        int count;
        bool canRepeat;
    };

    // Should be sorted from highest to lowest count.
    std::vector<Response> responses {
        { "same", 20, true },
        { "same tbh", 10, true },
        { "s a m e", 3, false },
        { "sameee", 2, false },
        { "saaaame", 2, false },
        { "yeah same", 1, false },
        { "yea same", 1, false },
        { "lol same", 1, false },
        { "same lol", 1, false },
        { "same af", 1, false },
        { "yo same", 1, false },
        { "s-same", 1, false },
    };

    int totalCount = 0;
    for (const auto& response : responses) {
        totalCount += response.count;
    }

    int index = rand() % (totalCount + 1);
    for (const auto& response : responses) {
        index -= response.count;
        if (index <= 0 && (response.canRepeat || response.text != m_lastResponse)) {
            return (m_lastResponse = response.text);
        }
    }

    // If we fell through to here, we randomly picked the last response in the list but it was the response we sent last time, so
    // we couldn't find any alternatives. Wrap around to the first response.
    return (m_lastResponse = responses.front().text);
}

double SameModule::minimumElapsedTimeIntervalInMinutesForResponseWithProbability(double probability) const {
    ASSERT(m_lastResponse.length());
    ASSERT(probability >= 0);
    ASSERT(probability <= 1);

    // The threshold falls off linearly based on how high the probability is.
    static const double largestTimeIntervalInMinutes = 90;
    static const double smallestTimeIntervalInMinutes = 30;
    return smallestTimeIntervalInMinutes + (largestTimeIntervalInMinutes - smallestTimeIntervalInMinutes) * (1 - probability);
}

void SameModule::respondWithSameIfAppropriate() {
    auto probability = m_stringProcessor.probabilityOfStringBeingInteresting(m_lastNonSameMessage->parameters().back());
    if (probability < 0.5) {
        LOG_DEBUG("SameModule: '%s' has probability %.2f of being sameworthy; ignoring", m_lastNonSameMessage->parameters().back().c_str(), probability);
        return;
    }

    int randomNumber = rand() % 100;
    if (randomNumber > probability / 2 * 100) {
        LOG_DEBUG("SameModule: Randomly decided not to same '%s' despite %.2f probability of being sameworthy", m_lastNonSameMessage->parameters().back().c_str(), probability);
        return;
    }

    if (m_lastResponse.length()) {
        auto elapsedMinutes = std::chrono::duration_cast<std::chrono::minutes>(std::chrono::steady_clock::now() - m_timeOfLastResponse).count();
        auto minimumElapsedMinutes = minimumElapsedTimeIntervalInMinutesForResponseWithProbability(probability);
        if (elapsedMinutes < minimumElapsedMinutes) {
            LOG_DEBUG("SameModule: Not sameing '%s' (%.2f sameworthiness probability) because only %d minutes have elapsed (threshold %.2f)",
                m_lastNonSameMessage->parameters().back().c_str(), probability, static_cast<int>(elapsedMinutes), minimumElapsedMinutes);
            return;
        }
    }

    LOG_DEBUG("SameModule: '%s' has probability %.2f of being sameworthy; sameing", m_lastNonSameMessage->parameters().back().c_str(), probability);
    std::this_thread::sleep_for(std::chrono::seconds(1 + rand() % 3));
    replyToMessage(*m_lastNonSameMessage, randomResponse());
    m_lastMessageRespondedTo = m_lastNonSameMessage;
    m_timeOfLastResponse = std::chrono::steady_clock::now();
}

bool SameModule::tryToIncorporateFeedback(const IRCMessage& message) {
    if (!m_lastMessageRespondedTo) {
        return false;
    }

    auto body = message.parameters().back();
    if (!isPrivateMessage(message) && !ModuleActiveResponseHandler::hasSamebotPrefix(body)) {
        return false;
    }

    body = lowercaseString(stringByTrimmingString(ModuleActiveResponseHandler::removeSamebotPrefix(body)));

    auto bodyOfLastMessageRespondedTo = m_lastMessageRespondedTo->parameters().back();

    if (hasElongatedSuffixMatch(body, { "good", "same" }) || hasElongatedSuffixMatch(body, { "great", "same" })) {
        LOG_DEBUG("SameModule: Interpreted '%s' as positive feedback for response to '%s'", body.c_str(), bodyOfLastMessageRespondedTo.c_str());
        m_stringProcessor.noteStringWasInteresting(bodyOfLastMessageRespondedTo);
        replyToMessage(message, "thanks! you'll see more like that from me");
        return true;
    }

    if (hasElongatedSuffixMatch(body, { "bad", "same" })) {

        LOG_DEBUG("SameModule: Interpreted '%s' as negative feedback for response to '%s'", body.c_str(), bodyOfLastMessageRespondedTo.c_str());
        m_stringProcessor.noteStringWasUninteresting(bodyOfLastMessageRespondedTo);
        replyToMessage(message, "noted! i'll try to do better next time");
        return true;
    }

    return false;
}
