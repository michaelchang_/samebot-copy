#ifndef __SAME_MODULE_H
#define __SAME_MODULE_H

#include "bag_of_words/BagOfWordsSQLiteDataStore.h"
#include "bag_of_words/BagOfWordsStringProcessor.h"
#include "modules/Module.h"

namespace samebot {

class SameModule : public Module {
public:
    SameModule();
    virtual std::string moduleIdentifier() const override { return "SameModule"; }
    virtual double getActiveScore(std::shared_ptr<IRCMessage> message) override { return -1; };
#ifdef DEBUG
    std::string test_randomResponse() { return randomResponse(); }
#endif

private:
    virtual void didReceiveMessage(std::shared_ptr<IRCMessage>) override;
    virtual bool wantsPrivateMessages() const override { return true; }

    bool isSelfSame(const IRCMessage&);
    bool tryToIncorporateFeedback(const IRCMessage&);

    std::string randomResponse();
    double minimumElapsedTimeIntervalInMinutesForResponseWithProbability(double) const;
    void respondWithSameIfAppropriate();

    std::string m_lastResponse;
    std::chrono::steady_clock::time_point m_timeOfLastResponse;
    std::shared_ptr<IRCMessage> m_lastNonSameMessage;
    std::shared_ptr<IRCMessage> m_lastMessageRespondedTo;
    BagOfWordsSQLiteDataStore m_dataStore;
    BagOfWordsStringProcessor m_stringProcessor;
};

} // namespace

#endif // __SAME_MODULE_H

