#ifndef __REVERSE_MODULE_H
#define __REVERSE_MODULE_H

#include "modules/Module.h"

namespace samebot {

class ReverseModule : public Module {
public:
    virtual std::string moduleIdentifier() const override { return "ReverseModule"; }
    virtual double getActiveScore(std::shared_ptr<IRCMessage> message) override;
private:
    virtual void didReceiveMessage(std::shared_ptr<IRCMessage>) override;
};

} // namespace

#endif // __REVERSE_MODULE_H

