#include "ReverseModule.h"

#include "basics/StringUtilities.h"
#include "network/irc/IRCMessage.h"
#include "modules/ModuleActiveResponseHandler.h"
#include <algorithm>

using namespace samebot;

void ReverseModule::didReceiveMessage(std::shared_ptr<IRCMessage> message) {
    auto recipient = message->parameters().front();
    auto body = message->parameters().back();

    if (!ModuleActiveResponseHandler::hasSamebotPrefix(body)) return;
    body = ModuleActiveResponseHandler::removeSamebotPrefix(body);
    if (body != "ayy lmao") {
        std::reverse(body.begin(), body.end());
    }

    body = message->prefix()->user().nickname + ": " + body;
    sendMessage(recipient, body);
}

double ReverseModule::getActiveScore(std::shared_ptr<IRCMessage> message) {
    return -1;
}
