#ifndef __SYNCHRONIZED_QUEUE_PROCESSOR_H
#define __SYNCHRONIZED_QUEUE_PROCESSOR_H

#include "SynchronizedQueue.h"
#include "basics/Assertions.h"

namespace samebot {

template <typename T>
class SynchronizedQueueProcessor {
public:
    SynchronizedQueueProcessor()
        : m_state(State::Stopped)
    {
    }

    enum class ShouldFlushQueue {
        No,
        Yes,
    };

    void startProcessing(ShouldFlushQueue flushImmediately = ShouldFlushQueue::No) {
        ASSERT(m_state == State::Stopped);

        switch (flushImmediately) {
            case ShouldFlushQueue::No:
                m_state = State::Running;
                break;
            case ShouldFlushQueue::Yes:
                m_state = State::FlushingQueue;
                break;
        }

        queueProcessorLoop();
    }

    void stopProcessing(ShouldFlushQueue shouldFlushQueue = ShouldFlushQueue::No) {
        switch (shouldFlushQueue) {
            case ShouldFlushQueue::No:
                m_state = State::Stopped;
                m_queue.stopWaiting();
                break;
            case ShouldFlushQueue::Yes:
                m_state = State::FlushingQueue;
                break;
        }
    }

    bool isProcessing() const { return m_state != State::Stopped; }

    void enqueue(T t) {
        m_queue.push(t);
    }

protected:
    virtual int queuePopTimeoutInSeconds() const { return 0; }
    virtual void process(T) = 0;
    virtual void handleTimeout() {}

private:
    void queueProcessorLoop() {
        while (m_state != State::Stopped) {
            if (m_state == State::FlushingQueue && m_queue.isEmpty()) {
                break;
            }

            T next;
            bool gotResult = m_queue.popWhenAvailable(next, queuePopTimeoutInSeconds());
            // It's possible for the processor to have been stopped while we were waiting for the queue to pop.
            if (m_state == State::Stopped) {
                break;
            }

            if (gotResult) {
                process(next);
            } else {
                handleTimeout();
            }
        }
    }

    enum class State {
        Stopped,
        Running,
        FlushingQueue,
    };

    std::atomic<State> m_state;
    SynchronizedQueue<T> m_queue;
    int m_timeoutInSeconds;
};

} // namespace

#endif // __SYNCHRONIZED_QUEUE_PROCESSOR_H

