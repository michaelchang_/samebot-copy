#ifndef __MODULE_SWITCHBOARD_H
#define __MODULE_SWITCHBOARD_H

#include "Module.h"
#include "ModuleActiveResponseHandler.h"

namespace samebot {

class ModuleSwitchboardMessageSender;

class ModuleSwitchboard : public ModuleClient, private SynchronizedQueueProcessor<std::shared_ptr<IRCMessage>>, public std::enable_shared_from_this<ModuleSwitchboard> {
public:
    void registerModule(std::unique_ptr<Module>);
    void dispatchMessageToModules(std::shared_ptr<IRCMessage>);
    void notifyModulesMessageWasSent(std::shared_ptr<IRCMessage>);

    void startProcessingOutgoingMessages();
    void stopProcessingOutgoingMessages();

    void setMessageSender(std::shared_ptr<ModuleSwitchboardMessageSender> messageSender) { m_messageSender = messageSender; }

private:
    virtual void sendMessage(std::shared_ptr<IRCMessage>) override;
    virtual std::vector<Module*> modulesWithIdentifier(std::string) override;

    virtual void process(std::shared_ptr<IRCMessage>) override;

    std::vector<std::unique_ptr<Module>> m_modules;
    std::weak_ptr<ModuleSwitchboardMessageSender> m_messageSender;
    ModuleActiveResponseHandler m_responseHandler;
};

class ModuleSwitchboardMessageSender {
public:
    virtual void sendMessageForModuleSwitchboard(ModuleSwitchboard&, std::shared_ptr<IRCMessage>) = 0;
};

} // namespace

#endif // __MODULE_SWITCHBOARD_H

