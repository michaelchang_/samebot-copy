#include "ScrapbookSQLiteDataStore.h"

#include "basics/Assertions.h"
#include "basics/StringUtilities.h"

using namespace samebot;

void ScrapbookSQLiteDataStore::storeMemory(std::shared_ptr<ScrapbookMemory> memory) {
    if (!setUpSchemaIfNeeded()) {
        return;
    }

    database().executeSQL("INSERT INTO scrapbook_memories VALUES (NULL, ?, ?)", memory->memory(), memory->context());
}

void ScrapbookSQLiteDataStore::deleteMemory(const ScrapbookMemory& memory) {
    if (!setUpSchemaIfNeeded()) {
        return;
    }

    database().executeSQL("DELETE FROM scrapbook_memories WHERE memory = ?", memory.memory());
}

std::shared_ptr<ScrapbookMemory> ScrapbookSQLiteDataStore::retrieveRandomMemory() {
    std::string memory;
    std::string context;
    database().executeSQLQuery("SELECT memory, context FROM scrapbook_memories ORDER BY RANDOM() LIMIT 1;", [&memory, &context](const SQLiteResultRow& row) {
        memory = row.get<std::string>(0);
        context = row.get<std::string>(1);
    });

    return std::make_shared<ScrapbookMemory>(memory, context);
}

std::shared_ptr<ScrapbookMemory> ScrapbookSQLiteDataStore::findMemoryContainingString(std::string query) const {
    query = stringByTrimmingString(lowercaseString(query));
    auto enumerator = database().rowEnumeratorForSQLQuery("SELECT id, memory, context FROM scrapbook_memories WHERE memory LIKE ?", "%" + query + "%");
    if (!enumerator) {
        return nullptr;
    }

    int id = -1;
    std::string memory;
    std::string context;
    bool hasWholeWordMatch = false;
    bool isCompleteMatch = false;
    (*enumerator)([&query, &id, &memory, &context, &hasWholeWordMatch, &isCompleteMatch](const SQLiteResultRow& row) {
        int candidateID = row.get<int>(0);
        std::string candidateMemory = row.get<std::string>(1);
        std::string candidateContext = row.get<std::string>(2);
        bool candidateHasWholeWordMatch = false;
        bool candidateIsCompleteMatch = stringByTrimmingString(lowercaseString(candidateMemory)) == query;
        for (auto word : splitString(candidateMemory)) {
            word = stringByTrimmingString(lowercaseString(word));
            if (word == query) {
                candidateHasWholeWordMatch = true;
                break;
            }
        }

        bool candidateIsBetterMatch = candidateHasWholeWordMatch && !hasWholeWordMatch;
        bool candidateIsWorseMatch = (hasWholeWordMatch && !candidateHasWholeWordMatch) || (isCompleteMatch && !candidateIsCompleteMatch);
        bool candidateIsMoreRecentMatch = id == -1 || candidateID > id;
        if (!candidateIsWorseMatch && (candidateIsCompleteMatch || candidateIsBetterMatch || candidateIsMoreRecentMatch)) {
            id = candidateID;
            memory = candidateMemory;
            context = candidateContext;
            hasWholeWordMatch = candidateHasWholeWordMatch;
            isCompleteMatch = candidateIsCompleteMatch;
        }
    });

    if (id == -1) {
        return nullptr;
    }

    return std::make_shared<ScrapbookMemory>(memory, context);
}

#pragma mark - SQLiteDataStore overrides

bool ScrapbookSQLiteDataStore::createSchema(SQLiteDatabase& database) {
    return database.executeSQL("CREATE TABLE scrapbook_memories (id INTEGER PRIMARY KEY, memory TEXT NOT NULL, context TEXT)");
}

