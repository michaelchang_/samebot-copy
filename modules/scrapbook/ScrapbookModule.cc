#include "ScrapbookModule.h"

#include "ScrapbookSQLiteDataStore.h"
#include "ScrapbookStringUtilities.h"
#include "basics/StringUtilities.h"
#include "network/irc/IRCMessage.h"
#include "modules/ModuleActiveResponseHandler.h"
using namespace samebot;

const int contextWindowSize = 14;

// FIXME: Should this be configurable?
static const std::string databaseFileName = "scrapbook.db";

ScrapbookModule::ScrapbookModule()
    : ScrapbookModule(std::make_unique<ScrapbookSQLiteDataStore>(databaseFileName))
{
}

#pragma mark - Internals

void ScrapbookModule::didReceiveMessage(std::shared_ptr<IRCMessage> message) {
    if (!isPrivateMessage(*message)) {
        m_history.addMessageToHistory(message, ScrapbookMessageHistory::MessageType::Received);
        m_hasSentAutomaticConversationStarterMemory = false;
    }

    if (handleMessageCommand(message)) {
        return;
    }

    // None of the proactive memory logic applies to private messages; we only want it in public chat rooms.
    if (isPrivateMessage(*message)) {
        return;
    }

    auto proactiveMemory = m_proactiveMemoryManager.createProactiveMemory();
    if (!proactiveMemory || proactiveMemory->messageIndex == m_proactiveMemoryManager.indexOfMostRecentProactiveMemory()) {
        return;
    }

    // Avoid overlapping context when saving memories.
    if (m_proactiveMemoryManager.indexOfMostRecentProactiveMemory() != -1 && abs(m_proactiveMemoryManager.indexOfMostRecentProactiveMemory() - proactiveMemory->messageIndex) <= contextWindowSize) {
        LOG_DEBUG("ScrapbookModule: Created a proactive memory too recently; ignoring");
        return;
    }

    m_memoryMostRecentlyMentioned = saveMemory(proactiveMemory->messageIndex, proactiveMemory->contextWindowStart, proactiveMemory->contextWindowEnd, IncludeLastMessage::Yes);
    m_proactiveMemoryManager.setIndexOfMostRecentProactiveMemory(proactiveMemory->messageIndex);
    auto channel = message->parameters().front();
    sendMessage("TOPIC", { channel, formatMessage(m_history.messageAtIndex(proactiveMemory->messageIndex)) });
}

bool ScrapbookModule::handleMessageCommand(std::shared_ptr<IRCMessage> message) {
    auto messageText = stringByTrimmingString(message->parameters().back());
    if (!isPrivateMessage(*message) && !ModuleActiveResponseHandler::hasSamebotPrefix(messageText)) {
        return false;
    }

    bool positiveFeedback = isPositiveFeedback(messageText);
    bool negativeFeedback = isNegativeFeedback(messageText);
    if (positiveFeedback || negativeFeedback) {
        if (!m_memoryMostRecentlyMentioned) {
            replyToMessage(*message, "i don't know what memory that feedback is referring to");
            return true;
        }

        if (positiveFeedback && negativeFeedback) {
            LOG_ERROR("ScrapbookModule: Interpreted '%s' as both positive and negative feedback; ignoring", messageText.c_str());
        } else {
            LOG_DEBUG("ScrapbookModule: Interpreted '%s' as %s feedback", messageText.c_str(), positiveFeedback ? "positive" : "negative");
            m_proactiveMemoryManager.incorporateFeedbackForMemory(positiveFeedback ? ScrapbookProactiveMemoryManager::Feedback::Positive : ScrapbookProactiveMemoryManager::Feedback::Negative, *m_memoryMostRecentlyMentioned);
            if (positiveFeedback) {
                replyToMessage(*message, "thanks! i'll come up with more like that");
            } else {
                replyToMessage(*message, "noted! i'll remove that memory");
                m_dataStore->deleteMemory(*m_memoryMostRecentlyMentioned);
                m_memoryMostRecentlyMentioned = nullptr;
            }
            return true;
        }
    }

    if (auto memory = m_dataStoreSearcher.tryToSearchDataStoreForMemory(message)) {
        LOG_DEBUG("ScrapbookModule: Interpreted '%s' as memory search", messageText.c_str());
        static std::vector<std::string> explanations {
            "i found this memory: ",
            "here's what i found: ",
            "is this what you wanted? ",
            "is this what you were looking for? ",
        };

        m_memoryMostRecentlyMentioned = memory;
        replyToMessage(*message, explanations[rand() % explanations.size()] + memory->memory());
        return true;
    }

    int indexToSave = -1;
    if (m_historyMatcher.tryToMatchMessageAgainstHistory(message, indexToSave)) {
        ASSERT(indexToSave >= 0);
        // If we were asked to remember something via public message, don't include the last message as it's the "remember" command.
        IncludeLastMessage includeLastMessage = isPrivateMessage(*message) ? IncludeLastMessage::Yes : IncludeLastMessage::No;
        m_memoryMostRecentlyMentioned = saveMemoryForMessageAtIndex(indexToSave, includeLastMessage, message);
        // If the user specifically asked us to create this memory, consider it a good memory automatically and update our models.
        m_proactiveMemoryManager.incorporateFeedbackForMemory(ScrapbookProactiveMemoryManager::Feedback::Positive, *m_memoryMostRecentlyMentioned);
        return true;
    }

    if (isRequestForRandomMemory(messageText)) {
        LOG_DEBUG("ScrapbookModule: Interpreted '%s' as request for random memory", messageText.c_str());
        auto memory = m_dataStore->retrieveRandomMemory();
        m_memoryMostRecentlyMentioned = memory;
        // Interleave nicknames with zero-width space characters to avoid mentioning people.
        replyToMessage(*message, interleaveNicknamesWithZeroWidthSpaces(memory->memory()));
        return true;
    }

    if (isRequestForContext(messageText)) {
        LOG_DEBUG("ScrapbookModule: Interpreted '%s' as request for memory context", messageText.c_str());
        if (m_memoryMostRecentlyMentioned) {
            // Interleave nicknames with zero-width space characters to avoid mentioning people.
            auto context = interleaveNicknamesWithZeroWidthSpaces(m_memoryMostRecentlyMentioned->context());
            std::string separator = "------------";
            replyToMessage(*message, separator + "\r\n" + indentLines(context, "    ") + "\r\n" + separator);
        } else {
            LOG_DEBUG("ScrapbookModule: Ignoring request for memory context because there was no memory");
        }
        return true;
    }

    return false;
}

void ScrapbookModule::messageWasSent(std::shared_ptr<IRCMessage> message) {
    if (!isPrivateMessage(*message)) {
        m_history.addMessageToHistory(message, ScrapbookMessageHistory::MessageType::Sent);
    }
}

void ScrapbookModule::didWakeUpDueToInactivity() {
    if (m_hasSentAutomaticConversationStarterMemory) {
        return;
    }

    // FIXME: This is awkward. We should have some higher-level way to know which channel the bot is a part of.
    std::string channel;
    for (int i = static_cast<int>(m_history.size()) - 1; i >= 0; --i) {
        channel = m_history.messageAtIndex(i)->parameters().front();
        if (channel.length()) {
            break;
        }
    }

    if (!channel.length()) {
        LOG_DEBUG("ScrapbookModule: Skipping conversation starter because I have no idea where to send the memory");
        return;
    }

    m_memoryMostRecentlyMentioned = m_dataStore->retrieveRandomMemory();
    sendMessage(channel, "hey, remember this? " + m_memoryMostRecentlyMentioned->memory());
    m_hasSentAutomaticConversationStarterMemory = true;
}

bool ScrapbookModule::isRequestForRandomMemory(std::string message) {
    // Pretty naive heuristic for now. Just check if the request ends with a number of predefined triggers.
    static std::vector<std::string> triggers {
        "memory",
        "story",
        "from the scrapbook",
        "from scrapbook",
        "you remember",
    };

    return hasFuzzySuffixMatch(message, triggers);
}

bool ScrapbookModule::isRequestForContext(std::string message) {
    static std::vector<std::string> triggers {
        "what",
        "huh",
        "context",
        "context please",
        "context pls",
        "wut",
        "wat",
        "wot",
        "wtf",
        "wth",
        "?",
    };

    return hasFuzzySuffixMatch(message, triggers);
}

bool ScrapbookModule::isPositiveFeedback(std::string message) {
    return stringHasSuffix(message, "good memory");
}

bool ScrapbookModule::isNegativeFeedback(std::string message) {
    return stringHasSuffix(message, "bad memory");
}

std::shared_ptr<ScrapbookMemory> ScrapbookModule::saveMemoryForMessageAtIndex(int index, IncludeLastMessage includeLastMessage, std::shared_ptr<IRCMessage> messageToSendFeedbackInResponseTo) {
    auto start = std::max(index - contextWindowSize / 2, 0);
    auto minimumEnd = static_cast<int>(m_history.size()) - 1;
    if (includeLastMessage == IncludeLastMessage::No) {
        --minimumEnd;
    }
    auto end = std::min<int>(index + contextWindowSize / 2, minimumEnd);

    return saveMemory(index, start, end, includeLastMessage, messageToSendFeedbackInResponseTo);
}

std::shared_ptr<ScrapbookMemory> ScrapbookModule::saveMemory(int index, int contextWindowStart, int contextWindowEnd, IncludeLastMessage includeLastMessage, std::shared_ptr<IRCMessage> messageToSendFeedbackInResponseTo) {
    std::string context;
    for (int i = contextWindowStart; i <= contextWindowEnd; ++i) {
        context += formatMessage(m_history.messageAtIndex(i));
        if (i != contextWindowEnd) {
            context += "\r\n";
        }
    }

    auto message = formatMessage(m_history.messageAtIndex(index));
    auto memory = std::make_shared<ScrapbookMemory>(message, context);
    m_dataStore->storeMemory(memory);

    LOG_DEBUG("ScrapbookModule: Saving memory '%s'", message.c_str());
    if (messageToSendFeedbackInResponseTo) {
        replyToMessage(*messageToSendFeedbackInResponseTo, "ok, I'll remember: " + message);
    }

    return memory;
}

ScrapbookModule::ScrapbookModule(std::unique_ptr<ScrapbookDataStore> dataStore)
    : m_dataStore(std::move(dataStore))
    , m_proactiveMemoryManager(m_history, contextWindowSize)
    , m_historyMatcher(m_history)
    , m_dataStoreSearcher(*m_dataStore)
{
}

#pragma mark - Unit test support

#ifdef DEBUG

std::shared_ptr<ScrapbookModule> ScrapbookModuleTestShim::createModule(std::unique_ptr<ScrapbookDataStore> dataStore) {
    return std::shared_ptr<ScrapbookModule>(new ScrapbookModule(std::move(dataStore)));
}

ScrapbookModuleTestShim::ScrapbookModuleTestShim(std::shared_ptr<ScrapbookModule> module)
    : m_module(module)
{
}

void ScrapbookModuleTestShim::didReceiveMessage(std::shared_ptr<IRCMessage> message) {
    m_module->didReceiveMessage(message);
}

bool ScrapbookModuleTestShim::isRequestForRandomMemory(std::string message) {
    return m_module->isRequestForRandomMemory(message);
}

int ScrapbookModuleTestShim::bestIndexForProactiveMemory() {
    if (auto memory = m_module->m_proactiveMemoryManager.createProactiveMemory()) {
        return memory->messageIndex;
    }
    return -1;
}

void ScrapbookModuleTestShim::insertMessageInHistory(std::shared_ptr<IRCMessage> message, int secondsSinceStart, ScrapbookMessageHistory::MessageType type) {
    auto& history = m_module->m_history;
    history.addMessageToHistory(message, ScrapbookMessageHistory::MessageType::Received);
    history.test_overwriteLastEntryFields(secondsSinceStart, type);
}

void ScrapbookModuleTestShim::useProactiveMemoryAlgorithm(std::unique_ptr<ScrapbookProactiveMemoryAlgorithm> algorithm) {
    m_module->m_proactiveMemoryManager.m_proactiveMemoryAlgorithm = std::move(algorithm);
}

#endif // DEBUG
