#ifndef __SCRAPBOOK_PROACTIVE_MEMORY_ALGORITHM_H
#define __SCRAPBOOK_PROACTIVE_MEMORY_ALGORITHM_H

#include <memory>
#include <string>

namespace samebot {

class IRCMessage;
class ScrapbookMemory;
class ScrapbookMessageHistory;

class ScrapbookProactiveMemoryAlgorithm {
public:
    // Expected to be nonnegative. Larger scores should be better.
    virtual double scoreForHistoryWindow(const ScrapbookMessageHistory&, int start, int end) = 0;
    virtual double scoreThresholdForProactiveMemory() = 0;

    virtual double scoreForMessage(std::shared_ptr<IRCMessage>) { return 0; }

    virtual void incorporatePositiveFeedbackForMemory(const ScrapbookMemory&) {}
    virtual void incorporateNegativeFeedbackForMemory(const ScrapbookMemory&) {}

    virtual bool shouldRejectCandidateWindowsBasedOnCenterMessageScore() const { return true; }

#ifdef DEBUG
    virtual std::string debugDescriptionForHistoryWindow(const ScrapbookMessageHistory&, int start, int end) = 0;
#endif
};

} // namespace

#endif // __SCRAPBOOK_PROACTIVE_MEMORY_ALGORITHM_H

