#include "BagOfWordsProactiveMemoryAlgorithm.h"

#include "ScrapbookMemory.h"
#include "ScrapbookMessageHistory.h"
#include "network/irc/IRCMessage.h"

using namespace samebot;

static const std::string defaultDatabaseLocation = "bag_of_words.db";

BagOfWordsProactiveMemoryAlgorithm::BagOfWordsProactiveMemoryAlgorithm(std::string databaseLocation)
    : m_dataStore(databaseLocation.length() ? databaseLocation : defaultDatabaseLocation)
    , m_stringProcessor(m_dataStore)
{
}

double BagOfWordsProactiveMemoryAlgorithm::scoreForHistoryWindow(const ScrapbookMessageHistory& history, int start, int end) {
    std::vector<std::string> messages;
    for (int i = start; i <= end; ++i) {
        messages.push_back(history.messageAtIndex(i)->parameters().back());
    }

    return m_stringProcessor.combinedProbabilityOfStringsBeingInteresting(messages);
}

double BagOfWordsProactiveMemoryAlgorithm::scoreForMessage(std::shared_ptr<IRCMessage> message) {
    return m_stringProcessor.probabilityOfStringBeingInteresting(message->parameters().back());
}

void BagOfWordsProactiveMemoryAlgorithm::incorporatePositiveFeedbackForMemory(const ScrapbookMemory& memory) {
    m_stringProcessor.noteStringWasInteresting(memory.context());
}

void BagOfWordsProactiveMemoryAlgorithm::incorporateNegativeFeedbackForMemory(const ScrapbookMemory& memory) {
    m_stringProcessor.noteStringWasUninteresting(memory.context());
}

#ifdef DEBUG
std::string BagOfWordsProactiveMemoryAlgorithm::debugDescriptionForHistoryWindow(const ScrapbookMessageHistory& history, int start, int end) {
    std::string probabilities = "";
    for (int i = start; i <= end; ++i) {
        auto probability = m_stringProcessor.probabilityOfStringBeingInteresting(history.messageAtIndex(i)->parameters().back());
        probabilities += std::to_string(probability);
        if (i != end) {
            probabilities += " ";
        }
    }
    return probabilities;
}
#endif

