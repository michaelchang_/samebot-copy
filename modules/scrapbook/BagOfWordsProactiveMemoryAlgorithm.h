#ifndef __BAG_OF_WORDS_PROACTIVE_MEMORY_ALGORITHM_H
#define __BAG_OF_WORDS_PROACTIVE_MEMORY_ALGORITHM_H

#include "ScrapbookProactiveMemoryAlgorithm.h"

#include "bag_of_words/BagOfWordsStringProcessor.h"
#include "bag_of_words/BagOfWordsSQLiteDataStore.h"
#include <vector>

namespace samebot {

class IRCMessage;

class BagOfWordsProactiveMemoryAlgorithm : public ScrapbookProactiveMemoryAlgorithm {
public:
    BagOfWordsProactiveMemoryAlgorithm(std::string databaseLocation = "");

    virtual double scoreForHistoryWindow(const ScrapbookMessageHistory&, int start, int end) override;
    virtual double scoreForMessage(std::shared_ptr<IRCMessage>) override;

    // Slightly lower than 0.5, because with zero information we'll default to providing 0.5 probabilities for every window, and
    // we want all of these to become memories so that we can have users classify them for us to learn based on.
    virtual double scoreThresholdForProactiveMemory() override { return 0.49; }

    virtual void incorporatePositiveFeedbackForMemory(const ScrapbookMemory&) override;
    virtual void incorporateNegativeFeedbackForMemory(const ScrapbookMemory&) override;

#ifdef DEBUG
    virtual std::string debugDescriptionForHistoryWindow(const ScrapbookMessageHistory&, int start, int end) override;
#endif

private:
    BagOfWordsSQLiteDataStore m_dataStore;
    BagOfWordsStringProcessor m_stringProcessor;
};

} // namespace

#endif // __BAG_OF_WORDS_PROACTIVE_MEMORY_ALGORITHM_H

