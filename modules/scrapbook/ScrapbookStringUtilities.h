#ifndef __SCRAPBOOK_STRING_UTILITIES_H
#define __SCRAPBOOK_STRING_UTILITIES_H

#include <memory>
#include <string>
#include <vector>

namespace samebot {

class IRCMessage;

std::string formatMessage(std::shared_ptr<IRCMessage>);
std::string interleaveNicknamesWithZeroWidthSpaces(std::string);
std::string indentLines(std::string text, std::string indentation);
bool messageProbablyContainsFormattedOtherMessage(std::shared_ptr<IRCMessage>);
std::string substringAfterTrigger(std::string, const std::vector<std::string>& triggers);

} // namespace

#endif // __SCRAPBOOK_STRING_UTILITIES_H

