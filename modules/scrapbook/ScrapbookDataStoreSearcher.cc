#include "ScrapbookDataStoreSearcher.h"

#include "ScrapbookDataStore.h"
#include "ScrapbookStringUtilities.h"
#include "basics/Assertions.h"
#include "basics/StringUtilities.h"
#include "network/irc/IRCMessage.h"

using namespace samebot;

ScrapbookDataStoreSearcher::ScrapbookDataStoreSearcher(const ScrapbookDataStore& dataStore)
    : m_dataStore(dataStore)
{
}

std::shared_ptr<ScrapbookMemory> ScrapbookDataStoreSearcher::tryToSearchDataStoreForMemory(std::shared_ptr<IRCMessage> message) const {
    auto query = extractSearchQueryFromPotentialSearchRequest(message->parameters().back());
    auto words = splitString(query);
    // Try every possible subsentence of the query, prioritizing the longest, most right-aligned ones.
    for (int length = static_cast<int>(words.size()); length >= 1; --length) {
        for (int offset = static_cast<int>(words.size()) - length; offset >= 0; --offset) {
            std::string subsentence;
            for (int i = 0; i < length; ++i) {
                subsentence += words[offset + i] + " ";
            }

            subsentence = stringByTrimmingString(subsentence);
            if (!subsentence.length()) {
                continue;
            }

            if (auto memory = m_dataStore.findMemoryContainingString(subsentence)) {
                return memory;
            }
        }
    }

    return nullptr;
}

std::string ScrapbookDataStoreSearcher::extractSearchQueryFromPotentialSearchRequest(std::string request) const {
    static std::vector<std::string> triggers {
        "find",
        "search",
        "show",
        // FIXME: Add support for "what did X say about Y?"
    };

    return substringAfterTrigger(request, triggers);
}

