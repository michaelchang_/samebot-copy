#include "ScrapbookMessageHistory.h"

#include "basics/Assertions.h"

using namespace samebot;

static const size_t maximumRecentMessageMemory = 100;

void ScrapbookMessageHistory::addMessageToHistory(std::shared_ptr<IRCMessage> message, MessageType type) {
    m_entries.push_back({
        .message = message,
        .timeStamp = std::chrono::steady_clock::now(),
        .type = type,
    });

    // Messages should be inserted into history in chronological order.
    ASSERT(size() == 1 || secondsBetweenMessagesAtIndices(static_cast<int>(size()) - 2, static_cast<int>(size()) - 1) >= 0);

    if (size() > maximumRecentMessageMemory) {
        m_entries.pop_front();
    }

    ASSERT(size() <= maximumRecentMessageMemory);
}

size_t ScrapbookMessageHistory::size() const {
    return m_entries.size();
}

std::shared_ptr<IRCMessage> ScrapbookMessageHistory::messageAtIndex(int index) const {
    ASSERT(index >= 0);
    ASSERT(index < (int)size());
    return m_entries[index].message;
}

double ScrapbookMessageHistory::secondsBetweenMessagesAtIndices(int start, int end) const {
    ASSERT(start >= 0);
    ASSERT(start <= end);
    ASSERT(end < (int)size());

    std::chrono::duration<double> duration = m_entries[end].timeStamp - m_entries[start].timeStamp;
    ASSERT(duration.count() >= 0);
    return duration.count();
}

ScrapbookMessageHistory::MessageType ScrapbookMessageHistory::typeOfMessageAtIndex(int index) const {
    ASSERT(index >= 0);
    ASSERT(index < (int)size());
    return m_entries[index].type;
}

#pragma mark - Unit test support

#ifdef DEBUG
void ScrapbookMessageHistory::test_overwriteLastEntryFields(int seconds, MessageType type) {
    m_entries.back().timeStamp = std::chrono::time_point<std::chrono::steady_clock>() + std::chrono::seconds(seconds);
    m_entries.back().type = type;
}
#endif // DEBUG

