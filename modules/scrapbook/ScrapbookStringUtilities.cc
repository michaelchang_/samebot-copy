#include "ScrapbookStringUtilities.h"

#include "basics/Assertions.h"
#include "basics/StringUtilities.h"
#include "network/irc/IRCMessage.h"

namespace samebot {

std::string formatMessage(std::shared_ptr<IRCMessage> message) {
    return "<" + message->prefix()->user().nickname + "> " + message->parameters().back();
}

static auto interleaveStringWithZeroWidthSpaces(std::string string) {
    std::string newString;
    for (size_t i = 0; i < string.length(); ++i) {
        newString += string[i];
        if (i != string.length() - 1) {
            newString += "\u200B";
        }
    }
    return newString;
}

std::string interleaveNicknamesWithZeroWidthSpaces(std::string messages) {
    std::string result;
    auto lines = splitString(messages, '\n');
    for (size_t i = 0; i < lines.size(); ++i) {
        auto line = lines[i];
        auto nicknameEnd = line.find(' ');
        if (nicknameEnd != std::string::npos) {
            auto nickname = line.substr(0, nicknameEnd);
            auto rest = line.substr(nicknameEnd);
            if (nickname.length() >= 2 && nickname[0] == '<' && nickname[nickname.length() - 1] == '>') {
                nickname = nickname.substr(1, nickname.length() - 2);
                line = "<" + interleaveStringWithZeroWidthSpaces(nickname) + ">" + rest;
            }
        }

        result += line;
        if (i != lines.size() - 1) {
            result += "\n";
        }
    }
    return result;
}

std::string indentLines(std::string text, std::string indentation) {
    auto lines = splitString(text, '\n');
    std::string result;
    for (size_t i = 0; i < lines.size(); ++i) {
        result += indentation;
        result += lines[i];
        if (i != lines.size() - 1) {
            result += "\n";
        }
    }
    return result;
}

bool messageProbablyContainsFormattedOtherMessage(std::shared_ptr<IRCMessage> message) {
    auto text = message->parameters().back();
    if (!text.length()) {
        return false;
    }

    auto firstWord = splitString(stringByTrimmingString(text)).front();
    if (firstWord.length() < 3) {
        return false;
    }

    return (firstWord[0] == '<' && firstWord[firstWord.length() - 1] == '>');
}

std::string substringAfterTrigger(std::string string, const std::vector<std::string>& triggers) {
    for (const auto& trigger : triggers) {
        std::string triggerFoundInString;
        auto location = findFirstFuzzyWordMatch(string, trigger, &triggerFoundInString);
        if (location != std::string::npos) {
            return stringByTrimmingString(string.substr(location + triggerFoundInString.length()));
        }
    }

    return "";
}

} // namespace

