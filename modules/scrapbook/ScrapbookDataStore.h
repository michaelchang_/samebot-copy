#ifndef __SCRAPBOOK_DATA_STORE_H
#define __SCRAPBOOK_DATA_STORE_H

#include "ScrapbookMemory.h"
#include <memory>

namespace samebot {

class ScrapbookDataStore {
public:
    virtual void storeMemory(std::shared_ptr<ScrapbookMemory>) = 0;
    virtual void deleteMemory(const ScrapbookMemory&) = 0;
    virtual std::shared_ptr<ScrapbookMemory> retrieveRandomMemory() = 0;
    virtual std::shared_ptr<ScrapbookMemory> findMemoryContainingString(std::string) const = 0;
};

} // namespace

#endif // __SCRAPBOOK_DATA_STORE_H

