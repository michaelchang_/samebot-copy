#ifndef __SCRAPBOOK_MESSAGE_HISTORY_MATCHER_H
#define __SCRAPBOOK_MESSAGE_HISTORY_MATCHER_H

#include <map>
#include <memory>
#include <string>

namespace samebot {

class IRCMessage;
class ScrapbookMessageHistory;

class ScrapbookMessageHistoryMatcher {
public:
    ScrapbookMessageHistoryMatcher(const ScrapbookMessageHistory&);

    bool tryToMatchMessageAgainstHistory(std::shared_ptr<IRCMessage>, int& outMessageIndexToSave) const;

private:
    // Returns a negative value if no message could be determined (e.g. if the request wasn't clear enough). The input is not
    // assumed to be known to be a new memory request. This method will return a negative value for messages that are determined
    // not to be new memory requests.
    int indexOfMessageToRememberForPotentialNewMemoryRequest(const IRCMessage&) const;

    std::string extractThingToRememberFromRequest(std::string request) const;
    int indexOfMostRecentMessageMatchingThingToRememberFromMemoryRequest(std::string, std::string nickname = "", int* outLevenshteinDistance = nullptr) const;
    void populateMapOfNicknamesToMostRecentMessageIndices(std::map<std::string, int>&) const;
    int indexOfMostRecentMessageFromMatchingNickname(std::string thingToRemember, std::map<std::string, int>& nicknamesToMostRecentMessageIndices) const;
    int indexOfMessageMatchingMessageTextAndNicknameFromThingToRemember(std::string, std::map<std::string, int>& nicknamesToMostRecentMessageIndices) const;
    int indexOfMessageAssumingThingToRememberIsSemanticRequest(std::string thingToRemember) const;

    const ScrapbookMessageHistory& m_history;
};

} // namespace

#endif // __SCRAPBOOK_MESSAGE_HISTORY_MATCHER_H

