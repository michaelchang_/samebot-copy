#ifndef __SCRAPBOOK_MODULE_H
#define __SCRAPBOOK_MODULE_H

#include "ScrapbookDataStore.h"
#include "ScrapbookDataStoreSearcher.h"
#include "ScrapbookProactiveMemoryManager.h"
#include "ScrapbookMessageHistory.h"
#include "ScrapbookMessageHistoryMatcher.h"
#include "modules/Module.h"

extern const int contextWindowSize;

namespace samebot {

class ScrapbookModule : public Module {
public:
    ScrapbookModule();

    virtual std::string moduleIdentifier() const override { return "ScrapbookModule"; }
    virtual double getActiveScore(std::shared_ptr<IRCMessage> message) override { return -1; }
private:
    virtual bool wantsPrivateMessages() const override { return true; }

    // Wake up periodically and bring up an old memory to prompt conversation.
    virtual int secondsOfInactivityBeforeModuleWakesUp() const override { return 60 * 90; }

    virtual void didReceiveMessage(std::shared_ptr<IRCMessage>) override;
    virtual void messageWasSent(std::shared_ptr<IRCMessage>) override;
    virtual void didWakeUpDueToInactivity() override;

    bool handleMessageCommand(std::shared_ptr<IRCMessage>);
    bool isRequestForRandomMemory(std::string);
    bool isRequestForContext(std::string);
    bool isPositiveFeedback(std::string);
    bool isNegativeFeedback(std::string);

    enum class IncludeLastMessage {
        No,
        Yes
    };
    std::shared_ptr<ScrapbookMemory> saveMemoryForMessageAtIndex(int messageIndex, IncludeLastMessage, std::shared_ptr<IRCMessage> messageToSendFeedbackInResponseTo = nullptr);
    std::shared_ptr<ScrapbookMemory> saveMemory(int messageIndex, int contextWindowStart, int contextWindowEnd, IncludeLastMessage, std::shared_ptr<IRCMessage> messageToSendFeedbackInResponseTo = nullptr);

    ScrapbookModule(std::unique_ptr<ScrapbookDataStore>);

    ScrapbookMessageHistory m_history;
    std::unique_ptr<ScrapbookDataStore> m_dataStore;
    ScrapbookProactiveMemoryManager m_proactiveMemoryManager;
    ScrapbookMessageHistoryMatcher m_historyMatcher;
    ScrapbookDataStoreSearcher m_dataStoreSearcher;
    std::shared_ptr<ScrapbookMemory> m_memoryMostRecentlyMentioned;
    bool m_hasSentAutomaticConversationStarterMemory = false;

#ifdef DEBUG
    friend class ScrapbookModuleTestShim;
#endif
};

#ifdef DEBUG
class ScrapbookModuleTestShim {
public:
    static std::shared_ptr<ScrapbookModule> createModule(std::unique_ptr<ScrapbookDataStore>);
    ScrapbookModuleTestShim(std::shared_ptr<ScrapbookModule>);
    void didReceiveMessage(std::shared_ptr<IRCMessage>);
    bool isRequestForRandomMemory(std::string);
    int bestIndexForProactiveMemory();

    void insertMessageInHistory(std::shared_ptr<IRCMessage>, int secondsSinceStart = 0, ScrapbookMessageHistory::MessageType = ScrapbookMessageHistory::MessageType::Received);

    void useProactiveMemoryAlgorithm(std::unique_ptr<ScrapbookProactiveMemoryAlgorithm>);

private:
    std::shared_ptr<ScrapbookModule> m_module;
};
#endif // DEBUG

} // namespace

#endif // __SCRAPBOOK_MODULE_H

