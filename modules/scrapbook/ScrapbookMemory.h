#ifndef __SCRAPBOOK_MEMORY_H
#define __SCRAPBOOK_MEMORY_H

#include <string>
#include <vector>

namespace samebot {

class ScrapbookMemory {
public:
    ScrapbookMemory(std::string memory, std::string context = {})
        : m_memory(memory)
        , m_context(context)
    {
    }

    const std::string& memory() const { return m_memory; }
    const std::string& context() const { return m_context; }

private:
    std::string m_memory;
    std::string m_context;
};

} // namespace

#endif // __SCRAPBOOK_MEMORY_H

