#ifndef __SCRAPBOOK_SQLITE_DATA_STORE_H
#define __SCRAPBOOK_SQLITE_DATA_STORE_H

#include "ScrapbookDataStore.h"
#include "database/SQLiteDataStore.h"

namespace samebot {

class ScrapbookSQLiteDataStore : public SQLiteDataStore, public ScrapbookDataStore {
public:
    using SQLiteDataStore::SQLiteDataStore;

    virtual void storeMemory(std::shared_ptr<ScrapbookMemory>) override;
    virtual void deleteMemory(const ScrapbookMemory&) override;
    virtual std::shared_ptr<ScrapbookMemory> retrieveRandomMemory() override;
    virtual std::shared_ptr<ScrapbookMemory> findMemoryContainingString(std::string) const override;

private:
    virtual int currentSchemaVersion() const override { return 1; }
    virtual bool createSchema(SQLiteDatabase&) override;
    // FIXME: Implement caching.
};

} // namespace

#endif // __SCRAPBOOK_SQLITE_DATA_STORE_H

