#ifndef __SCRAPBOOK_DATA_STORE_SEARCHER_H
#define __SCRAPBOOK_DATA_STORE_SEARCHER_H

#include <memory>
#include <string>

namespace samebot {

class IRCMessage;
class ScrapbookDataStore;
class ScrapbookMemory;

class ScrapbookDataStoreSearcher {
public:
    ScrapbookDataStoreSearcher(const ScrapbookDataStore&);

    std::shared_ptr<ScrapbookMemory> tryToSearchDataStoreForMemory(std::shared_ptr<IRCMessage>) const;

private:
    std::string extractSearchQueryFromPotentialSearchRequest(std::string) const;

    const ScrapbookDataStore& m_dataStore;
};

} // namespace

#endif // __SCRAPBOOK_DATA_STORE_SEARCHER_H

