#include "ScrapbookMessageHistoryMatcher.h"

#include "ScrapbookMessageHistory.h"
#include "ScrapbookModule.h"
#include "ScrapbookStringUtilities.h"
#include "basics/Assertions.h"
#include "basics/StringUtilities.h"
#include "network/irc/IRCMessage.h"

using namespace samebot;

ScrapbookMessageHistoryMatcher::ScrapbookMessageHistoryMatcher(const ScrapbookMessageHistory& history)
    : m_history(history)
{
}

bool ScrapbookMessageHistoryMatcher::tryToMatchMessageAgainstHistory(std::shared_ptr<IRCMessage> message, int& outMessageIndexToSave) const {
    ASSERT(message);

    int index = indexOfMessageToRememberForPotentialNewMemoryRequest(*message);
    if (index >= 0) {
        outMessageIndexToSave = index;
        return true;
    }

    return false;
}

#pragma mark - Internals

int ScrapbookMessageHistoryMatcher::indexOfMessageToRememberForPotentialNewMemoryRequest(const IRCMessage& message) const {
    auto request = message.parameters().back();
    auto thingToRemember = extractThingToRememberFromRequest(request);
    if (!thingToRemember.length()) {
        return -1;
    }

    // The user might have asked us to just remember something that someone said, so we also keep track of all of the nicknames of
    // people who wrote recent messages.
    std::map<std::string, int> nicknamesToMostRecentMessageIndices;
    populateMapOfNicknamesToMostRecentMessageIndices(nicknamesToMostRecentMessageIndices);

    // Insert nickname entries for placeholders like "I" and "you".
    if (auto* prefix = message.prefix()) {
        auto requesterNickname = prefix->user().nickname;
        if (requesterNickname.length() && nicknamesToMostRecentMessageIndices.find(requesterNickname) != nicknamesToMostRecentMessageIndices.end()) {
            nicknamesToMostRecentMessageIndices["i"] = nicknamesToMostRecentMessageIndices[prefix->user().nickname];
        }
        if (nicknamesToMostRecentMessageIndices.find("samebot") != nicknamesToMostRecentMessageIndices.end()) {
            nicknamesToMostRecentMessageIndices["you"] = nicknamesToMostRecentMessageIndices["samebot"];
        }
    }

    // First check if the user gave us a hint about both the nickname and message text. This is superior to something that only
    // matches message text or a nickname, so we prioritize it above everything else.
    int indexOfMatchingMessageAndNickname = indexOfMessageMatchingMessageTextAndNicknameFromThingToRemember(thingToRemember, nicknamesToMostRecentMessageIndices);
    if (indexOfMatchingMessageAndNickname != -1) {
        return indexOfMatchingMessageAndNickname;
    }

    // Look for a message that matches the request; the user might've told us what message to remember.
    int indexOfMatchingMessage = indexOfMostRecentMessageMatchingThingToRememberFromMemoryRequest(thingToRemember);

    // The user might've also just asked us to remember something that a person with a nickname said most recently, but not
    // specified the message text.
    int indexOfMessageWithMatchingNickname = indexOfMostRecentMessageFromMatchingNickname(thingToRemember, nicknamesToMostRecentMessageIndices);

    // Alternatively, the user might have requested we remember something that isn't an actual transcript message or nickname but
    // has some semantic meaning, such as "remember everything that just happened."
    int indexOfMessageForSemanticCommand = indexOfMessageAssumingThingToRememberIsSemanticRequest(thingToRemember);

    // Only use the semantic interpretation if we didn't find a message match or nickname match. We want to prioritize those
    // cases as they're far more common.
    if (indexOfMessageForSemanticCommand != -1 && indexOfMatchingMessage == -1 && indexOfMessageWithMatchingNickname == -1) {
        return indexOfMessageForSemanticCommand;
    }

    // This case is ambiguous. For now, assume the user meant the matching message unless the request was sufficiently small (e.g.
    // the user just said "remember <name>" or "remember what <name> said" which happened to match a recent message also).
    // FIXME: This is not a very good heuristic. We should take recency into account, perhaps.
    if (indexOfMatchingMessage != -1 && indexOfMessageWithMatchingNickname != -1) {
        if (splitString(request).size() <= 4) {
            return indexOfMessageWithMatchingNickname;
        }
        return indexOfMatchingMessage;
    }

    if (indexOfMatchingMessage != -1) {
        return indexOfMatchingMessage;
    }

    if (indexOfMessageWithMatchingNickname != -1) {
        return indexOfMessageWithMatchingNickname;
    }

    return -1;
}

std::string ScrapbookMessageHistoryMatcher::extractThingToRememberFromRequest(std::string request) const {
    static std::vector<std::string> triggers {
        "remember",
        "memory",
        "scrapbook",
    };

    return substringAfterTrigger(request, triggers);
}

int ScrapbookMessageHistoryMatcher::indexOfMostRecentMessageMatchingThingToRememberFromMemoryRequest(std::string thingToRemember, std::string nickname, int* outLevenshteinDistance) const {
    // Iterate backwards; we want the most recent closely-matching message.
    // Skip the first message as it's the new memory request, so we don't want to save it.
    for (int index = static_cast<int>(m_history.size()) - 2; index >= 0; --index) {
        auto message = m_history.messageAtIndex(index);
        if (nickname.length() && message->prefix()->user().nickname != nickname) {
            continue;
        }

        // Prefer the most recent matching message (even if there's a better match further back in history).

        // If the whole message matches, return it immediately.
        if (isFuzzyMatch(thingToRemember, message->parameters().back())) {
            if (outLevenshteinDistance) {
                // This is a really good match, so return the minimum distance.
                // FIXME: This is a bit deceptive from an API standpoint; we should define our own "score" here.
                *outLevenshteinDistance = 0;
            }
            return index;
        }

        // Try going through all sub-sentences of the sentence and fuzzy match against the thing.
        auto words = splitString(message->parameters().back());
        for (size_t i = 0; i < words.size(); ++i) {
            std::string substring;
            for (size_t j = i; j < words.size(); ++j) {
                substring += words[j];
                if (isFuzzyMatch(substring, thingToRemember)) {
                    if (outLevenshteinDistance) {
                        *outLevenshteinDistance = levenshtein(substring, thingToRemember);
                    }
                    return index;
                }
                substring += " ";
            }
        }
    }

    return -1;
}

void ScrapbookMessageHistoryMatcher::populateMapOfNicknamesToMostRecentMessageIndices(std::map<std::string, int>& map) const {
    for (int index = static_cast<int>(m_history.size()) - 2; index >= 0; --index) {
        auto message = m_history.messageAtIndex(index);
        auto nickname = message->prefix()->user().nickname;
        if (map.find(nickname) == map.end()) {
            map[nickname] = index;
        }
    }
}

int ScrapbookMessageHistoryMatcher::indexOfMostRecentMessageFromMatchingNickname(std::string thingToRemember, std::map<std::string, int>& nicknamesToMostRecentMessageIndices) const {
    // For nickname matching, we look for the best match over the most recent.
    int index = -1;
    int lowestLevenshteinScore = -1;
    for (auto word : splitString(thingToRemember)) {
        for (auto nicknameIndexPair : nicknamesToMostRecentMessageIndices) {
            const int nicknameLevenshteinDistanceThreshold = levenshteinThresholdForString(nicknameIndexPair.first);
            int levenshteinScore = levenshtein(lowercaseString(nicknameIndexPair.first), lowercaseString(word));
            if (levenshteinScore <= nicknameLevenshteinDistanceThreshold) {
                if (lowestLevenshteinScore == -1 || levenshteinScore < lowestLevenshteinScore) {
                    index = nicknameIndexPair.second;
                    lowestLevenshteinScore = levenshteinScore;
                }
            }
        }
    }

    return index;
}

int ScrapbookMessageHistoryMatcher::indexOfMessageMatchingMessageTextAndNicknameFromThingToRemember(std::string thingToRemember, std::map<std::string, int>& nicknamesToMostRecentMessageIndices) const {
    auto words = splitString(thingToRemember);
    // No need to look at the last word because we're interested in the stuff after the nickname in the request, so if the
    // last word matches the nickname we won't get anything out of it.
    // We should perhaps consider last word nickname matches valuable for constructions like "remember message by X" but these
    // seem infrequent enough to not worry about for now.
    int lowestDistance = -1;
    int bestIndex = -1;
    for (size_t i = 0; i < words.size() - 1; ++i) {
        for (auto nicknameIndexPair : nicknamesToMostRecentMessageIndices) {
            if (!isFuzzyMatch(words[i], nicknameIndexPair.first)) {
                continue;
            }

            // Try treating everything after the nickname as a message and see if we get any matches.
            // Drop words from the left (in an attempt to skip over words like "said" or "wrote" in messages like "remember when X
            // said Y").
            for (size_t j = i; j < words.size() - 1; ++j) {
                ASSERT(thingToRemember.find(words[j]) != std::string::npos);
                auto remainder = thingToRemember.substr(thingToRemember.find(words[j]) + words[j].length());
                int levenshteinDistance;
                int index = indexOfMostRecentMessageMatchingThingToRememberFromMemoryRequest(remainder, nicknameIndexPair.first, &levenshteinDistance);
                if (index != -1) {
                    ASSERT(m_history.messageAtIndex(index)->prefix()->user().nickname == nicknameIndexPair.first);
                    if (lowestDistance == -1 || levenshteinDistance < lowestDistance) {
                        bestIndex = index;
                        lowestDistance = levenshteinDistance;
                    }
                }
            }
        }
    }

    return bestIndex;
}

int ScrapbookMessageHistoryMatcher::indexOfMessageAssumingThingToRememberIsSemanticRequest(std::string thingToRemember) const {
    // For now, the only supported semantic requests are requests like "remember this" which remember the most messages.
    static std::vector<std::string> triggers {
        "this",
        "that",
        "all this",
        "all that",
        "everything",
    };

    auto words = splitString(thingToRemember);
    std::string substring;
    for (auto word : words) {
        substring += word;
        if (hasFuzzySuffixMatch(substring, triggers)) {
            // Pick a message to maximize the amount of immediate context. Skip the last message as it's the memory request.
            int midpoint = static_cast<int>(m_history.size()) - 2 - contextWindowSize / 2;
            // If the midpoint is out of bounds, we don't have enough history to form a half context window, so just remember the
            // most recent message, which should cover the full history.
            if (midpoint < 0) {
                return static_cast<int>(m_history.size()) - 2;
            }

            return midpoint;
        }

        substring += " ";
    }

    return -1;
}

