#ifndef __SCRAPBOOK_MESSAGE_HISTORY_H
#define __SCRAPBOOK_MESSAGE_HISTORY_H

#include "network/irc/IRCMessage.h"
#include <chrono>
#include <deque>

namespace samebot {

class ScrapbookMessageHistory {
public:
    enum class MessageType {
        Received,
        Sent,
    };
    void addMessageToHistory(std::shared_ptr<IRCMessage>, MessageType);
    size_t size() const;
    std::shared_ptr<IRCMessage> messageAtIndex(int) const;
    double secondsBetweenMessagesAtIndices(int, int) const;
    MessageType typeOfMessageAtIndex(int) const;

#ifdef DEBUG
    void test_overwriteLastEntryFields(int seconds, MessageType);
#endif

private:
    struct HistoryEntry {
        std::shared_ptr<IRCMessage> message;
        std::chrono::time_point<std::chrono::steady_clock> timeStamp;
        MessageType type;
    };
    std::deque<HistoryEntry> m_entries;
};

} // namespace

#endif // __SCRAPBOOK_MESSAGE_HISTORY_H

