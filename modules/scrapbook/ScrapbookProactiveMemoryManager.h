#ifndef __SCRAPBOOK_PROACTIVE_MEMORY_MANAGER_H
#define __SCRAPBOOK_PROACTIVE_MEMORY_MANAGER_H

#include "ScrapbookProactiveMemoryAlgorithm.h"
#include <memory>

namespace samebot {

class ScrapbookMessageHistory;

class ScrapbookProactiveMemoryManager {
public:
    ScrapbookProactiveMemoryManager(const ScrapbookMessageHistory&, size_t contextWindowSize);

    struct ProactiveMemory {
        int messageIndex;
        int contextWindowStart;
        int contextWindowEnd;
    };

    std::unique_ptr<ProactiveMemory> createProactiveMemory() const;

    enum class Feedback {
        Negative,
        Positive,
    };
    void incorporateFeedbackForMemory(Feedback, const ScrapbookMemory&);

    void setIndexOfMostRecentProactiveMemory(int index) { m_indexOfMostRecentProactiveMemory = index; }
    int indexOfMostRecentProactiveMemory() const { return m_indexOfMostRecentProactiveMemory; }

private:
    const ScrapbookMessageHistory& m_history;
    std::unique_ptr<ScrapbookProactiveMemoryAlgorithm> m_proactiveMemoryAlgorithm;
    size_t m_contextWindowSize;
    int m_indexOfMostRecentProactiveMemory = -1;

#ifdef DEBUG
    friend class ScrapbookModuleTestShim;
#endif
};

} // namespace

#endif // __SCRAPBOOK_PROACTIVE_MEMORY_MANAGER_H

