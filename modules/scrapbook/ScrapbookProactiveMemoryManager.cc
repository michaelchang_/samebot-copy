#include "ScrapbookProactiveMemoryManager.h"

#include "BagOfWordsProactiveMemoryAlgorithm.h"
#include "ScrapbookMessageHistory.h"
#include "ScrapbookStringUtilities.h"
#include "basics/Assertions.h"

using namespace samebot;

ScrapbookProactiveMemoryManager::ScrapbookProactiveMemoryManager(const ScrapbookMessageHistory& history, size_t contextWindowSize)
    : m_history(history)
    , m_proactiveMemoryAlgorithm(std::make_unique<BagOfWordsProactiveMemoryAlgorithm>())
    , m_contextWindowSize(contextWindowSize)
{
}

std::unique_ptr<ScrapbookProactiveMemoryManager::ProactiveMemory> ScrapbookProactiveMemoryManager::createProactiveMemory() const {
    if (m_history.size() < m_contextWindowSize) {
        return nullptr;
    }

    ASSERT(m_proactiveMemoryAlgorithm);

    // We assume this algorithm is run every time we get a message, so it is only necessary to consider the latest potential
    // context window. We add one to the context window because we want to include contextWindow / 2 messages on either side of
    // the memory, so we don't count the memory itself.
    int end = static_cast<int>(m_history.size()) - 1;
    int start = std::max<int>(end - (static_cast<int>(m_contextWindowSize) + 1) + 1, 0);
    int center = (end - start + 1) / 2 + start;

    if (m_proactiveMemoryAlgorithm->shouldRejectCandidateWindowsBasedOnCenterMessageScore() &&
        m_proactiveMemoryAlgorithm->scoreForMessage(m_history.messageAtIndex(center)) <= m_proactiveMemoryAlgorithm->scoreThresholdForProactiveMemory()) {
        return nullptr;
    }

    // Exclude windows that contain us retelling a previous memory, as these clearly shouldn't be added back to the database.
    bool windowContainsOldMemory = false;
    for (int i = start; i <= end; ++i) {
        if (m_history.typeOfMessageAtIndex(i) == ScrapbookMessageHistory::MessageType::Sent) {
            if (messageProbablyContainsFormattedOtherMessage(m_history.messageAtIndex(i))) {
                LOG_DEBUG("ScrapbookModule: Skipping window containing '%s' because it's probably ourselves giving memory context", formatMessage(m_history.messageAtIndex(i)).c_str());
                windowContainsOldMemory = true;
                break;
            }
        }
    }
    if (windowContainsOldMemory) {
        return nullptr;
    }

    double score = m_proactiveMemoryAlgorithm->scoreForHistoryWindow(m_history, start, end);
    if (score <= m_proactiveMemoryAlgorithm->scoreThresholdForProactiveMemory()) {
#ifdef DEBUG
        LOG_DEBUG("ScrapbookModule: ScrapbookProactiveMemoryManager decided window centered on '%s' was not good enough for proactive memory (score %f, info: %s)",
                formatMessage(m_history.messageAtIndex(center)).c_str(), score,
                m_proactiveMemoryAlgorithm->debugDescriptionForHistoryWindow(m_history, start, end).c_str());
#endif
        return nullptr;
    }

    LOG_DEBUG("ScrapbookModule: ScrapbookProactiveMemoryManager decided '%s' was good enough (score: %f, info %s)",
            formatMessage(m_history.messageAtIndex(center)).c_str(),
            m_proactiveMemoryAlgorithm->scoreForMessage(m_history.messageAtIndex(center)),
            m_proactiveMemoryAlgorithm->debugDescriptionForHistoryWindow(m_history, start, end).c_str());

    return std::unique_ptr<ProactiveMemory>(new ProactiveMemory {
        .messageIndex = center,
        .contextWindowStart = start,
        .contextWindowEnd = end,
    });
}

void ScrapbookProactiveMemoryManager::incorporateFeedbackForMemory(Feedback feedback, const ScrapbookMemory& memory) {
    switch (feedback) {
        case Feedback::Negative:
            m_proactiveMemoryAlgorithm->incorporateNegativeFeedbackForMemory(memory);
            break;
        case Feedback::Positive:
            m_proactiveMemoryAlgorithm->incorporatePositiveFeedbackForMemory(memory);
            break;
    }
}

