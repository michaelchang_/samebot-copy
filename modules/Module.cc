#include "Module.h"

#include "basics/Assertions.h"
#include "basics/Configuration.h"
#include "basics/StringUtilities.h"
#include "intelligence/UserIntelligenceServer.h"
#include "network/irc/IRCMessage.h"
#include "samebot/Preferences.h"

using namespace samebot;

Module::Module()
    : m_messageQueueFlushPending(false)
{
}

Module::~Module() {
    if (isProcessing()) {
        stop();
    }
}

void Module::setConfiguration(const Configuration *configuration) {
    m_configuration = configuration;
    didSetConfiguration(configuration);
}

bool Module::isEnabled() const {
    return !Preferences::standardPreferences().get(moduleIdentifier() + ".disabled").length();
}

void Module::setEnabled(bool enabled) {
    Preferences::standardPreferences().set(moduleIdentifier() + ".disabled", enabled ? "" : "1");
}

ModuleClient& Module::client() const {
    auto client = m_client.lock();
    // It is a programmer error for the module to outlive its client. It is also a programmer error if the module is able to reach
    // a state where it wants to use its client without the client having been set yet.
    ASSERT(client);
    return *client;
}

void Module::start() {
    m_messageProcessorThread = std::thread([this] {
        startProcessing(m_messageQueueFlushPending ? ShouldFlushQueue::Yes : ShouldFlushQueue::No);
        m_messageQueueFlushPending = false;
    });
}

void Module::stop() {
    m_messageQueueFlushPending = false;
    stopProcessing();
    if (m_messageProcessorThread.joinable()) {
        m_messageProcessorThread.join();
    }
}

void Module::processRemainingMessagesAndStop() {
    if (isProcessing()) {
        stopProcessing(ShouldFlushQueue::Yes);
    } else {
        // The thread hasn't had a chance to run yet. Once it does, we want to enter the flushing state.
        m_messageQueueFlushPending = true;
    }

    if (m_messageProcessorThread.joinable()) {
        m_messageProcessorThread.join();
    }
}

void Module::handleMessage(std::shared_ptr<IRCMessage> message, MessageType messageType) {
    if (!isEnabled()) {
        return;
    }

    if (m_userIntelligenceClient && isPrivateMessage(*message)) {
        m_userIntelligenceClient->processPrivateMessage(*message);
    }

    if (!wantsMessageFiltering()) {
        enqueue(std::make_pair(message, messageType));
        return;
    }

    if (!message->prefix()) {
        LOG_DEBUG("%s received message without prefix. Ignoring", moduleIdentifier().c_str());
        return;
    }

    if (message->command() != "PRIVMSG") {
        LOG_DEBUG("%s received something other than PRIVMSG. Ignoring.", moduleIdentifier().c_str());
        return;
    }

    switch (message->prefix()->type()) {
        case IRCMessagePrefix::Type::User:
            if (!wantsUserMessages()) {
                return;
            }
            break;
        case IRCMessagePrefix::Type::ServerName:
            if (!wantsServerMessages()) {
                return;
            }
            break;
    }

    auto recipient = message->parameters().front();
    auto body = message->parameters().back();
    if (!recipient.length() || !body.length()) {
        LOG_ERROR("%s received malformed PRIVMSG (missing recipient or length); ignoring", moduleIdentifier().c_str());
        return;
    }

    bool isChannelMessage = recipient[0] == '#';
    if (isChannelMessage && !wantsChannelMessages()) {
        LOG_DEBUG("%s received a channel message; ignoring", moduleIdentifier().c_str());
        return;
    }
    if (!isChannelMessage && !wantsPrivateMessages()) {
        LOG_DEBUG("%s received a private message; ignoring", moduleIdentifier().c_str());
        return;
    }

    enqueue(std::make_pair(message, messageType));
}

#pragma mark - Exposed to subclasses

bool Module::isMention(const IRCMessage& message) const {
    if (configuration()) {
        auto prefix = configuration()->nickname() + ": ";
        return stringHasPrefix(message.parameters().back(), prefix);
    }

    return false;
}

std::string Module::messageBodyWithMentionStripped(const IRCMessage& message) const {
    auto body = message.parameters().back();
    if (isMention(message)) {
        auto prefix = configuration()->nickname() + ": ";
        return body.substr(prefix.length());
    }

    return body;
}

bool Module::isPrivateMessage(const IRCMessage& message) const {
    return message.command() == "PRIVMSG" && message.parameters().front()[0] != '#';
}

void Module::sendMessage(std::string command, std::initializer_list<std::string> parameters) {
    sendMessage(std::make_shared<IRCMessage>(command, parameters));
}

void Module::sendMessage(std::string userOrChannel, std::string message) {
    // FIXME: It's awkward that this logic is duplicated across here and IRCClient. We need to consolidate these.
    for (std::string line : splitString(message, '\n')) {
        if (line.length()) {
            sendMessage("PRIVMSG", { userOrChannel, line });
        }
    }
}

void Module::replyToMessage(const IRCMessage& message, std::string reply) {
    ASSERT(message.command() == "PRIVMSG");
    if (isPrivateMessage(message)) {
        sendMessage(message.prefix()->user().nickname, reply);
    } else {
        sendMessage(message.parameters().front(), reply);
    }
}

#pragma mark - SynchronizedQueueProcessor overrides

void Module::process(std::pair<std::shared_ptr<IRCMessage>, MessageType> messageAndType) {
    switch (messageAndType.second) {
        case MessageType::Incoming:
            didReceiveMessage(messageAndType.first);
            break;
        case MessageType::Outgoing:
            messageWasSent(messageAndType.first);
            break;
    }
}

#pragma mark - User intelligence

UserIntelligenceClient& Module::userIntelligenceClient() {
    if (!m_userIntelligenceClient) {
        m_userIntelligenceClient = UserIntelligenceServer::shared().createClient(*this);
    }

    return *m_userIntelligenceClient;
}

void Module::sendPublicMessageForUserIntelligenceClient(UserIntelligenceClient& client, const std::string& message) {
    ASSERT(&client == m_userIntelligenceClient.get());

    if (m_configuration) {
        sendMessage(m_configuration->channel(), message);
    }
}

void Module::sendPrivateMessageForUserIntelligenceClient(UserIntelligenceClient& client, const std::string& recipient, const std::string& message) {
    ASSERT(&client == m_userIntelligenceClient.get());

    sendMessage(recipient, message);
}

#pragma mark - Internals

void Module::handleTimeout() {
    if (isEnabled()) {
        didWakeUpDueToInactivity();
    }
}

void Module::sendMessage(std::shared_ptr<IRCMessage> message) {
    client().sendMessage(message);
}

