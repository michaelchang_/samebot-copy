#include "ControlCenterModule.h"

#include "basics/StringUtilities.h"
#include "network/irc/IRCMessage.h"

using namespace samebot;

void ControlCenterModule::didReceiveMessage(std::shared_ptr<IRCMessage> message) {
    auto words = splitString(message->parameters().back());
    if (!words.size() || words[0] != "!cc") {
        return;
    }

    auto response = respondToCommand(words.begin() + 1, words.end());
    replyToMessage(*message, response);
}

std::string ControlCenterModule::respondToCommand(StringIterator start, StringIterator end) {
    if (start == end) {
        return "Missing command";
    }

    if (*start == "module") {
        return respondToModuleCommand(start + 1, end);
    }

    return "Unknown command '" + *start + "'";
}

std::string ControlCenterModule::respondToModuleCommand(StringIterator start, StringIterator end) {
    if (start == end) {
        return "Missing subcommand for 'module'";
    }

    if (*start == "enable" || *start == "disable") {
        return respondToModuleToggleCommand(*start == "enable", start + 1, end);
    }

    return "Unknown subcommand '" + *start + "' for 'module'";
}

std::string ControlCenterModule::respondToModuleToggleCommand(bool enable, StringIterator start, StringIterator end) {
    if (start == end) {
        return "Missing module name";
    }

    if (!enable && *start == moduleIdentifier()) {
        return moduleIdentifier() + " cannot be disabled";
    }

    std::string response;
    for (auto* module : client().modulesWithIdentifier(*start)) {
        if (module->isEnabled() == enable) {
            response += module->moduleIdentifier() + " is already " + (enable ? "enabled" : "disabled") + "\r\n";
        } else {
            module->setEnabled(enable);
            response += module->moduleIdentifier() + " -> " + (enable ? "enabled" : "disabled") + "\r\n";
        }
    }

    if (response.length()) {
        return response;
    }

    return "No module named '" + *start + "' found";
}

double ControlCenterModule::getActiveScore(std::shared_ptr<IRCMessage> message) {
    auto words = splitString(message->parameters().back());
    if (words.size() && words[0] == "!cc") return 100;
    return 0;
}
