#ifndef __CONTROL_CENTER_MODULE_H
#define __CONTROL_CENTER_MODULE_H

#include "modules/Module.h"

namespace samebot {

class ControlCenterModule : public Module {
public:
    virtual std::string moduleIdentifier() const override { return "ControlCenterModule"; }

private:
    virtual bool wantsPrivateMessages() const override { return true; }
    virtual void didReceiveMessage(std::shared_ptr<IRCMessage>) override;

    typedef std::vector<std::string>::iterator StringIterator;
    std::string respondToCommand(StringIterator start, StringIterator end);
    std::string respondToModuleCommand(StringIterator start, StringIterator end);
    std::string respondToModuleToggleCommand(bool enable, StringIterator start, StringIterator end);

    virtual double getActiveScore(std::shared_ptr<IRCMessage> message) override;
};

} // namespace

#endif // __CONTROL_CENTER_MODULE_H

