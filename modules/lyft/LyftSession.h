#ifndef __LYFT_SESSION_H
#define __LYFT_SESSION_H

#include "LyftAuthenticationContext.h"
#include "basics/Geocoder.h"
#include <memory>
#include <string>

namespace samebot {

class LyftAuthenticationContext;
class LyftRideEstimate;
class LyftSessionDelegate;

class LyftSession {
public:
    /// Takes ownership of the given authentication context if one is provided.
    LyftSession(std::string clientID, std::string clientSecret, std::unique_ptr<LyftAuthenticationContext>);

    LyftSessionDelegate* delegate() const { return m_delegate; }
    void setDelegate(LyftSessionDelegate* delegate) { m_delegate = delegate; }

    std::string clientID() const { return m_clientID; }
    std::string clientSecret() const { return m_clientSecret; }
    const LyftAuthenticationContext* authenticationContext() const { return m_authenticationContext.get(); }

    void authenticateIfNecessary();
    std::unique_ptr<LyftRideEstimate> fetchRideEstimate(Geocoder::Coordinate startCoordinate, Geocoder::Coordinate endCoordinate);

private:
    std::string m_clientID;
    std::string m_clientSecret;
    std::unique_ptr<LyftAuthenticationContext> m_authenticationContext;
    LyftSessionDelegate* m_delegate = nullptr;
};

class LyftSessionDelegate {
public:
    virtual void lyftSessionDidUpdateAuthenticationContext(LyftSession&) {}
};

} // namespace

#endif // __LYFT_SESSION_H
