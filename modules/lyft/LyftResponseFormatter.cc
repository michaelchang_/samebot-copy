#include "LyftResponseFormatter.h"

#include "LyftRideEstimate.h"
#include "basics/StringUtilities.h"
#include "basics/format.h"
#include <cmath>

using namespace fmt::literals;
using namespace samebot;

std::string LyftResponseFormatter::priceExplanationForRideEstimate(const Geocoder::Response& startGeocoderResponse, const Geocoder::Response& endGeocoderResponse, const LyftRideEstimate& rideEstimate) const {
    auto displayName = rideEstimate.displayName();
    auto minimumCostInDollars = rideEstimate.estimatedMinimumCostInCents() / 100;
    auto maximumCostInDollars = rideEstimate.estimatedMaximumCostInCents() / 100;
    auto primetimePercentage = rideEstimate.primetimePercentage();

    bool hasPriceEstimate = minimumCostInDollars && maximumCostInDollars;
    bool needsPrimetimeWarning = primetimePercentage.length() && primetimePercentage != "0%";

    if (!hasPriceEstimate) {
        return "i can't find any price information about that trip";
    }

    auto startAddress = startGeocoderResponse.formattedAddress;
    auto endAddress = endGeocoderResponse.formattedAddress;
    bool hasAddresses = startAddress.length() && endAddress.length();

    auto response = "a " + lowercaseString(displayName);
    if (hasAddresses) {
        response += " from {} to {}"_format(startAddress, endAddress);
    }

    if (minimumCostInDollars == maximumCostInDollars) {
        response += " would cost about ${}"_format(maximumCostInDollars);
    } else {
        response += " would cost ${} - ${}"_format(minimumCostInDollars, maximumCostInDollars);
    }

    if (needsPrimetimeWarning) {
        response += " (this includes {} surge pricing right now)"_format(primetimePercentage.c_str());
    }

    return response;
}

std::string LyftResponseFormatter::durationAndDistanceExplanationForRideEstimate(const Geocoder::Response& startGeocoderResponse, const Geocoder::Response& endGeocoderResponse, const LyftRideEstimate& rideEstimate) const {
    auto distanceInMiles = rideEstimate.estimatedDistanceInMiles();
    auto durationInMinutes = rideEstimate.estimatedDuration().count() / 60;
    if (distanceInMiles && durationInMinutes) {
        auto minuteWord = pluralizeWord("minute", durationInMinutes);
        auto mileWord = pluralizeWord("mile", distanceInMiles);
        return "the trip is {} {} long and would take about {} {} right now"_format(distanceInMiles, mileWord, durationInMinutes, minuteWord);
    }

    if (distanceInMiles) {
        return "it's a {} mile trip"_format(distanceInMiles);
    }

    if (durationInMinutes) {
        auto minuteWord = pluralizeWord("minute", durationInMinutes);
        return "the trip would take about {} {} right now"_format(durationInMinutes, minuteWord);
    }

    // If we have a price estimate, we would've displayed that, so we should explain that we got one piece of
    // information but not the other.
    auto minimumCostInDollars = rideEstimate.estimatedMinimumCostInCents() / 100;
    auto maximumCostInDollars = rideEstimate.estimatedMaximumCostInCents() / 100;
    bool hasPriceEstimate = minimumCostInDollars && maximumCostInDollars;
    if (hasPriceEstimate) {
        return "i'm not sure how long it'll take or how far it is though";
    }

    // If we didn't have a price estimate, we got nothing at all.
    return "i can't find anything about the trip length or duration either";
}