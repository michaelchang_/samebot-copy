#ifndef __LYFT_RESPONSE_FORMATTER_H
#define __LYFT_RESPONSE_FORMATTER_H

#include "basics/Geocoder.h"

namespace samebot {

class LyftRideEstimate;

class LyftResponseFormatter {
public:
    std::string priceExplanationForRideEstimate(const Geocoder::Response& startGeocoderResponse, const Geocoder::Response& endGeocoderResponse, const LyftRideEstimate&) const;
    std::string durationAndDistanceExplanationForRideEstimate(const Geocoder::Response& startGeocoderResponse, const Geocoder::Response& endGeocoderResponse, const LyftRideEstimate&) const;
};

} // namespace

#endif // __LYFT_RESPONSE_FORMATTER_H
