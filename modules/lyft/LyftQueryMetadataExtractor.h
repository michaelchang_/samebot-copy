#ifndef __LYFT_QUERY_METADATA_EXTRACTOR_H
#define __LYFT_QUERY_METADATA_EXTRACTOR_H

#include <string>

namespace samebot {

class LyftQueryMetadataExtractor {
public:
    bool extractStartAndEndAddressesForRideEstimateRequest(const std::string& query, std::string& outStartAddress, std::string& outEndAddress);
};

} // namespace

#endif // __LYFT_QUERY_METADATA_EXTRACTOR_H