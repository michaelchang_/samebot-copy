#include "LyftAuthenticationContext.h"

#include "basics/json.h"
#include "basics/Assertions.h"

using namespace samebot;

std::unique_ptr<LyftAuthenticationContext> LyftAuthenticationContext::create(std::string jsonString) {
    try {
        auto json = nlohmann::json::parse(jsonString);
        std::string accessToken = json["access_token"];

        // The refresh token is optional (not specified for public authentication).
        std::string refreshToken;
        if (!json["refresh_token"].is_null()) {
            refreshToken = json["refresh_token"];
        }

        std::chrono::system_clock::time_point expirationTimePoint;

        // This is an extension of Lyft's JSON format which we use to be able to persist the expiration
        // date to disk.
        if (json["expiration_date"].is_number_unsigned()) {
            expirationTimePoint += std::chrono::seconds(json["expiration_date"]);
        } else if (json["expires_in"].is_number_unsigned()) {
            expirationTimePoint = std::chrono::system_clock::now() + std::chrono::seconds(json["expires_in"]);
        } else {
            return nullptr;
        }

        return std::make_unique<LyftAuthenticationContext>(accessToken, refreshToken, expirationTimePoint);
    } catch(const std::exception& exception) {
        LOG_ERROR("Error parsing Lyft authentication context %s: %s", jsonString.c_str(), exception.what());
        return nullptr;
    }
}

bool LyftAuthenticationContext::isExpired() const {
    return std::chrono::system_clock::now() > m_expirationTimePoint;
}

std::string LyftAuthenticationContext::jsonRepresentation() const {
    nlohmann::json json;
    json["access_token"] = m_accessToken;
    json["expiration_date"] = std::chrono::duration_cast<std::chrono::seconds>(m_expirationTimePoint.time_since_epoch()).count();
    if (m_refreshToken.length()) {
        json["refresh_token"] = m_refreshToken;
    }

    return json.dump();
}

#pragma mark - Private methods

LyftAuthenticationContext::LyftAuthenticationContext(std::string accessToken, std::string refreshToken, std::chrono::system_clock::time_point expirationTimePoint)
    : m_accessToken(accessToken)
    , m_refreshToken(refreshToken)
    , m_expirationTimePoint(expirationTimePoint)
{
}
