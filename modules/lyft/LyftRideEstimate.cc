#include "LyftRideEstimate.h"

#include "basics/json.h"
#include "basics/Assertions.h"

using namespace samebot;

namespace samebot {
    const std::string lyftRideType = "lyft";
}

std::unique_ptr<LyftRideEstimate> LyftRideEstimate::create(std::string jsonString) {
    try {
        auto json = nlohmann::json::parse(jsonString);
        if (!json["cost_estimates"].size()) {
            return nullptr;
        }

        json = json["cost_estimates"].front();
        auto estimate = std::make_unique<LyftRideEstimate>();
        estimate->m_rideType = json["ride_type"];
        estimate->m_displayName = json["display_name"];
        estimate->m_primetimePercentage = json["primetime_percentage"];

        int estimatedDurationInSeconds = json["estimated_duration_seconds"];
        if (estimatedDurationInSeconds < 0) {
            return nullptr;
        }
        estimate->m_estimatedDuration = std::chrono::seconds(estimatedDurationInSeconds);

        estimate->m_estimatedDistanceInMiles = json["estimated_distance_miles"];
        if (estimate->m_estimatedDistanceInMiles < 0) {
            return nullptr;
        }

        if (!json["estimated_cost_cents_min"].is_number_unsigned()) {
            return nullptr;
        }

        if (!json["estimated_cost_cents_max"].is_number_unsigned()) {
            return nullptr;
        }

        estimate->m_estimatedMinimumCostInCents = json["estimated_cost_cents_min"];
        estimate->m_estimatedMaximumCostInCents = json["estimated_cost_cents_max"];

        return estimate;
    } catch(const std::exception& exception) {
        LOG_ERROR("Error parsing Lyft ride estimate: %s: %s", jsonString.c_str(), exception.what());
        return nullptr;
    }
}
