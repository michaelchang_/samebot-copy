#include "LyftSession.h"

#include "LyftRideEstimate.h"
#include "network/URLRequest.h"
#include "network/URLSession.h"

using namespace samebot;

LyftSession::LyftSession(std::string clientID, std::string clientSecret, std::unique_ptr<LyftAuthenticationContext> authenticationContext)
    : m_clientID(clientID)
    , m_clientSecret(clientSecret)
    , m_authenticationContext(std::move(authenticationContext))
{
}

void LyftSession::authenticateIfNecessary() {
    if (m_authenticationContext && !m_authenticationContext->isExpired()) {
        return;
    }

    URLRequest request("https://api.lyft.com/oauth/token", URLRequest::Method::POST, {
        { "grant_type", "client_credentials" },
        { "scope", "public" },
    });

    request.addHeader("Content-Type", "application/json");
    request.setFormatsPOSTDataAsJSON(true);

    request.setUsernameForBasicAuthentication(m_clientID);
    request.setPasswordForBasicAuthentication(m_clientSecret);

    auto response = URLSession::sendSynchronousRequest(request);
    if (!response.length()) {
        return;
    }

    m_authenticationContext = LyftAuthenticationContext::create(response);
    if (m_delegate) {
        m_delegate->lyftSessionDidUpdateAuthenticationContext(*this);
    }
}

std::unique_ptr<LyftRideEstimate> LyftSession::fetchRideEstimate(Geocoder::Coordinate startCoordinate, Geocoder::Coordinate endCoordinate) {
    authenticateIfNecessary();
    if (!m_authenticationContext || m_authenticationContext->isExpired()) {
        return nullptr;
    }

    URLRequest request("https://api.lyft.com/v1/cost", URLRequest::Method::GET, {
        { "start_lat", std::to_string(startCoordinate.latitude) },
        { "start_lng", std::to_string(startCoordinate.longitude) },
        { "end_lat", std::to_string(endCoordinate.latitude) },
        { "end_lng", std::to_string(endCoordinate.longitude) },
        { "ride_type", lyftRideType },
    });

    request.addHeader("Authorization", "bearer " + m_authenticationContext->accessToken());

    auto response = URLSession::sendSynchronousRequest(request);
    if (!response.length()) {
        return nullptr;
    }

    return LyftRideEstimate::create(response);
}
