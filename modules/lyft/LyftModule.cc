#include "LyftModule.h"

#include "LyftAuthenticationContext.h"
#include "LyftRideEstimate.h"
#include "LyftSession.h"
#include "basics/Configuration.h"
#include "basics/Geocoder.h"
#include "basics/StringUtilities.h"
#include "intelligence/UserAddressResolver.h"
#include "modules/ModuleActiveResponseHandler.h"
#include "network/irc/IRCMessage.h"
#include "samebot/Preferences.h"

using namespace samebot;

static const char* authenticationContextPreferenceKey = "lyft.authenticationContext";

void LyftModule::didSetConfiguration(const Configuration* configuration) {
    if (!configuration) {
        m_session = nullptr;
        return;
    }

    std::unique_ptr<LyftAuthenticationContext> authenticationContext;
    auto serializedAuthenticationContext = Preferences::standardPreferences().get(authenticationContextPreferenceKey);
    if (serializedAuthenticationContext.length()) {
        authenticationContext = LyftAuthenticationContext::create(serializedAuthenticationContext);
    }

    auto clientID = configuration->lyftClientID();
    auto clientSecret = configuration->lyftClientSecret();
    if (clientID.length() && clientSecret.length()) {
        m_session = std::make_unique<LyftSession>(clientID, clientSecret, std::move(authenticationContext));
        m_session->setDelegate(this);
    } else {
        LOG_ERROR("LyftModule: Client ID and secret not specified in configuration");
    }
}

void LyftModule::didReceiveMessage(std::shared_ptr<IRCMessage> message) {
    auto body = stringByTrimmingString(message->parameters().back());
    if (!isPrivateMessage(*message) && !ModuleActiveResponseHandler::hasSamebotPrefix(body)) {
        return;
    }

    if (handleRideEstimateRequest(message)) {
        return;
    }

#ifdef DEBUG
    if (handleDebugCommand(message)) {
        return;
    }
#endif
}

#pragma mark - LyftSessionDelegate

void LyftModule::lyftSessionDidUpdateAuthenticationContext(LyftSession& session) {
    auto contextRepresentation = session.authenticationContext() ? session.authenticationContext()->jsonRepresentation() : "";
    Preferences::standardPreferences().set(authenticationContextPreferenceKey, contextRepresentation);
}

#pragma mark - Internals

bool LyftModule::handleRideEstimateRequest(std::shared_ptr<IRCMessage> message) {
    if (!m_session) {
        return false;
    }

    std::string startAddress;
    std::string endAddress;
    if (!m_queryMetadataExtractor.extractStartAndEndAddressesForRideEstimateRequest(message->parameters().back(), startAddress, endAddress)) {
        return false;
    }

    if (!message->prefix() || message->prefix()->type() != IRCMessagePrefix::Type::User) {
        finishRideEstimateRequestIfPossible(*message, startAddress, endAddress);
        return true;
    }

    struct ResolvedAddresses {
        std::string start;
        std::string end;
    };

    auto username = message->prefix()->user().username;
    auto addressResolver = std::make_shared<UserAddressResolver>(userIntelligenceClient());

    auto resolvedAddresses = std::make_shared<ResolvedAddresses>();
    addressResolver->resolve(startAddress, *message, [this, message, resolvedAddresses](auto resolvedAddress) {
        resolvedAddresses->start = resolvedAddress;
        this->finishRideEstimateRequestIfPossible(*message, resolvedAddresses->start, resolvedAddresses->end);
    });

    addressResolver->resolve(endAddress, *message, [this, message, resolvedAddresses](auto resolvedAddress) {
        resolvedAddresses->end = resolvedAddress;
        this->finishRideEstimateRequestIfPossible(*message, resolvedAddresses->start, resolvedAddresses->end);
    });

    return true;
}


void LyftModule::finishRideEstimateRequestIfPossible(const IRCMessage& message, const std::string& startAddress, const std::string& endAddress) {
    if (!startAddress.length()) {
        return;
    }

    if (!endAddress.length()) {
        return;
    }

    auto startResponse = Geocoder::geocode(startAddress);
    if (!startResponse) {
        return;
    }

    auto endResponse = Geocoder::geocode(endAddress);
    if (!endResponse) {
        return;
    }

    auto rideEstimate = m_session->fetchRideEstimate(startResponse->coordinate, endResponse->coordinate);
    if (!rideEstimate) {
        return;
    }

    replyToMessage(message, m_responseFormatter.priceExplanationForRideEstimate(*startResponse, *endResponse, *rideEstimate));
    replyToMessage(message, m_responseFormatter.durationAndDistanceExplanationForRideEstimate(*startResponse, *endResponse, *rideEstimate));
}

#ifdef DEBUG
bool LyftModule::handleDebugCommand(std::shared_ptr<IRCMessage> message) {
    ASSERT(message);

    auto body = message->parameters().back();
    auto words = splitString(body);
    if (!words.size()) {
        return false;
    }

    auto wordIterator = words.begin();
    if (isFuzzyMatch(*wordIterator, "samebot:")) {
        ++wordIterator;
    }

    if (*wordIterator != "!lyft") {
        return false;
    }

    ++wordIterator;
    if (wordIterator == words.end()) {
        replyToMessage(*message, "Supported commands: dump-authentication-context, try-renew-authentication-context");
        return true;
    }

    if (*wordIterator == "dump-authentication-context") {
        if (m_session && m_session->authenticationContext()) {
            replyToMessage(*message, m_session->authenticationContext()->jsonRepresentation());
        } else {
            replyToMessage(*message, "No authentication context");
        }
        return true;
    }

    if (*wordIterator == "try-renew-authentication-context") {
        m_session->authenticateIfNecessary();
        replyToMessage(*message, "Done");
        return true;
    }

    replyToMessage(*message, "Unrecognized command: " + *wordIterator);
    return true;
}
#endif

double LyftModule::getActiveScore(std::shared_ptr<IRCMessage> message) {
    double res = 0;
    auto body = stringByTrimmingString(message->parameters().back());

    if (findFirstFuzzyWordMatch(body, "lyft") != std::string::npos)
        res = 70;
    else return 0;

    if (findFirstFuzzyWordMatch(body, "to") != std::string::npos &&
        findFirstFuzzyWordMatch(body, "from") != std::string::npos)
        res = 100;
    return res;
}
