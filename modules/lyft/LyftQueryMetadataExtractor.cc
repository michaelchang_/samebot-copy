#include "LyftQueryMetadataExtractor.h"

#include "basics/Assertions.h"
#include "basics/StringUtilities.h"

using namespace samebot;

bool LyftQueryMetadataExtractor::extractStartAndEndAddressesForRideEstimateRequest(const std::string& query, std::string& outStartAddress, std::string& outEndAddress) {
    // We expect requests to be structured roughly like:
    // samebot: how much would a lyft from 1 Infinite Loop to 123 Stevens Creek Blvd, Cupertino cost?
    //                           ---^ ---^ --------------^ -^ --------------------------------^ ---^
    //                              A    B               C  D                                 E    F
    // A:    We need "Lyft" to be present somewhere, to know that the user is asking for Lyft information.
    // B, D: We need "from" and "to" to be present, to figure out what the addresses are.
    // C, E: These are the actual addresses, which we'll need to geocode.
    // F:    We need something like "cost" to be present, to signal that an estimate is desired.

    auto words = splitString(query);

    bool sawLyft = false;
    bool sawFrom = false;
    bool sawTo = false;
    bool sawCost = false;
    bool finishedToAddress = false;
    std::string fromAddress;
    std::string toAddress;

    for (const auto& word : words) {
        // If we're between 'from' and 'to', we're collecting the start address, so we shouldn't do any other
        // matching logic.
        if (sawFrom && !sawTo) {
            if (isFuzzyMatch(word, "to")) {
                // At this point, we'll stop collecting words for the start address.
                sawTo = true;
                continue;
            }

            fromAddress += word + " ";
            continue;
        }

        if (!sawFrom) {
            if (isFuzzyMatch(word, "from")) {
                sawFrom = true;
                continue;
            }
        }

        if (!sawCost) {
            sawCost = isFuzzyMatch(word, {
                "cost",
                "price",
                "estimate",
                "cash",
                "dollars",
                "money",
            });

            if (sawCost) {
                if (sawTo && !finishedToAddress) {
                    // If we saw 'to', we're collecting toAddress words. However, if we ran into a cost word, it's
                    // most likely a construction similar to the example above. The cost word should terminate the
                    // address.
                    finishedToAddress = true;
                }

                continue;
            }
        }

        if (sawTo && !finishedToAddress) {
            // We shouldn't set this flag until we've seen 'from'.
            ASSERT(sawFrom);
            toAddress += word + " ";
        }

        if (!sawLyft) {
            if (isFuzzyMatch(word, "lyft")) {
                sawLyft = true;
            }
        }
    }

    if (!sawCost) {
        // We also consider the sentence starting with "how much" as seeing the cost trigger, to support constructions
        // like "how much for a Lyft from A to B?"
        sawCost = hasFuzzyPrefixMatch(query, { "how much" });
    }

    // We need to have seen the Lyft and cost triggers and have addresses to proceed.
    if (!sawLyft || !sawCost || !fromAddress.length() || !toAddress.length()) {
        return false;
    }

    outStartAddress = stringByTrimmingString(fromAddress);
    outEndAddress = stringByTrimmingString(toAddress);
    return true;
}
