#ifndef __LYFT_AUTHENTICATION_CONTEXT_H
#define __LYFT_AUTHENTICATION_CONTEXT_H

#include <chrono>
#include <memory>
#include <string>

namespace samebot {

class LyftAuthenticationContext {
public:
    static std::unique_ptr<LyftAuthenticationContext> create(std::string json);
    LyftAuthenticationContext(std::string accessToken, std::string refreshToken, std::chrono::system_clock::time_point expirationTimePoint);

    std::string accessToken() const { return m_accessToken; }
    std::string refreshToken() const { return m_refreshToken; }
    bool isExpired() const;

    std::string jsonRepresentation() const;

private:
    std::string m_accessToken;
    std::string m_refreshToken;
    std::chrono::system_clock::time_point m_expirationTimePoint;
};

} // namespace

#endif // __LYFT_AUTHENTICATION_CONTEXT_H
