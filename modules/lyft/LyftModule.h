#ifndef __LYFT_MODULE_H
#define __LYFT_MODULE_H

#include "LyftQueryMetadataExtractor.h"
#include "LyftResponseFormatter.h"
#include "LyftSession.h"
#include "modules/Module.h"

namespace samebot {

class LyftSession;

class LyftModule : public Module, private LyftSessionDelegate {
public:
    virtual std::string moduleIdentifier() const override { return "LyftModule"; }
    virtual double getActiveScore(std::shared_ptr<IRCMessage> message) override;
private:
    void finishRideEstimateRequestIfPossible(const IRCMessage& message, const std::string& startAddress, const std::string& endAddress);

    // Module overrides:
    virtual void didSetConfiguration(const Configuration*) override;
    virtual bool wantsPrivateMessages() const override { return true; }
    virtual void didReceiveMessage(std::shared_ptr<IRCMessage>) override;

    // LyftSessionDelegate overrides:
    virtual void lyftSessionDidUpdateAuthenticationContext(LyftSession&) override;

    bool handleRideEstimateRequest(std::shared_ptr<IRCMessage>);
#ifdef DEBUG
    bool handleDebugCommand(std::shared_ptr<IRCMessage>);
#endif

    std::unique_ptr<LyftSession> m_session;
    LyftQueryMetadataExtractor m_queryMetadataExtractor;
    LyftResponseFormatter m_responseFormatter;
};

} // namespace

#endif // __LYFT_MODULE_H
