#ifndef __LYFT_RIDE_ESTIMATE_H
#define __LYFT_RIDE_ESTIMATE_H

#include <chrono>
#include <memory>
#include <string>

namespace samebot {

extern const std::string lyftRideType;

class LyftRideEstimate {
public:
    static std::unique_ptr<LyftRideEstimate> create(std::string json);
    LyftRideEstimate() {}

    const std::string& rideType() const { return m_rideType; }
    const std::string& displayName() const { return m_displayName; }
    const std::string& primetimePercentage() const { return m_primetimePercentage; }
    std::chrono::seconds estimatedDuration() const { return m_estimatedDuration; }
    float estimatedDistanceInMiles() const { return m_estimatedDistanceInMiles; }
    unsigned int estimatedMinimumCostInCents() const { return m_estimatedMinimumCostInCents; }
    unsigned int estimatedMaximumCostInCents() const { return m_estimatedMaximumCostInCents; }

private:
    std::string m_rideType;
    std::string m_displayName;
    std::string m_primetimePercentage;
    std::chrono::seconds m_estimatedDuration;
    float m_estimatedDistanceInMiles = 0;
    unsigned int m_estimatedMinimumCostInCents = 0;
    unsigned int m_estimatedMaximumCostInCents = 0;

#ifdef DEBUG
    friend class TestLyftRideEstimateBuilder;
#endif
};

#ifdef DEBUG
class TestLyftRideEstimateBuilder {
public:
    void setDisplayName(const std::string& displayName) { m_rideEstimate.m_displayName = displayName; }
    void setPrimetimePercentage(const std::string& primetimePercentage) { m_rideEstimate.m_primetimePercentage = primetimePercentage; }
    void setEstimatedDuration(std::chrono::seconds estimatedDuration) { m_rideEstimate.m_estimatedDuration = estimatedDuration; }
    void setEstimatedDistanceInMiles(float estimatedDistanceInMiles) { m_rideEstimate.m_estimatedDistanceInMiles = estimatedDistanceInMiles; }
    void setEstimatedMinimumCostInCents(unsigned int estimatedMinimumCostInCents) { m_rideEstimate.m_estimatedMinimumCostInCents = estimatedMinimumCostInCents; }
    void setEstimatedMaximumCostInCents(unsigned int estimatedMaximumCostInCents) { m_rideEstimate.m_estimatedMaximumCostInCents = estimatedMaximumCostInCents; }

    const LyftRideEstimate& rideEstimate() const { return m_rideEstimate; }

private:
    LyftRideEstimate m_rideEstimate;
};
#endif

} // namespace

#endif // __LYFT_RIDE_ESTIMATE_H
