#include "ModuleSwitchboard.h"

#include "ModuleActiveResponseHandler.h"
#include "network/irc/IRCMessage.h"
#include "basics/StringUtilities.h"
#include "basics/Assertions.h"

using namespace samebot;

void ModuleSwitchboard::registerModule(std::unique_ptr<Module> module) {
    m_modules.push_back(std::move(module));
    m_modules.back()->setClient(shared_from_this());
    m_modules.back()->start();
}

void ModuleSwitchboard::dispatchMessageToModules(std::shared_ptr<IRCMessage> message) {

    //Filter the message first.
    if (message->prefix() || message->command() == "PRIVMSG") {
        //If the message is prefaced with a "samebot:" or a fuzzy match of it,
        //it is an active message. Send to the most relevant module.
        auto body = message->parameters().back();
        if (ModuleActiveResponseHandler::hasSamebotPrefix(body)) {
            body = ModuleActiveResponseHandler::removeSamebotPrefix(body);
            std::vector<int> relevantModuleIndices = m_responseHandler.moduleScoring(message, m_modules);
            if (!relevantModuleIndices.empty()) {
                for (auto& index : relevantModuleIndices) {
                    m_modules[index]->handleMessage(message, MessageType::Incoming);
                }
                return;
            }
        }
    }

    for (auto& module : m_modules) {
        module->handleMessage(message, MessageType::Incoming);
    }
}

void ModuleSwitchboard::notifyModulesMessageWasSent(std::shared_ptr<IRCMessage> message) {
    for (auto& module : m_modules) {
        module->handleMessage(message, MessageType::Outgoing);
    }
}

#pragma mark - Outgoing message queue handling

void ModuleSwitchboard::startProcessingOutgoingMessages() {
    startProcessing();
}

void ModuleSwitchboard::stopProcessingOutgoingMessages() {
    stopProcessing();
}

#pragma mark ModuleClient

void ModuleSwitchboard::sendMessage(std::shared_ptr<IRCMessage> message) {
    enqueue(message);
}

std::vector<Module*> ModuleSwitchboard::modulesWithIdentifier(std::string identifier) {
    std::vector<Module*> modules;
    for (auto& module : m_modules) {
        if (module->moduleIdentifier() == identifier) {
            modules.push_back(module.get());
        }
    }
    return modules;
}

#pragma mark SynchronizedQueueProcessor

void ModuleSwitchboard::process(std::shared_ptr<IRCMessage> message) {
    auto messageSender = m_messageSender.lock();
    ASSERT(messageSender);
    messageSender->sendMessageForModuleSwitchboard(*this, message);
}

