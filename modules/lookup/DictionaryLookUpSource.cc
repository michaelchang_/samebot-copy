#include "DictionaryLookUpSource.h"

#include <vector>
#include <string>
#include <string.h>

using namespace samebot;

URLRequest DictionaryLookUpSource::requestForQuery(std::string query) const {
    return URLRequest("http://dictionaryapi.net/api/definition/" + query);
}

std::vector<std::string> DictionaryLookUpSource::parseResults(std::string s) {
	std::vector<std::string> definitions;


	std::string dictionaryToken = "\"Definitions\": [";
	std::string        endToken = "]\r\n}";

	if (s.find(dictionaryToken) == std::string::npos) {
		std::string noValidResult = "No valid results";
		definitions.push_back(noValidResult);
		return definitions;
	}

	unsigned long currPos = 0;

	while (s.find(dictionaryToken, currPos) != std::string::npos) {
		auto listStartPos = s.find(dictionaryToken, currPos) + dictionaryToken.length();
		auto   listEndPos = s.find(endToken, listStartPos);

		currPos = listStartPos;
		while (s.find("\"", currPos) != std::string::npos) {
			if (s.find("\"", currPos) > s.find(endToken, listStartPos)) break;

			auto defStartPos = s.find("\"", currPos) + 1;
			unsigned long defEndPos;

			auto moreDefs = s.find("\",", currPos);
            if (moreDefs != std::string::npos && moreDefs <= listEndPos) {
				defEndPos = moreDefs;
			}
			else {
				defEndPos = s.find("\"\r\n", defStartPos+2);
			}

			std::string def = s.substr(defStartPos, defEndPos-defStartPos);
			definitions.push_back(def);
			currPos = defEndPos+2;
		}
	}

	if (definitions.size() > 0) m_isResultValid = 1;
	return definitions;
}

std::string DictionaryLookUpSource::formatResults(std::vector<std::string>& results, int fieldFlags) {

	if (results.size() == 0) return "";

	std::string definition = results[0];
	if (definition.length() > 200) {
		definition = definition.substr(0, 200);
		definition.push_back('.');
		definition.push_back('.');
		definition.push_back('.');
	}
	return definition;
}
