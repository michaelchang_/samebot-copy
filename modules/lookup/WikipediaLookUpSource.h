#ifndef __WIKIPEDIA_LOOK_UP_SOURCE_H
#define __WIKIPEDIA_LOOK_UP_SOURCE_H

#include "LookUpSource.h"
#include <vector>

namespace samebot {

class WikipediaLookUpSource : public LookUpSource<std::string> {
private:
    virtual URLRequest requestForQuery(std::string) const override;
    std::string parseResults(std::string s) override;
    std::string formatResults(std::string& results, int fieldFlags = OpAllFields) override;
};

} // namespace

#endif // __WIKIPEDIA_LOOK_UP_SOURCE_H
