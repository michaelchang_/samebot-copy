#ifndef __LOOK_UP_SOURCE_H
#define __LOOK_UP_SOURCE_H

#include "network/URLRequest.h"
#include "network/URLSession.h"
#include <stdio.h>
#include <string>
#include <vector>

std::string replaceUnicode(std::string s);

std::string convertToHexCharacters(std::string s);
std::string replaceEscapeSequences(std::string s);

namespace samebot {

class URLRequest;

enum printFlags {
    OpAllFields         = 0x01,
    OpNameField         = 0x02,
    OpLinkField         = 0x04,
    OpExtractField      = 0x08,
    OpExampleField      = 0x10
};

template <class resultType>
class LookUpSource {
private:
    resultType m_results;

public:

    bool isResultValid() { return m_isResultValid; }
    int searchForQuery(std::string query);
    resultType getResults() { return m_results; }
    std::string getFormattedResult(int fieldFlags = OpAllFields);

protected:
    bool m_isResultValid = false;
    virtual bool prepareToCreateRequestForQuery(const std::string&) { return true; }
    virtual URLRequest requestForQuery(std::string) const = 0;
    virtual resultType parseResults(std::string rawPage) = 0;
    virtual std::string formatResults(resultType& results, int fieldFlags) = 0;
};


/*  ---------------------------------------------------------------------------
                                CLASS IMPLEMENTATION
    ---------------------------------------------------------------------------*/

template <class resultType>
std::string LookUpSource<resultType>::getFormattedResult(int fieldFlags) {
    return formatResults(m_results, fieldFlags);
}

template <class resultType>
int LookUpSource<resultType>::searchForQuery(std::string query) {
    if (!prepareToCreateRequestForQuery(query)) {
        return -1;
    }

    auto response = URLSession::sendSynchronousRequest(requestForQuery(query));
    if (!response.length()) {
        return -1;
    }

    m_results = parseResults(response);
    return 1;
}

} // namespace

#endif // __LOOK_UP_SOURCE_H
