#include "WikipediaLookUpSource.h"

#include <string>
#include <string.h>
#include <vector>

using namespace samebot;


// std::string disambiguation(std::string s) {


// 	std::string startToken = "may refer to: \\n";
// 	int currPos = rawPage.find(startToken)+startToken.length();
// 	currPos++;
// 	std::vector<std::string> links;
// 	return startToken;

// }

URLRequest WikipediaLookUpSource::requestForQuery(std::string query) const {
    return URLRequest("https://en.wikipedia.org/w/api.php", URLRequest::Method::GET, {
        { "action", "query" },
        { "titles", query },
        { "format", "json" },
        { "prop", "extracts" },
        { "explaintext", "1" },
        { "exsectionformat", "plain" },
        { "exsentences", "3" },
    });
}

std::string WikipediaLookUpSource::parseResults(std::string rawPage) {

	std::string extractToken = "\"extract\":\"";

	if (rawPage.find(extractToken) == std::string::npos || rawPage.find("\"extract\":\"\"") != std::string::npos) {
		std::string s = "No valid result";
		return s;
	}

	std::string extract;

	auto startPos = rawPage.find(extractToken)+extractToken.length();
	auto   endPos = rawPage.find_last_of("\"");

	extract = rawPage.substr(startPos, endPos-startPos+1);

	if (extract.length() > 300) {
		extract = extract.substr(0, 300);
		extract.push_back('.');
		extract.push_back('.');
		extract.push_back('.');
	}

	extract = replaceEscapeSequences(replaceUnicode(extract));

	m_isResultValid = 1;
	return extract;

}

std::string WikipediaLookUpSource::formatResults(std::string& results, int fieldFlags) {
	return results;
}
