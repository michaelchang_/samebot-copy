#include "LookUpSource.h"
#include "MemeLookUpSource.h"


#include <string>
#include <string.h>

#define ABOUT_TOKEN "<h2 id=\"about\">"
#define END_ABOUT_TOKEN "</h2>"
#define END_P_TOKEN "</p>"
using namespace samebot;

URLRequest MemeLookUpSource::requestForQuery(std::string query) const {
    return URLRequest("http://knowyourmeme.com/memes/" + query, URLRequest::Method::GET, {});
}

std::string MemeLookUpSource::parseResults(std::string s) {
	std::string extract;

	auto startPos = s.find(ABOUT_TOKEN);
    if (startPos == std::string::npos) {
        return "";
    }

	auto endPos = s.find(END_P_TOKEN, startPos);
    if (endPos == std::string::npos) {
		return "";
	}

 	s = s.substr(startPos, endPos-startPos+strlen(END_P_TOKEN)+10);
 	s = s.substr(s.find(END_ABOUT_TOKEN));

 	//remove html tags
 	bool READTAG = 0;
 	for (unsigned int i = 0; i < s.length(); i++) {
 		if (READTAG && s[i] == '>') {
 			READTAG = 0;
 		}
		else if (s[i] == '<') {
 			READTAG = 1;
 		}
		else if (READTAG == 0) {
			extract.push_back(s[i]);
		}
 	}
 	extract = replaceEscapeSequences(extract);
 	m_isResultValid = 1;
	return extract;
}

std::string MemeLookUpSource::formatResults(std::string& results, int fieldFlags) {
	return results;
}
