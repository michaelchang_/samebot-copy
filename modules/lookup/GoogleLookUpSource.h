#ifndef __GOOGLE_LOOK_UP_SOURCE_H
#define __GOOGLE_LOOK_UP_SOURCE_H

#include "LookUpSource.h"
#include <vector>

namespace samebot {

struct GoogleAPIResult {
        std::string title, link, snippet;
};

class GoogleLookUpSource : public LookUpSource<std::vector<GoogleAPIResult>> {

public:
    GoogleLookUpSource(std::string key, std::string id)
        : LookUpSource<std::vector<GoogleAPIResult>>()
        , m_key(key)
        , m_id(id)
    {}

    bool hasResults() { return (m_results.size() > 0); }
    GoogleAPIResult getFirstResult() {
        if (!hasResults()) {
            return GoogleAPIResult();
        }
        return m_results[0];
    }
private:
    virtual URLRequest requestForQuery(std::string) const override;

    std::string m_key;
    std::string m_id;

    std::vector<GoogleAPIResult> m_results;
    std::vector<GoogleAPIResult> parseResults(std::string s) override;
    std::string formatResults(std::vector<GoogleAPIResult>& results, int fieldFlags = OpAllFields) override;

};

} // namespace

#endif // __GOOGLE_LOOK_UP_SOURCE_H
