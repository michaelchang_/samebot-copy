#include "MerriamWebsterLookUpSource.h"

#include "basics/StringUtilities.h"

#include <vector>
#include <string>
#include <string.h>

using namespace samebot;

MerriamWebsterLookUpSource::MerriamWebsterLookUpSource(std::string key)
    : LookUpSource<std::vector<std::string>>()
    , m_key(key)
{
}

URLRequest MerriamWebsterLookUpSource::requestForQuery(std::string query) const {
    return URLRequest("http://www.dictionaryapi.com/api/v1/references/collegiate/xml/"+query, URLRequest::Method::GET, {
        { "key", m_key },
        });
}


std::string removeTags(std::string s) {
    std::string::size_type startpos = 0, endpos, viTagStart = 0;
    bool inViTag = false;
    for (;;) {
        startpos = s.find('<', startpos);
        if (startpos == std::string::npos) break;
        endpos = s.find('>', startpos+1);
        if (endpos == std::string::npos) break;

        std::string tag = s.substr(startpos+1, endpos-startpos-1);

        //if the tag is <vi> we want to delete everything inside the tag as well
        if (inViTag && tag.compare("/vi") == 0) {
            s.erase(viTagStart, endpos-viTagStart+1);
            startpos = viTagStart;
            viTagStart = 0;
            inViTag = false;
        }
        else if (tag.compare("vi") == 0) {
            inViTag = true;
            viTagStart = startpos;
            startpos += 1;
        }
        //otherwise just delete the tag
        else s.erase(startpos, endpos-startpos+1);
    } // for loop
    return s;
}

std::string getDefinitionFromEntry(std::string s) {
    std::string def = "";
    std::string dtStartToken = "<dt>";
    std::string   dtEndToken = "</dt>";

    std::string::size_type currPos = 0, endPos;
    while (s.find(dtStartToken, currPos) != std::string::npos) {
        currPos = s.find(dtStartToken, currPos);
        endPos = s.find(dtEndToken, currPos);

        std::string rawDef = s.substr(currPos+dtStartToken.length(), endPos-currPos-dtEndToken.length()+1);
        std::string parsedDef = removeTags(rawDef);
        parsedDef = stringByTrimmingString(parsedDef);
        def += parsedDef + " ";
        currPos = endPos;
    }
    return def;
}

std::vector<std::string> MerriamWebsterLookUpSource::parseResults(std::string s) {
    std::vector<std::string> definitions;

    std::string entryStartToken = "<entry id=\"";
    std::string   entryEndToken = "</entry>";

    if (s.find(entryStartToken) == std::string::npos) {
        std::string noValidResult = "No valid results";
        definitions.push_back(noValidResult);

        if (s.find("<suggestion>") != std::string::npos) {
            m_hasSuggestion = grabTopSuggestion(s);
        }
        return definitions;
    }

    unsigned long currPos = 0;
    while (s.find(entryStartToken, currPos) != std::string::npos) {
        auto entryStartPos = s.find(entryStartToken, currPos);
        auto entryEndPos   = s.find(entryEndToken, entryStartPos);

        auto entryWordStart = entryStartPos + entryStartToken.length();
        std::string entryName = s.substr(entryWordStart, s.find("\"", entryWordStart)-entryWordStart);
        std::string entry = s.substr(entryStartPos, entryEndPos-entryStartPos);
        std::string def = getDefinitionFromEntry(entry);

        definitions.push_back(def);
        currPos = entryEndPos;
    }
    if (definitions.size() > 0) m_isResultValid = 1;
    return definitions;
}

std::string MerriamWebsterLookUpSource::formatResults(std::vector<std::string>& results, int fieldFlags) {
    if (results.size() == 0) return "";
    std::string definition = results[0];

    return definition;
}

bool MerriamWebsterLookUpSource::grabTopSuggestion(std::string s) {
    std::string startTag = "<suggestion>";
    std::string endTag = "</suggestion>";

    std::string::size_type start = s.find(startTag), end = s.find(endTag);
    if (start == std::string::npos || end == std::string::npos || end < start) return 0;

    auto startPos = start+startTag.length();
    m_suggestion = s.substr(startPos, end-startPos);
    return true;
}
