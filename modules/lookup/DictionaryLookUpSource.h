#ifndef __DICTIONARY_LOOK_UP_SOURCE_H
#define __DICTIONARY_LOOK_UP_SOURCE_H

#include "LookUpSource.h"
#include <vector>

namespace samebot {

class DictionaryLookUpSource : public LookUpSource<std::vector<std::string>> {
private:
    virtual URLRequest requestForQuery(std::string) const override;
    std::vector<std::string> parseResults(std::string s) override;
    std::string formatResults(std::vector<std::string>& results, int fieldFlags = OpAllFields) override;
};

} // namespace

#endif // __DICTIONARY_LOOK_UP_SOURCE_H
