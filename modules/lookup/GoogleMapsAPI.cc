#include "GoogleMapsAPI.h"
#include "Parser.h"
#include <string>

using namespace samebot;

GoogleMapsAPIStatusCodes stringToGoogleMapsAPIStatusCode(std::string s) {
    if (s.compare("OK") == 0) return OK;
    if (s.compare("ZERO_RESULTS") == 0) return ZERO_RESULTS;
    if (s.compare("OVER_QUERY_LIMIT") == 0) return OVER_QUERY_LIMIT;
    if (s.compare("REQUEST_DENIED") == 0) return REQUEST_DENIED;
    if (s.compare("INVALID_REQUEST") == 0) return INVALID_REQUEST;
    return UNKNOWN_ERROR;
}

GoogleMapsAPIStatusCodes getStatusCode(Parser& p) {
    std::string statusTag = "\"status\"";
    p.moveAfterTag(statusTag);
    std::string statusCode = p.getBetweenTags();
    return stringToGoogleMapsAPIStatusCode(statusCode);
}


