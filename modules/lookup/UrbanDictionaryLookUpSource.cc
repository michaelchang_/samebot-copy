#include "UrbanDictionaryLookUpSource.h"

#include "basics/Assertions.h"

#include <vector>
#include <string>
#include <string.h>

using namespace samebot;

URLRequest UrbanDictionaryLookUpSource::requestForQuery(std::string query) const {
    return URLRequest("http://api.urbandictionary.com/v0/define", URLRequest::Method::GET, {
        { "term", query }
    });
}

int UrbanDictionaryLookUpSource::grabFields(const std::string& page, int currPos, UrbanDictionaryResult& result) {

    auto numFields = urbanDictionaryFields.size();
    std::vector<std::string::size_type> fieldOrder;
    bool outOfOrder = false;
    //get the index positions of the fields in the page
    for (auto& fieldName : urbanDictionaryFields) {
        std::string fieldTag = "\""+fieldName+"\":";
        std::string::size_type fieldLocation = page.find(fieldTag, currPos);
        if (fieldLocation == std::string::npos) continue;
        if (!fieldOrder.empty() && fieldLocation < fieldOrder.back()) outOfOrder = true;
        fieldOrder.push_back(fieldLocation);
    }

    //if the index positions are out of order, put them in the right order, and change the field positions too
    if (outOfOrder) {
        for (size_t i = 0; i < numFields; i++) {
            size_t min = i;
            for (size_t j = i + 1; j < numFields; j++) {
                if (fieldOrder[j] < fieldOrder[min]) min = j;
            }
            std::swap(fieldOrder[i], fieldOrder[min]);
            std::swap(urbanDictionaryFields[i], urbanDictionaryFields[min]);
        }
    }

    //extract the fields and put them in result
    for (size_t i = 0; i < numFields; i++) {
        std::string::size_type start = fieldOrder[i]+urbanDictionaryFields[i].length()+3;
        std::string::size_type   end;
        if (i == numFields-1) end = page.find("}", start);
        else end  = fieldOrder[i+1]-1;
        if (end < start) continue;
        std::string fieldValue = page.substr(start, end-start);
        if (urbanDictionaryFields[i].compare("thumbs_up") != 0 &&
                urbanDictionaryFields[i].compare("thumbs_down") != 0) {
            //get rid of the quotation marks
            if (fieldValue.length() >= 2) {
                fieldValue.pop_back();
                fieldValue.erase(0, 1);
            }
        }
        if (urbanDictionaryFields[i].compare("definition") == 0 ||
                urbanDictionaryFields[i].compare("example")    == 0 ||
                urbanDictionaryFields[i].compare("word")       == 0) {

            fieldValue = replaceEscapeSequences(replaceUnicode(fieldValue));
        }
        result.fields[urbanDictionaryFields[i]] = fieldValue;
    }
    return 1;
}

std::vector<UrbanDictionaryResult> UrbanDictionaryLookUpSource::parseResults(std::string s) {
    //delete up to the list of definitions
    s.erase(0, s.find("list\":[{"));
    std::vector<UrbanDictionaryResult> definitions;

    std::string entryStartToken = "{\"";
    std::string entryEndToken   = "},{";

    size_t currPos = 0;
    while (currPos != std::string::npos || s.find(entryStartToken, currPos) != std::string::npos) {
        currPos = s.find(entryStartToken, currPos);
        UrbanDictionaryResult definition;
        grabFields(s, static_cast<int>(currPos), definition);


        definitions.push_back(definition);
        currPos = s.find(entryEndToken, currPos);
        //only grab the first definition
        break;
    }

    if (definitions.size() > 0) m_isResultValid = 1;
    return definitions;
}

std::string UrbanDictionaryLookUpSource::formatResults(std::vector<UrbanDictionaryResult>& results, int fieldFlags) {

    if (results.size() == 0) return "";

    std::string s;
    if (fieldFlags & OpAllFields || fieldFlags & OpNameField)
        s.append(results[0].fields["word"]);
    if (fieldFlags & OpAllFields || fieldFlags & OpLinkField) {
        if (!s.empty()) s.append("\n");
        s.append(results[0].fields["link"]);
    }
    if (fieldFlags & OpAllFields || fieldFlags & OpExtractField) {
        if (!s.empty()) s.append("\n");
        s.append(results[0].fields["definition"]);
    }
    if (fieldFlags & OpAllFields || fieldFlags & OpExampleField) {
        if (!s.empty()) s.append("\n");
        s.append(results[0].fields["example"]);
    }
    return s;
}
