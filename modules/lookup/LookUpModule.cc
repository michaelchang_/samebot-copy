#include "LookUpModule.h"

#include "LookUpSession.h"
#include "MerriamWebsterLookUpSource.h"
#include "GoogleLookUpSource.h"
#include "MemeLookUpSource.h"
#include "UrbanDictionaryLookUpSource.h"
#include "WikipediaLookUpSource.h"
#include "ForecastLookUpSource.h"

#include "network/URLSession.h"
#include "network/irc/IRCMessage.h"
#include "basics/StringUtilities.h"
#include "basics/Configuration.h"
#include "modules/ModuleActiveResponseHandler.h"

#include <string.h>
#include <string>
#include <vector>
#include <algorithm>

#define SEARCH_ERROR -1
#define NO_RESULTS 0
#define WIKIPEDIA_TOKEN "en.wikipedia.org/wiki/"
#define MEME_TOKEN "knowyourmeme.com/memes/"
#define MAX_RESULT_LENGTH 450

using namespace samebot;

const std::vector<std::string> LookUpModule::googleTriggerKeywords {
    "google",
        "googl",
        "goog",
};

const std::vector<std::string> LookUpModule::generalGoogleTriggerKeywords {
    "who",
        "what",
        "when",
        "where",
        "which",
        "how",
};

const std::vector<std::string> LookUpModule::wikipediaTriggerKeywords {
    "wiki",
        "wikipedia"
};

const std::vector<std::string> LookUpModule::dictionaryTriggerKeywords {
    "dictionary",
        "define",
        "def",
};

const std::vector<std::string> LookUpModule::urbanDictionaryTriggerKeywords {
    "urbandict",
        "street slang",
        "udef",
};

const std::vector<std::string> LookUpModule::memeTriggerKeywords {
    "meme",
        "meem",
        "maymay",
};

const std::vector<std::string> LookUpModule::weatherTriggerWords {
    "weather",
        "forecast",
};

void LookUpModule::didSetConfiguration(const Configuration* configuration) {
    if (!configuration) {
        m_session = nullptr;
        return;
    }

    auto merriamWebsterAPIKey = configuration->merriamWebsterAPIKey();
    auto forecastAPIKey = configuration->forecastAPIKey();
    auto memeAPIKey = configuration->memeAPIKey();
    auto memeAPIID = configuration->memeAPIID();
    auto googleAPIKey = configuration->googleAPIKey();
    auto googleAPIID = configuration->googleAPIID();
    if (merriamWebsterAPIKey.length() && forecastAPIKey.length()) {
        m_session = std::make_unique<LookUpSession>(merriamWebsterAPIKey,
                                                    forecastAPIKey,
                                                    memeAPIKey, memeAPIID,
                                                    googleAPIKey, googleAPIID);
    }
    else {
        LOG_ERROR("LookUpModule: API keys not specified in configuration.");
    }
}

int LookUpModule::containsPage(std::string s, std::string token) {
    if (s.find(token) != std::string::npos) {
        return 1;
    }
    return 0;
}

std::string LookUpModule::getPage(std::string s, std::string token) {
    return s.substr(s.find(token)+token.length());
}

std::string addEllipsis(std::string s) {
    std::string res;
    std::vector<std::string> messages;

    //break the string apart by newlines
    std::string::size_type currPos = 0;
    std::string::size_type newlinePos = s.find("\n");
    while (newlinePos != std::string::npos) {
        std::string temp = s.substr(currPos, newlinePos-currPos);
        messages.push_back(temp);
        currPos = newlinePos+1;
        newlinePos = s.find("\n", currPos);
    }
    messages.push_back(s.substr(currPos));

    //check if each message is less than MAX_RESULT_LENGTH
    for (auto& message : messages) {
        if (message.length() > MAX_RESULT_LENGTH) {
            message = message.substr(0, MAX_RESULT_LENGTH-3);
            message += "...";
        }
        res += message + "\n";
    }
    res.pop_back();
    return res;
}

template <typename T>
void LookUpModule::printResult(std::string recipient, int status, LookUpSource<T>& source, int fieldFlags, bool suppressErrorMessage) {

    /*--------------------------------
      GOOGLE_RESULT    std::vector<GoogleAPIResult>
      WIKIPEDIA_RESULT std::string
      DICTIONARY_RESULT std::vector<std::string>
      URBAN_DICTIONARY_RESULT std::vector<UrbanDictionaryResult>
      KNOWYOURMEME_RESULT std::string
      --------------------------------*/
    if (status > 0 && source.isResultValid()) {
        std::string formattedResult = source.getFormattedResult(fieldFlags);
        int resultLength = static_cast<int>(formattedResult.length());
        if (resultLength > 0 && resultLength < MAX_RESULT_LENGTH)
            sendMessage(recipient, formattedResult);
        else if (resultLength > MAX_RESULT_LENGTH) {
            formattedResult = addEllipsis(formattedResult);
            sendMessage(recipient, formattedResult);
        }
        else if (!suppressErrorMessage) {
            sendMessage(recipient, statusToErrorMessage(NO_RESULTS));
        }
    }

    else if (!suppressErrorMessage)
        sendMessage(recipient, statusToErrorMessage(status));
}

bool LookUpModule::wantSomeSlang(std::string s) {
    if (s.find(" kids ") == std::string::npos) return 0;
    if (s.find("saying") == std::string::npos) return 0;
    return 1;
}

std::string LookUpModule::getRandomUrbanDictionaryPage() {
    std::string rawPage = URLSession::httpGet("http://www.urbandictionary.com/random.php");
    if (!rawPage.length()) {
        return "";
    }

    if (rawPage.find("href=\"") == std::string::npos || rawPage.find("define.php?term=") == std::string::npos) {
        return "";
    }

    int hrefStartPos = static_cast<int>(rawPage.find("href=\"") + strlen("href=\""));
    int startPos = static_cast<int>(rawPage.find("define.php?term=", hrefStartPos)) + strlen("define.php?term=");
    int   endPos = static_cast<int>(rawPage.find("\">", startPos));

    std::string query = rawPage.substr(startPos, endPos-startPos);
    return query;
}

template <typename T>
int LookUpModule::searchWithSource(LookUpSource<T>& source, std::string query) {
    int status = source.searchForQuery(query);

    if (status < 0)
        return SEARCH_ERROR;
    if (!source.isResultValid())
        return NO_RESULTS;
    return 1;
}

std::string LookUpModule::statusToErrorMessage(int status, std::string msg) {
    if (status == SEARCH_ERROR) {
        return "Search error.";
    }
    else if (status == NO_RESULTS) {
        return msg;
    }
    return msg;
}

void LookUpModule::didReceiveMessage(std::shared_ptr<IRCMessage> message) {
    if (!message->prefix() || message->prefix()->type() != IRCMessagePrefix::Type::User) {
        LOG_DEBUG("Received message from server, not user; ignoring");
        return;
    }

    auto recipient = message->parameters().front();
    auto body = message->parameters().back();

    if (!recipient.length() || !body.length()) {
        LOG_ERROR("Received malformed PRIVMSG (missing recipient or length); ignoring");
        return;
    } else if (recipient[0] != '#') {
        LOG_DEBUG("Received a private message; ignoring");
        return;
    }

    if (!isPrivateMessage(*message) && !ModuleActiveResponseHandler::hasSamebotPrefix(body)) {
        return;
    }

    if (wantSomeSlang(body)) {
        int status;
        std::string randomWord = getRandomUrbanDictionaryPage();

        UrbanDictionaryLookUpSource u;
        status = searchWithSource(u, randomWord);
        printResult(recipient, status, u, OpNameField | OpExtractField | OpExampleField, 1);
        return;
    }

    body = lowercaseString(stringByTrimmingString(ModuleActiveResponseHandler::removeSamebotPrefix(body)));
    auto words = splitString(body);

    LookUpSources lookUpSource = LookUpSources::none;
    std::string fuzzyMatchedWord;
    for (auto& word : words) {
        if (isExactMatch(word, googleTriggerKeywords)) {
            lookUpSource = LookUpSources::google;
            fuzzyMatchedWord = word;
            break;
        }
        if (isExactMatch(word, generalGoogleTriggerKeywords)) {
            lookUpSource = LookUpSources::google;
            fuzzyMatchedWord = "";
            break;
        }
        if (isExactMatch(word, wikipediaTriggerKeywords)) {
            lookUpSource = LookUpSources::wikipedia;
            fuzzyMatchedWord = word;
            break;
        }
        if (isExactMatch(word, weatherTriggerWords)) {
            lookUpSource = LookUpSources::weather;
            fuzzyMatchedWord = word;
            break;
        }
        if (isExactMatch(word, dictionaryTriggerKeywords)) {
            lookUpSource = LookUpSources::dictionary;
            fuzzyMatchedWord = word;
            break;
        }
        if (isExactMatch(word, memeTriggerKeywords)) {
            lookUpSource = LookUpSources::meme;
            fuzzyMatchedWord = word;
            break;
        }
        if (isExactMatch(word, urbanDictionaryTriggerKeywords)) {
            lookUpSource = LookUpSources::urbanDictionary;
            fuzzyMatchedWord = word;
            break;
        }
    }


    /* -------------------------------------------------------------
       GOOGLE
       ------------------------------------------------------------ */

    //special case for google
    //we separate "hard" keywords (google, goog, etc) from "soft" keywords (who, what, etc).
    if (lookUpSource == LookUpSources::google) {
        //if the keywords are google or goog, remove them, otherwise just search (ie "who is george washington")
        std::string q = (fuzzyMatchedWord.empty()? body : removeFirstStringFromString(body, fuzzyMatchedWord));
        auto keywordLocation = body.find(fuzzyMatchedWord);
        if (keywordLocation == 0 || keywordLocation == 1) {
            GoogleLookUpSource g(m_session->googleAPIKey(), m_session->googleAPIID());
            int status = searchWithSource(g, q);
            //If the result is a Wikipedia, knowyourmeme.com, or urban dictionary result,
            //we can use those parsers for better results.
            if (g.hasResults()) {
                auto result = g.getFirstResult();
                if (result.link.find("wikipedia.org") != std::string::npos) {
                    lookUpSource = LookUpSources::wikipedia;
                } else if (result.link.find("knowyourmeme.com") != std::string::npos) {
                    lookUpSource = LookUpSources::meme;
                } else if (result.link.find("urbandictionary.com") != std::string::npos) {
                    lookUpSource = LookUpSources::urbanDictionary;
                } else {
                    printResult(recipient, status, g);
                }
            } else {
                printResult(recipient, status, g);
            }
        }
    }

    /* -------------------------------------------------------------
       WEATHER
       ------------------------------------------------------------ */
    if (lookUpSource == LookUpSources::weather) {
        std::string q = removeFirstStringFromString(body, fuzzyMatchedWord);
        ForecastLookUpSource f(m_session->forecastAPIKey());
        int status = searchWithSource(f, q);
        printResult(recipient, status, f, OpAllFields);
    }

    /* -------------------------------------------------------------
       URBAN DICTIONARY
       ------------------------------------------------------------ */
    if (lookUpSource == LookUpSources::urbanDictionary) {
        std::string q = removeFirstStringFromString(body, fuzzyMatchedWord);

        UrbanDictionaryLookUpSource u;
        int status = searchWithSource(u, q);
        printResult(recipient, status, u, OpExtractField | OpExampleField);
    }

    /* -------------------------------------------------------------
       KNOWYOURMEME
       ------------------------------------------------------------ */
    if (lookUpSource == LookUpSources::meme) {
        std::string q = removeFirstStringFromString(body, fuzzyMatchedWord);
        //add meme to the beginning of google search to let google know you're searching for the dank memes
        q.insert(0, "meme ");
        GoogleLookUpSource m(m_session->memeAPIKey(), m_session->memeAPIID());

        int status = searchWithSource(m, q);

        if (status > 0) {
            std::vector<GoogleAPIResult> googleResults = m.getResults();

            bool foundMemePage = 0;
            for (unsigned int i = 0; i < googleResults.size(); i++) {
                std::string googleLink = googleResults[i].link;
                if (containsPage(googleLink, MEME_TOKEN)) {
                    foundMemePage = 1;
                    MemeLookUpSource w;
                    status = searchWithSource(w, getPage(googleLink, MEME_TOKEN));
                    if (status > 0) sendMessage(recipient, googleLink);
                    printResult(recipient, status, w);
                    break;
                } // if the google link contains a wikipedia page
            } // for loop through all google results

            if (!foundMemePage) {
                sendMessage(recipient, statusToErrorMessage(NO_RESULTS));
            }
        } //if the google search was valid
        else {
            sendMessage(recipient, statusToErrorMessage(status));
        }
    }

    /* -------------------------------------------------------------
       WIKIPEDIA
       ------------------------------------------------------------ */
    if (lookUpSource == LookUpSources::wikipedia) {
        std::string q = removeFirstStringFromString(body, fuzzyMatchedWord);

        //add wikipedia to the beginning of the search to let google know you're looking for the dank wiki pages
        q.insert(0, "wikipedia ");

        GoogleLookUpSource g(m_session->googleAPIKey(), m_session->googleAPIID());
        int status = searchWithSource(g, q);
        if (status > 0) {
            std::vector<GoogleAPIResult> googleResults = g.getResults();

            bool foundWikiPage = 0;
            for (unsigned int i = 0; i < googleResults.size(); i++) {
                std::string googleLink = googleResults[i].link;
                if (containsPage(googleLink, WIKIPEDIA_TOKEN)) {
                    foundWikiPage = 1;
                    WikipediaLookUpSource w;
                    status = searchWithSource(w, getPage(googleLink, WIKIPEDIA_TOKEN));
                    if (status > 0) sendMessage(recipient, googleLink);
                    printResult(recipient, status, w);
                    break;
                } // if the google link contains a wikipedia page
            } // for loop through all google results
            if (!foundWikiPage) {
                sendMessage(recipient, statusToErrorMessage(NO_RESULTS));
            }
        } //if the google search was valid

        else {
            sendMessage(recipient, statusToErrorMessage(status));
        }
    }

    /* -------------------------------------------------------------
       DICTIONARY
       ------------------------------------------------------------ */
    if (lookUpSource == LookUpSources::dictionary) {
        std::string q = stringByTrimmingString(removeFirstStringFromString(body, fuzzyMatchedWord));

        if (q.length() < 5 || q.compare("same") == 0) {
            //if the length of the query is less than 4 (first char is a space), search urban dictionary instead for funnier results
            UrbanDictionaryLookUpSource u;
            int status = searchWithSource(u, q);
            printResult(recipient, status, u, OpExtractField | OpExampleField);
        }
        else {
            //otherwise just search using the regular dictionary
            MerriamWebsterLookUpSource d(m_session->merriamWebsterAPIKey());
            int status = searchWithSource(d, q);
            if (status > 0) {
                // found the word we are looking for
                printResult(recipient, status, d, OpAllFields, 1);
            }
            else if (d.hasSuggestion()) {
                // couldnt find the word. searching for suggestion
                status = searchWithSource(d, d.getSuggestion());
                if (status > 0) {
                    sendMessage(recipient, "Did you mean: "+d.getSuggestion());
                    printResult(recipient, status, d, OpAllFields, 1);
                }
            }
            else {
                //we didnt find a definition. Look in urban dictionary
                UrbanDictionaryLookUpSource u;
                int status = searchWithSource(u, q);
                printResult(recipient, status, u, OpExtractField | OpExampleField);
            }
        }

    }

} // didReceiveMessage

double LookUpModule::getActiveScore(std::shared_ptr<IRCMessage> message) {
    //todo: if the word is an exact match, give it more relevance
    auto body = stringByTrimmingString(message->parameters().back());
    auto words = splitString(ModuleActiveResponseHandler::removeSamebotPrefix(body));

    double res = 0;
    //The earlier the keyword, the stronger the relevance, up to the first 5 words.
    for (int i = 0; i < (int)words.size(); i++) {
        std::string word = words[i];
        if (isFuzzyMatch(word, googleTriggerKeywords) ||
            isFuzzyMatch(word, wikipediaTriggerKeywords) ||
            isFuzzyMatch(word, dictionaryTriggerKeywords) ||
            isFuzzyMatch(word, urbanDictionaryTriggerKeywords) ||
            isFuzzyMatch(word, memeTriggerKeywords) ||
            isFuzzyMatch(word, weatherTriggerWords))
        {
            res = 75;
            res *= 1 + (i < 5? i*0.1 : 0);
            return res;
        }
        if (isFuzzyMatch(word, generalGoogleTriggerKeywords)) {
            res = 40;
            res *= 1 + (i < 5? i*0.1 : 0);
            return res;
        }
    }
    return res;
}
