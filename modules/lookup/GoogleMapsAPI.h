#ifndef __GOOGLE_MAPS_API_H
#define __GOOGLE_MAPS_API_H

#include "Parser.h"
#include <string>

using namespace samebot;

enum GoogleMapsAPIStatusCodes {
    OK, ZERO_RESULTS, OVER_QUERY_LIMIT, REQUEST_DENIED, INVALID_REQUEST, UNKNOWN_ERROR
};

GoogleMapsAPIStatusCodes stringToGoogleMapsAPIStatusCode(std::string);
GoogleMapsAPIStatusCodes getStatusCode(Parser& p);
#endif //__GOOGLE_MAPS_API_H
