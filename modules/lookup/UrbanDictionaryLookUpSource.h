#ifndef __URBAN_DICTIONARY_LOOK_UP_SOURCE_H
#define __URBAN_DICTIONARY_LOOK_UP_SOURCE_H

#include "LookUpSource.h"
#include <vector>
#include <map>

namespace samebot {

static std::vector<std::string> urbanDictionaryFields =
    {"definition", "permalink", "thumbs_up", "author", "word", "defid", "current_vote", "example", "thumbs_down"};

struct UrbanDictionaryResult {
    std::map<std::string, std::string> fields;
};

class UrbanDictionaryLookUpSource : public LookUpSource<std::vector<UrbanDictionaryResult>> {
private:
    virtual URLRequest requestForQuery(std::string) const override;
    std::vector<UrbanDictionaryResult> parseResults(std::string s) override;
    int grabFields(const std::string& page, int currPos, UrbanDictionaryResult& result);
    std::string formatResults(std::vector<UrbanDictionaryResult>& results, int fieldFlags = OpAllFields) override;

    std::string convertToString(const char* c) {
        std::string s(c);
        return s;
    }

    int getIndex(const char* c){
        std::string field(c);
        for (unsigned int i = 0; i < urbanDictionaryFields.size(); i++) {
            if (urbanDictionaryFields[i].compare(field) == 0) return i;
        }
        return 0;
    }

};


} // namespace

#endif // __URBAN_DICTIONARY_LOOK_UP_SOURCE_H
