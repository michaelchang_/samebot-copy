#ifndef __MEME_LOOK_UP_MODULE_H
#define __MEME_LOOK_UP_MODULE_H

#include "LookUpSource.h"


namespace samebot {

class MemeLookUpSource : public LookUpSource<std::string> {
private:
    virtual URLRequest requestForQuery(std::string) const override;
    std::string parseResults(std::string s) override;
    std::string formatResults(std::string& results, int fieldFlags = OpAllFields) override;
};

} // samebot

#endif //__MEME_LOOK_UP_MODULE_H
