#ifndef __LOOK_UP_MODULE_H
#define __LOOK_UP_MODULE_H

#include "LookUpSource.h"
#include "modules/Module.h"
#include "LookUpSession.h"

/* -----------------------------------------------------------------
    Implemented modules:
    Google
    Wikipedia
    Merriam Webter
    Urban Dictionary
    KnowYourMeme
    Forecast
    ----------------------------------------------------------------- */

namespace samebot {


class LookUpModule : public Module {
public:
    virtual std::string moduleIdentifier() const override { return "LookUpModule"; }
    virtual double getActiveScore(std::shared_ptr<IRCMessage> message) override;

private:
    static const std::vector<std::string> googleTriggerKeywords;
    static const std::vector<std::string> generalGoogleTriggerKeywords;
    static const std::vector<std::string> wikipediaTriggerKeywords;
    static const std::vector<std::string> dictionaryTriggerKeywords;
    static const std::vector<std::string> urbanDictionaryTriggerKeywords;
    static const std::vector<std::string> memeTriggerKeywords;
    static const std::vector<std::string> weatherTriggerWords;

    enum LookUpSources {
        none, google, wikipedia, dictionary, urbanDictionary, meme, weather
    };

    template <typename T>
    void printResult(std::string recipient, int status, LookUpSource<T>& source, int fieldFlags = OpAllFields, bool suppressErrorMessage = 0);

    template<typename T>
    int searchWithSource(LookUpSource<T>& source, std::string query);
    std::string statusToErrorMessage(int status, std::string msg = "Couldn't find anything. Big sorry!");
    std::string parseInput(std::string s, std::string keyword = "");

    int containsPage(std::string s, std::string token);
    std::string getPage(std::string s, std::string token);

    bool wantSomeSlang(std::string s);
    std::string getRandomUrbanDictionaryPage();

    virtual void didReceiveMessage(std::shared_ptr<IRCMessage>) override;

    virtual void didSetConfiguration(const Configuration*) override;
    std::unique_ptr<LookUpSession> m_session;
};

} // namespace

#endif // __LOOK_UP_MODULE_H
