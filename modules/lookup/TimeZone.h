#ifndef __TIME_ZONE_H
#define __TIME_ZONE_H

#include <time.h>
#include <string>

enum dayOfWeek{
    Sunday, Monday, Tuesday, Wednesday,
    Thursday, Friday, Saturday, None
};

std::string timeZoneAPICall(double, double);
int getTimeOffset(double, double);
time_t getTimeStampForDayAtAddress(dayOfWeek, double, double, struct tm**);
void fillTmStruct(struct tm**, time_t);
#endif //__TIME_ZONE_H
