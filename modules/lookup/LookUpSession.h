#ifndef __LOOK_UP_SESSION_H
#define __LOOK_UP_SESSION_H

#include <string>

namespace samebot{


class LookUpSession {
public:
    LookUpSession(std::string merriamWebsterAPIKey,
                  std::string forecastAPIKey,
                  std::string memeAPIKey,
                  std::string memeAPIID,
                  std::string googleAPIKey,
                  std::string googleAPIID)
        : m_merriamWebsterAPIKey(merriamWebsterAPIKey)
        , m_forecastAPIKey(forecastAPIKey)
        , m_memeAPIKey(memeAPIKey)
        , m_memeAPIID(memeAPIID)
        , m_googleAPIKey(googleAPIKey)
        , m_googleAPIID(googleAPIID) {}

    std::string merriamWebsterAPIKey() const { return m_merriamWebsterAPIKey; }
    std::string forecastAPIKey() const { return m_forecastAPIKey; }
    std::string memeAPIKey() const { return m_memeAPIKey; }
    std::string memeAPIID() const { return m_memeAPIID; }
    std::string googleAPIKey() const { return m_googleAPIKey; }
    std::string googleAPIID() const { return m_googleAPIID; }

private:
    std::string m_merriamWebsterAPIKey;
    std::string m_forecastAPIKey;
    std::string m_memeAPIKey;
    std::string m_memeAPIID;
    std::string m_googleAPIKey;
    std::string m_googleAPIID;
};

} // namespace samebot

#endif // __LOOK_UP_SESSION_H
