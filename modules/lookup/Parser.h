#ifndef __PARSER_H
#define __PARSER_H

#include <algorithm>
#include <utility>
#include <vector>
#include <string>
#include <string.h>

namespace samebot {

class Parser {
public:
    Parser(std::string s) : m_string(s) {}
    inline std::string getBetweenTags(std::string::size_type, const char*, const char*);
    inline std::string getBetweenTags(const char*, const char*);
    inline void moveToTag(std::string);
    inline void moveToTag(const char*);
    inline void moveAfterTag(std::string);
    inline void moveAfterTag(const char*);
    inline std::string::size_type getCurrPos() {return m_currPos;}
    inline void setCurrPos(std::string::size_type currPos) {m_currPos = currPos;}
    inline std::string getField(std::string field, const char*, const char*);
    inline std::string getField(const char*, const char*, const char*);
    inline std::string getString() {return m_string;}
private:
    std::string m_string;
    std::string::size_type m_currPos = 0;
    int m_status;
};

std::string Parser::getBetweenTags(std::string::size_type currPos, const char* startTag="\"", const char* endTag="\"") {
    std::string::size_type startPos, endPos;
    startPos = m_string.find(startTag, currPos);
    if (startPos == std::string::npos) return "";
    endPos = m_string.find(endTag, startPos+strlen(startTag));
    if (endPos == std::string::npos) return "";
    return m_string.substr(startPos+strlen(startTag), endPos-startPos-strlen(startTag));
}
std::string Parser::getBetweenTags(const char* startTag="\"", const char* endTag="\"") {
    return getBetweenTags(m_currPos, startTag, endTag);
}
void Parser::moveToTag(std::string tag) {
    std::string::size_type tagPos = m_string.find(tag, m_currPos);
    if (tagPos == std::string::npos) m_status = -1;
    m_currPos = tagPos;
}
void Parser::moveToTag(const char* ctag) {
    std::string tag(ctag);
    moveToTag(tag);
}
void Parser::moveAfterTag(std::string tag) {
    std::string::size_type tagPos = m_string.find(tag, m_currPos)+tag.length();
    if (tagPos == std::string::npos) m_status = -1;
    m_currPos = tagPos;
}
void Parser::moveAfterTag(const char* ctag) {
    std::string tag(ctag);
    moveAfterTag(tag);
}
std::string Parser::getField(std::string field, const char* startTag="\"", const char* endTag="\"") {
    moveAfterTag(field);
    return getBetweenTags(startTag, endTag);
}
std::string Parser::getField(const char* field, const char* startTag="\"", const char* endTag="\"") {
    moveAfterTag(field);
    return getBetweenTags(startTag, endTag);
}
} // namespace
#endif
