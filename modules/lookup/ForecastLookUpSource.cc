#include "ForecastLookUpSource.h"
#include "Parser.h"
#include "TimeZone.h"
#include "basics/StringUtilities.h"
#include <cmath>
#include <regex>

using namespace samebot;

ForecastLookUpSource::ForecastLookUpSource(std::string key)
    : LookUpSource<std::string>()
    , m_key(key)
{
    m_wantSpecificDay = m_wantToday = m_wantTomorrow = false;
}

//"�", "☀", "🌣", "🌧", "❄", "⛆", "🌬", "🌫", "☁", "⛅", "☁", "🌨", "🌩", "🌪"

const std::string ForecastLookUpSource::weatherUnicodeCharacters[] = {
    "�", //unknown
    //"●", //clear-day
    "☀", //clear-day
    "☾", //clear-night
    "☂", //rain
    "❄", //snow
    "⛆", //sleet
    "⚟", //wind
    "☁", //fog
    "☁", //cloudy
    "⛅", //parly-cloudy-day
    "☁", //partly-cloudy-night
    "☂", //hail
    "⚡", //thunderstorm
    "⛛" //tornado
};

//update NUM_FORECAST_ICONS if you add
const std::string ForecastLookUpSource::forecastIOIcons[] = {
    "unknown",
        "clear-day",
        "clear-night",
        "rain",
        "snow",
        "sleet",
        "wind",
        "fog",
        "cloudy",
        "partly-cloudy-day",
        "partly-cloudy-night",
        "hail",
        "thunderstorm",
        "tornado"
};

const std::map<std::string, std::string> ForecastLookUpSource::iconMap = {
    { forecastIOIcons[0], weatherUnicodeCharacters[0] },
    { forecastIOIcons[1], weatherUnicodeCharacters[1] },
    { forecastIOIcons[2], weatherUnicodeCharacters[2] },
    { forecastIOIcons[3], weatherUnicodeCharacters[3] },
    { forecastIOIcons[4], weatherUnicodeCharacters[4] },
    { forecastIOIcons[5], weatherUnicodeCharacters[5] },
    { forecastIOIcons[6], weatherUnicodeCharacters[6] },
    { forecastIOIcons[7], weatherUnicodeCharacters[7] },
    { forecastIOIcons[8], weatherUnicodeCharacters[8] },
    { forecastIOIcons[9], weatherUnicodeCharacters[9] },
    { forecastIOIcons[10], weatherUnicodeCharacters[10] },
    { forecastIOIcons[11], weatherUnicodeCharacters[11] },
    { forecastIOIcons[12], weatherUnicodeCharacters[12] },
    { forecastIOIcons[13], weatherUnicodeCharacters[13] },
};

std::string ForecastLookUpSource::convertIntToTime(int i) {
    std::string res;
    res += std::to_string((i%12 == 0)? 12 : (i%12));
    res += (i < 12? "AM":"PM");
    return res;
}

const std::vector<std::string> ForecastLookUpSource::specificDateShortenedTriggerWords[] = {
    {"sun"},
    {"mon"},
    {"tue", "tues"},
    {"wed", "wednes"},
    {"thu", "thurs"},
    {"fri"},
    {"sat"}
};

const std::vector<std::string> ForecastLookUpSource::specificDateTriggerWords[] = {
    {"sun"},
    {"mon"},
    {"tues"},
    {"wednes"},
    {"thurs"},
    {"fri"},
    {"satur", "katur"}
};

const std::vector<std::string> ForecastLookUpSource::todayTriggerWords = {
    "today"
};

const std::vector<std::string> ForecastLookUpSource::tomorrowTriggerWords = {
    "tomorrow",
    "tmr",
    "tmrw"
};

void setColors(std::pair<IRCColor, IRCColor>& colors, IRCColor first, IRCColor second) {
    colors.first = first;
    colors.second = second;
}

std::string ForecastLookUpSource::testIcons() {
    std::string res;

    for (int i = 0; i < NUM_FORECAST_ICONS; i++) {
        for (int j = 0; j < 3; j++) {
            res += getColorCodeFromIcon(forecastIOIcons[i], .19*(j+1));
            res += iconMap.at(forecastIOIcons[i]);
        }
    }
    return res;
}

std::string ForecastLookUpSource::getColorCodeFromIcon(std::string icon, double precipProbability=0) {
    std::string res;
    std::pair<IRCColor, IRCColor> colors;
    for (int i = 0; i < NUM_FORECAST_ICONS; i++) {
        if (icon.compare(forecastIOIcons[i]) == 0) {
            switch (i) {
                case 1:  //clear-day
                         //setColors(colors, black, yellow);
                        setColors(colors, orange, white);
                         break;
                case 2:  //clear-night
                         setColors(colors, black, light_grey);
                         break;
                case 3:  //rain
                case 11: //hail
                         if (precipProbability < .20)
                             setColors(colors, white, light_cyan);
                         else if (precipProbability < .50)
                             setColors(colors, white, light_blue);
                         else setColors(colors, white, blue);
                         break;
                case 4:  //snow
                         setColors(colors, white, light_blue);
                         break;
                case 5:  //sleet
                         setColors(colors, white, light_blue);
                         break;
                case 6:  //wind
                         setColors(colors, white, light_grey);
                         break;
                case 7:  //fog
                         setColors(colors, black, light_grey);
                         break;
                case 8:  //cloudy
                         setColors(colors, black, light_grey);
                         break;
                case 9:  //party-cloudy-day
                         //setColors(colors, black, yellow);
                         setColors(colors, orange, white);
                         break;
                case 10:  //partly-cloudy-night
                         setColors(colors, black, light_grey);
                         break;
                case 12: //thunderstorm
                         //setColors(colors, yellow, light_grey);
                         setColors(colors, orange, white);
                         break;
                case 13: //tornado
                         setColors(colors, grey, light_grey);
                         break;
                default:
                         setColors(colors, black, white);
            } //switch
        } //if we match with an icon
    }

    res = IRCColorCode + std::to_string(colors.first)+","+std::to_string(colors.second);
    return res;
}

std::string ForecastLookUpSource::createWeatherUnicodeString(std::vector<forecastInfo> info) {
    std::string res = "";
    std::vector<std::string> unicodeDailyForecastChars;
    std::vector<std::string> unicodeDailyForecastColors;
    for (unsigned int i = 0; i < info.size(); i++) {
        std::string icon = info[i].icon;
        if (iconMap.find(icon) != iconMap.end())
            unicodeDailyForecastChars.push_back(iconMap.at(icon));
        else
            unicodeDailyForecastChars.push_back(iconMap.at("unknown"));
        unicodeDailyForecastColors.push_back(getColorCodeFromIcon(info[i].icon, info[i].precipProbability));
    }
    for (unsigned int i = 0; i < unicodeDailyForecastChars.size() &&
                             i < unicodeDailyForecastColors.size(); i++) {
        res += unicodeDailyForecastColors[i]+unicodeDailyForecastChars[i];
    }
    return res;
}

std::string ForecastLookUpSource::createHumanReadableSummaryForCurrentForecast(forecastInfo info) {
    std::string res;
    res += info.summary + " at ";
    res += info.temperature + "°F";
    res += " for the next hour.";
    return res;
}

std::string ForecastLookUpSource::createHumanReadableSummaryForDailyForecast(std::string summary, std::vector<forecastInfo> info) {
    std::string res;
    int maxTemp = INT_MIN, maxTempTime = 0;
    for (unsigned int i = 0; i < info.size(); i++) {
        int currTemp = (int)std::nearbyint(std::stod(info[i].temperature));
        if (currTemp > maxTemp) { maxTemp = currTemp; maxTempTime = i; }
    }

    std::transform(summary.begin(), summary.end(), summary.begin(), ::tolower);
    summary[0] = std::toupper(summary[0]);
    if (summary[summary.length()-1] == '.') {
        summary.pop_back();
    }
    res += summary + " with temperatures reaching a high of ";
    res += std::to_string(maxTemp) + "°F at " + convertIntToTime(maxTempTime);
    res += ". ";
    return res;
}

bool ForecastLookUpSource::iterateVectorForTriggerWords(std::string& baseString,
                                                       const std::vector<std::string>& vec,
                                                       bool wordStandsAlone = true) const {

    //no need to match the word separately from the rest of the words
    if (wordStandsAlone == false) {
        for (auto& word : vec) {
            std::string::size_type pos = baseString.find(word);
            if (pos != std::string::npos) {
                baseString.erase(pos, word.length());
                return true;
            }
        }
        return false;
    }

    //otherwise, use regex to match the word
    std::regex regexPattern;
    for (auto& word : vec) {
        regexPattern = std::regex("(%20|^)("+word+")(%20|$)");
        if (std::regex_search(baseString, regexPattern)) {
            baseString = std::regex_replace(baseString, regexPattern, "%20");
            return true;
        }
    }
    return false;
}

dayOfWeek ForecastLookUpSource::getQueriedDayOfWeek(std::string& s) const {
    bool foundWord = false;
    for (int i = 0; i < 7; i++) {
        for (auto& word : specificDateTriggerWords[i]) {
            std::vector<std::string> fullWords = {word+"day", word+"dank"};
            foundWord = iterateVectorForTriggerWords(s, fullWords, false);
            if (foundWord)
                return (dayOfWeek)(i);
        }
        foundWord = iterateVectorForTriggerWords(s, specificDateShortenedTriggerWords[i], true);
        if (foundWord)
            return (dayOfWeek)(i);
    }
    return dayOfWeek::None;
}

struct tm* convertTime(int time) {
    time_t rawTime(time);
    struct tm* timeInfo = localtime(&rawTime);
    return timeInfo;
}

void ForecastLookUpSource::fillForecastInfo(Parser& p, forecastInfo& info, int dailyFlag) {
    std::string stime = p.getField("\"time\"",":", "\"");
    info.time = convertTime(std::stoi(stime));

    info.summary = p.getField("\"summary\"");
    info.icon = p.getField("\"icon\"");
    std::transform(info.summary.begin(), info.summary.end(), info.summary.begin(), ::tolower);
    info.summary[0] = std::toupper(info.summary[0]);

    info.precipProbability = std::stod(p.getField("\"precipProbability\"", ":", "\""));
    if (info.precipProbability >= 0.01) {
        info.precipType = p.getField("\"precipType\"");
    }
    if (dailyFlag == 0)
        info.temperature = p.getField("\"temperature\"", ":", ",");
    if (dailyFlag == 1) {
        info.temperatureMax = p.getField("\"temperatureMax\"", ":", ",");
    }
}

forecastInfo ForecastLookUpSource::getCurrentForecast(Parser& p) {
    std::string currentTag = "\"currently\"";
    p.setCurrPos(p.getString().find(currentTag));
    forecastInfo currentForecast;
    fillForecastInfo(p, currentForecast);
    return currentForecast;
}

std::string ForecastLookUpSource::getWeeklyForecastSummary(Parser& p) {
    std::string weeklyTag = "\"daily\"";
    p.setCurrPos(p.getString().find(weeklyTag));
    std::string summary = p.getField("\"summary\"");
    return summary;
}

std::vector<forecastInfo> ForecastLookUpSource::getHourlyForecast(Parser &p) {
    std::string hourlyTag = "\"hourly\"";
    p.setCurrPos(0);
    p.moveToTag(hourlyTag);
    std::vector<forecastInfo> hourlyForecast ;
    //fetch hourly until we pass the daily tag
    std::string dailyTag = "\"daily\"";
    std::string::size_type dailyTagPos = p.getString().find(dailyTag);
    for (int i = 0; i < 24 && p.getCurrPos() < dailyTagPos; i++) {
        forecastInfo info;
        fillForecastInfo(p, info);
        hourlyForecast.push_back(info);
    }
    return hourlyForecast;
}

std::vector<forecastInfo> ForecastLookUpSource::getDailyForecast(Parser& p) {
    std::string dailyTag = "\"daily\"";
    p.moveAfterTag(dailyTag);
    std::vector<forecastInfo> dailyForecast;
    for (int i = 0; i < 7; i++) {
        forecastInfo info;
        fillForecastInfo(p, info, true);
        dailyForecast.push_back(info);
    }
    return dailyForecast;
}

std::string ForecastLookUpSource::parseUserInputForAddress(std::string query) const {
    std::string res = query;
    const std::vector<std::string> unwantedWords = {
        "in",
        "at",
        "like",
        "the",
        "current",
        "weather",
        "on",

        "give me ",
        "gimme ",
        "what is ",
        "what's",
        "whats",
        "how is",
        "how's",
        "hows",
    };

    std::regex regexPattern;
    for (auto& word : unwantedWords) {
        regexPattern = std::regex("(%20|^)("+word+")(%20|$)");
        if (std::regex_search(res, regexPattern)) {
            res = std::regex_replace(res, regexPattern, "%20");
        }
    }
    return res;
}

bool ForecastLookUpSource::prepareToCreateRequestForQuery(const std::string& rawQuery) {
    //grab specific date first so we remove it from the query
    //before we pass it to geocoding for parsing
    m_wantSpecificDay = false;
    auto query = lowercaseString(rawQuery);

    getQueriedDayOfWeek(query);
    m_wantToday = iterateVectorForTriggerWords(query, todayTriggerWords, false);
    if (!m_wantToday)
        m_wantTomorrow = iterateVectorForTriggerWords(query, tomorrowTriggerWords, false);

    std::string address = parseUserInputForAddress(query);
    auto response = Geocoder::geocode(address);
    if (!response) {
        return false;
    }

    m_location = response->coordinate;
    m_formattedAddress = response->formattedAddress;
    return true;
}

URLRequest ForecastLookUpSource::requestForQuery(std::string s) const {
    dayOfWeek specificDay = getQueriedDayOfWeek(s);

    std::string url = "https://api.forecast.io/forecast/"+m_key+"/";
    url += std::to_string(m_location.latitude);
    url += ",";
    url += std::to_string(m_location.longitude);

    if (m_wantToday) {
        time_t t = time(0);
        url += ","+std::to_string((int)t);
        fillTmStruct(&m_forecastTm, t);
        m_wantSpecificDay = true;
    }
    else if (m_wantTomorrow) {
        time_t t = time(0);
        t += SECONDS_IN_ONE_DAY;
        url += ","+std::to_string((int)t);
        fillTmStruct(&m_forecastTm, t);
        m_wantSpecificDay = true;
    }
    else if (specificDay != dayOfWeek::None) {
        time_t t = getTimeStampForDayAtAddress(specificDay,
                                               m_location.latitude,
                                               m_location.longitude,
                                               &m_forecastTm);
        url += "," + std::to_string((int)t);
        m_wantSpecificDay = true;
    }

    return URLRequest(url, URLRequest::Method::GET, {
        {"exclude", "minutely,flags"},
    });
}

std::string ForecastLookUpSource::parseResults(std::string s) {
    if (m_location.latitude && m_location.longitude == 0) return "";
    std::string res;
    Parser p(s);
    forecastInfo currentForecast = getCurrentForecast(p);
    std::string weeklyForecastSummary = getWeeklyForecastSummary(p);
    if (!m_wantSpecificDay) {
        res += "Showing current weather in " + m_formattedAddress + ".\n";
        auto hourlyForecast = getHourlyForecast(p);
        res += createHumanReadableSummaryForCurrentForecast(currentForecast);
        res += "\n" + createWeatherUnicodeString(hourlyForecast);
        res += "\n" + weeklyForecastSummary;
    }
    else {
        p.setCurrPos(p.getString().find("\"daily\""));
        std::string summary = p.getField("\"summary\"");
        p.setCurrPos(0);
        auto hourlyForecast = getHourlyForecast(p);

        char buffer[80];
        strftime(buffer, 80, "%A, %b %d", m_forecastTm);
        std::string tmString(buffer);
        res += "Showing weather in "+m_formattedAddress;
        res += " for " + tmString + ".\n";
        res += createHumanReadableSummaryForDailyForecast(summary, hourlyForecast);
        res += "\n" + createWeatherUnicodeString(hourlyForecast);
    }
    m_isResultValid = 1;
    return res;
}

std::string ForecastLookUpSource::formatResults(std::string& results, int fieldFlags) {
    return results;
}


