#ifndef  __FORECAST_LOOK_UP_SOURCE_INC
#define  __FORECAST_LOOK_UP_SOURCE_INC

#include "LookUpSource.h"
#include "Parser.h"
#include "TimeZone.h"
#include "basics/Geocoder.h"
#include <string>
#include <utility>
#include <vector>

#define NUM_FORECAST_ICONS 14
#define IRCColorCode '\x03'
#define SECONDS_IN_ONE_DAY 86400

namespace samebot {

struct forecastInfo {
    struct tm* time;
    std::string summary;
    std::string icon;
    double precipProbability;
    std::string precipType;
    std::string temperature;
    std::string temperatureMax;
};

enum IRCColor{
    none=-1, white=0, black, blue, green, red, brown, purple, orange,
    yellow, light_green, cyan, light_cyan, light_blue, pink, grey, light_grey
};

class ForecastLookUpSource : public LookUpSource<std::string> {

public:

    ForecastLookUpSource(std::string key);
    std::string testIcons();
private:
    std::string m_key;
    std::string m_formattedAddress;
    Geocoder::Coordinate m_location = {};
    //we are looking for a specific date
    mutable bool m_wantSpecificDay = false;
    bool m_wantToday = false, m_wantTomorrow = false;
    mutable struct tm* m_forecastTm;

    virtual bool prepareToCreateRequestForQuery(const std::string&) override;
    virtual URLRequest requestForQuery(std::string) const override;
    std::string parseResults(std::string s) override;
    std::string formatResults(std::string& results, int fieldFlags = OpAllFields) override;

    std::string createHumanReadableSummaryForCurrentForecast(forecastInfo info);
    void fillForecastInfo(Parser& p, forecastInfo& info, int dailyFlag=0);
    forecastInfo getCurrentForecast(Parser& p);
    std::string getWeeklyForecastSummary(Parser& p);
    std::vector<forecastInfo> getHourlyForecast(Parser& p);
    std::vector<forecastInfo> getDailyForecast(Parser& p);

    std::string parseUserInputForAddress(std::string query) const;

    //functions and members for icons
    static const std::string weatherUnicodeCharacters[];
    static const std::string forecastIOIcons[];
    static const std::map<std::string, std::string> iconMap;
    std::string getColorCodeFromIcon(std::string, double);
    std::string createWeatherUnicodeString(std::vector<forecastInfo>);
    std::string createHumanReadableSummaryForDailyForecast(std::string, std::vector<forecastInfo>);


    std::string convertIntToTime(int);
    static const std::vector<std::string> specificDateShortenedTriggerWords[];
    static const std::vector<std::string> specificDateTriggerWords[];
    static const std::vector<std::string> todayTriggerWords;
    static const std::vector<std::string> tomorrowTriggerWords;
    bool iterateVectorForTriggerWords(std::string&, const std::vector<std::string>&, bool) const;
    dayOfWeek getQueriedDayOfWeek(std::string& s) const;

};

} // namespace
#endif   /* ----- #ifndef __FORECAST_LOOK_UP_SOURCE_INC  ----- */
