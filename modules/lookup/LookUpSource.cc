#include "LookUpSource.h"

#include <cstdlib>
#include <locale.h>
#include <string>
#include <vector>

using namespace samebot;

#define MAX_UNICODE_BYTES 4

std::string replaceEscapeSequences(std::string s) {
	unsigned int i = 0;

	//if there is no space before the \r or \n, and no space after, we should insert a space to improve readability
	//take care in case there are multiple newlines (\r\n or \n\n\n)
	bool spaceBefore = 0;
	bool deletedNewLine = 0;

	while (i+1 < s.length()) {

		if (s[i] != '\\') {
			if (deletedNewLine && spaceBefore == 0 && s[i] != ' ') {
				s.insert(i, " ");
			}

			if (s[i] == ' ') {
				spaceBefore = 1;
			}
			else {
				spaceBefore = 0;
			}
			deletedNewLine = 0;
			i++;
			continue;
		}
		switch (s[i+1]) {

			case 'n':
			case 'r':
				deletedNewLine = 1;
				s.erase(i, 2);
				break;

			case '\\':
			case '\"':
			case '\'':
			case '\?':
				deletedNewLine = 0;
				s.erase(i, 1);
				break;

			default:
				deletedNewLine = 0;
				i++;
				break;
		}
	}
	return s;
}

std::string replaceUnicode(std::string s) {
	setlocale(LC_ALL, "en_US.utf8");

	auto currPos = s.find("\\u");
    while (currPos != std::string::npos) {
		bool isCodeValid = 1;
		std::string unicodeString;
		char* code = (char*)calloc(4, sizeof(char));

		for (int i = 2; i < 6; i++) {
			if (currPos + i >= s.length()) {
				free(code);
				return s;
			}
			if (!isalnum(s[currPos+i])) {
				currPos = currPos + i;
				isCodeValid = 0;
				break;
			}
			code[i-2] = s[currPos+i];
		}

		if (isCodeValid) {
			wchar_t wideChar = static_cast<wchar_t>(strtol(code, NULL, 16));
			char multiByteChar[MAX_UNICODE_BYTES];
			int len = wctomb(multiByteChar, wideChar);
			for (int j = 0; j < len; j++) {
				unicodeString.push_back(multiByteChar[j]);
			}
			s.replace(currPos, 6, unicodeString);
		}

		currPos = static_cast<int>(s.find("\\u", currPos));
		free(code);
	}

	return s;
}

std::string convertToHexCharacters(std::string s) {
	char const hex_chars[16] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

	std::string hexForm;
	for (unsigned int i = 0; i < s.length(); i++) {
		char c = s[i];
		hexForm.push_back(hex_chars[(c & 0xF0) >> 4]);
		hexForm.push_back(hex_chars[(c & 0x0F) >> 0]);
	}
	return hexForm;
}

