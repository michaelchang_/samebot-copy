#include "network/URLRequest.h"
#include "network/URLSession.h"
#include "TimeZone.h"
#include "GoogleMapsAPI.h"
#include <string>
#include "Parser.h"
#include <time.h>

using namespace samebot;

std::string timeZoneAPICall(double latitude, double longitude) {
    //get current timestamp
    time_t currTime = time(0);

    std::string url = "https://maps.googleapis.com/maps/api/timezone/json?location=";
    url += std::to_string(latitude);
    url += ",";
    url += std::to_string(longitude);
    url += "&timestamp="+std::to_string((int)currTime);

    URLRequest request(url);
    std::string response = URLSession::sendSynchronousRequest(request);

    return response;
}

int getTimeOffset(double latitude, double longitude) {
    std::string timeZoneAPIResponse = timeZoneAPICall(latitude, longitude);
    Parser p(timeZoneAPIResponse);
    if (getStatusCode(p) != GoogleMapsAPIStatusCodes::OK) {
        return 0;
    }
    p.setCurrPos(0);
    std::string dstOffset = p.getField("\"dstOffset\"", ":", ",");
    std::string rawOffset = p.getField("\"rawOffset\"", ":", ",");
    return std::stoi(dstOffset)+std::stoi(rawOffset);
}

time_t getTimeStampForDayAtAddress(dayOfWeek queriedDay, double latitude, double longitude, struct tm** forecastDate) {
    const int secondsForOneDay = 86400;

    time_t currTime = time(0);

    int timeOffset = getTimeOffset(latitude, longitude);
    currTime += timeOffset;
    struct tm* tm = gmtime(&currTime);
    dayOfWeek currentDayOfWeekAtAddress = (dayOfWeek)tm->tm_wday;
    while (currentDayOfWeekAtAddress != queriedDay) {
        currentDayOfWeekAtAddress = dayOfWeek((currentDayOfWeekAtAddress+1)%7);
        currTime += secondsForOneDay;
    }
    fillTmStruct(forecastDate, currTime);
    currTime -= timeOffset;
    return currTime;
}

void fillTmStruct(struct tm** tm, time_t time) {
    *tm = gmtime(&time);
}
