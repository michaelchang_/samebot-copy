#ifndef __MERRIAM_WEBSTER_LOOK_UP_SOURCE_H
#define __MERRIAM_WEBSTER_LOOK_UP_SOURCE_H

#include "LookUpSource.h"
#include <vector>

namespace samebot {

class MerriamWebsterLookUpSource : public LookUpSource<std::vector<std::string>> {
public:
    MerriamWebsterLookUpSource(std::string key);
    bool hasSuggestion() { return m_hasSuggestion; }
    std::string getSuggestion() { return m_suggestion; }

private:
    std::string m_key;

    std::string m_suggestion = "";
    bool m_hasSuggestion = false;
    bool grabTopSuggestion(std::string s);

    virtual URLRequest requestForQuery(std::string) const override;
    std::vector<std::string> parseResults(std::string s) override;
    std::string formatResults(std::vector<std::string>& results, int fieldFlags = OpAllFields) override;

};

} // namespace

#endif // __MERRIAM_WEBSTER_LOOK_UP_SOURCE_H
