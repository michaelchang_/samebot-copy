#include "GoogleLookUpSource.h"

#include <string>
#include <string.h>

using namespace samebot;

#define TITLE              "\"title\":\""
#define LINK               "\"link\":\""
#define SNIPPET            "\"snippet\":\""
#define SNIPPETEND         "\"},{"
#define FILEEND            "\"}]}"


URLRequest GoogleLookUpSource::requestForQuery(std::string query) const {
    return URLRequest("https://www.googleapis.com/customsearch/v1", URLRequest::Method::GET, {
        { "key", m_key },
        { "cx", m_id },
        { "q", query },
        { "fields", "items(title,snippet,link)" },
        { "prettyPrint", "false" },
    });
}

std::vector<GoogleAPIResult> GoogleLookUpSource::parseResults(std::string s) {

    if (s.find(TITLE) == std::string::npos) {
        m_isResultValid=0;
        return m_results;
    }

    unsigned long currPos = 0;
    for (int i = 0; s.find(TITLE, currPos) != std::string::npos && i < 10; i++) {
        GoogleAPIResult r;
        std::string rawSnippet;

        auto titleLocation      = s.find(TITLE, currPos);
        auto linkLocation       = s.find(LINK, titleLocation);
        auto snippetLocation    = s.find(SNIPPET, linkLocation);
        unsigned long snippetEndLocation;

        if (i == 9) {
            snippetEndLocation = s.find(FILEEND, snippetLocation);
        }
        else {
            snippetEndLocation = s.find(SNIPPETEND, snippetLocation);
        }

        r.title = s.substr(titleLocation+strlen(TITLE), linkLocation-titleLocation-strlen(LINK)-3);
        r.link = s.substr(linkLocation+strlen(LINK), snippetLocation-linkLocation-strlen(SNIPPET)+1);
        r.snippet = s.substr(snippetLocation+strlen(SNIPPET), snippetEndLocation-snippetLocation-strlen(SNIPPETEND)-7);

        r.title = replaceEscapeSequences(replaceUnicode(r.title));
        r.snippet = replaceEscapeSequences(replaceUnicode(r.snippet));

        currPos = snippetEndLocation - strlen(SNIPPETEND);

        m_results.push_back(r);
    }

    if (m_results.size() > 0) m_isResultValid=1;
    return m_results;
}

std::string GoogleLookUpSource::formatResults(std::vector<GoogleAPIResult>& results, int fieldFlags) {
    if (results.size() == 0) return "";

    std::string s;
    if (fieldFlags & OpAllFields || fieldFlags & OpNameField) {
        s.append(results[0].title);
    }
    if (fieldFlags & OpAllFields || fieldFlags & OpLinkField) {
        if (!s.empty()) s.append("\n");
        s.append(results[0].link);
    }
    if (fieldFlags & OpAllFields || fieldFlags & OpExtractField) {
        if (!s.empty()) s.append("\n");
        s.append(results[0].snippet);
    }
    return s;
}
