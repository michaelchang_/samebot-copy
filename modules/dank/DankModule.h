#ifndef __DANK_MODULE_H
#define __DANK_MODULE_H

#include "modules/Module.h"

namespace samebot {

class DankModule : public Module {
public:
    virtual std::string moduleIdentifier() const override { return "DankModule"; }
#ifdef DEBUG
    bool test_shouldRespondToMessage(std::string message) { return shouldRespondToMessage(message); }
#endif
    virtual double getActiveScore(std::shared_ptr<IRCMessage> message) override { return -1; }
private:
    bool shouldRespondToMessage(std::string);
    virtual bool wantsPrivateMessages() const override { return true; }
    virtual void didReceiveMessage(std::shared_ptr<IRCMessage>) override;
};

} // namespace

#endif // __DANK_MODULE_H
