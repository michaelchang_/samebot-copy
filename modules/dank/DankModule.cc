#include "DankModule.h"

#include "basics/StringUtilities.h"
#include "network/irc/IRCMessage.h"
#include <algorithm>

using namespace samebot;

static const std::string dankString = "dank";

bool DankModule::shouldRespondToMessage(std::string message) {
    message = stringByCompressingIntrawordSpacesInString(message);
    if (message.find("dank") != std::string::npos) {
        return true;
    }

    return findFirstFuzzyWordMatch(message, "dank") != std::string::npos;
}

void DankModule::didReceiveMessage(std::shared_ptr<IRCMessage> message) {
    std::vector<std::string> responses {
        "That's Dank!",
        "very dank",
        "so dank",
        "très danque",
        "pretty dank",
        "dank",
        "dankkkk",
        "d a n k",
        "dank af",
        "2dank4me",
        "now that's dank",
        "now that's what i call dank",
        "dankdankdank",
    };

    if (shouldRespondToMessage(message->parameters().back())) {
        std::this_thread::sleep_for(std::chrono::seconds(2 + rand() % 3));
        replyToMessage(*message, responses[rand() % responses.size()]);
        if (rand() % 2) {
            std::this_thread::sleep_for(std::chrono::seconds(1 + rand() % 3));
            replyToMessage(*message, "tbh");
        }
    }
}
