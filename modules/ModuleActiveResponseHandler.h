#ifndef __MODULE_ACTIVE_RESPONSE_HANDLER_H
#define __MODULE_ACTIVE_RESPONSE_HANDLER_H

#include "modules/Module.h"
#include <vector>

namespace samebot {

class ModuleActiveResponseHandler {
public:
    ModuleActiveResponseHandler() {}

    std::vector<int> moduleScoring(std::shared_ptr<IRCMessage> message,
                                   std::vector<std::unique_ptr<Module>>& modules);

    static std::string removeSamebotPrefix(std::string& body);
    static bool hasSamebotPrefix(std::string& body);

}; // class ModuleHandler

} // namespace samebot

#endif // __MODULE_ACTIVE_RESPONSE_HANDLER_H
