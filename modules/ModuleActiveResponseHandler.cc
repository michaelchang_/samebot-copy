#include "ModuleActiveResponseHandler.h"

#include "basics/Assertions.h"
#include "basics/StringUtilities.h"

#include <vector>
#include <string>

using namespace samebot;

std::vector<int> ModuleActiveResponseHandler::moduleScoring(std::shared_ptr<IRCMessage> message,
                                                            std::vector<std::unique_ptr<Module>>& modules) {

    double maxScore = 0;
    int maxScoreIndex = -1;
    std::vector<int> res;
    for (int i = 0; i < (int)modules.size(); i++) {
        double score = modules[i]->getActiveScore(message);
        if (score > maxScore) {
            score = maxScore;
            maxScoreIndex = i;
        }
        else if (score == -1) res.push_back(i);
    }
    if (maxScoreIndex != -1)
        res.push_back(maxScoreIndex);
    return res;
}

bool ModuleActiveResponseHandler::hasSamebotPrefix(std::string& body) {
    auto colonPos = body.find(':');
    if (colonPos != std::string::npos && isFuzzyMatch("samebot", body.substr(0, colonPos))) {
        return true;
    }
    return false;
}

std::string ModuleActiveResponseHandler::removeSamebotPrefix(std::string& body) {
    auto colonPos = body.find(':');
    if (colonPos != std::string::npos && isFuzzyMatch("samebot", body.substr(0, colonPos))) {
        body.erase(0, colonPos+1);
    }
    return body;
}
