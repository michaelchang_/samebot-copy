#ifndef __SQLITE_DATABASE_H
#define __SQLITE_DATABASE_H

#include "SQLiteResultRow.h"
#include "basics/ScopeExitHandler.h"
#include <chrono>
#include <iomanip>
#include <memory>
#include <sqlite3.h>
#include <sstream>
#include <string>

inline bool bind(sqlite3_stmt*, int) {
    // Base case. Do nothing.
    return true;
}

template <typename... Args>
bool bind(sqlite3_stmt* statement, int index, int argument, Args... args) {
    if (sqlite3_bind_int(statement, index, argument) != SQLITE_OK) {
        return false;
    }
    return bind(statement, index + 1, args...);
}

template <typename... Args>
bool bind(sqlite3_stmt* statement, int index, double argument, Args... args) {
    if (sqlite3_bind_double(statement, index, argument) != SQLITE_OK) {
        return false;
    }
    return bind(statement, index + 1, args...);
}

template <typename... Args>
bool bind(sqlite3_stmt* statement, int index, std::string argument, Args... args) {
    return bind(statement, index, argument.c_str(), args...);
}

template <typename... Args>
bool bind(sqlite3_stmt* statement, int index, const char* argument, Args... args) {
    if (sqlite3_bind_text(statement, index, argument, -1, SQLITE_TRANSIENT) != SQLITE_OK) {
        return false;
    }
    return bind(statement, index + 1, args...);
}

template <typename... Args>
bool bind(sqlite3_stmt* statement, int index, std::chrono::system_clock::time_point argument, Args... args) {
    std::time_t time = std::chrono::system_clock::to_time_t(argument);
    std::stringstream stream;
    stream << std::put_time(std::gmtime(&time), "%Y-%m-%dT%H:%M:%SZ");
    return bind(statement, index, stream.str(), args...);
}

namespace samebot {

class SQLiteDatabase {
public:
    SQLiteDatabase(std::string path);
    ~SQLiteDatabase();

    const std::string& path() const { return m_path; }

    void destroyData();

    bool hasError() const;
    std::string errorMessage() const;

    template <typename... Args>
    bool executeSQL(std::string format, Args... args) {
        if (auto statement = prepareStatement(format, args...)) {
            return sqlite3_step(statement.get()) == SQLITE_DONE;
        }
        return false;
    }

    typedef std::function<void (const SQLiteResultRow&)> SQLiteResultRowHandler;
    typedef std::function<bool (SQLiteResultRowHandler)> SQLiteResultRowEnumerator;
    template <typename... Args>
    std::unique_ptr<SQLiteResultRowEnumerator> rowEnumeratorForSQLQuery(std::string format, Args... args) const {
        if (auto statement = prepareStatement(format, args...)) {
            return std::make_unique<SQLiteResultRowEnumerator>([statement](SQLiteResultRowHandler rowHandler) {
                SQLiteResultRow row(statement.get());
                int result;
                while ((result = sqlite3_step(statement.get())) == SQLITE_ROW) {
                    rowHandler(row);
                }
                return result == SQLITE_DONE;
            });
        }
        return nullptr;
    }

    bool executeSQLQuery(std::string query, SQLiteResultRowHandler rowHandler) const {
        if (auto enumerator = rowEnumeratorForSQLQuery(query)) {
            (*enumerator)(rowHandler);
            return true;
        }
        return false;
    }

    template <typename T, typename... Args>
    bool executeSQLQuery(T* outT, std::string format, Args... args) const {
        ASSERT(outT);
        if (auto enumerator = rowEnumeratorForSQLQuery(format, args...)) {
            (*enumerator)([outT](const SQLiteResultRow& row) {
                *outT = row.get<T>(0);
            });
            return true;
        }
        return false;
    }

    template <typename... Args>
    bool executeSQLQuery(SQLiteResultRowHandler rowHandler, std::string query, Args... args) const {
        if (auto enumerator = rowEnumeratorForSQLQuery(query, args...)) {
            (*enumerator)(rowHandler);
            return true;
        }
        return false;
    }

private:
    void open();
    void close();

    template <typename... Args>
    std::shared_ptr<sqlite3_stmt> prepareStatement(std::string format, Args... args) const {
        sqlite3_stmt* statement;
        int result = sqlite3_prepare_v2(m_handle, format.c_str(), -1, &statement, nullptr);
        if (result != SQLITE_OK) {
            return nullptr;
        }

        if (!bind(statement, 1, args...)) {
            sqlite3_finalize(statement);
            return nullptr;
        }

        return std::shared_ptr<sqlite3_stmt>(statement, sqlite3_finalize);
    }

    sqlite3* m_handle = nullptr;
    std::string m_path;
};

} // namespace

#endif // __SQLITE_DATABASE_H
