#include "SQLiteDataStore.h"

#include "basics/Assertions.h"

using namespace samebot;

SQLiteDataStore::SQLiteDataStore(const std::string& path)
    : m_database(path)
{
}

SQLiteDatabase& SQLiteDataStore::database() {
#ifdef DEBUG
    if (m_debug_isInCreateSchema) {
        // Did you call this method from createSchema()? That's illegal.
        ASSERT_NOT_REACHED();
    }
#endif

    return m_database;
}

const SQLiteDatabase& SQLiteDataStore::database() const {
#ifdef DEBUG
    if (m_debug_isInCreateSchema) {
        // Did you call this method from createSchema()? That's illegal.
        ASSERT_NOT_REACHED();
    }
#endif

    return m_database;
}

void SQLiteDataStore::destroyDatabase() {
#ifdef DEBUG
    if (m_debug_isInCreateSchema) {
        // Did you call this method from createSchema()? That's illegal.
        ASSERT_NOT_REACHED();
    }
#endif

    m_database.destroyData();
    m_databaseReady = false;
}

bool SQLiteDataStore::setUpSchemaIfNeeded() {
#ifdef DEBUG
    if (m_debug_isInCreateSchema) {
        // Did you call this method from createSchema()? That's illegal.
        ASSERT_NOT_REACHED();
    }
#endif

    if (m_databaseReady) {
        return true;
    }

    if (m_database.hasError()) {
        LOG_ERROR("Cannot access database at %s: %s", m_database.path().c_str(), m_database.errorMessage().c_str());
        return false;
    }

    int version;
    if (m_database.executeSQLQuery(&version, "PRAGMA user_version") && version == currentSchemaVersion()) {
        m_databaseReady = true;
        return true;
    }

    // Database is either empty or corrupt. Throw it out regardless.
    // FIXME: Implement schema migrations.
    m_database.destroyData();

#ifdef DEBUG
    m_debug_isInCreateSchema = true;
#endif
    if (!createSchema(m_database)) {
        LOG_ERROR("Could not create schema for database at %s", m_database.path().c_str());
#ifdef DEBUG
        m_debug_isInCreateSchema = false;
#endif
        return false;
    }
#ifdef DEBUG
    m_debug_isInCreateSchema = false;
#endif

    // Note that we intentionally do string concatenation instead of using a prepared statement as pragma statements do not
    // support positional parameters.
    if (!m_database.executeSQL("PRAGMA user_version = " + std::to_string(currentSchemaVersion()))) {
        return false;
    }

    m_databaseReady = true;
    return true;
}
