#ifndef __SQLITE_RESULT_ROW_H
#define __SQLITE_RESULT_ROW_H

#include "basics/Assertions.h"
#include <sqlite3.h>
#include <string>

namespace samebot {

class SQLiteResultRow {
public:
    SQLiteResultRow(sqlite3_stmt*);

    // Note that this is specialized for supported column types and not implemented for others, so trying to use an unsupported
    // type will result in a linker error.
    template <typename T>
    T get(int column) const;

private:
    sqlite3_stmt* m_statement;
};

} // namespace

#endif // __SQLITE_RESULT_ROW_H
