#include "SQLiteDatabase.h"

using namespace samebot;

SQLiteDatabase::SQLiteDatabase(std::string path)
    : m_path(path)
{
    open();
}

SQLiteDatabase::~SQLiteDatabase() {
    close();
}

void SQLiteDatabase::destroyData() {
    close();
    remove(m_path.c_str());
    open();
}

bool SQLiteDatabase::hasError() const {
    if (!m_handle) {
        return true;
    }
    return sqlite3_errcode(m_handle) != SQLITE_OK;
}

std::string SQLiteDatabase::errorMessage() const {
    if (!m_handle) {
        return "Could not create database connection";
    }
    return sqlite3_errmsg(m_handle);
}

#pragma mark - Internals

void SQLiteDatabase::open() {
    close();
    sqlite3_open(m_path.c_str(), &m_handle);
}

void SQLiteDatabase::close() {
    if (m_handle) {
        sqlite3_close(m_handle);
    }
}

