#ifndef __SQLITE_DATA_STORE_H
#define __SQLITE_DATA_STORE_H

#include "SQLiteDatabase.h"

namespace samebot {

class SQLiteDataStore {
public:
    SQLiteDataStore(const std::string& path);

protected:
    SQLiteDatabase& database();
    const SQLiteDatabase& database() const;
    bool setUpSchemaIfNeeded();
    void destroyDatabase();

    virtual int currentSchemaVersion() const = 0;

    /// Subclasses must use the provided database to create the schema. They
    /// should not attempt to access the database through the usual getters,
    /// as the database is not fully initialized at this point.
    virtual bool createSchema(SQLiteDatabase&) = 0;

private:
#ifdef DEBUG
    bool m_debug_isInCreateSchema = false;
#endif
    bool m_databaseReady = false;
    SQLiteDatabase m_database;
};

} // namespace

#endif // __SQLITE_DATA_STORE_H
