#include "SQLiteResultRow.h"

#include <chrono>
#include <iomanip>
#include <sstream>

using namespace samebot;

SQLiteResultRow::SQLiteResultRow(sqlite3_stmt* statement)
    : m_statement(statement)
{
}

namespace samebot {

template <>
int SQLiteResultRow::get(int column) const {
    return sqlite3_column_int(m_statement, column);
}

template <>
std::string SQLiteResultRow::get(int column) const {
    return std::string((const char*)sqlite3_column_text(m_statement, column));
}

template <>
std::chrono::system_clock::time_point SQLiteResultRow::get(int column) const {
    std::istringstream stream(get<std::string>(column));
    std::tm time;
    stream >> std::get_time(&time, "%Y-%m-%dT%H:%M:%SZ");
    if (stream.fail()) {
        LOG_ERROR("Unable to parse column '%s' (column: %d) into time", stream.str().c_str(), column);
        return std::chrono::system_clock::time_point();
    }
    return std::chrono::system_clock::from_time_t(timegm(&time));
}

} // namespace
