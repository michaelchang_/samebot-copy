#include "TCPSocket.h"

#include "basics/Assertions.h"
#include <cstring>
#include <fcntl.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

using namespace samebot;

#pragma mark - API

bool TCPSocket::connect() {
    ASSERT(!isConnected());

    if (!setUp()) {
        return false;
    }

    hostent* host = gethostbyname(this->host().c_str());
    if (!host) {
        LOG_ERROR("Unable to resolve host %s", this->host().c_str());
        return false;
    }

    sockaddr_in address;
    address.sin_family = AF_INET;
    address.sin_port = htons(port());
    address.sin_addr = *(in_addr*)host->h_addr;
    memset(&address.sin_zero, 0, 8);

    if (::connect(m_socketDescriptor, (sockaddr*)&address, sizeof(address)) == -1) {
        LOG_ERROR("Unable to connect to %s:%d", this->host().c_str(), port());
        tearDown();
        return false;
    }

    m_connected = true;
    LOG_DEBUG("Connected to %s:%d", this->host().c_str(), port());
    return true;
}

void TCPSocket::disconnect() {
    ASSERT(isConnected());

    tearDown();
    m_connected = false;
}

std::string TCPSocket::readUntilDelimiter(char delimiter) {
    auto response = Socket::readUntilDelimiter(delimiter);
    if (!response.length()) {
        // An empty response from the server indicates that the connection was broken.
        disconnect();
    }

    return response;
}

#pragma mark - Internals

void TCPSocket::socketSend(std::string string) {
    ::send(m_socketDescriptor, string.c_str(), string.length(), 0);
}

std::string TCPSocket::socketRecv() {
    const int bufferSize = 1024;
    char buffer[bufferSize];
    memset(buffer, 0, bufferSize);

    auto bytes = recv(m_socketDescriptor, buffer, bufferSize - 1, 0);
    if (bytes > 0) {
        return std::string(buffer);
    }

    return "";
}

bool TCPSocket::setUp() {
    ASSERT(!m_socketDescriptor);

    m_socketDescriptor = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (m_socketDescriptor == -1) {
        LOG_ERROR("Unable to create socket.");
        return false;
    }

    int enabled = 1;
    if (setsockopt(m_socketDescriptor, SOL_SOCKET, SO_REUSEADDR, (const char*)&enabled, sizeof(enabled)) == -1) {
        LOG_ERROR("Unable to set socket options.");
        return false;
    }

    fcntl(m_socketDescriptor, F_SETFL, O_NONBLOCK);
    fcntl(m_socketDescriptor, F_SETFL, O_ASYNC);
    return true;
}

void TCPSocket::tearDown() {
    ASSERT(m_socketDescriptor);

    close(m_socketDescriptor);
    m_socketDescriptor = 0;
}

