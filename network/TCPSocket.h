#ifndef __TCP_SOCKET_H
#define __TCP_SOCKET_H

#include "network/Socket.h"

namespace samebot {

class TCPSocket : public Socket {
public:
    using Socket::Socket;

    virtual bool connect() override;
    virtual void disconnect() override;
    virtual bool isConnected() const override { return m_connected; }

    virtual std::string readUntilDelimiter(char delimiter) override;

protected:
    virtual void socketSend(std::string) override;
    virtual std::string socketRecv() override;

private:
    bool setUp();
    void tearDown();

    int m_socketDescriptor = 0;
    bool m_connected = false;
};

} // namespace

#endif // __TCP_SOCKET_H

