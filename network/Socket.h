#ifndef __SOCKET_H
#define __SOCKET_H

#include <string>

namespace samebot {

class Socket {
public:
    Socket(std::string host, int port);

    const std::string& host() const { return m_host; }
    int port() const { return m_port; }

    virtual bool connect() { return false; }
    virtual void disconnect() {}
    virtual bool isConnected() const { return false; }

    void write(std::string);
    virtual std::string readUntilDelimiter(char delimiter);

protected:
    virtual void socketSend(std::string) = 0;
    virtual std::string socketRecv() = 0;

private:
    std::string m_host;
    int m_port;
    std::string m_buffer;
};

} // namespace

#endif // __SOCKET_H

