#ifndef __URL_SESSION_H
#define __URL_SESSION_H

#include <curl/curl.h>
#include <string>

namespace samebot {

class URLRequest;

class URLSession {
public:
    // Must be called only once, before any networking is performed.
    static void initialize();
    // Must be called only once, only if initialize() was called.
    static void deinitialize();

    // Returns an empty string on failure.
    static std::string sendSynchronousRequest(const URLRequest&);
    static std::string httpGet(std::string url);
};

} // namespace

#endif // __URL_SESSION_H

