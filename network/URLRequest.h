#ifndef __URL_REQUEST_H
#define __URL_REQUEST_H

#include <functional>
#include <map>
#include <string>
#include <vector>

namespace samebot {

class URLRequest {
public:
    enum class Method {
        GET,
        POST,
    };

    URLRequest(std::string url, Method, std::map<std::string, std::string> parameters);
    URLRequest(std::string url);

    std::string url() const { return m_url; }
    Method method() const { return m_method; }
    const std::map<std::string, std::string> parameters() const { return m_parameters; }
    std::string queryString() const;
    std::string postData() const;

    void addHeader(std::string header, std::string value);
    void enumerateHeaders(std::function<void (const std::string& header, const std::string& value)>) const;

    bool formatsPOSTDataAsJSON() const { return m_formatsPOSTDataAsJSON; }
    void setFormatsPOSTDataAsJSON(bool formatsPOSTDataAsJSON) { m_formatsPOSTDataAsJSON = formatsPOSTDataAsJSON; }

    std::string usernameForBasicAuthentication() const { return m_usernameForBasicAuthentication; }
    std::string passwordForBasicAuthentication() const { return m_passwordForBasicAuthentication; }
    void setUsernameForBasicAuthentication(std::string username) { m_usernameForBasicAuthentication = username; }
    void setPasswordForBasicAuthentication(std::string password) { m_passwordForBasicAuthentication = password; }

private:
    std::string m_url;
    Method m_method;
    std::map<std::string, std::string> m_parameters;

    std::vector<std::string> m_headerNames;
    std::map<std::string, std::string> m_headerNamesToValues;

    std::string m_usernameForBasicAuthentication;
    std::string m_passwordForBasicAuthentication;

    bool m_formatsPOSTDataAsJSON = false;
};

} // namespace

#endif // __URL_REQUEST_H

