#include "URLSession.h"

#include "URLRequest.h"
#include "basics/Assertions.h"
#include "basics/ScopeExitHandler.h"

using namespace samebot;

void URLSession::initialize() {
#ifdef DEBUG
    // Assert that this method is only called once.
    static bool initialized = false;
    ASSERT(!initialized);
    initialized = true;
#endif

    curl_global_init(CURL_GLOBAL_DEFAULT);
}

void URLSession::deinitialize() {
#ifdef DEBUG
    // Assert that this method is only called once.
    static bool initialized = false;
    ASSERT(!initialized);
    initialized = true;
#endif

    curl_global_cleanup();
}

size_t writeFunction(char* data, size_t length, size_t bytesPerCharacter, std::string* string) {
    string->append(data, bytesPerCharacter * length);
    return bytesPerCharacter * length;
}

std::string URLSession::sendSynchronousRequest(const URLRequest& request) {
    CURL* handle = curl_easy_init();
    if (!handle) {
        return "";
    }

    curl_slist* headers = nullptr;
    ScopeExitHandler handler([handle, &headers]{
        if (headers) {
            curl_slist_free_all(headers);
        }

        curl_easy_cleanup(handle);
    });

    request.enumerateHeaders([&headers](const std::string& header, const std::string& value) {
        auto formattedHeader = header + ": " + value;
        headers = curl_slist_append(headers, formattedHeader.c_str());
    });

    if (headers) {
        curl_easy_setopt(handle, CURLOPT_HTTPHEADER, headers);
    }

    if (request.usernameForBasicAuthentication().length()) {
        curl_easy_setopt(handle, CURLOPT_USERNAME, request.usernameForBasicAuthentication().c_str());
    }

    if (request.passwordForBasicAuthentication().length()) {
        curl_easy_setopt(handle, CURLOPT_PASSWORD, request.passwordForBasicAuthentication().c_str());
    }

    std::string response;
    curl_easy_setopt(handle, CURLOPT_WRITEDATA, &response);
    curl_easy_setopt(handle, CURLOPT_WRITEFUNCTION, writeFunction);

    curl_easy_setopt(handle, CURLOPT_USERAGENT, "samebot");
    curl_easy_setopt(handle, CURLOPT_VERBOSE, 1);

    std::string url = request.url();
    std::string postData;
    switch (request.method()) {
        case URLRequest::Method::GET: {
            if (request.parameters().size()) {
                url += "?";
            }

            size_t i = 0;
            for (const auto& pair : request.parameters()) {
                char* escapedValue = curl_easy_escape(handle, pair.second.c_str(), static_cast<int>(pair.second.length()));
                url += pair.first + "=" + escapedValue;
                curl_free(escapedValue);
                if (++i < request.parameters().size()) {
                    url += "&";
                }
            }
            break;
        }

        case URLRequest::Method::POST: {
            postData = request.postData();
            curl_easy_setopt(handle, CURLOPT_POSTFIELDS, postData.c_str());
            break;
        }
    }

    curl_easy_setopt(handle, CURLOPT_URL, url.c_str());

    if (curl_easy_perform(handle) == CURLE_OK) {
        return response;
    }

    return "";
}

std::string URLSession::httpGet(std::string url) {
    URLRequest request(url);
    return sendSynchronousRequest(request);
}

