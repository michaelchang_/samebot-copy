#include "URLRequest.h"

#include "basics/Assertions.h"
#include "basics/json.h"

using namespace samebot;

URLRequest::URLRequest(std::string url, Method method, std::map<std::string, std::string> parameters)
    : m_url(url)
    , m_method(method)
    , m_parameters(parameters)
{
}

URLRequest::URLRequest(std::string url)
    : m_url(url)
    , m_method(Method::GET)
{
}

std::string URLRequest::queryString() const {
    std::string queryString;
    for (const auto& pair : m_parameters) {
        ASSERT(pair.first.length());
        ASSERT(pair.second.length());
        queryString += pair.first + "=" + pair.second + "&";
    }

    if (!queryString.length()) {
        return "";
    }

    return queryString.substr(0, queryString.length() - 1);
}

std::string URLRequest::postData() const {
    ASSERT(m_method == Method::POST);

    if (!m_formatsPOSTDataAsJSON) {
        return queryString();
    }

    nlohmann::json data;
    for (const auto& pair : m_parameters) {
        ASSERT(pair.first.length());
        ASSERT(pair.second.length());
        data[pair.first] = pair.second;
    }

    return data.dump();
}

void URLRequest::addHeader(std::string header, std::string value) {
    // If the header already exists, remove the old one in the header name list.
    // The value will get overwritten, and the name will be reinserted at the end.
    if (m_headerNamesToValues.find(header) != m_headerNamesToValues.end()) {
        auto iterator = std::find(m_headerNames.begin(), m_headerNames.end(), header);
        ASSERT(iterator != m_headerNames.end());
        m_headerNames.erase(iterator);
    }

    m_headerNames.push_back(header);
    m_headerNamesToValues[header] = value;
}

void URLRequest::enumerateHeaders(std::function<void (const std::string& header, const std::string& value)> function) const {
    for (const auto& name : m_headerNames) {
        function(name, m_headerNamesToValues.at(name));
    }
}
