#include "IRCClient.h"

#include "IRCMessage.h"
#include "IRCMessageParser.h"
#include "basics/Assertions.h"
#include "basics/StringUtilities.h"
#include "network/TCPSocket.h"
#include <algorithm>
#include <map>

using namespace samebot;

#pragma mark - API

IRCClient::IRCClient(std::string hostname, int port)
    : IRCClient(std::make_shared<TCPSocket>(hostname, port))
{
}

void IRCClient::connect() {
    socket().connect();
}

void IRCClient::disconnect() {
    socket().disconnect();
}

bool IRCClient::isConnected() const {
    return socket().isConnected();
}

#pragma mark IRC commands

void IRCClient::logIn(std::string nickname, std::string username, std::string realName) {
    sendMessage("HELLO");
    sendMessage("NICK", { nickname });
    sendMessage("USER", { username, "8", "*", realName });
    m_prefix = std::make_shared<IRCMessagePrefix>(nickname, username, "");
}

void IRCClient::joinChannel(std::string channel) {
    sendMessage("JOIN", { channel });
}

void IRCClient::sendMessage(std::string channelOrNickname, std::string message) {
    for (std::string line : splitString(message, '\n')) {
        if (line.length()) {
            sendMessage("PRIVMSG", { channelOrNickname, line });
        }
    }
}

void IRCClient::pong(std::string serverName) {
    sendMessage("PONG", { serverName });
}

void IRCClient::quit(std::string reason) {
    sendMessage("QUIT", { reason });
    disconnect();
}

void IRCClient::sendMessage(std::string command, std::initializer_list<std::string> parameters) {
    IRCMessage message(command, parameters);
    sendMessage(message);
}

void IRCClient::sendMessage(const IRCMessage& message) {
    m_messageSendingMutex.lock();
    socket().write(message.toString());
    m_messageSendingMutex.unlock();
}

std::shared_ptr<IRCMessage> IRCClient::attachPrefixToMessageIfNecessary(std::shared_ptr<IRCMessage> message) {
    if (message->prefix()) {
        return message;
    }
    return std::make_shared<IRCMessage>(m_prefix, message->command(), message->parameters());
}

#pragma mark - Internals

IRCClient::IRCClient(std::shared_ptr<Socket> socket)
    : m_socket(socket)
{
}

Socket& IRCClient::socket() const {
    ASSERT(m_socket);
    return *m_socket.get();
}

void IRCClient::beginProcessingMessages() {
    std::string line;
    while (socket().isConnected()) {
        // IRC messages end with \r\n, so we read to \n and check if
        // there was an \r behind it.
        line.append(socket().readUntilDelimiter('\n'));
        if (line.length() > 2 && line[line.length() - 2] == '\r') {
            IRCMessageParser parser(line);
            if (parser.message()) {
                dispatchMessage(*parser.message());
            }
            line.clear();
        }
    }
}

void IRCClient::dispatchMessage(const IRCMessage& message) {
    static std::map<std::string, std::function<void (IRCClientDelegate&, IRCClient&, const IRCMessage&)>> dispatchTable {
        { "ERROR",          &IRCClientDelegate::handleError },
        { "PING",           &IRCClientDelegate::handlePing },
        { "PRIVMSG",        &IRCClientDelegate::handleMessage },
    };

    auto delegate = m_delegate.lock();
    if (!delegate || dispatchTable.find(message.command()) == dispatchTable.end()) {
        std::string messageString = message.toString();
        messageString.erase(std::find_if(messageString.rbegin(), messageString.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), messageString.end());
        LOG_DEBUG("Unhandled message: %s", messageString.c_str());
        return;
    }

    dispatchTable[message.command()](*delegate.get(), *this, message);
}

#pragma mark - Unit test support

#ifdef DEBUG

std::shared_ptr<IRCClient> IRCClient::test_createClient(std::shared_ptr<Socket> socket) {
    return std::shared_ptr<IRCClient>(new IRCClient(socket));
}

#endif // DEBUG

