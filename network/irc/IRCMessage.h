#ifndef __IRC_MESSAGE_H
#define __IRC_MESSAGE_H

#include "IRCMessagePrefix.h"
#include <memory>

namespace samebot {

class IRCMessage {
public:
    IRCMessage(std::shared_ptr<IRCMessagePrefix>, std::string, std::vector<std::string>);
    IRCMessage(std::string, std::vector<std::string>);
    IRCMessage(std::string);

    const std::string& command() const { return m_command; }
    const IRCMessagePrefix* prefix() const { return m_prefix.get(); }
    const std::vector<std::string>& parameters() const { return m_parameters; }

    std::string toString() const;

private:
    std::shared_ptr<IRCMessagePrefix> m_prefix;
    std::string m_command;
    std::vector<std::string> m_parameters;
};

} // namespace

#endif // __IRC_MESSAGE_H

