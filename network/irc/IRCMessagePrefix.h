#ifndef __IRC_MESSAGE_PREFIX_H
#define __IRC_MESSAGE_PREFIX_H

#include <string>
#include <vector>

namespace samebot {

class IRCMessagePrefix {
public:
    enum class Type {
        ServerName,
        User,
    };

    struct User {
        std::string nickname;
        std::string username;
        std::string hostname;
    };

    IRCMessagePrefix(std::string serverName);
    IRCMessagePrefix(std::string nickname, std::string username, std::string hostname);

    Type type() const { return m_type; }
    User user() const;
    std::string serverName() const;

    std::string toString() const;

private:
    union Data {
        Data();
        ~Data() {}
        std::string serverName;
        User user;
    };

    Type m_type;
    Data m_data;
};

} // namespace

#endif // __IRC_MESSAGE_PREFIX_H

