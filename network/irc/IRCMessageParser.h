#ifndef __IRC_MESSAGE_PARSER_H
#define __IRC_MESSAGE_PARSER_H

#include "IRCMessage.h"
#include <functional>
#include <stack>
#include <string>

namespace samebot {

class IRCMessagePrefix;

class IRCMessageParser {
public:
    IRCMessageParser(std::string);

    const std::string& string() const { return m_string; }
    void setString(std::string);

    const IRCMessage* message();

private:
    void parseMessage();

    std::unique_ptr<IRCMessagePrefix> tryParseMessagePrefix();
    std::string tryParseServerName();
    std::string tryParseNickname();
    std::string tryParseUsername();
    std::string tryParseHostname();
    std::string tryParseCommand();

    std::vector<std::string> tryParseParameters();
    std::string tryParseTrailing();
    std::string tryParseMiddle();

    std::string tryParseStringWithCharacterValidator(std::function<bool (char)> characterValidator);

    char peek() const;
    char pop();

    void saveState();
    void restoreState();
    void discardState();

    std::stack<int> m_states;
    unsigned int m_index;
    std::string m_string;
    std::unique_ptr<IRCMessage> m_message;

#ifdef DEBUG
    friend class IRCMessageParserTestShim;
#endif
};

#ifdef DEBUG
class IRCMessageParserTestShim {
public:
    IRCMessageParserTestShim(std::shared_ptr<IRCMessageParser>);

    std::string tryParseStringWithCharacterValidator(std::function<bool (char)>);
    std::unique_ptr<IRCMessagePrefix> tryParseMessagePrefix();
    std::string tryParseCommand();
    std::vector<std::string> tryParseParameters();

private:
    std::shared_ptr<IRCMessageParser> m_parser;
};
#endif // DEBUG

} // namespace

#endif // __IRC_MESSAGE_PARSER_H

