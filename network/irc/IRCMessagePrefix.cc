#include "IRCMessagePrefix.h"

#include "basics/Assertions.h"

using namespace samebot;

IRCMessagePrefix::Data::Data()
    : user { "", "", "" }
{
}

IRCMessagePrefix::IRCMessagePrefix(std::string serverName)
    : m_type(IRCMessagePrefix::Type::ServerName)
{
    m_data.serverName = serverName;
}

IRCMessagePrefix::IRCMessagePrefix(std::string nickname, std::string username, std::string hostname)
    : m_type(IRCMessagePrefix::Type::User)
{
    m_data.user.nickname = nickname;
    m_data.user.username = username;
    m_data.user.hostname = hostname;
}

std::string IRCMessagePrefix::serverName() const {
    ASSERT(m_type == IRCMessagePrefix::Type::ServerName);
    return m_data.serverName;
}

IRCMessagePrefix::User IRCMessagePrefix::user() const {
    ASSERT(m_type == IRCMessagePrefix::Type::User);
    return m_data.user;
}

std::string IRCMessagePrefix::toString() const {
    switch (m_type) {
        case IRCMessagePrefix::Type::ServerName:
            return serverName();

        case IRCMessagePrefix::Type::User:
            std::string prefix = user().nickname;
            if (user().username.length()) {
                prefix += "!" + user().username;
            }
            if (user().hostname.length()) {
                prefix += "@" + user().hostname;
            }
            return prefix;
    }

    ASSERT_NOT_REACHED();
    return "";
}

