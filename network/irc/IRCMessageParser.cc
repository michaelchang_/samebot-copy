#include "IRCMessageParser.h"

#include "IRCMessage.h"
#include "basics/Assertions.h"

using namespace samebot;

static auto isSpecial(char c) {
    return c == '-' || c == '[' || c == ']' || c == '\\' || c == '`' || c == '^' || c == '{' || c == '}';
}

IRCMessageParser::IRCMessageParser(std::string string)
    : m_index(0)
    , m_string(string)
    , m_message(nullptr)
{
}

const IRCMessage* IRCMessageParser::message() {
    if (!m_message) {
        parseMessage();
    }

    return m_message.get();
}

#pragma mark - Internals

void IRCMessageParser::parseMessage() {
    m_message = nullptr;

    std::unique_ptr<IRCMessagePrefix> prefix;
    if (peek() == ':') {
        prefix = tryParseMessagePrefix();
    }

    while (isspace(peek())) {
        pop();
    }

    auto command = tryParseCommand();
    if (!command.length()) {
        LOG_ERROR("IRC message '%s' is malformed: missing command", m_string.c_str());
        return;
    }

    auto parameters = tryParseParameters();

    if (peek() != '\r') {
        LOG_ERROR("IRC message '%s' is missing trailing CR character; ignoring this", m_string.c_str());
    } else {
        pop();
    }

    if (peek() != '\n') {
        LOG_ERROR("IRC message '%s' is missing trailing LF character; ignoring this", m_string.c_str());
    } else {
        pop();
    }

    m_message = std::make_unique<IRCMessage>(std::move(prefix), command, parameters);
}

std::unique_ptr<IRCMessagePrefix> IRCMessageParser::tryParseMessagePrefix() {
    if (peek() != ':') {
        return nullptr;
    }

    pop(); // Skip :.

    // Not sure if it's a server name or nickname, so we checkpoint here so we can restore state later if we failed to parse a
    // server name.
    saveState();
    auto serverName = tryParseServerName();
    if (serverName.length()) {
        discardState();
        return std::make_unique<IRCMessagePrefix>(serverName);
    }

    restoreState();
    std::string nickname = tryParseNickname();
    if (!nickname.length()) {
        LOG_ERROR("IRC message '%s' is malformed: prefix has no server name or nickname", m_string.c_str());
        return nullptr;
    }

    std::string username;
    std::string hostname;
    if (peek() == '!') {
        pop();
        username = tryParseUsername();
        if (!username.length()) {
            LOG_ERROR("IRC message '%s' is malformed: prefix is missing a username", m_string.c_str());
            return nullptr;
        }
    }

    if (peek() == '@') {
        pop();
        hostname = tryParseHostname();
        if (!hostname.length()) {
            LOG_ERROR("IRC message '%s' is malformed: prefix is missing a hostname", m_string.c_str());
            return nullptr;
        }
    }

    return std::make_unique<IRCMessagePrefix>(nickname, username, hostname);
}

std::string IRCMessageParser::tryParseStringWithCharacterValidator(std::function<bool (char)> characterValidator) {
    std::string string;
    while (peek() && characterValidator(peek())) {
        string += pop();
    }

    return string;
}

std::string IRCMessageParser::tryParseServerName() {
    return tryParseHostname();
}

std::string IRCMessageParser::tryParseHostname() {
    // FIXME: This is a pretty naive and incomplete implementation of RFC 952. We should improve this at some point.
    std::string hostname;
    while (peek()) {
        auto part = tryParseStringWithCharacterValidator([](char c) {
            return isdigit(c) || isalpha(c) || c == '-';
        });
        if (!part.length()) {
            return "";
        }

        hostname += part;
        if (peek() == '!' || peek() == '@' || isSpecial(peek()))
            return "";

        if (peek() != '.')
            break;

        hostname += pop();
    }

    return hostname;
}

std::string IRCMessageParser::tryParseNickname() {
    if (!isalpha(peek()))
        return "";

    return tryParseStringWithCharacterValidator([](auto c) {
        return isalpha(c) || isdigit(c) || isSpecial(c);
    });
}

std::string IRCMessageParser::tryParseUsername() {
    return tryParseStringWithCharacterValidator([](auto c) {
        return c != '@' && !isspace(c);
    });
}

std::string IRCMessageParser::tryParseCommand() {
    std::string name = tryParseStringWithCharacterValidator(isalpha);
    if (name.length()) {
        return name;
    }

    const int numberOfDigits = 3;
    std::string code;
    for (int i = 0; i < numberOfDigits; ++i) {
        if (!isdigit(peek())) {
            return "";
        }
        code += pop();
    }

    return code;
}

std::vector<std::string> IRCMessageParser::tryParseParameters() {
    if (peek() != ' ') {
        return {};
    }

    while (peek() == ' ') {
        pop();
    }

    if (peek() == ':') {
        pop();
        auto trailing = tryParseTrailing();
        if (trailing.length()) {
            return { trailing };
        }
        return {};
    }

    auto middle = tryParseMiddle();
    if (!middle.length()) {
        return {};
    }

    auto parameters = tryParseParameters();
    parameters.insert(parameters.begin(), middle);
    return parameters;
}

std::string IRCMessageParser::tryParseTrailing() {
    return tryParseStringWithCharacterValidator([](auto c) {
        return c && c != '\r' && c != '\n';
    });
}

std::string IRCMessageParser::tryParseMiddle() {
    if (peek() == ':') {
        return "";
    }

    return tryParseStringWithCharacterValidator([](auto c) {
        return c && !isspace(c) && c != '\r' && c != '\n';
    });
}

char IRCMessageParser::peek() const {
    if (m_index >= m_string.length()) {
        return '\0';
    }

    return m_string.at(m_index);
}

char IRCMessageParser::pop() {
    ASSERT(m_index < m_string.length());
    return m_string.at(m_index++);
}

void IRCMessageParser::saveState() {
    m_states.push(m_index);
}

void IRCMessageParser::restoreState() {
    ASSERT(m_states.size());
    m_index = m_states.top();
    discardState();
}

void IRCMessageParser::discardState() {
    ASSERT(m_states.size());
    m_states.pop();
}

#pragma mark - Unit test support

#ifdef DEBUG

IRCMessageParserTestShim::IRCMessageParserTestShim(std::shared_ptr<IRCMessageParser> parser)
    : m_parser(parser)
{
}

std::string IRCMessageParserTestShim::tryParseStringWithCharacterValidator(std::function<bool (char)> delimiterFunction) {
    return m_parser->tryParseStringWithCharacterValidator(delimiterFunction);
}

std::unique_ptr<IRCMessagePrefix> IRCMessageParserTestShim::tryParseMessagePrefix() {
    return m_parser->tryParseMessagePrefix();
}

std::string IRCMessageParserTestShim::tryParseCommand() {
    return m_parser->tryParseCommand();
}

std::vector<std::string> IRCMessageParserTestShim::tryParseParameters() {
    return m_parser->tryParseParameters();
}

#endif // DEBUG

