#ifndef __IRC_CLIENT_H
#define __IRC_CLIENT_H

#include <memory>
#include <mutex>
#include <string>

namespace samebot {

class IRCMessage;
class IRCMessagePrefix;
class Socket;

class IRCClientDelegate;

class IRCClient {
public:
    IRCClient(std::string hostname, int port);

#ifdef DEBUG
    static std::shared_ptr<IRCClient> test_createClient(std::shared_ptr<Socket>);
#endif

    void connect();
    void disconnect();
    bool isConnected() const;

    const IRCClientDelegate* delegate() const { return m_delegate.lock().get(); }
    void setDelegate(std::shared_ptr<IRCClientDelegate> delegate) { m_delegate = delegate; }

    void beginProcessingMessages();

    // These are safe to call from any thread:
    void logIn(std::string nickname, std::string username, std::string realName);
    void joinChannel(std::string channel);
    void sendMessage(std::string channelOrNickname, std::string message);
    void pong(std::string serverName);
    void quit(std::string reason = "");
    void sendMessage(std::string command, std::initializer_list<std::string> parameters = {});
    void sendMessage(const IRCMessage&);

    std::shared_ptr<IRCMessage> attachPrefixToMessageIfNecessary(std::shared_ptr<IRCMessage>);

private:
    IRCClient(std::shared_ptr<Socket> socket);

    Socket& socket() const;

    void dispatchMessage(const IRCMessage&);

    std::shared_ptr<IRCMessagePrefix> m_prefix;
    std::shared_ptr<Socket> m_socket;
    std::weak_ptr<IRCClientDelegate> m_delegate;

    std::mutex m_messageSendingMutex;

#ifdef DEBUG
    friend class IRCClientTestShim;
#endif
};

class IRCClientDelegate {
public:
    virtual void handleError(IRCClient&, const IRCMessage&) {}
    virtual void handlePing(IRCClient&, const IRCMessage&) {}
    virtual void handleMessage(IRCClient&, const IRCMessage&) {}
};

} // namespace

#endif // __IRC_CLIENT_H

