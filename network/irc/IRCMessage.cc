#include "IRCMessage.h"

using namespace samebot;

IRCMessage::IRCMessage(std::shared_ptr<IRCMessagePrefix> prefix, std::string command, std::vector<std::string> parameters)
    : m_prefix(prefix)
    , m_command(command)
    , m_parameters(parameters)
{
}

IRCMessage::IRCMessage(std::string command, std::vector<std::string> parameters)
    : IRCMessage(nullptr, command, parameters)
{
}

IRCMessage::IRCMessage(std::string command)
    : IRCMessage(nullptr, command, {})
{
}

std::string IRCMessage::toString() const {
    std::string string;
    if (prefix()) {
        string += ":" + prefix()->toString() + " ";
    }

    string += command();
    for (unsigned int i = 0; i < parameters().size(); ++i) {
        string += " ";
        if (i && i == parameters().size() - 1) {
            string += ":";
        }

        string += parameters()[i];
    }

    return string + "\r\n";
}

