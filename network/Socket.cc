#include "Socket.h"

#include "basics/Assertions.h"

using namespace samebot;

Socket::Socket(std::string host, int port)
    : m_host(host)
    , m_port(port)
{
}

void Socket::write(std::string data) {
    ASSERT(isConnected());
    socketSend(data);
}

std::string Socket::readUntilDelimiter(char delimiter) {
    std::string data;
    bool foundDelimiter = false;
    while (!foundDelimiter) {
        if (m_buffer.empty()) {
            m_buffer = socketRecv();
            if (m_buffer.empty()) {
                break;
            }
        }

        size_t i;
        for (i = 0; i < m_buffer.length(); ++i) {
            if (m_buffer[i] == delimiter) {
                foundDelimiter = true;
                break;
            }
        }

        // Add one to the index because we want to include the delimiter in the return value.
        data += m_buffer.substr(0, i + 1);
        m_buffer.erase(0, i + 1);
    }

    return data;
}

