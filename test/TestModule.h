#ifndef __TEST_MODULE_H
#define __TEST_MODULE_H

#include "modules/Module.h"
#include "network/irc/IRCMessage.h"

namespace samebot {

class TestModule : public Module {
public:
    TestModule(std::function<void (std::string)> messageHandler)
        : m_messageHandler(messageHandler)
    {
    }

    virtual std::string moduleIdentifier() const override { return "TestModule"; }

    virtual void didReceiveMessage(std::shared_ptr<IRCMessage> message) override {
        m_messageHandler(message->command());
    }

    virtual double getActiveScore(std::shared_ptr<IRCMessage> message) override {
        return 0;
    }

private:
    virtual bool wantsMessageFiltering() const override { return false; }

    std::function<void (std::string)> m_messageHandler;
};

} // namespace

#endif // __TEST_MODULE_H

