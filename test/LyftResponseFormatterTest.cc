#include "Catch.h"

#include "modules/lyft/LyftResponseFormatter.h"
#include "modules/lyft/LyftRideEstimate.h"

using namespace samebot;

TEST_CASE("LyftResponseFormatter::priceExplanationForRideEstimate") {
    LyftResponseFormatter formatter;

    Geocoder::Response startResponse {
        .formattedAddress = "Point A",
        .coordinate = {
            .latitude = 12,
            .longitude = 34,
        },
    };

    Geocoder::Response endResponse {
        .formattedAddress = "Point B",
        .coordinate = {
            .latitude = 12,
            .longitude = 34,
        },
    };

    TestLyftRideEstimateBuilder rideEstimateBuilder;

    rideEstimateBuilder.setDisplayName("Lyft");
    rideEstimateBuilder.setPrimetimePercentage("50%");
    rideEstimateBuilder.setEstimatedMinimumCostInCents(100);
    rideEstimateBuilder.setEstimatedMaximumCostInCents(200);

    auto response = formatter.priceExplanationForRideEstimate(startResponse, endResponse, rideEstimateBuilder.rideEstimate());
    CHECK(response == "a lyft from Point A to Point B would cost $1 - $2 (this includes 50% surge pricing right now)");

    SECTION("With addresses") {
        rideEstimateBuilder.setPrimetimePercentage("0%");
        response = formatter.priceExplanationForRideEstimate(startResponse, endResponse, rideEstimateBuilder.rideEstimate());
        CHECK(response == "a lyft from Point A to Point B would cost $1 - $2");

        rideEstimateBuilder.setPrimetimePercentage("300%");
        rideEstimateBuilder.setEstimatedMaximumCostInCents(100);
        response = formatter.priceExplanationForRideEstimate(startResponse, endResponse, rideEstimateBuilder.rideEstimate());
        CHECK(response == "a lyft from Point A to Point B would cost about $1 (this includes 300% surge pricing right now)");

        rideEstimateBuilder.setPrimetimePercentage("0%");
        rideEstimateBuilder.setEstimatedMaximumCostInCents(100);
        response = formatter.priceExplanationForRideEstimate(startResponse, endResponse, rideEstimateBuilder.rideEstimate());
        CHECK(response == "a lyft from Point A to Point B would cost about $1");
    }

    SECTION("Without start address") {
        startResponse.formattedAddress = "";

        rideEstimateBuilder.setPrimetimePercentage("0%");
        response = formatter.priceExplanationForRideEstimate(startResponse, endResponse, rideEstimateBuilder.rideEstimate());
        CHECK(response == "a lyft would cost $1 - $2");

        rideEstimateBuilder.setPrimetimePercentage("300%");
        rideEstimateBuilder.setEstimatedMaximumCostInCents(100);
        response = formatter.priceExplanationForRideEstimate(startResponse, endResponse, rideEstimateBuilder.rideEstimate());
        CHECK(response == "a lyft would cost about $1 (this includes 300% surge pricing right now)");

        rideEstimateBuilder.setPrimetimePercentage("0%");
        rideEstimateBuilder.setEstimatedMaximumCostInCents(100);
        response = formatter.priceExplanationForRideEstimate(startResponse, endResponse, rideEstimateBuilder.rideEstimate());
        CHECK(response == "a lyft would cost about $1");
    }

    SECTION("Without end address") {
        endResponse.formattedAddress = "";

        rideEstimateBuilder.setPrimetimePercentage("0%");
        response = formatter.priceExplanationForRideEstimate(startResponse, endResponse, rideEstimateBuilder.rideEstimate());
        CHECK(response == "a lyft would cost $1 - $2");

        rideEstimateBuilder.setPrimetimePercentage("300%");
        rideEstimateBuilder.setEstimatedMaximumCostInCents(100);
        response = formatter.priceExplanationForRideEstimate(startResponse, endResponse, rideEstimateBuilder.rideEstimate());
        CHECK(response == "a lyft would cost about $1 (this includes 300% surge pricing right now)");

        rideEstimateBuilder.setPrimetimePercentage("0%");
        rideEstimateBuilder.setEstimatedMaximumCostInCents(100);
        response = formatter.priceExplanationForRideEstimate(startResponse, endResponse, rideEstimateBuilder.rideEstimate());
        CHECK(response == "a lyft would cost about $1");
    }

    SECTION("Without addresses") {
        startResponse.formattedAddress = "";
        endResponse.formattedAddress = "";

        rideEstimateBuilder.setPrimetimePercentage("0%");
        response = formatter.priceExplanationForRideEstimate(startResponse, endResponse, rideEstimateBuilder.rideEstimate());
        CHECK(response == "a lyft would cost $1 - $2");

        rideEstimateBuilder.setPrimetimePercentage("300%");
        rideEstimateBuilder.setEstimatedMaximumCostInCents(100);
        response = formatter.priceExplanationForRideEstimate(startResponse, endResponse, rideEstimateBuilder.rideEstimate());
        CHECK(response == "a lyft would cost about $1 (this includes 300% surge pricing right now)");

        rideEstimateBuilder.setPrimetimePercentage("0%");
        rideEstimateBuilder.setEstimatedMaximumCostInCents(100);
        response = formatter.priceExplanationForRideEstimate(startResponse, endResponse, rideEstimateBuilder.rideEstimate());
        CHECK(response == "a lyft would cost about $1");
    }

    rideEstimateBuilder.setEstimatedMaximumCostInCents(0);
    response = formatter.priceExplanationForRideEstimate(startResponse, endResponse, rideEstimateBuilder.rideEstimate());
    CHECK(response == "i can't find any price information about that trip");

    rideEstimateBuilder.setEstimatedMaximumCostInCents(100);
    rideEstimateBuilder.setEstimatedMinimumCostInCents(0);
    response = formatter.priceExplanationForRideEstimate(startResponse, endResponse, rideEstimateBuilder.rideEstimate());
    CHECK(response == "i can't find any price information about that trip");
}

TEST_CASE("LyftResponseFormatter::durationAndDistanceExplanationForRideEstimate") {
    LyftResponseFormatter formatter;

    Geocoder::Response startResponse {
        .formattedAddress = "Point A",
        .coordinate = {
            .latitude = 12,
            .longitude = 34,
        },
    };

    Geocoder::Response endResponse {
        .formattedAddress = "Point B",
        .coordinate = {
            .latitude = 12,
            .longitude = 34,
        },
    };

    TestLyftRideEstimateBuilder rideEstimateBuilder;

    rideEstimateBuilder.setDisplayName("Lyft");
    rideEstimateBuilder.setPrimetimePercentage("50%");
    rideEstimateBuilder.setEstimatedMinimumCostInCents(100);
    rideEstimateBuilder.setEstimatedMaximumCostInCents(200);
    rideEstimateBuilder.setEstimatedDuration(std::chrono::seconds(3600));
    rideEstimateBuilder.setEstimatedDistanceInMiles(20.5);

    auto response = formatter.durationAndDistanceExplanationForRideEstimate(startResponse, endResponse, rideEstimateBuilder.rideEstimate());
    CHECK(response == "the trip is 20.5 miles long and would take about 60 minutes right now");

    rideEstimateBuilder.setEstimatedDistanceInMiles(1);
    response = formatter.durationAndDistanceExplanationForRideEstimate(startResponse, endResponse, rideEstimateBuilder.rideEstimate());
    CHECK(response == "the trip is 1 mile long and would take about 60 minutes right now");

    rideEstimateBuilder.setEstimatedDuration(std::chrono::seconds(60));
    response = formatter.durationAndDistanceExplanationForRideEstimate(startResponse, endResponse, rideEstimateBuilder.rideEstimate());
    CHECK(response == "the trip is 1 mile long and would take about 1 minute right now");

    rideEstimateBuilder.setEstimatedDuration(std::chrono::seconds(59));
    response = formatter.durationAndDistanceExplanationForRideEstimate(startResponse, endResponse, rideEstimateBuilder.rideEstimate());
    CHECK(response == "it's a 1 mile trip");

    rideEstimateBuilder.setEstimatedDistanceInMiles(0.99);
    response = formatter.durationAndDistanceExplanationForRideEstimate(startResponse, endResponse, rideEstimateBuilder.rideEstimate());
    CHECK(response == "it's a 0.99 mile trip");

    rideEstimateBuilder.setEstimatedDistanceInMiles(1.59);
    response = formatter.durationAndDistanceExplanationForRideEstimate(startResponse, endResponse, rideEstimateBuilder.rideEstimate());
    CHECK(response == "it's a 1.59 mile trip");

    rideEstimateBuilder.setEstimatedDistanceInMiles(1.984732324);
    response = formatter.durationAndDistanceExplanationForRideEstimate(startResponse, endResponse, rideEstimateBuilder.rideEstimate());
    CHECK(response == "it's a 1.98473 mile trip");

    rideEstimateBuilder.setEstimatedDistanceInMiles(0);
    rideEstimateBuilder.setEstimatedDuration(std::chrono::seconds(61));
    response = formatter.durationAndDistanceExplanationForRideEstimate(startResponse, endResponse, rideEstimateBuilder.rideEstimate());
    CHECK(response == "the trip would take about 1 minute right now");
    rideEstimateBuilder.setEstimatedDistanceInMiles(0);

    rideEstimateBuilder.setEstimatedDuration(std::chrono::seconds(120));
    response = formatter.durationAndDistanceExplanationForRideEstimate(startResponse, endResponse, rideEstimateBuilder.rideEstimate());
    CHECK(response == "the trip would take about 2 minutes right now");

    rideEstimateBuilder.setEstimatedDistanceInMiles(0);
    rideEstimateBuilder.setEstimatedDuration(std::chrono::seconds(7200));
    response = formatter.durationAndDistanceExplanationForRideEstimate(startResponse, endResponse, rideEstimateBuilder.rideEstimate());
    CHECK(response == "the trip would take about 120 minutes right now");

    rideEstimateBuilder.setEstimatedDuration(std::chrono::seconds(0));
    response = formatter.durationAndDistanceExplanationForRideEstimate(startResponse, endResponse, rideEstimateBuilder.rideEstimate());
    CHECK(response == "i'm not sure how long it'll take or how far it is though");

    rideEstimateBuilder.setEstimatedMinimumCostInCents(0);
    response = formatter.durationAndDistanceExplanationForRideEstimate(startResponse, endResponse, rideEstimateBuilder.rideEstimate());
    CHECK(response == "i can't find anything about the trip length or duration either");

    rideEstimateBuilder.setEstimatedMinimumCostInCents(100);
    rideEstimateBuilder.setEstimatedMaximumCostInCents(0);
    response = formatter.durationAndDistanceExplanationForRideEstimate(startResponse, endResponse, rideEstimateBuilder.rideEstimate());
    CHECK(response == "i can't find anything about the trip length or duration either");
}
