#include "Catch.h"

#include "modules/dank/DankModule.h"

using namespace samebot;

TEST_CASE("DankModule::shouldRespondToMessage()") {
    DankModule module;
    REQUIRE(module.test_shouldRespondToMessage("dank"));
    REQUIRE(module.test_shouldRespondToMessage("very dank"));
    REQUIRE(module.test_shouldRespondToMessage("dank stuff"));
    REQUIRE(module.test_shouldRespondToMessage("well that's quite dank isn't it"));
    REQUIRE(module.test_shouldRespondToMessage("dankkkk"));
    REQUIRE(module.test_shouldRespondToMessage("daank"));
    REQUIRE(module.test_shouldRespondToMessage("danq"));
    REQUIRE(module.test_shouldRespondToMessage("THAT'S DANK"));
    REQUIRE(module.test_shouldRespondToMessage("WOW VERY DANK STUFF BROS"));

    REQUIRE_FALSE(module.test_shouldRespondToMessage("these are just some words"));
    REQUIRE_FALSE(module.test_shouldRespondToMessage("a blank page"));
    REQUIRE_FALSE(module.test_shouldRespondToMessage("'quack', said the duck"));

    // FIXME: Can we get the module reject dictionary words like these?
    REQUIRE(module.test_shouldRespondToMessage("tank"));
    REQUIRE(module.test_shouldRespondToMessage("bank"));
    REQUIRE(module.test_shouldRespondToMessage("the ship sank"));
}

