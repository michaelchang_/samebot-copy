#include "Catch.h"

#include "network/irc/IRCMessage.h"
#include <memory>

using namespace samebot;

TEST_CASE("IRCMessage") {
    auto prefix = std::make_shared<IRCMessagePrefix>("some server");
    IRCMessage message(prefix, "command", { "hello", "world" });

    REQUIRE(message.prefix());
    REQUIRE(message.prefix()->type() == IRCMessagePrefix::Type::ServerName);
    REQUIRE(message.prefix()->serverName() == "some server");
    REQUIRE(message.command() == "command");
    REQUIRE(message.parameters() == std::vector<std::string>({ "hello", "world" }));
}

TEST_CASE("IRCMessage::toString") {
    REQUIRE(IRCMessage("HELLO").toString() == "HELLO\r\n");
    REQUIRE(IRCMessage("NICK", { "test" }).toString() == "NICK test\r\n");
    REQUIRE(IRCMessage(std::make_shared<IRCMessagePrefix>("127.0.0.1"), "001", { "test", "Welcome" }).toString() == ":127.0.0.1 001 test :Welcome\r\n");
    REQUIRE(IRCMessage(std::make_shared<IRCMessagePrefix>("127.0.0.1"), "005", { "test", "FLAG1=1", "FLAG2=stuff", "FLAG3", "are supported by this server" }).toString() == ":127.0.0.1 005 test FLAG1=1 FLAG2=stuff FLAG3 :are supported by this server\r\n");
    REQUIRE(IRCMessage(std::make_shared<IRCMessagePrefix>("test", "user", "127.0.0.1"), "MODE", { "test", "+i" }).toString() == ":test!user@127.0.0.1 MODE test :+i\r\n");
}

