#include "Catch.h"

#include "intelligence/UserInformationSQLiteDataStore.h"
#include <string.h>

using namespace samebot;

static const char* testDatabasePath = "/tmp/test_user_information_database.db";

TEST_CASE("UserInformationSQLiteStore") {
    remove(testDatabasePath);

    UserInformationSQLiteDataStore dataStore(testDatabasePath);
    CHECK(dataStore.dataForUser("anshu", "key1") == "");

    dataStore.setDataForUser("anshu", "key1", "something");
    CHECK(dataStore.dataForUser("anshu", "key1") == "something");
    CHECK(dataStore.dataForUser("Anshu", "key1") == "something");
    CHECK(dataStore.dataForUser("   ANsHU ", "key1") == "something");
    CHECK(dataStore.dataForUser("anshu", "key1 ") == "");
    CHECK(dataStore.dataForUser("anshu", " key1") == "");
    CHECK(dataStore.dataForUser("anshu", "key 1") == "");
    CHECK(dataStore.dataForUser("anshu", "Key1") == "");
    CHECK(dataStore.dataForUser("tyrus", "key1") == "");
    CHECK(dataStore.dataForUser("ans hu", "key1") == "");
    CHECK(dataStore.dataForUser("", "key1") == "");

    dataStore.setDataForUser("anshu", "key 2", "other thing");
    CHECK(dataStore.dataForUser("anshu", "key1") == "something");
    CHECK(dataStore.dataForUser("anshu", "key 2") == "other thing");
    CHECK(dataStore.dataForUser("anshu", "Key 2") == "");
    CHECK(dataStore.dataForUser("tyrus", "key1") == "");
    CHECK(dataStore.dataForUser("tyrus", "key 2") == "");

    dataStore.setDataForUser("tyrus", "key1", "something");
    dataStore.setDataForUser("mchang", "key 2", "another thing");
    CHECK(dataStore.dataForUser("anshu", "key1") == "something");
    CHECK(dataStore.dataForUser("tyrus", "key1") == "something");
    CHECK(dataStore.dataForUser("mchang", "key 2") == "another thing");

    dataStore.setDataForUser("tyrus", "key1", "another thing");
    dataStore.setDataForUser("mchang", "key 2", "something");
    CHECK(dataStore.dataForUser("anshu", "key1") == "something");
    CHECK(dataStore.dataForUser("tyrus", "key1") == "another thing");
    CHECK(dataStore.dataForUser("mchang", "key 2") == "something");

    dataStore.setDataForUser("tyrus", "key1", "🔑🙏stuff🇺🇸");
    CHECK(dataStore.dataForUser("anshu", "key1") == "something");
    CHECK(dataStore.dataForUser("tyrus", "key1") == "🔑🙏stuff🇺🇸");
    CHECK(dataStore.dataForUser("mchang", "key 2") == "something");

    dataStore.setDataForUser("anshu", "key1", "");
    CHECK(dataStore.dataForUser("anshu", "key1") == "");
    CHECK(dataStore.dataForUser("tyrus", "key1") == "🔑🙏stuff🇺🇸");

    dataStore.setDataForUser("anshu", "key1", "whatever");
    CHECK(dataStore.dataForUser("anshu", "key1") == "whatever");
    CHECK(dataStore.dataForUser("tyrus", "key1") == "🔑🙏stuff🇺🇸");

    remove(testDatabasePath);
}
