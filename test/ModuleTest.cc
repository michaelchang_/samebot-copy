#include "Catch.h"

#include "TestModule.h"
#include <chrono>

#include <iostream>

using namespace samebot;

TEST_CASE("Module message queue") {
    int expected = 0;
    const int total = 100;

    SECTION("Interruptions") {
        TestModule module([&expected, total](std::string message) {
            int i = atoi(message.c_str());
            REQUIRE(i == expected);
            REQUIRE(expected <= total);
            ++expected;
            std::this_thread::sleep_for(std::chrono::milliseconds(1));
        });

        module.start();
        for (int i = 0; i < total; ++i) {
            module.handleMessage(std::make_shared<IRCMessage>(std::to_string(i)), MessageType::Incoming);
        }
        module.stop();

        REQUIRE(expected > 0);
        REQUIRE(expected <= total);
    }

    SECTION("Extra messages after stopping") {
        TestModule module([&expected, total](std::string message) {
            int i = atoi(message.c_str());
            REQUIRE(i == expected);
            REQUIRE(expected <= total);
            ++expected;
        });

        module.start();
        for (int i = 0; i < total; ++i) {
            module.handleMessage(std::make_shared<IRCMessage>(std::to_string(i)), MessageType::Incoming);
            if (i == total / 2) {
                module.processRemainingMessagesAndStop();
            }
        }

        REQUIRE(expected == total / 2 + 1);
    }

    SECTION("Messages queued up before starting") {
        TestModule module([&expected, total](std::string message) {
            int i = atoi(message.c_str());
            REQUIRE(i == expected);
            REQUIRE(expected <= total);
            ++expected;
        });

        for (int i = 0; i < total; ++i) {
            module.handleMessage(std::make_shared<IRCMessage>(std::to_string(i)), MessageType::Incoming);
            if (i == total / 3) {
                module.start();
            }
        }
        module.processRemainingMessagesAndStop();

        REQUIRE(expected == total);
    }

    SECTION("Message queue flushed immediately after starting") {
        TestModule module([&expected, total](std::string message) {
            int i = atoi(message.c_str());
            REQUIRE(i == expected);
            REQUIRE(expected <= total);
            ++expected;
        });

        for (int i = 0; i < total; ++i) {
            module.handleMessage(std::make_shared<IRCMessage>(std::to_string(i)), MessageType::Incoming);
        }

        module.start();
        module.processRemainingMessagesAndStop();

        REQUIRE(expected == total);
    }
}

