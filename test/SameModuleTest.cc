#include "Catch.h"

#include "modules/same/SameModule.h"

using namespace samebot;

TEST_CASE("SameModule::randomResponse() doesn't repeat itself") {
    std::set<std::string> responsesThatCanRepeat {
        "same",
        "same tbh",
    };

    SameModule module;
    auto lastResponse = module.test_randomResponse();
    for (int i = 0; i < 100; ++i) {
        auto response = module.test_randomResponse();
        if (responsesThatCanRepeat.find(response) == responsesThatCanRepeat.end()) {
            REQUIRE(response != lastResponse);
        }

        lastResponse = response;
    }
}

