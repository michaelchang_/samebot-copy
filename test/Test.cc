#define CATCH_CONFIG_RUNNER

#include "Catch.h"
#include "basics/Assertions.h"

int main(int argc, char** argv) {
    disableDebugLogging();
    disableErrorLogging();
    return  Catch::Session().run(argc, argv);
}

