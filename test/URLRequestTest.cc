#include "Catch.h"

#include "network/URLRequest.h"

using namespace samebot;

TEST_CASE("URLRequest headers") {
    URLRequest request("http://www.example.com");
    request.enumerateHeaders([](auto, auto) {
        FAIL();
    });

    request.addHeader("Accept", "application/json");
    request.enumerateHeaders([](auto header, auto value) {
        CHECK(header == "Accept");
        CHECK(value == "application/json");
    });

    int i = 0;
    request.addHeader("X-Custom-Header", "Hello, world!");
    request.enumerateHeaders([&i](auto header, auto value) {
        ++i;

        if (i == 1) {
            CHECK(header == "Accept");
            CHECK(value == "application/json");
            return;
        }

        if (i == 2) {
            CHECK(header == "X-Custom-Header");
            CHECK(value == "Hello, world!");
            return;
        }

        FAIL();
    });

    request.addHeader("Accept", "application/xml");
    i = 0;
    request.enumerateHeaders([&i](auto header, auto value) {
        ++i;

        if (i == 1) {
            CHECK(header == "X-Custom-Header");
            CHECK(value == "Hello, world!");
            return;
        }

        if (i == 2) {
            CHECK(header == "Accept");
            CHECK(value == "application/xml");
            return;
        }

        FAIL();
    });

    request.addHeader("accept", "application/json");
    i = 0;
    request.enumerateHeaders([&i](auto header, auto value) {
        ++i;

        if (i == 1) {
            CHECK(header == "X-Custom-Header");
            CHECK(value == "Hello, world!");
            return;
        }

        if (i == 2) {
            CHECK(header == "Accept");
            CHECK(value == "application/xml");
            return;
        }

        if (i == 3) {
            CHECK(header == "accept");
            CHECK(value == "application/json");
            return;
        }

        FAIL();
    });
}

TEST_CASE("URLRequest JSON formatting") {
    URLRequest request("http://www.example.com", URLRequest::Method::POST, {
        { "hello", "world" },
        { "thing", "some stuff" },
    });

    CHECK_FALSE(request.formatsPOSTDataAsJSON());
    CHECK(request.postData() == "hello=world&thing=some stuff");

    request.setFormatsPOSTDataAsJSON(true);
    CHECK(request.formatsPOSTDataAsJSON());
    CHECK(request.postData() == "{\"hello\":\"world\",\"thing\":\"some stuff\"}");

    request.setFormatsPOSTDataAsJSON(false);
    CHECK_FALSE(request.formatsPOSTDataAsJSON());
    CHECK(request.postData() == "hello=world&thing=some stuff");
}

