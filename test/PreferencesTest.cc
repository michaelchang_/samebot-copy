#include "Catch.h"

#include "samebot/Preferences.h"

using namespace samebot;

static const std::string testDatabaseLocation = "/tmp/test_preferences.db";

TEST_CASE("Preferences") {
    remove(testDatabaseLocation.c_str());

    {
        Preferences preferences(testDatabaseLocation);

        REQUIRE(preferences.test_numberOfRowsInDatabase() == -1);
        REQUIRE(preferences.get("key1") == "");
        REQUIRE(preferences.get("key2") == "");

        preferences.set("key1", "value1");
        REQUIRE(preferences.test_numberOfRowsInDatabase() == 1);
        REQUIRE(preferences.get("key1") == "value1");
        REQUIRE(preferences.get("key2") == "");

        preferences.set("key2", "value2");
        REQUIRE(preferences.test_numberOfRowsInDatabase() == 2);
        REQUIRE(preferences.get("key1") == "value1");
        REQUIRE(preferences.get("key2") == "value2");

        preferences.set("key1", "value2");
        REQUIRE(preferences.test_numberOfRowsInDatabase() == 2);
        REQUIRE(preferences.get("key1") == "value2");
        REQUIRE(preferences.get("key2") == "value2");

        preferences.set("key2", "value1");
        REQUIRE(preferences.test_numberOfRowsInDatabase() == 2);
        REQUIRE(preferences.get("key1") == "value2");
        REQUIRE(preferences.get("key2") == "value1");

        preferences.set("key2", "value1");
        REQUIRE(preferences.test_numberOfRowsInDatabase() == 2);
        REQUIRE(preferences.get("key1") == "value2");
        REQUIRE(preferences.get("key2") == "value1");

        preferences.set("key1", "");
        REQUIRE(preferences.test_numberOfRowsInDatabase() == 1);
        REQUIRE(preferences.get("key1") == "");
        REQUIRE(preferences.get("key2") == "value1");
    }

    Preferences preferences(testDatabaseLocation);
    REQUIRE(preferences.test_numberOfRowsInDatabase() == 1);
    REQUIRE(preferences.get("key1") == "");
    REQUIRE(preferences.get("key2") == "value1");

    remove(testDatabaseLocation.c_str());
}

