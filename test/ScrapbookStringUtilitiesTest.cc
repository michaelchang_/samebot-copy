#include "Catch.h"

#include "modules/scrapbook/ScrapbookStringUtilities.h"
#include "network/irc/IRCMessage.h"

using namespace samebot;

TEST_CASE("interleaveNicknamesWithZeroWidthSpaces") {
    REQUIRE(interleaveNicknamesWithZeroWidthSpaces("<user1> message") == "<u\u200Bs\u200Be\u200Br\u200B1> message");
    REQUIRE(interleaveNicknamesWithZeroWidthSpaces("<user1>    hello") == "<u\u200Bs\u200Be\u200Br\u200B1>    hello");
    REQUIRE(interleaveNicknamesWithZeroWidthSpaces("<us>er1> message") == "<u\u200Bs\u200B>\u200Be\u200Br\u200B1> message");
    REQUIRE(interleaveNicknamesWithZeroWidthSpaces("<badly formatted message") == "<badly formatted message");
    REQUIRE(interleaveNicknamesWithZeroWidthSpaces("<user1> message\r\n<user2> message") == "<u\u200Bs\u200Be\u200Br\u200B1> message\r\n<u\u200Bs\u200Be\u200Br\u200B2> message");
    REQUIRE(interleaveNicknamesWithZeroWidthSpaces("<badly formatted message\r\n<user2> message") == "<badly formatted message\r\n<u\u200Bs\u200Be\u200Br\u200B2> message");
    REQUIRE(interleaveNicknamesWithZeroWidthSpaces("<user1> message\r\n<badly formatted> message") == "<u\u200Bs\u200Be\u200Br\u200B1> message\r\n<badly formatted> message");
    REQUIRE(interleaveNicknamesWithZeroWidthSpaces("   ") == "   ");
    REQUIRE(interleaveNicknamesWithZeroWidthSpaces("") == "");
}

TEST_CASE("indentLines") {
    REQUIRE(indentLines("hello", ">>>") == ">>>hello");
    REQUIRE(indentLines("hello\r\nworld", ">>>") == ">>>hello\r\n>>>world");
    REQUIRE(indentLines("", ">>>") == "");

    // FIXME: These are weird cases that don't make a whole lot of sense.
    REQUIRE(indentLines("hello\r\nworld\r\n", ">>>") == ">>>hello\r\n>>>world\r");
    REQUIRE(indentLines("\r\n", ">>>") == ">>>\r");
    REQUIRE(indentLines("\r\n", "\r\n") == "\r\n\r");
}

TEST_CASE("messageProbablyContainsFormattedOtherMessage") {
    auto test = [](auto body) {
        auto message = std::make_shared<IRCMessage>("PRIVMSG", std::vector<std::string> { "#channel", body });
        return messageProbablyContainsFormattedOtherMessage(message);
    };

    REQUIRE(test("<user> stuff"));
    REQUIRE(test("<user> <stuff>"));
    REQUIRE(test("<us>er> stuff"));
    REQUIRE(test("<us<er> stuff"));
    REQUIRE(test("<us<er> stuff hello whatever <test>"));
    REQUIRE(test("    <user> stuff"));
    REQUIRE(test("    <user> <stuff>"));
    REQUIRE(test("    <us>er> stuff"));
    REQUIRE(test("    <us<er> stuff"));
    REQUIRE(test("    <us<er> stuff hello whatever <test>"));
    REQUIRE(test("<<<user> stuff hello whatever <test>"));
    REQUIRE_FALSE(test("<user stuff"));
    REQUIRE_FALSE(test("<user stuff>"));
    REQUIRE_FALSE(test("    <user stuff>"));
    REQUIRE_FALSE(test("<>user stuff"));
    REQUIRE_FALSE(test("<> stuff hello whatever <test>"));
}

