#include "Catch.h"

#include "basics/StringUtilities.h"

using namespace samebot;

TEST_CASE("levenshtein") {
    REQUIRE(levenshtein("a", "a") == 0);
    REQUIRE(levenshtein("a", "ab") == 1);
    REQUIRE(levenshtein("a", "abc") == 2);
    REQUIRE(levenshtein("ab", "abc") == 1);
    REQUIRE(levenshtein("ac", "abc") == 1);
    REQUIRE(levenshtein("bc", "abc") == 1);
    REQUIRE(levenshtein("bac", "abc") == 1);

    REQUIRE(levenshtein("", "a") == 1);
    REQUIRE(levenshtein("a", "") == 1);
    REQUIRE(levenshtein("", "") == 0);

    REQUIRE(levenshtein("hello", "hello") == 0);
    REQUIRE(levenshtein("hello", "hell") == 1);
    REQUIRE(levenshtein("hello", "world") == 4);
}

TEST_CASE("stringByCompressingIntrawordSpacesInString") {
    CHECK(stringByCompressingIntrawordSpacesInString("t e s t") == "test");
    CHECK(stringByCompressingIntrawordSpacesInString("test c a s e") == "test case");
    CHECK(stringByCompressingIntrawordSpacesInString("t e s t case") == "test case");
    CHECK(stringByCompressingIntrawordSpacesInString("t e s t  c a s e") == "test case");
    CHECK(stringByCompressingIntrawordSpacesInString("t e s t c a s e") == "testcase");
    CHECK(stringByCompressingIntrawordSpacesInString("this = t e s t") == "this = test");
    CHECK(stringByCompressingIntrawordSpacesInString("1 g o o d test c a s e") == "1 good test case");
    CHECK(stringByCompressingIntrawordSpacesInString("  t h i s  i s  a  t e s t  ") == "this is a test");
    CHECK(stringByCompressingIntrawordSpacesInString("this i s  a test") == "this is a test");
    CHECK(stringByCompressingIntrawordSpacesInString("this t e s t  c a s e is pretty great") == "this test case is pretty great");
    CHECK(stringByCompressingIntrawordSpacesInString("normal string") == "normal string");
    CHECK(stringByCompressingIntrawordSpacesInString("normal    string") == "normal string");
    CHECK(stringByCompressingIntrawordSpacesInString("a normal string") == "a normal string");
    CHECK(stringByCompressingIntrawordSpacesInString(" a normal string ") == "a normal string");
    CHECK(stringByCompressingIntrawordSpacesInString("that's a string") == "that's a string");
    CHECK(stringByCompressingIntrawordSpacesInString("") == "");
    CHECK(stringByCompressingIntrawordSpacesInString("     ") == "");
    CHECK(stringByCompressingIntrawordSpacesInString("    a    ") == "a");
    CHECK(stringByCompressingIntrawordSpacesInString("     a    b   ") == "a b");
}

TEST_CASE("isFuzzyMatch") {
    CHECK(isFuzzyMatch("test", "test"));
    CHECK(isFuzzyMatch("best", "test"));
    CHECK(isFuzzyMatch("tests", "test"));
    CHECK(isFuzzyMatch("twest", "test"));
    CHECK(isFuzzyMatch("tset", "test"));
    CHECK(isFuzzyMatch("testnig", "testing"));
    CHECK(isFuzzyMatch("tsetingg", "testing"));
    CHECK(isFuzzyMatch("test ing", "testing"));
    CHECK(isFuzzyMatch("t e s t", "test"));
    CHECK(isFuzzyMatch("t  e s t", "test"));
    CHECK(isFuzzyMatch("TEST", "test"));
    CHECK(isFuzzyMatch("T e S t", "test"));
    CHECK(isFuzzyMatch("t s e t", "test"));
    CHECK(isFuzzyMatch("TE$ST", "test"));
    CHECK_FALSE(isFuzzyMatch("asdf", "test"));
    CHECK_FALSE(isFuzzyMatch("something", "test"));
    CHECK_FALSE(isFuzzyMatch("testthing", "test"));
    CHECK_FALSE(isFuzzyMatch("testing", "test"));
}

