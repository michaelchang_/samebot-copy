#include "Catch.h"

#include "database/SQLiteDatabase.h"

using namespace samebot;

TEST_CASE("SQLiteDatabase") {
    remove("/tmp/test_database.db");

    SQLiteDatabase database("/tmp/test_database.db");
    REQUIRE_FALSE(database.hasError());

    bool success = true;

    auto expectSuccess = [&] {
        INFO(database.errorMessage());
        REQUIRE(success);
        REQUIRE_FALSE(database.hasError());
    };

    auto expectFailure = [&](std::string errorMessage) {
        INFO(database.errorMessage());
        REQUIRE_FALSE(success);
        REQUIRE(database.hasError());
        REQUIRE(database.errorMessage() == errorMessage);
    };

    success = database.executeSQL("CREATE TABLE test_table (id INTEGER PRIMARY KEY NOT NULL, text TEXT NOT NULL, datetime DATETIME NOT NULL)");
    expectSuccess();

    std::time_t time_s = 1468125287; // 2016-07-10T04:34:47Z
    auto time = std::chrono::system_clock::from_time_t(time_s);
    SECTION("executeSQL") {
        success = database.executeSQL("INSERT INTO test_table VALUES (?, ?, ?)", 1, "hello", time);
        expectSuccess();

        success = database.executeSQL("INSERT INTO test_table VALUES (?, ?, ?)", 2, "", time);
        expectSuccess();

        success = database.executeSQL("INSERT INTO test_table VALUES (?, ?, ?)", 3, 123, time);
        expectSuccess();

        success = database.executeSQL("INSERT INTO test_table VALUES (?, ?, ?)", 2, "sup", time);
        expectFailure("UNIQUE constraint failed: test_table.id");

        success = database.executeSQL("INSERT INTO test_table VALUES (?, ?, ?)", 4);
        expectFailure("NOT NULL constraint failed: test_table.text");

        success = database.executeSQL("INSERT INTO test_table VALUES (?, ?, ?)", 5, nullptr, nullptr);
        expectFailure("NOT NULL constraint failed: test_table.text");

        success = database.executeSQL("INSERT INTO test_table VALUES (?, ?, ?)", 6, 123, 345, time);
        expectFailure("bind or column index out of range");
    }

    SECTION("executeSQLQuery") {
        for (int i = 0; i < 10; ++i) {
            success = database.executeSQL("INSERT INTO test_table VALUES (?, ?, ?)", i, "row number " + std::to_string(i), time + std::chrono::hours(i));
            expectSuccess();
        }

        int i = 5;
        success = database.executeSQLQuery("SELECT * FROM test_table WHERE id >= 5", [&](const SQLiteResultRow& row) {
            REQUIRE(row.get<int>(0) == i);
            REQUIRE(row.get<std::string>(1) == "row number " + std::to_string(i));
            REQUIRE(row.get<std::chrono::system_clock::time_point>(2) == time + std::chrono::hours(i));
            ++i;
        });

        REQUIRE(i == 10);
        expectSuccess();
    }

    SECTION("SQLiteResultRow::get error handling") {
        success = database.executeSQL("INSERT INTO test_table VALUES (?, ?, 'desu')", 0, "row number 0");
        expectSuccess();

        success = database.executeSQLQuery("SELECT datetime FROM test_table", [&](const SQLiteResultRow& row) {
            REQUIRE(row.get<std::chrono::system_clock::time_point>(0) == std::chrono::system_clock::time_point());
        });
        expectSuccess();
    }

    remove("/tmp/test_database.db");
}
