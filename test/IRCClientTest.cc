#include "Catch.h"

#include "TestSocket.h"
#include "network/TCPSocket.h"
#include "network/irc/IRCClient.h"
#include "network/irc/IRCMessage.h"

using namespace samebot;

TEST_CASE("IRCClient::sendMessage") {
    auto socket = std::make_shared<TestSocket>("127.0.0.1", 1234);
    auto client = IRCClient::test_createClient(socket);
    client->connect();

    bool messageWasSent = false;
    socket->setSendHandler([&messageWasSent](std::string message) {
        REQUIRE(message == ":127.0.0.1 001 test :Welcome\r\n");
        messageWasSent = true;
    });

    client->sendMessage(IRCMessage(std::make_shared<IRCMessagePrefix>("127.0.0.1"), "001", { "test", "Welcome" }));
    REQUIRE(messageWasSent);
}

TEST_CASE("IRCClient::dispatchMessage") {
    auto socket = std::make_shared<TestSocket>("127.0.0.1", 1234);
    auto client = IRCClient::test_createClient(socket);

    class Delegate : public IRCClientDelegate {
    public:
        std::function<void (const IRCMessage&)> m_handleError;
        virtual void handleError(IRCClient&, const IRCMessage& message) override { m_handleError(message); }

        std::function<void (const IRCMessage&)> m_handlePing;
        virtual void handlePing(IRCClient&, const IRCMessage& message) override { m_handlePing(message); }
    };

    auto delegate = std::make_shared<Delegate>();
    client->setDelegate(delegate);

    bool handledPingMessage = false;
    socket->setResponse("PING\r\n");
    client->connect();
    delegate->m_handlePing = [&handledPingMessage, &client](const IRCMessage& message) {
        REQUIRE(message.command() == "PING");
        REQUIRE(!message.parameters().size());
        handledPingMessage = true;
        client->disconnect();
    };
    client->beginProcessingMessages();
    REQUIRE(handledPingMessage);

    bool handledErrorMessage = false;
    socket->setResponse("ERROR stuff :happened\r\n");
    client->connect();
    delegate->m_handleError = [&handledErrorMessage, &client](const IRCMessage& message) {
        REQUIRE(message.command() == "ERROR");
        REQUIRE(message.parameters() == std::vector<std::string>({ "stuff", "happened" }));
        handledErrorMessage = true;
        client->disconnect();
    };
    client->beginProcessingMessages();
    REQUIRE(handledErrorMessage);
}

TEST_CASE("IRCClient handles server disconnects gracefully") {
    // Test for issue #34, where IRCClient could enter an infinite loop if the server disconnected.
    class TCPSocketWithNoResponse : public TCPSocket {
    public:
        using TCPSocket::TCPSocket;

        // Override these to ensure we don't talk to a server.
        virtual bool connect() override {
            m_connected = true;
            return true;
        }
        virtual void disconnect() override { m_connected = false; }
        virtual bool isConnected() const override { return m_connected; }
        virtual void socketSend(std::string) override {}

        int failsafeCounter() const { return m_failsafeCounter; }

        // Always respond with an empty string, as if the server disconnected.
        // After some amount of calls, break the connection to avoid looping forever.
        virtual std::string socketRecv() override {
            if (++m_failsafeCounter == 5) {
                disconnect();
            }
            return "";
        }

    private:
        int m_failsafeCounter = 0;
        bool m_connected = false;
    };

    auto socket = std::make_shared<TCPSocketWithNoResponse>("127.0.0.1", 1234);
    auto client = IRCClient::test_createClient(socket);
    client->connect();
    client->beginProcessingMessages();

    REQUIRE_FALSE(socket->isConnected());
    REQUIRE(socket->failsafeCounter() == 1);
}

