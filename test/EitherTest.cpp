#include "Catch.h"

#include "Either.h"
#include <map>
#include <vector>

using namespace samebot;

TEST_CASE("Either") {
    {
        Either<int, float> either(5);
        CHECK(either.is<int>());
        CHECK_FALSE(either.is<float>());
        CHECK(either.as<int>() == 5);
    }

    {
        Either<int, float> either(3.14f);
        CHECK_FALSE(either.is<int>());
        CHECK(either.is<float>());
        CHECK(either.as<float>() == 3.14f);
    }

    {
        Either<int, float> either(1.f);
        CHECK_FALSE(either.is<int>());
        CHECK(either.is<float>());
        CHECK(either.as<float>() == 1);
    }

    {
        Either<std::string, int> either("Hello");
        CHECK(either.is<std::string>());
        CHECK_FALSE(either.is<int>());
        CHECK(either.as<std::string>() == "Hello");
    }

    {
        Either<std::string, int> either(123);
        CHECK_FALSE(either.is<std::string>());
        CHECK(either.is<int>());
        CHECK(either.as<int>() == 123);
    }

    {
        Either<std::string, std::vector<std::string>> either("Hello");
        CHECK(either.is<std::string>());
        CHECK_FALSE(either.is<std::vector<std::string>>());
        CHECK(either.as<std::string>() == "Hello");
    }

    {
        Either<std::string, std::vector<std::string>> either({ "hi", "how's", "it", "going" });
        CHECK_FALSE(either.is<std::string>());
        CHECK(either.is<std::vector<std::string>>());
        CHECK(either.as<std::vector<std::string>>() == std::vector<std::string>({ "hi", "how's", "it", "going" }));
    }

    {
        Either<std::map<std::string, std::vector<int>>, std::vector<std::string>> either({ "hi", "how's", "it", "going" });
        CHECK_FALSE((either.is<std::map<std::string, std::vector<int>>>()));
        CHECK(either.is<std::vector<std::string>>());
        CHECK(either.as<std::vector<std::string>>() == std::vector<std::string>({ "hi", "how's", "it", "going" }));
    }

    {
        Either<std::map<std::string, std::vector<int>>, std::vector<std::string>> either({
            { "key1", { 1, 2, 3 } },
            { "key2", { 3, 4, 5 } },
        });

        CHECK((either.is<std::map<std::string, std::vector<int>>>()));
        CHECK_FALSE(either.is<std::vector<std::string>>());
        CHECK((either.as<std::map<std::string, std::vector<int>>>().at("key1")) == std::vector<int>({ 1, 2, 3 }));
        CHECK((either.as<std::map<std::string, std::vector<int>>>().at("key2")) == std::vector<int>({ 3, 4, 5 }));
    }

    {
        Either<std::map<std::string, std::vector<int>>, std::vector<std::string>> either({
            { "key1", { 1, 2, 3 } },
            { "key2", { 3, 4, 5 } },
        });

        CHECK(either.is<decltype(either)::Left>());
        CHECK_FALSE(either.is<decltype(either)::Right>());
        CHECK(either.as<decltype(either)::Left>().at("key1") == std::vector<int>({ 1, 2, 3 }));
        CHECK(either.as<decltype(either)::Left>().at("key2") == std::vector<int>({ 3, 4, 5 }));
    }
}
