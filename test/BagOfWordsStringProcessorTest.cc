#include "Catch.h"

#include "bag_of_words/BagOfWordsDataStore.h"
#include "bag_of_words/BagOfWordsStringProcessor.h"
#include <map>

using namespace samebot;

class BagOfWordsInMemoryDataStore : public BagOfWordsDataStore {
public:
    virtual void countInterestingWord(std::string word) override {
        if (m_interestingCounts.find(word) == m_interestingCounts.end())
            m_interestingCounts[word] = 1;
        else
            ++m_interestingCounts[word];
    }

    virtual void countUninterestingWord(std::string word) override {
        if (m_uninterestingCounts.find(word) == m_uninterestingCounts.end())
            m_uninterestingCounts[word] = 1;
        else
            ++m_uninterestingCounts[word];
    }

    virtual BagOfWordsDataStore::WordCounts countsForWord(std::string word) override {
        int interestingCount = 0;
        if (m_interestingCounts.find(word) != m_interestingCounts.end()) {
            interestingCount = m_interestingCounts.at(word);
        }

        int uninterestingCount = 0;
        if (m_uninterestingCounts.find(word) != m_uninterestingCounts.end()) {
            uninterestingCount = m_uninterestingCounts.at(word);
        }

        return BagOfWordsDataStore::WordCounts { interestingCount, uninterestingCount };
    }

    virtual int totalInterestingWords() override {
        int sum = 0;
        for (const auto& pair : m_interestingCounts) {
            sum += pair.second;
        }
        return sum;
    }

    virtual int totalUninterestingWords() override {
        int sum = 0;
        for (const auto& pair : m_uninterestingCounts) {
            sum += pair.second;
        }
        return sum;
    }

private:
    std::map<std::string, int> m_interestingCounts;
    std::map<std::string, int> m_uninterestingCounts;
};

TEST_CASE("BagOfWordsStringProcessor") {
    BagOfWordsInMemoryDataStore dataStore;
    BagOfWordsStringProcessor processor(dataStore);

    processor.noteStringWasInteresting("I am an interesting string");
    REQUIRE(processor.probabilityOfStringBeingInteresting("Hi, I'm a string") == 1);
    REQUIRE(processor.probabilityOfStringBeingInteresting("I am also interesting") == 1);
    REQUIRE(processor.probabilityOfStringBeingInteresting("I, however, am not") == Approx(0.5));

    processor.noteStringWasUninteresting("Hello, I am a very uninteresting string");
    REQUIRE(processor.probabilityOfStringBeingInteresting("Hi, I'm a string") == Approx(0.666666));
    REQUIRE(processor.probabilityOfStringBeingInteresting("I am also interesting") == 1);
    REQUIRE(processor.probabilityOfStringBeingInteresting("I, however, am not") == Approx(0.5));
    REQUIRE(processor.probabilityOfStringBeingInteresting("I'm very uninteresting") == 0);
    REQUIRE(processor.probabilityOfStringBeingInteresting("I'm interesting and uninteresting") == 0);
}

