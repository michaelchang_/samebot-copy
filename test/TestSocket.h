#ifndef __TEST_SOCKET_H
#define __TEST_SOCKET_H

#include "network/Socket.h"
#include <deque>

class TestSocket : public samebot::Socket {
public:
    using samebot::Socket::Socket;

    virtual bool connect() override { return (m_connected = true); }
    virtual void disconnect() override { m_connected = false; }
    virtual bool isConnected() const override { return m_connected; }

    virtual void socketSend(std::string data) override { m_sendHandler(data); }
    virtual std::string socketRecv() override {
        if (m_responses.empty()) {
            return "";
        }

        auto response = m_responses.front();
        m_responses.pop_front();
        return response;
    }

    void setSendHandler(std::function<void (std::string)> sendHandler) { m_sendHandler = sendHandler; }
    void setResponses(std::initializer_list<std::string> responses) { m_responses = responses; }
    void setResponse(std::string response) { setResponses({ response }); }

private:
    std::function<void (std::string)> m_sendHandler;
    std::deque<std::string> m_responses;
    bool m_connected;
};

#endif // __TEST_SOCKET_H

