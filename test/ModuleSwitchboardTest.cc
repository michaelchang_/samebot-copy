#include "Catch.h"

#include "TestModule.h"
#include "modules/ModuleSwitchboard.h"
#include <chrono>

using namespace samebot;

TEST_CASE("ModuleSwitchboard") {
    std::mutex mutex;
    std::condition_variable didReceiveMessageCondition;
    bool didReceiveMessage = false;
    auto module = std::make_unique<TestModule>([&didReceiveMessage, &didReceiveMessageCondition](std::string) {
        didReceiveMessage = true;
        didReceiveMessageCondition.notify_one();
    });

    auto switchboard = std::make_shared<ModuleSwitchboard>();
    switchboard->registerModule(std::move(module));

    std::atomic_bool keepSendingMessages(true);
    std::thread continouslyReceiveMessages([switchboard, &keepSendingMessages] {
        while (keepSendingMessages) {
            switchboard->dispatchMessageToModules(std::make_shared<IRCMessage>("HELLO"));
            std::this_thread::sleep_for(std::chrono::milliseconds(1));
        }
    });

    std::unique_lock<std::mutex> lock(mutex);
    didReceiveMessageCondition.wait(lock, [&didReceiveMessage] { return didReceiveMessage; });
    REQUIRE(didReceiveMessage);

    keepSendingMessages = false;
    continouslyReceiveMessages.join();
}

