#include "Catch.h"

#include "network/irc/IRCMessagePrefix.h"

using namespace samebot;

TEST_CASE("IRCMessagePrefix") {
    IRCMessagePrefix prefix1("some server");
    REQUIRE(prefix1.type() == IRCMessagePrefix::Type::ServerName);
    REQUIRE(prefix1.serverName() == "some server");

    IRCMessagePrefix prefix2("anshu", "achimala", "127.0.0.1");
    REQUIRE(prefix2.type() == IRCMessagePrefix::Type::User);
    REQUIRE(prefix2.user().nickname == "anshu");
    REQUIRE(prefix2.user().username == "achimala");
    REQUIRE(prefix2.user().hostname == "127.0.0.1");
}

TEST_CASE("IRCMessagePrefix::toString") {
    REQUIRE(IRCMessagePrefix("some server").toString() == "some server");
    REQUIRE(IRCMessagePrefix("nick", "user", "host").toString() == "nick!user@host");
    REQUIRE(IRCMessagePrefix("nick", "", "host").toString() == "nick@host");
    REQUIRE(IRCMessagePrefix("nick", "user", "").toString() == "nick!user");
    REQUIRE(IRCMessagePrefix("nick", "", "").toString() == "nick");
    REQUIRE(IRCMessagePrefix("").toString() == "");
    REQUIRE(IRCMessagePrefix("", "", "").toString() == "");
}

