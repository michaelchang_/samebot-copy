#include "Catch.h"

#include "TestSocket.h"

using namespace samebot;

TEST_CASE("Socket::readUntilDelimiter()") {
    TestSocket socket("", 0);

    socket.setResponse("a string");
    CHECK(socket.readUntilDelimiter(';') == "a string");
    CHECK(socket.readUntilDelimiter(';') == "");

    socket.setResponse("a string;");
    CHECK(socket.readUntilDelimiter(';') == "a string;");
    CHECK(socket.readUntilDelimiter(';') == "");

    socket.setResponse("a string; that's what this is;");
    CHECK(socket.readUntilDelimiter(';') == "a string;");
    CHECK(socket.readUntilDelimiter(';') == " that's what this is;");
    CHECK(socket.readUntilDelimiter(';') == "");

    socket.setResponse(";this is also a string;");
    CHECK(socket.readUntilDelimiter(';') == ";");
    CHECK(socket.readUntilDelimiter(';') == "this is also a string;");
    CHECK(socket.readUntilDelimiter(';') == "");

    socket.setResponse(";wow;what;;a;string;");
    CHECK(socket.readUntilDelimiter(';') == ";");
    CHECK(socket.readUntilDelimiter(';') == "wow;");
    CHECK(socket.readUntilDelimiter(';') == "what;");
    CHECK(socket.readUntilDelimiter(';') == ";");
    CHECK(socket.readUntilDelimiter(';') == "a;");
    CHECK(socket.readUntilDelimiter(';') == "string;");
    CHECK(socket.readUntilDelimiter(';') == "");

    socket.setResponse(";");
    CHECK(socket.readUntilDelimiter(';') == ";");
    CHECK(socket.readUntilDelimiter(';') == "");
    CHECK(socket.readUntilDelimiter(';') == "");

    socket.setResponse(";;");
    CHECK(socket.readUntilDelimiter(';') == ";");
    CHECK(socket.readUntilDelimiter(';') == ";");
    CHECK(socket.readUntilDelimiter(';') == "");
    CHECK(socket.readUntilDelimiter(';') == "");

    socket.setResponse("");
    CHECK(socket.readUntilDelimiter(';') == "");
    CHECK(socket.readUntilDelimiter(';') == "");

    socket.setResponses({ "a string", "a string", "a string;" });
    CHECK(socket.readUntilDelimiter(';') == "a stringa stringa string;");
    CHECK(socket.readUntilDelimiter(';') == "");

    socket.setResponses({ "a string", "a str;in;g", "a ;string;" });
    CHECK(socket.readUntilDelimiter(';') == "a stringa str;");
    CHECK(socket.readUntilDelimiter(';') == "in;");
    CHECK(socket.readUntilDelimiter(';') == "ga ;");
    CHECK(socket.readUntilDelimiter(';') == "string;");
    CHECK(socket.readUntilDelimiter(';') == "");
    CHECK(socket.readUntilDelimiter(';') == "");
}

