#include "Catch.h"
#include <string.h>

#include "basics/StringUtilities.h"
#include "modules/scrapbook/ScrapbookSQLiteDataStore.h"

static const char* testDatabasePath = "/tmp/test_scrapbook_database.db";

using namespace samebot;

TEST_CASE("ScrapbookSQLiteDataStore") {
    remove(testDatabasePath);

    ScrapbookSQLiteDataStore dataStore(testDatabasePath);
    dataStore.storeMemory(std::make_unique<ScrapbookMemory>("memory1", "context1"));

    auto memory = dataStore.retrieveRandomMemory();
    REQUIRE(memory->memory() == "memory1");
    REQUIRE(memory->context() == "context1");

    for (int i = 1; i < 20; ++i) {
        dataStore.storeMemory(std::make_unique<ScrapbookMemory>("memory" + std::to_string(i % 5), "context" + std::to_string(i % 5)));
    }

    for (int i = 1; i < 20; ++i) {
        auto memory = dataStore.retrieveRandomMemory();
        REQUIRE(stringHasPrefix(memory->memory(), "memory"));
        auto number = memory->memory().substr(strlen("memory"));
        REQUIRE(memory->context() == "context" + number);
    }

    //remove(testDatabasePath);
}

TEST_CASE("ScrapbookSQLiteDataStore::findMemoryContainingString") {
    remove(testDatabasePath);

    ScrapbookSQLiteDataStore dataStore(testDatabasePath);
    for (int i = 1; i <= 20; ++i) {
        dataStore.storeMemory(std::make_unique<ScrapbookMemory>("memory" + std::to_string(i), "context" + std::to_string(i)));
    }

    // This case is completely unambiguous. There is only one matching memory.
    auto memory = dataStore.findMemoryContainingString("memory13");
    REQUIRE(memory);
    REQUIRE(memory->memory() == "memory13");
    REQUIRE(memory->context() == "context13");

    // This is ambiguous. We should prefer the memory with an exact word match.
    memory = dataStore.findMemoryContainingString("memory1");
    REQUIRE(memory);
    REQUIRE(memory->memory() == "memory1");
    REQUIRE(memory->context() == "context1");

    // This is ambiguous. There are no exact matches, so we should prefer the most recent match.
    memory = dataStore.findMemoryContainingString("emory2");
    REQUIRE(memory);
    REQUIRE(memory->memory() == "memory20");
    REQUIRE(memory->context() == "context20");

    // This is ambiguous. There are no exact matches, so we should prefer the most recent match.
    memory = dataStore.findMemoryContainingString("emory1");
    REQUIRE(memory);
    REQUIRE(memory->memory() == "memory19");
    REQUIRE(memory->context() == "context19");

    remove(testDatabasePath);
}

