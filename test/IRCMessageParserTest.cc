#include "Catch.h"

#include "network/irc/IRCMessageParser.h"

using namespace samebot;

TEST_CASE("IRCMessageParser") {
    SECTION("NOTICE AUTH") {
        IRCMessageParser parser(":hybrid8.localhost.localdomain NOTICE AUTH :*** Looking up your hostname...\r\n");
        auto* message = parser.message();
        REQUIRE(message);

        REQUIRE(message->prefix());
        REQUIRE(message->prefix()->type() == IRCMessagePrefix::Type::ServerName);
        REQUIRE(message->prefix()->serverName() == "hybrid8.localhost.localdomain");

        REQUIRE(message->command() == "NOTICE");
        REQUIRE(message->parameters() == std::vector<std::string>({ "AUTH", "*** Looking up your hostname..." }));
    }

    SECTION("HELLO") {
        IRCMessageParser parser("HELLO\r\n");
        auto* message = parser.message();
        REQUIRE(message);

        REQUIRE(!message->prefix());
        REQUIRE(message->command() == "HELLO");
        REQUIRE(message->parameters().empty());
    }

    SECTION("001") {
        IRCMessageParser parser(":hybrid8.localhost.localdomain 001 test :Welcome to the arch Internet Relay Chat Network test\r\n");
        auto* message = parser.message();
        REQUIRE(message);

        REQUIRE(message->prefix());
        REQUIRE(message->prefix()->type() == IRCMessagePrefix::Type::ServerName);
        REQUIRE(message->prefix()->serverName() == "hybrid8.localhost.localdomain");

        REQUIRE(message->command() == "001");
        REQUIRE(message->parameters() == std::vector<std::string>({ "test", "Welcome to the arch Internet Relay Chat Network test" }));
    }

    SECTION("MODE") {
        IRCMessageParser parser(":nick-name!test@192.168.1.1 MODE test :+i\r\n");
        auto* message = parser.message();
        REQUIRE(message);

        REQUIRE(message->prefix());
        REQUIRE(message->prefix()->type() == IRCMessagePrefix::Type::User);
        REQUIRE(message->prefix()->user().nickname == "nick-name");
        REQUIRE(message->prefix()->user().username == "test");
        REQUIRE(message->prefix()->user().hostname == "192.168.1.1");

        REQUIRE(message->command() == "MODE");
        REQUIRE(message->parameters() == std::vector<std::string>({ "test", "+i" }));
    }

    SECTION("PING") {
        IRCMessageParser parser("PING :hybrid8.localhost.localdomain\r\n");
        auto* message = parser.message();
        REQUIRE(message);

        REQUIRE(!message->prefix());
        REQUIRE(message->command() == "PING");
        REQUIRE(message->parameters() == std::vector<std::string>({ "hybrid8.localhost.localdomain" }));
    }

    SECTION("Issue #93") {
        IRCMessageParser parser(":anshu!anshu@c-67-180-22-89.hsd1.ca.comcast.net PRIVMSG #samebot-dev :samebot: hello\r\n");
        auto* message = parser.message();
        REQUIRE(message);
        REQUIRE(message->prefix());
        REQUIRE(message->prefix()->type() == IRCMessagePrefix::Type::User);
        CHECK(message->prefix()->user().nickname == "anshu");
        CHECK(message->prefix()->user().username == "anshu");
        CHECK(message->prefix()->user().hostname == "c-67-180-22-89.hsd1.ca.comcast.net");
        CHECK(message->command() == "PRIVMSG");
        CHECK(message->parameters() == std::vector<std::string>({ "#samebot-dev", "samebot: hello" }));
    }
}

TEST_CASE("IRCMessageParser::tryParseStringWithCharacterValidator") {
    IRCMessageParserTestShim shim(std::make_shared<IRCMessageParser>("hello this is a 123 test string!"));
    REQUIRE(shim.tryParseStringWithCharacterValidator([](auto c) { return !isspace(c); }) == "hello");
    REQUIRE(shim.tryParseStringWithCharacterValidator([](auto c) { return !isdigit(c); }) == " this is a ");
    REQUIRE(shim.tryParseStringWithCharacterValidator([](auto c) { return c != '!'; }) == "123 test string");
    REQUIRE(shim.tryParseStringWithCharacterValidator([](auto c) { return c != '!'; }) == "");
    REQUIRE(shim.tryParseStringWithCharacterValidator([](auto c) { return true; }) == "!");
    REQUIRE(shim.tryParseStringWithCharacterValidator([](auto c) { return true; }) == "");
}

TEST_CASE("IRCMessageParser::tryParseMessagePrefix") {
    SECTION("Server name prefix") {
        auto parser = std::make_shared<IRCMessageParser>(":hybrid8.localhost.localdomain NOTICE AUTH :*** Looking up your hostname...");
        IRCMessageParserTestShim shim(parser);
        std::unique_ptr<IRCMessagePrefix> prefix = shim.tryParseMessagePrefix();
        REQUIRE(prefix);
        REQUIRE(prefix->type() == IRCMessagePrefix::Type::ServerName);
        REQUIRE(prefix->serverName() == "hybrid8.localhost.localdomain");

        parser = std::make_shared<IRCMessageParser>(":127.0.0.1 NOTICE AUTH :*** Looking up your hostname...");
        shim = IRCMessageParserTestShim(parser);
        prefix = shim.tryParseMessagePrefix();
        REQUIRE(prefix);
        REQUIRE(prefix->type() == IRCMessagePrefix::Type::ServerName);
        REQUIRE(prefix->serverName() == "127.0.0.1");
    }

    SECTION("User prefix") {
        auto parser = std::make_shared<IRCMessageParser>(":nick!test@192.168.1.1 MODE test :+i");
        IRCMessageParserTestShim shim(parser);

        std::unique_ptr<IRCMessagePrefix> prefix = shim.tryParseMessagePrefix();
        REQUIRE(prefix);
        REQUIRE(prefix->type() == IRCMessagePrefix::Type::User);
        REQUIRE(prefix->user().nickname == "nick");
        REQUIRE(prefix->user().username == "test");
        REQUIRE(prefix->user().hostname == "192.168.1.1");
    }
}

TEST_CASE("IRCMessageParser::tryParseCommand") {
    IRCMessageParserTestShim shim(std::make_shared<IRCMessageParser>("PING :hybrid8.localhost.localdomain"));
    REQUIRE(shim.tryParseCommand() == "PING");


    shim = IRCMessageParserTestShim(std::make_shared<IRCMessageParser>("123 :hybrid8.localhost.localdomain"));
    REQUIRE(shim.tryParseCommand() == "123");

    shim = IRCMessageParserTestShim(std::make_shared<IRCMessageParser>("1234 :hybrid8.localhost.localdomain"));
    REQUIRE(shim.tryParseCommand() == "123");

    shim = IRCMessageParserTestShim(std::make_shared<IRCMessageParser>("12 :hybrid8.localhost.localdomain"));
    REQUIRE(shim.tryParseCommand() == "");

    shim = IRCMessageParserTestShim(std::make_shared<IRCMessageParser>("hello :hybrid8.localhost.localdomain"));
    REQUIRE(shim.tryParseCommand() == "hello");

    shim = IRCMessageParserTestShim(std::make_shared<IRCMessageParser>("001 :hybrid8.localhost.localdomain"));
    REQUIRE(shim.tryParseCommand() == "001");
}

TEST_CASE("IRCMessageParser::tryParseParameters") {
    IRCMessageParserTestShim shim(std::make_shared<IRCMessageParser>(" :Welcome to the arch Internet Relay Chat Network test"));
    REQUIRE(shim.tryParseParameters() == std::vector<std::string>({ "Welcome to the arch Internet Relay Chat Network test" }));

    shim = IRCMessageParserTestShim(std::make_shared<IRCMessageParser>(" 0AAAAAABH :your unique ID"));
    REQUIRE(shim.tryParseParameters() == std::vector<std::string>({ "0AAAAAABH", "your unique ID" }));

    shim = IRCMessageParserTestShim(std::make_shared<IRCMessageParser>(" CHANTYPES=# CHANLIMIT=#:25 CHANNELLEN=50 TOPICLEN=300 CHANMODES=beI,k,l,imnprstORS ELIST=CMNTU SAFELIST WATCH=60 KNOCK AWAYLEN=180 :are supported by this server"));
    REQUIRE(shim.tryParseParameters() == std::vector<std::string>({ "CHANTYPES=#", "CHANLIMIT=#:25", "CHANNELLEN=50", "TOPICLEN=300", "CHANMODES=beI,k,l,imnprstORS", "ELIST=CMNTU", "SAFELIST", "WATCH=60", "KNOCK", "AWAYLEN=180", "are supported by this server" }));

    shim = IRCMessageParserTestShim(std::make_shared<IRCMessageParser>(" :Current local users: 9  Max: 9"));
    REQUIRE(shim.tryParseParameters() == std::vector<std::string>({ "Current local users: 9  Max: 9" }));
}

