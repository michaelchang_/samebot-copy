#include "Catch.h"

#include "modules/scrapbook/ScrapbookDataStoreSearcher.h"
#include "modules/scrapbook/ScrapbookMemory.h"
#include "modules/scrapbook/ScrapbookSQLiteDataStore.h"
#include "network/irc/IRCMessage.h"

static const char* testDatabasePath = "/tmp/test_scrapbook_database.db";

using namespace samebot;

static std::shared_ptr<IRCMessage> ircMessage(std::string message) {
    auto prefix = std::make_shared<IRCMessagePrefix>("", "", "");
    return std::make_shared<IRCMessage>(prefix, "PRIVMSG", std::vector<std::string> { "#somechannel", message });
}

TEST_CASE("ScrapbookDataStoreSearcher") {
    remove(testDatabasePath);

    std::vector<std::string> memories {
        "memory",
        "another memory",
        "this is a long sentence",
        "here is a memory",
        "the quick brown fox jumped over the lazy dog",
        "the quick brown fox jumped over the lazy dog and into this SQLite data store",
    };

    ScrapbookSQLiteDataStore dataStore(testDatabasePath);
    for (auto memory : memories) {
        dataStore.storeMemory(std::make_unique<ScrapbookMemory>(memory, "context doesn't matter"));
    }

    ScrapbookDataStoreSearcher searcher(dataStore);
    auto memory = searcher.tryToSearchDataStoreForMemory(ircMessage("find memory"));
    REQUIRE(memory);
    REQUIRE(memory->memory() == "memory");

    memory = searcher.tryToSearchDataStoreForMemory(ircMessage("samebot: find memory"));
    REQUIRE(memory);
    REQUIRE(memory->memory() == "memory");

    memory = searcher.tryToSearchDataStoreForMemory(ircMessage("samebot: please help me find the memory that says memory"));
    REQUIRE(memory);
    REQUIRE(memory->memory() == "memory");

    memory = searcher.tryToSearchDataStoreForMemory(ircMessage("samebot: search for fox"));
    REQUIRE(memory);
    REQUIRE(memory->memory() == "the quick brown fox jumped over the lazy dog and into this SQLite data store");

    memory = searcher.tryToSearchDataStoreForMemory(ircMessage("samebot: search for sqlite"));
    REQUIRE(memory);
    REQUIRE(memory->memory() == "the quick brown fox jumped over the lazy dog and into this SQLite data store");

    memory = searcher.tryToSearchDataStoreForMemory(ircMessage("find when someone said something about a sentence"));
    REQUIRE(memory);
    REQUIRE(memory->memory() == "this is a long sentence");

    memory = searcher.tryToSearchDataStoreForMemory(ircMessage("find a memory about another memory"));
    REQUIRE(memory);
    REQUIRE(memory->memory() == "another memory");

    memory = searcher.tryToSearchDataStoreForMemory(ircMessage("find something"));
    REQUIRE(!memory);

    memory = searcher.tryToSearchDataStoreForMemory(ircMessage("find words that don't exist"));
    REQUIRE(!memory);

    memory = searcher.tryToSearchDataStoreForMemory(ircMessage("samebot: find something"));
    REQUIRE(!memory);

    memory = searcher.tryToSearchDataStoreForMemory(ircMessage("samebot: give me a memory"));
    REQUIRE(!memory);

    memory = searcher.tryToSearchDataStoreForMemory(ircMessage("samebot: good memory"));
    REQUIRE(!memory);

    memory = searcher.tryToSearchDataStoreForMemory(ircMessage("samebot: remember a quick brown fox"));
    REQUIRE(!memory);

    remove(testDatabasePath);
}

