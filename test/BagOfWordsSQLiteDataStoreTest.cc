#include "Catch.h"

#include "bag_of_words/BagOfWordsSQLiteDataStore.h"

using namespace samebot;

static std::string testDatabasePath = "/tmp/test_bag_of_words.db";

TEST_CASE("BagOfWordsSQLiteDataStore") {
    remove(testDatabasePath.c_str());

    BagOfWordsSQLiteDataStore dataStore(testDatabasePath);

    BagOfWordsSQLiteDataStore::WordCounts counts = dataStore.countsForWord("test");
    REQUIRE(!counts.interestingCount);
    REQUIRE(!counts.uninterestingCount);

    REQUIRE(!dataStore.totalInterestingWords());
    REQUIRE(!dataStore.totalUninterestingWords());

    dataStore.countInterestingWord("test");
    dataStore.countInterestingWord("Test");
    dataStore.countInterestingWord("TEST");
    dataStore.countInterestingWord("    TEST");
    dataStore.countUninterestingWord(" \r\n   tEsT\r\n ");
    dataStore.countUninterestingWord("\ttEst\r\n ");

    REQUIRE(dataStore.totalInterestingWords() == 4);
    REQUIRE(dataStore.totalUninterestingWords() == 2);

    dataStore.countInterestingWord("testt");
    dataStore.countUninterestingWord("te stt");
    dataStore.countUninterestingWord("te$stt");

    counts = dataStore.countsForWord("test");
    REQUIRE(counts.interestingCount == 4);
    REQUIRE(counts.uninterestingCount == 2);

    REQUIRE(dataStore.totalInterestingWords() == 5);
    REQUIRE(dataStore.totalUninterestingWords() == 4);

    counts = dataStore.countsForWord("testt");
    REQUIRE(counts.interestingCount == 1);
    REQUIRE(counts.uninterestingCount == 2);

    counts = dataStore.countsForWord("te stt");
    REQUIRE(counts.interestingCount == 1);
    REQUIRE(counts.uninterestingCount == 2);

    remove(testDatabasePath.c_str());
}

