#ifndef __TEST_USER_INTELLIGENCE_CLIENT_MESSAGE_SENDER_H
#define __TEST_USER_INTELLIGENCE_CLIENT_MESSAGE_SENDER_H

#include "intelligence/UserIntelligenceClient.h"

namespace samebot {

struct TestUserIntelligenceClientMessageSender : public UserIntelligenceClientMessageSender {
    virtual void sendPublicMessageForUserIntelligenceClient(UserIntelligenceClient&, const std::string& message) override {
        sendPublicMessageHandler(message);
    }

    virtual void sendPrivateMessageForUserIntelligenceClient(UserIntelligenceClient&, const std::string& recipient, const std::string& message) override {
        sendPrivateMessageHandler(recipient, message);
    }

    std::function<void (const std::string& message)> sendPublicMessageHandler;
    std::function<void (const std::string& recipient, const std::string& message)> sendPrivateMessageHandler;
};

} // namespace

#endif // __TEST_USER_INTELLIGENCE_CLIENT_MESSAGE_SENDER_H
