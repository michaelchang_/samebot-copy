#include "Catch.h"

#include "modules/lyft/LyftQueryMetadataExtractor.h"

using namespace samebot;

TEST_CASE("LyftQueryMetadataExtractor::extractStartAndEndAddressesForRideEstimateRequest") {
    LyftQueryMetadataExtractor extractor;
    std::string startAddress;
    std::string endAddress;
    CHECK_FALSE(extractor.extractStartAndEndAddressesForRideEstimateRequest("hello how's it going", startAddress, endAddress));
    CHECK_FALSE(extractor.extractStartAndEndAddressesForRideEstimateRequest("lyft is much better than uber", startAddress, endAddress));
    CHECK_FALSE(extractor.extractStartAndEndAddressesForRideEstimateRequest("lyft from A to B was lots of fun", startAddress, endAddress));
    CHECK_FALSE(extractor.extractStartAndEndAddressesForRideEstimateRequest("lyft prices have been going up", startAddress, endAddress));
    CHECK_FALSE(extractor.extractStartAndEndAddressesForRideEstimateRequest("the cost of a lyft from 2015 was lower than it is now", startAddress, endAddress));
    CHECK_FALSE(extractor.extractStartAndEndAddressesForRideEstimateRequest("a lyft from 2 Infinite Loop to SJC airport sounds really fun", startAddress, endAddress));
    CHECK_FALSE(extractor.extractStartAndEndAddressesForRideEstimateRequest("how much would it cost to travel from 2 Infinite Loop to SJC airport?", startAddress, endAddress));
    CHECK_FALSE(extractor.extractStartAndEndAddressesForRideEstimateRequest("how much would it cost to lyft from 2 Infinite Loop SJC airport?", startAddress, endAddress));
    CHECK_FALSE(extractor.extractStartAndEndAddressesForRideEstimateRequest("how much would it cost to lyft 2 Infinite Loop to SJC airport?", startAddress, endAddress));
    CHECK_FALSE(extractor.extractStartAndEndAddressesForRideEstimateRequest("from how much would it cost to lyft 2 Infinite Loop to SJC airport?", startAddress, endAddress));

    CHECK(extractor.extractStartAndEndAddressesForRideEstimateRequest("how much would it cost to lyft from 2 Infinite Loop to SJC airport?", startAddress, endAddress));
    CHECK(startAddress == "2 Infinite Loop");
    CHECK(endAddress == "SJC airport?");

    CHECK(extractor.extractStartAndEndAddressesForRideEstimateRequest("price of a lyft from 2 Infinite Loop to 12345 Stevens Creek Blvd, Cupertino, CA", startAddress, endAddress));
    CHECK(startAddress == "2 Infinite Loop");
    CHECK(endAddress == "12345 Stevens Creek Blvd, Cupertino, CA");

    CHECK(extractor.extractStartAndEndAddressesForRideEstimateRequest("price of a lyft from 12345 Stevens Creek Blvd, Cupertino, CA to 2 Infinite Loop", startAddress, endAddress));
    CHECK(startAddress == "12345 Stevens Creek Blvd, Cupertino, CA");
    CHECK(endAddress == "2 Infinite Loop");

    CHECK(extractor.extractStartAndEndAddressesForRideEstimateRequest("how much is a lyft from point A to point B?", startAddress, endAddress));
    CHECK(startAddress == "point A");
    CHECK(endAddress == "point B?");
}
