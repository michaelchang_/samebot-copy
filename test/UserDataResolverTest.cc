#include "Catch.h"

#include "TestUserIntelligenceClientMessageSender.h"
#include "intelligence/UserDataResolver.h"
#include "intelligence/UserIntelligenceServer.h"
#include "network/irc/IRCMessage.h"

using namespace samebot;

TEST_CASE("UserDataResolver") {
    class TestUserDataResolver : public UserDataResolver {
    public:
        using UserDataResolver::UserDataResolver;

    private:
        virtual std::string userInformationKeyForData(const std::string& data) override {
            if (data == "home") {
                return "home_address";
            }

            return "";
        }

        virtual std::string responseToUserRequestInvolvingDataWithKey(const std::string& key) override {
            if (key == "home_address") {
                return "I don't know your home address";
            }

            return "";
        }

        virtual std::string privatePromptForInformationWithKey(const std::string& key) override {
            if (key == "home_address") {
                return "Please PM me your home address";
            }

            return "";
        }
    };

    remove("/tmp/test_user_intelligence.db");

    TestUserIntelligenceClientMessageSender messageSender;

    bool didSendResponse = false;
    messageSender.sendPublicMessageHandler = [&didSendResponse](auto message) {
        CHECK(message == "anshu: I don't know your home address");
        CHECK_FALSE(didSendResponse);
        didSendResponse = true;
    };

    bool didSendPrompt = false;
    messageSender.sendPrivateMessageHandler = [&didSendPrompt](auto nickname, auto message) {
        CHECK(nickname == "anshu");
        CHECK(message == "Please PM me your home address");
        didSendPrompt = true;
    };

    UserIntelligenceServer server("/tmp/test_user_intelligence.db");
    auto client = server.createClient(messageSender);

    CHECK_FALSE(didSendResponse);
    CHECK_FALSE(didSendPrompt);

    TestUserDataResolver resolver(*client);
    bool didResolveData = false;
    resolver.resolve("something", "~anshu", "anshu", InResponseToPrivateMessage::No, [&didResolveData](auto data) {
        CHECK(data == "something");
        CHECK_FALSE(didResolveData);
        didResolveData = true;
    });

    CHECK_FALSE(didSendResponse);
    CHECK_FALSE(didSendPrompt);
    CHECK(didResolveData);

    client->setUserDataForKey("~mchang", "home_address", "some address");
    didResolveData = false;
    resolver.resolve("home", "~mchang", "mchang", InResponseToPrivateMessage::No, [&didResolveData](auto data) {
        CHECK(data == "some address");
        CHECK_FALSE(didResolveData);
        didResolveData = true;
    });

    CHECK_FALSE(didSendResponse);
    CHECK_FALSE(didSendPrompt);
    CHECK(didResolveData);

    didResolveData = false;
    resolver.resolve("home", "~anshu", "anshu", InResponseToPrivateMessage::No, [&didResolveData](auto data) {
        CHECK(data == "anshu's address");
        CHECK_FALSE(didResolveData);
        didResolveData = true;
    });

    CHECK(didSendResponse);
    CHECK(didSendPrompt);
    CHECK_FALSE(didResolveData);

    client->processPrivateMessage(IRCMessage(std::make_shared<IRCMessagePrefix>("anshu", "~anshu", ""), "PRIVMSG", { "samebot", "anshu's address" }));

    CHECK(didResolveData);

    remove("/tmp/test_user_intelligence.db");
}
