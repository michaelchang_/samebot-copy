#include "Catch.h"

#include "TestUserIntelligenceClientMessageSender.h"
#include "intelligence/UserInformationRequest.h"
#include "intelligence/UserIntelligenceClient.h"
#include "intelligence/UserIntelligenceServer.h"
#include "network/irc/IRCMessage.h"

using namespace samebot;

TEST_CASE("UserIntelligenceClient basic functionality") {
    remove("/tmp/test_user_intelligence.db");

    UserIntelligenceServer server("/tmp/test_user_intelligence.db");
    TestUserIntelligenceClientMessageSender messageSender;
    auto client = server.createClient(messageSender);

    bool didSendPublicMessage = false;
    messageSender.sendPublicMessageHandler = [&didSendPublicMessage](auto message) {
        CHECK(message == "anshu: I don't know your password");
        CHECK_FALSE(didSendPublicMessage);
        didSendPublicMessage = true;
    };

    bool didSendPrivateMessage = false;
    messageSender.sendPrivateMessageHandler = [&didSendPrivateMessage](auto recipient, auto message) {
        CHECK(recipient == "anshu");
        CHECK(message == "give me your password");
        CHECK_FALSE(didSendPrivateMessage);
        didSendPrivateMessage = true;
    };

    CHECK_FALSE(didSendPublicMessage);
    CHECK_FALSE(didSendPrivateMessage);

    bool didReceivePassword = false;
    UserInformationRequest request("~anshu", "anshu", "password", "I don't know your password", "give me your password");
    client->requestInformationFromUser(request, [&didReceivePassword](auto password) {
        CHECK(password == "hunter2");
        CHECK_FALSE(didReceivePassword);
        didReceivePassword = true;
    });

    CHECK(didSendPublicMessage);
    CHECK(didSendPrivateMessage);

    CHECK(client->userDataForKey("anshu", "password") == "");
    CHECK(client->userDataForKey("Anshu", "password") == "");
    CHECK(client->userDataForKey(" AnShU  ", "password") == "");
    CHECK(client->userDataForKey("an shu", "password") == "");
    CHECK(client->userDataForKey("achimala", "password") == "");
    CHECK(client->userDataForKey("gavin", "password") == "");

    CHECK_FALSE(didReceivePassword);
    client->processPrivateMessage(IRCMessage(std::make_shared<IRCMessagePrefix>("anshu", "~anshu", ""), "PRIVMSG", { "samebot", "hunter2" }));
    CHECK(didReceivePassword);

    CHECK(client->userDataForKey("~anshu", "password") == "hunter2");
    CHECK(client->userDataForKey("~Anshu", "password") == "hunter2");
    CHECK(client->userDataForKey(" ~AnShU  ", "password") == "hunter2");
    CHECK(client->userDataForKey("~an shu", "password") == "");
    CHECK(client->userDataForKey("anshu", "password") == "");
    CHECK(client->userDataForKey("~gavin", "password") == "");

    client->setUserDataForKey("~gavin", "password", "i d0n't care");

    CHECK(client->userDataForKey("~anshu", "password") == "hunter2");
    CHECK(client->userDataForKey("~Anshu", "password") == "hunter2");
    CHECK(client->userDataForKey(" ~AnShU  ", "password") == "hunter2");
    CHECK(client->userDataForKey("~an shu", "password") == "");
    CHECK(client->userDataForKey("anshu", "password") == "");
    CHECK(client->userDataForKey("~gavin", "password") == "i d0n't care");

    remove("/tmp/test_user_intelligence.db");
}

TEST_CASE("UserIntelligenceClient responses to private messages") {
    remove("/tmp/test_user_intelligence.db");

    UserIntelligenceServer server("/tmp/test_user_intelligence.db");
    TestUserIntelligenceClientMessageSender messageSender;
    auto client = server.createClient(messageSender);

    messageSender.sendPublicMessageHandler = [](auto) {
        FAIL("UserIntelligenceClient should never send public messages in response to private messages");
    };

    bool didSendResponse = false;
    bool didSendPrompt = false;
    messageSender.sendPrivateMessageHandler = [&didSendResponse, &didSendPrompt](auto recipient, auto message) {
        CHECK(recipient == "anshu");

        if (!didSendResponse) {
            CHECK(message == "I don't know your password");
            didSendResponse = true;
            return;
        }

        if (!didSendPrompt) {
            CHECK(message == "give me your password");
            didSendPrompt = true;
            return;
        }

        FAIL("UserIntelligenceClient tried to send an unrecognized private message");
    };

    CHECK(client->userDataForKey("~anshu", "password") == "");
    CHECK_FALSE(didSendResponse);
    CHECK_FALSE(didSendPrompt);

    bool didReceivePassword = false;
    UserInformationRequest request("~anshu", "anshu", "password", "I don't know your password", "give me your password", InResponseToPrivateMessage::Yes);
    client->requestInformationFromUser(request, [&didReceivePassword](auto password) {
        CHECK(password == "hunter2");
        CHECK_FALSE(didReceivePassword);
        didReceivePassword = true;
    });

    CHECK(client->userDataForKey("~anshu", "password") == "");
    CHECK(didSendResponse);
    CHECK(didSendPrompt);

    CHECK_FALSE(didReceivePassword);
    client->processPrivateMessage(IRCMessage(std::make_shared<IRCMessagePrefix>("anshu", "~anshu", ""), "PRIVMSG", { "samebot", "hunter2" }));
    CHECK(didReceivePassword);

    CHECK(client->userDataForKey("~anshu", "password") == "hunter2");

    remove("/tmp/test_user_intelligence.db");
}

TEST_CASE("UserIntelligenceClient request deferral (single-user)") {
    remove("/tmp/test_user_intelligence.db");

    UserIntelligenceServer server("/tmp/test_user_intelligence.db");
    TestUserIntelligenceClientMessageSender messageSender;
    auto client = server.createClient(messageSender);

    bool didSendPublicMessageForPassword = false;
    bool didSendPublicMessageForAddress = false;
    messageSender.sendPublicMessageHandler = [&didSendPublicMessageForPassword, &didSendPublicMessageForAddress](auto message) {
        if (message == "anshu: I don't know your password") {
            CHECK_FALSE(didSendPublicMessageForPassword);
            didSendPublicMessageForPassword = true;
            return;
        }

        if (message == "anshu: I don't know your address") {
            CHECK_FALSE(didSendPublicMessageForAddress);
            didSendPublicMessageForAddress = true;
            return;
        }

        FAIL();
    };

    bool didSendPrivateMessageForPassword = false;
    bool didSendPrivateMessageForAddress = false;
    messageSender.sendPrivateMessageHandler = [&didSendPrivateMessageForPassword, &didSendPrivateMessageForAddress](auto recipient, auto message) {
        CHECK(recipient == "anshu");
        if (message == "give me your password") {
            CHECK_FALSE(didSendPrivateMessageForPassword);
            didSendPrivateMessageForPassword = true;
            return;
        }

        if (message == "give me your address") {
            CHECK_FALSE(didSendPrivateMessageForAddress);
            didSendPrivateMessageForAddress = true;
            return;
        }

        FAIL();
    };

    CHECK_FALSE(didSendPublicMessageForPassword);
    CHECK_FALSE(didSendPublicMessageForAddress);
    CHECK_FALSE(didSendPrivateMessageForPassword);
    CHECK_FALSE(didSendPrivateMessageForAddress);

    bool didReceivePassword = false;
    UserInformationRequest passwordRequest("~anshu", "anshu", "password", "I don't know your password", "give me your password");
    client->requestInformationFromUser(passwordRequest, [&didReceivePassword](auto password) {
        CHECK(password == "hunter2");
        CHECK_FALSE(didReceivePassword);
        didReceivePassword = true;
    });

    bool didReceiveAddress = false;
    UserInformationRequest addressRequest("~anshu", "anshu", "address", "I don't know your address", "give me your address");
    client->requestInformationFromUser(addressRequest, [&didReceiveAddress](auto address) {
        CHECK(address == "123 Some St");
        CHECK_FALSE(didReceiveAddress);
        didReceiveAddress = true;
    });

    // Both public messages should be sent immediately, in response to whatever user request triggered the data lookups.
    CHECK(didSendPublicMessageForPassword);
    CHECK(didSendPublicMessageForAddress);

    // Only the first private prompt should've been sent at this time; there's only one active request to the user at a time.
    CHECK(didSendPrivateMessageForPassword);
    CHECK_FALSE(didSendPrivateMessageForAddress);

    CHECK(client->userDataForKey("~anshu", "password") == "");
    CHECK(client->userDataForKey("~anshu", "address") == "");

    CHECK_FALSE(didReceivePassword);
    CHECK_FALSE(didReceiveAddress);

    client->processPrivateMessage(IRCMessage(std::make_shared<IRCMessagePrefix>("anshu", "~anshu", ""), "PRIVMSG", { "samebot", "hunter2" }));

    CHECK(didReceivePassword);
    CHECK_FALSE(didReceiveAddress);

    CHECK(client->userDataForKey("~anshu", "password") == "hunter2");
    CHECK(client->userDataForKey("~anshu", "address") == "");

    // After fulfilling the first request, we should've prompted for the next request.
    CHECK(didSendPrivateMessageForPassword);
    CHECK(didSendPrivateMessageForAddress);

    client->processPrivateMessage(IRCMessage(std::make_shared<IRCMessagePrefix>("anshu", "~anshu", ""), "PRIVMSG", { "samebot", "123 Some St" }));

    CHECK(didReceivePassword);
    CHECK(didReceiveAddress);

    CHECK(client->userDataForKey("~anshu", "password") == "hunter2");
    CHECK(client->userDataForKey("~anshu", "address") == "123 Some St");

    remove("/tmp/test_user_intelligence.db");
}

TEST_CASE("UserIntelligenceClient request deferral (multi-user)") {
    remove("/tmp/test_user_intelligence.db");

    UserIntelligenceServer server("/tmp/test_user_intelligence.db");
    TestUserIntelligenceClientMessageSender messageSender;
    auto client = server.createClient(messageSender);

    bool didSendPublicMessageForPasswordToAnshu = false;
    bool didSendPublicMessageForPasswordToGavin = false;
    bool didSendPublicMessageForAddressToAnshu = false;
    bool didSendPublicMessageForPhoneNumberToTyrus = false;
    bool didSendPublicMessageForEmailToGavin = false;
    messageSender.sendPublicMessageHandler = [&](auto message) {
        if (message == "anshu: I don't know your password") {
            CHECK_FALSE(didSendPublicMessageForPasswordToAnshu);
            didSendPublicMessageForPasswordToAnshu = true;
            return;
        }

        if (message == "anshu: I don't know your address") {
            CHECK_FALSE(didSendPublicMessageForAddressToAnshu);
            didSendPublicMessageForAddressToAnshu = true;
            return;
        }

        if (message == "gavin: I don't know your password") {
            CHECK_FALSE(didSendPublicMessageForPasswordToGavin);
            didSendPublicMessageForPasswordToGavin = true;
            return;
        }

        if (message == "tyrus: I don't know your phone number") {
            CHECK_FALSE(didSendPublicMessageForPhoneNumberToTyrus);
            didSendPublicMessageForPhoneNumberToTyrus = true;
            return;
        }

        if (message == "gavin: I don't know your email") {
            CHECK_FALSE(didSendPublicMessageForEmailToGavin);
            didSendPublicMessageForEmailToGavin = true;
            return;
        }

        FAIL();
    };

    bool didSendPrivateMessageForPasswordToAnshu = false;
    bool didSendPrivateMessageForPasswordToGavin = false;
    bool didSendPrivateMessageForAddressToAnshu = false;
    bool didSendPrivateMessageForPhoneNumberToTyrus = false;
    bool didSendPrivateMessageForEmailToGavin = false;
    messageSender.sendPrivateMessageHandler = [&](auto recipient, auto message) {
        if (recipient == "anshu") {
            if (message == "give me your password") {
                CHECK_FALSE(didSendPrivateMessageForPasswordToAnshu);
                didSendPrivateMessageForPasswordToAnshu = true;
                return;
            }

            if (message == "give me your address") {
                CHECK_FALSE(didSendPrivateMessageForAddressToAnshu);
                didSendPrivateMessageForAddressToAnshu = true;
                return;
            }

            FAIL();
        }

        if (recipient == "gavin") {
            if (message == "give me your password") {
                CHECK_FALSE(didSendPrivateMessageForPasswordToGavin);
                didSendPrivateMessageForPasswordToGavin = true;
                return;
            }

            if (message == "give me your email") {
                CHECK_FALSE(didSendPrivateMessageForEmailToGavin);
                didSendPrivateMessageForEmailToGavin = true;
                return;
            }

            FAIL();
        }

        if (recipient == "tyrus") {
            CHECK(message == "give me your phone number");
            CHECK_FALSE(didSendPrivateMessageForPhoneNumberToTyrus);
            didSendPrivateMessageForPhoneNumberToTyrus = true;
            return;
        }

        FAIL();
    };

    CHECK_FALSE(didSendPublicMessageForPasswordToAnshu);
    CHECK_FALSE(didSendPublicMessageForPasswordToGavin);
    CHECK_FALSE(didSendPublicMessageForAddressToAnshu);
    CHECK_FALSE(didSendPublicMessageForPhoneNumberToTyrus);
    CHECK_FALSE(didSendPublicMessageForEmailToGavin);

    CHECK_FALSE(didSendPrivateMessageForPasswordToAnshu);
    CHECK_FALSE(didSendPrivateMessageForPasswordToGavin);
    CHECK_FALSE(didSendPrivateMessageForAddressToAnshu);
    CHECK_FALSE(didSendPrivateMessageForPhoneNumberToTyrus);
    CHECK_FALSE(didSendPrivateMessageForEmailToGavin);

    bool didReceivePasswordFromAnshu = false;
    UserInformationRequest passwordRequest("~anshu", "anshu", "password", "I don't know your password", "give me your password");
    client->requestInformationFromUser(passwordRequest, [&didReceivePasswordFromAnshu](auto password) {
        CHECK(password == "anshu's password");
        CHECK_FALSE(didReceivePasswordFromAnshu);
        didReceivePasswordFromAnshu = true;
    });

    bool didReceivePasswordFromGavin = false;
    passwordRequest = UserInformationRequest("~gavin", "gavin", "password", "I don't know your password", "give me your password");
    client->requestInformationFromUser(passwordRequest, [&didReceivePasswordFromGavin](auto password) {
        CHECK(password == "gavin's password");
        CHECK_FALSE(didReceivePasswordFromGavin);
        didReceivePasswordFromGavin = true;
    });

    bool didReceiveAddressFromAnshu = false;
    UserInformationRequest addressRequest("~anshu", "anshu", "address", "I don't know your address", "give me your address");
    client->requestInformationFromUser(addressRequest, [&didReceiveAddressFromAnshu](auto address) {
        CHECK(address == "anshu's address");
        CHECK_FALSE(didReceiveAddressFromAnshu);
        didReceiveAddressFromAnshu = true;
    });

    bool didReceivePhoneNumberFromTyrus = false;
    UserInformationRequest phoneNumberAddress("~tyrus", "tyrus", "phone", "I don't know your phone number", "give me your phone number");
    client->requestInformationFromUser(phoneNumberAddress, [&didReceivePhoneNumberFromTyrus](auto phoneNumber) {
        CHECK(phoneNumber == "1 (420) 69-TYRUS");
        CHECK_FALSE(didReceivePhoneNumberFromTyrus);
        didReceivePhoneNumberFromTyrus = true;
    });

    bool didReceiveEmailFromGavin = false;
    UserInformationRequest emailRequest("~gavin", "gavin", "email", "I don't know your email", "give me your email");
    client->requestInformationFromUser(emailRequest, [&didReceiveEmailFromGavin](auto email) {
        CHECK(email == "gavin@gaver.gav");
        CHECK_FALSE(didReceiveEmailFromGavin);
        didReceiveEmailFromGavin = true;
    });

    // All public messages should've been sent immediately.
    CHECK(didSendPublicMessageForPasswordToAnshu);
    CHECK(didSendPublicMessageForPasswordToGavin);
    CHECK(didSendPublicMessageForAddressToAnshu);
    CHECK(didSendPublicMessageForPhoneNumberToTyrus);
    CHECK(didSendPublicMessageForEmailToGavin);

    // Only the first private prompt should've been sent to each user.
    CHECK(didSendPrivateMessageForPasswordToAnshu);
    CHECK(didSendPrivateMessageForPasswordToGavin);
    CHECK_FALSE(didSendPrivateMessageForAddressToAnshu);
    CHECK(didSendPrivateMessageForPhoneNumberToTyrus);
    CHECK_FALSE(didSendPrivateMessageForEmailToGavin);

    CHECK(client->userDataForKey("~anshu", "password") == "");
    CHECK(client->userDataForKey("~gavin", "password") == "");
    CHECK(client->userDataForKey("~anshu", "address") == "");
    CHECK(client->userDataForKey("~tyrus", "phone") == "");
    CHECK(client->userDataForKey("~gavin", "email") == "");

    CHECK_FALSE(didReceivePasswordFromAnshu);
    CHECK_FALSE(didReceivePasswordFromGavin);
    CHECK_FALSE(didReceiveAddressFromAnshu);
    CHECK_FALSE(didReceivePhoneNumberFromTyrus);
    CHECK_FALSE(didReceiveEmailFromGavin);

    client->processPrivateMessage(IRCMessage(std::make_shared<IRCMessagePrefix>("gavin", "~gavin", ""), "PRIVMSG", { "samebot", "gavin's password" }));

    CHECK_FALSE(didReceivePasswordFromAnshu);
    CHECK(didReceivePasswordFromGavin);
    CHECK_FALSE(didReceiveAddressFromAnshu);
    CHECK_FALSE(didReceivePhoneNumberFromTyrus);
    CHECK_FALSE(didReceiveEmailFromGavin);

    CHECK(client->userDataForKey("~anshu", "password") == "");
    CHECK(client->userDataForKey("~gavin", "password") == "gavin's password");
    CHECK(client->userDataForKey("~anshu", "address") == "");
    CHECK(client->userDataForKey("~tyrus", "phone") == "");
    CHECK(client->userDataForKey("~gavin", "email") == "");

    CHECK(didSendPrivateMessageForPasswordToAnshu);
    CHECK(didSendPrivateMessageForPasswordToGavin);
    CHECK_FALSE(didSendPrivateMessageForAddressToAnshu);
    CHECK(didSendPrivateMessageForPhoneNumberToTyrus);
    CHECK(didSendPrivateMessageForEmailToGavin);

    client->processPrivateMessage(IRCMessage(std::make_shared<IRCMessagePrefix>("tyrus", "~tyrus", ""), "PRIVMSG", { "samebot", "1 (420) 69-TYRUS" }));

    CHECK_FALSE(didReceivePasswordFromAnshu);
    CHECK(didReceivePasswordFromGavin);
    CHECK_FALSE(didReceiveAddressFromAnshu);
    CHECK(didReceivePhoneNumberFromTyrus);
    CHECK_FALSE(didReceiveEmailFromGavin);

    CHECK(client->userDataForKey("~anshu", "password") == "");
    CHECK(client->userDataForKey("~gavin", "password") == "gavin's password");
    CHECK(client->userDataForKey("~anshu", "address") == "");
    CHECK(client->userDataForKey("~tyrus", "phone") == "1 (420) 69-TYRUS");
    CHECK(client->userDataForKey("~gavin", "email") == "");

    CHECK(didSendPrivateMessageForPasswordToAnshu);
    CHECK(didSendPrivateMessageForPasswordToGavin);
    CHECK_FALSE(didSendPrivateMessageForAddressToAnshu);
    CHECK(didSendPrivateMessageForPhoneNumberToTyrus);
    CHECK(didSendPrivateMessageForEmailToGavin);

    client->processPrivateMessage(IRCMessage(std::make_shared<IRCMessagePrefix>("tyrus", "~tyrus", ""), "PRIVMSG", { "samebot", "asdf random stuff" }));

    CHECK_FALSE(didReceivePasswordFromAnshu);
    CHECK(didReceivePasswordFromGavin);
    CHECK_FALSE(didReceiveAddressFromAnshu);
    CHECK(didReceivePhoneNumberFromTyrus);
    CHECK_FALSE(didReceiveEmailFromGavin);

    CHECK(client->userDataForKey("~anshu", "password") == "");
    CHECK(client->userDataForKey("~gavin", "password") == "gavin's password");
    CHECK(client->userDataForKey("~anshu", "address") == "");
    CHECK(client->userDataForKey("~tyrus", "phone") == "1 (420) 69-TYRUS");
    CHECK(client->userDataForKey("~gavin", "email") == "");

    CHECK(didSendPrivateMessageForPasswordToAnshu);
    CHECK(didSendPrivateMessageForPasswordToGavin);
    CHECK_FALSE(didSendPrivateMessageForAddressToAnshu);
    CHECK(didSendPrivateMessageForPhoneNumberToTyrus);
    CHECK(didSendPrivateMessageForEmailToGavin);

    client->processPrivateMessage(IRCMessage(std::make_shared<IRCMessagePrefix>("gavin", "~gavin", ""), "PRIVMSG", { "samebot", "gavin@gaver.gav" }));

    CHECK_FALSE(didReceivePasswordFromAnshu);
    CHECK(didReceivePasswordFromGavin);
    CHECK_FALSE(didReceiveAddressFromAnshu);
    CHECK(didReceivePhoneNumberFromTyrus);
    CHECK(didReceiveEmailFromGavin);

    CHECK(client->userDataForKey("~anshu", "password") == "");
    CHECK(client->userDataForKey("~gavin", "password") == "gavin's password");
    CHECK(client->userDataForKey("~anshu", "address") == "");
    CHECK(client->userDataForKey("~tyrus", "phone") == "1 (420) 69-TYRUS");
    CHECK(client->userDataForKey("~gavin", "email") == "gavin@gaver.gav");

    CHECK(didSendPrivateMessageForPasswordToAnshu);
    CHECK(didSendPrivateMessageForPasswordToGavin);
    CHECK_FALSE(didSendPrivateMessageForAddressToAnshu);
    CHECK(didSendPrivateMessageForPhoneNumberToTyrus);
    CHECK(didSendPrivateMessageForEmailToGavin);

    client->processPrivateMessage(IRCMessage(std::make_shared<IRCMessagePrefix>("anshu", "~anshu", ""), "PRIVMSG", { "samebot", "anshu's password" }));

    CHECK(didReceivePasswordFromAnshu);
    CHECK(didReceivePasswordFromGavin);
    CHECK_FALSE(didReceiveAddressFromAnshu);
    CHECK(didReceivePhoneNumberFromTyrus);
    CHECK(didReceiveEmailFromGavin);

    CHECK(client->userDataForKey("~anshu", "password") == "anshu's password");
    CHECK(client->userDataForKey("~gavin", "password") == "gavin's password");
    CHECK(client->userDataForKey("~anshu", "address") == "");
    CHECK(client->userDataForKey("~tyrus", "phone") == "1 (420) 69-TYRUS");
    CHECK(client->userDataForKey("~gavin", "email") == "gavin@gaver.gav");

    CHECK(didSendPrivateMessageForPasswordToAnshu);
    CHECK(didSendPrivateMessageForPasswordToGavin);
    CHECK(didSendPrivateMessageForAddressToAnshu);
    CHECK(didSendPrivateMessageForPhoneNumberToTyrus);
    CHECK(didSendPrivateMessageForEmailToGavin);

    client->processPrivateMessage(IRCMessage(std::make_shared<IRCMessagePrefix>("gavin", "~gavin", ""), "PRIVMSG", { "samebot", "unrelated" }));

    CHECK(didReceivePasswordFromAnshu);
    CHECK(didReceivePasswordFromGavin);
    CHECK_FALSE(didReceiveAddressFromAnshu);
    CHECK(didReceivePhoneNumberFromTyrus);
    CHECK(didReceiveEmailFromGavin);

    CHECK(client->userDataForKey("~anshu", "password") == "anshu's password");
    CHECK(client->userDataForKey("~gavin", "password") == "gavin's password");
    CHECK(client->userDataForKey("~anshu", "address") == "");
    CHECK(client->userDataForKey("~tyrus", "phone") == "1 (420) 69-TYRUS");
    CHECK(client->userDataForKey("~gavin", "email") == "gavin@gaver.gav");

    client->processPrivateMessage(IRCMessage(std::make_shared<IRCMessagePrefix>("anshu", "~anshu", ""), "PRIVMSG", { "samebot", "anshu's address" }));

    CHECK(didReceivePasswordFromAnshu);
    CHECK(didReceivePasswordFromGavin);
    CHECK(didReceiveAddressFromAnshu);
    CHECK(didReceivePhoneNumberFromTyrus);
    CHECK(didReceiveEmailFromGavin);

    CHECK(client->userDataForKey("~anshu", "password") == "anshu's password");
    CHECK(client->userDataForKey("~gavin", "password") == "gavin's password");
    CHECK(client->userDataForKey("~anshu", "address") == "anshu's address");
    CHECK(client->userDataForKey("~tyrus", "phone") == "1 (420) 69-TYRUS");
    CHECK(client->userDataForKey("~gavin", "email") == "gavin@gaver.gav");

    remove("/tmp/test_user_intelligence.db");
}
