#include "Catch.h"

#include "modules/lyft/LyftAuthenticationContext.h"

using namespace samebot;

TEST_CASE("LyftAuthenticationContext::create") {
    CHECK_FALSE(LyftAuthenticationContext::create("hello"));
    CHECK_FALSE(LyftAuthenticationContext::create("{}"));
    CHECK_FALSE(LyftAuthenticationContext::create("{\"access_token\": 1234, \"expires_in\": \"what\"}"));
    CHECK_FALSE(LyftAuthenticationContext::create("{\"access_token\": \"1234\", \"expires_in\": \"what\"}"));
    CHECK_FALSE(LyftAuthenticationContext::create("{\"access_token\": \"1234\", \"expires_in\": -1234}"));
    CHECK_FALSE(LyftAuthenticationContext::create("{\"access_token\": \"1234\", \"expires_in\": 1234, \"refresh_token\": false}"));
    CHECK_FALSE(LyftAuthenticationContext::create("{\"access_token\": \"1234\", \"expiration_date\": \"1234\", \"refresh_token\": false}"));
    CHECK_FALSE(LyftAuthenticationContext::create("{\"access_token\": \"1234\", \"expiration_date\": -1234, \"refresh_token\": false}"));

    auto context = LyftAuthenticationContext::create("{\"access_token\": \"1234\", \"expires_in\": 3600 }");
    REQUIRE(context);
    CHECK(context->accessToken() == "1234");
    CHECK(context->refreshToken() == "");
    CHECK_FALSE(context->isExpired());

    context = LyftAuthenticationContext::create("{\"access_token\": \"hello world\", \"expires_in\": 0, \"refresh_token\": \"1234\" }");
    REQUIRE(context);
    CHECK(context->accessToken() == "hello world");
    CHECK(context->refreshToken() == "1234");
    CHECK(context->isExpired());

    context = LyftAuthenticationContext::create("{\"access_token\": \"hello world\", \"expiration_date\": 1234, \"refresh_token\": \"1234\" }");
    REQUIRE(context);
    CHECK(context->accessToken() == "hello world");
    CHECK(context->refreshToken() == "1234");
    CHECK(context->isExpired());

    auto max_time = std::chrono::system_clock::time_point::max();
    std::time_t max_time_t = std::chrono::system_clock::to_time_t(max_time);
    context = LyftAuthenticationContext::create("{\"access_token\": \"hello world\", \"expiration_date\": " + std::to_string(max_time_t) + ", \"refresh_token\": \"1234\" }");
    REQUIRE(context);
    CHECK(context->accessToken() == "hello world");
    CHECK(context->refreshToken() == "1234");
    CHECK_FALSE(context->isExpired());
}
