#include "Catch.h"

#include "basics/json.h"
#include "modules/lyft/LyftRideEstimate.h"

using namespace samebot;

TEST_CASE("LyftRideEstimate::create") {
    nlohmann::json validJSON;
    validJSON["cost_estimates"] = nlohmann::json::array();
    validJSON["cost_estimates"][0] = nlohmann::json();
    validJSON["cost_estimates"][0]["ride_type"] = "test";
    validJSON["cost_estimates"][0]["display_name"] = "Test";
    validJSON["cost_estimates"][0]["primetime_percentage"] = "50%";
    validJSON["cost_estimates"][0]["estimated_duration_seconds"] = 3600;
    validJSON["cost_estimates"][0]["estimated_distance_miles"] = 3.14159f;
    validJSON["cost_estimates"][0]["estimated_cost_cents_min"] = 1000;
    validJSON["cost_estimates"][0]["estimated_cost_cents_max"] = 2000;

    auto estimate = LyftRideEstimate::create(validJSON.dump());
    REQUIRE(estimate);
    CHECK(estimate->rideType() == "test");
    CHECK(estimate->displayName() == "Test");
    CHECK(estimate->primetimePercentage() == "50%");
    CHECK(estimate->estimatedDuration().count() == 3600);
    CHECK(estimate->estimatedDistanceInMiles() == 3.14159f);
    CHECK(estimate->estimatedMinimumCostInCents() == 1000);
    CHECK(estimate->estimatedMaximumCostInCents() == 2000);

    nlohmann::json invalidJSON = validJSON;
    invalidJSON["cost_estimates"][0]["ride_type"] = false;
    CHECK_FALSE(LyftRideEstimate::create(invalidJSON.dump()));

    invalidJSON = validJSON;
    invalidJSON["cost_estimates"][0]["display_name"] = 123;
    CHECK_FALSE(LyftRideEstimate::create(invalidJSON.dump()));

    invalidJSON = validJSON;
    invalidJSON["cost_estimates"][0]["primetime_percentage"] = 0.5;
    CHECK_FALSE(LyftRideEstimate::create(invalidJSON.dump()));

    invalidJSON = validJSON;
    invalidJSON["cost_estimates"][0]["estimated_duration_seconds"] = -1;
    CHECK_FALSE(LyftRideEstimate::create(invalidJSON.dump()));

    invalidJSON = validJSON;
    invalidJSON["cost_estimates"][0]["estimated_distance_miles"] = -1.2;
    CHECK_FALSE(LyftRideEstimate::create(invalidJSON.dump()));

    invalidJSON = validJSON;
    invalidJSON["cost_estimates"][0]["estimated_cost_cents_min"] = -1;
    CHECK_FALSE(LyftRideEstimate::create(invalidJSON.dump()));

    invalidJSON = validJSON;
    invalidJSON["cost_estimates"][0]["estimated_cost_cents_min"] = 10.5;
    CHECK_FALSE(LyftRideEstimate::create(invalidJSON.dump()));

    invalidJSON = validJSON;
    invalidJSON["cost_estimates"][0]["estimated_cost_cents_max"] = -1234;
    CHECK_FALSE(LyftRideEstimate::create(invalidJSON.dump()));

    invalidJSON = validJSON;
    invalidJSON["cost_estimates"][0]["estimated_cost_cents_max"] = -12.34;
    CHECK_FALSE(LyftRideEstimate::create(invalidJSON.dump()));

    CHECK_FALSE(LyftRideEstimate::create("hello"));
    CHECK_FALSE(LyftRideEstimate::create("{}"));
    CHECK_FALSE(LyftRideEstimate::create("{\"cost_estimates\": null}"));
    CHECK_FALSE(LyftRideEstimate::create("{\"cost_estimates\": []}"));
    CHECK_FALSE(LyftRideEstimate::create("{\"cost_estimates\": [{ \"ride_type\": \"lyft\" }]}"));
}
