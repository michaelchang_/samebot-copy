#include "Catch.h"

#include "modules/scrapbook/ScrapbookModule.h"
#include "network/irc/IRCMessage.h"

using namespace samebot;

class TestDataStore : public ScrapbookDataStore {
public:
    virtual void storeMemory(std::shared_ptr<ScrapbookMemory> memory) override {
        m_memories.push_back(memory);
    }

    virtual void deleteMemory(const ScrapbookMemory& memory) override {
        for (unsigned int i = 0; i < m_memories.size(); ++i) {
            if (m_memories[i]->memory() == memory.memory()) {
                m_memories.erase(m_memories.begin() + i);
                return;
            }
        }
    }

    virtual std::shared_ptr<ScrapbookMemory> retrieveRandomMemory() override {
        if (m_memories.size()) {
            return m_memories.back();
        }
        return nullptr;
    }

    virtual std::shared_ptr<ScrapbookMemory> findMemoryContainingString(std::string) const override {
        return nullptr;
    }

private:
    std::vector<std::shared_ptr<ScrapbookMemory>> m_memories;
};

class TestClient : public ModuleClient {
public:
    virtual void sendMessage(std::shared_ptr<IRCMessage>) override {}
    virtual std::vector<Module*> modulesWithIdentifier(std::string) override { ASSERT_NOT_REACHED(); }
};

static auto testMessage(std::string user, std::string message) {
    auto prefix = std::make_shared<IRCMessagePrefix>(user, "", "");
    return std::make_shared<IRCMessage>(prefix, "PRIVMSG", std::vector<std::string> { "#somechannel", message });
}

#define PREPARE_TEST_CONTEXT(ProactiveMemoryAlgorithm) \
    auto dataStore = std::make_unique<TestDataStore>(); \
    auto* dataStorePointer = dataStore.get(); \
    auto module = ScrapbookModuleTestShim::createModule(std::move(dataStore)); \
    auto client = std::make_shared<TestClient>(); \
    module->setClient(client); \
    auto shim = ScrapbookModuleTestShim(module); \

#define PREPARE_PROACTIVE_MEMORIES_TEST_CONTEXT(ProactiveMemoryAlgorithm, ...) \
    PREPARE_TEST_CONTEXT(); \
    shim.useProactiveMemoryAlgorithm(std::make_unique<ProactiveMemoryAlgorithm>(__VA_ARGS__)); \

TEST_CASE("ScrapbookModule simple cases") {
    PREPARE_TEST_CONTEXT();

    shim.didReceiveMessage(testMessage("user1", "message1"));
    shim.didReceiveMessage(testMessage("user2", "message2"));
    shim.didReceiveMessage(testMessage("user1", "message3"));
    shim.didReceiveMessage(testMessage("user3", "important message"));
    shim.didReceiveMessage(testMessage("user2", "message4"));
    shim.didReceiveMessage(testMessage("user2", "message5 blah blah user3 blah"));
    shim.didReceiveMessage(testMessage("user1", "message6"));

    auto expectSuccess = [&] {
        auto memory = dataStorePointer->retrieveRandomMemory();
        REQUIRE(memory->memory() == "<user3> important message");

        std::string expectedContext =
            "<user1> message1\r\n"
            "<user2> message2\r\n"
            "<user1> message3\r\n"
            "<user3> important message\r\n"
            "<user2> message4\r\n"
            "<user2> message5 blah blah user3 blah\r\n"
            "<user1> message6";
        REQUIRE(memory->context() == expectedContext);
    };

    SECTION("Message matching") {
        SECTION("Identical message match") {
            shim.didReceiveMessage(testMessage("user1", "samebot: remember important message"));
            expectSuccess();
        }

        SECTION("Fuzzy request matching") {
            shim.didReceiveMessage(testMessage("user1", "samebot: rememe important message"));
            expectSuccess();
        }

        SECTION("Fuzzy message matching") {
            shim.didReceiveMessage(testMessage("user1", "samebot: remember important msg"));
            expectSuccess();
        }

        SECTION("Fuzzy matching both") {
            shim.didReceiveMessage(testMessage("user1", "samebot: remembre importat messg"));
            expectSuccess();
        }
    }

    SECTION("User matching") {
        SECTION("Just the username") {
            shim.didReceiveMessage(testMessage("user1", "samebot: remember user3"));
            expectSuccess();
        }

        SECTION("Natural language") {
            shim.didReceiveMessage(testMessage("user1", "samebot: remember thing that user3 said"));
            expectSuccess();
        }

        SECTION("Ambiguity") {
            shim.didReceiveMessage(testMessage("user1", "samebot: remember blah user3"));
        }
    }
}

TEST_CASE("ScrapbookModule fuzzy sub-sentence matching") {
    PREPARE_TEST_CONTEXT();

    std::vector<std::tuple<std::string, std::string, int>> messages {
        std::make_tuple( "archit", "So you can see the fishhook action underwater?", 0 ),
        std::make_tuple( "archit", "Like is there a glass pane", 7 ),
        std::make_tuple( "tyrus", "Yeah it's like above ground glass tank", 359 ),
        std::make_tuple( "tyrus", "Oh yeah there is Silver Dollar City", 634 ),
        std::make_tuple( "tyrus", "The theme park", 741 ),
        std::make_tuple( "archit", "That's pretty cool", 745 ),
        std::make_tuple( "archit", "Oh man", 747 ),
        std::make_tuple( "archit", "I haven't been to a theme park in so long", 759 ),
        std::make_tuple( "archit", "Not since the roller coaster history and design course", 793 ),
        std::make_tuple( "tyrus", "Lol", 798 ),
        std::make_tuple( "tyrus", "Have you been to six flags in socal", 821 ),
        std::make_tuple( "tyrus", "That place is great", 827 ),
        std::make_tuple( "archit", "No I havent", 864 ),
        std::make_tuple( "archit", "I've been to six flags magic mountain", 877 ),
        std::make_tuple( "archit", "And great America", 883 ),
        std::make_tuple( "tyrus", "That is the six flags", 903 ),
        std::make_tuple( "tyrus", "In socal", 905 ),
        std::make_tuple( "tyrus", "Southern California", 912 ),
        std::make_tuple( "archit", "Wait really", 919 ),
        std::make_tuple( "archit", "That", 926 ),
        std::make_tuple( "archit", "Can't be", 930 ),
        std::make_tuple( "tyrus", "Is there another one?", 931 ),
        std::make_tuple( "archit", "I must be thinking of another one", 937 ),
        std::make_tuple( "tyrus", "In southern California", 937 ),
        std::make_tuple( "archit", "I went for a friend's birthday here so it can't have been far, let me look it up", 950 ),
    };

    for (auto tuple : messages) {
        shim.insertMessageInHistory(testMessage(std::get<0>(tuple), std::get<1>(tuple)), std::get<2>(tuple));
    }

    SECTION("1") {
        shim.didReceiveMessage(testMessage("anshu", "samebot: remember six flags"));
        REQUIRE(dataStorePointer->retrieveRandomMemory()->memory() == "<tyrus> That is the six flags");
    }
    SECTION("2") {
        shim.didReceiveMessage(testMessage("anshu", "samebot: remember archit talking about six flags"));
        REQUIRE(dataStorePointer->retrieveRandomMemory()->memory() == "<archit> I've been to six flags magic mountain");
    }
    SECTION("3") {
        shim.didReceiveMessage(testMessage("anshu", "samebot: remember glass"));
        REQUIRE(dataStorePointer->retrieveRandomMemory()->memory() == "<tyrus> Yeah it's like above ground glass tank");
    }
    SECTION("4") {
        shim.didReceiveMessage(testMessage("anshu", "samebot: remember what archit was saying about glass"));
        REQUIRE(dataStorePointer->retrieveRandomMemory()->memory() == "<archit> Like is there a glass pane");
    }
}

TEST_CASE("ScrapbookModule semantic matching for recent messages") {
    PREPARE_TEST_CONTEXT();

    SECTION("No ambiguities, short history") {
        shim.didReceiveMessage(testMessage("user1", "message1"));
        shim.didReceiveMessage(testMessage("user2", "message2"));
        shim.didReceiveMessage(testMessage("user1", "message3"));
        shim.didReceiveMessage(testMessage("user2", "message4"));
        shim.didReceiveMessage(testMessage("user2", "message5"));
        shim.didReceiveMessage(testMessage("user1", "message6"));
        shim.didReceiveMessage(testMessage("user1", "message7"));

        SECTION("Query 1") {
            shim.didReceiveMessage(testMessage("anshu", "samebot: remember this"));
        }
        SECTION("Query 2") {
            shim.didReceiveMessage(testMessage("anshu", "samebot: remember all of this"));
        }
        SECTION("Query 3") {
            shim.didReceiveMessage(testMessage("anshu", "samebot: remember all this stuff"));
        }
        SECTION("Query 4") {
            shim.didReceiveMessage(testMessage("anshu", "samebot: remember everything these people just said"));
        }
        SECTION("Query 5") {
            shim.didReceiveMessage(testMessage("anshu", "samebot: remember all of the things that were said"));
        }

        CHECK(dataStorePointer->retrieveRandomMemory()->memory() == "<user1> message7");
    }

    SECTION("No ambiguities, long history") {
        shim.didReceiveMessage(testMessage("user1", "message1"));
        shim.didReceiveMessage(testMessage("user2", "message2"));
        shim.didReceiveMessage(testMessage("user1", "message3"));
        shim.didReceiveMessage(testMessage("user2", "message4"));
        shim.didReceiveMessage(testMessage("user2", "message5"));
        shim.didReceiveMessage(testMessage("user1", "message6"));
        shim.didReceiveMessage(testMessage("user1", "message7"));
        shim.didReceiveMessage(testMessage("user1", "message8"));
        shim.didReceiveMessage(testMessage("user2", "message9"));
        shim.didReceiveMessage(testMessage("user1", "message10"));
        shim.didReceiveMessage(testMessage("user2", "message11"));
        shim.didReceiveMessage(testMessage("user2", "message12"));
        shim.didReceiveMessage(testMessage("user1", "message13"));
        shim.didReceiveMessage(testMessage("user1", "message14"));
        shim.didReceiveMessage(testMessage("user1", "message15"));
        shim.didReceiveMessage(testMessage("user2", "message16"));
        shim.didReceiveMessage(testMessage("user2", "message17"));
        shim.didReceiveMessage(testMessage("user1", "message18"));
        shim.didReceiveMessage(testMessage("user1", "message19"));

        SECTION("Query 1") {
            shim.didReceiveMessage(testMessage("anshu", "samebot: remember this"));
        }
        SECTION("Query 2") {
            shim.didReceiveMessage(testMessage("anshu", "samebot: remember all of this"));
        }
        SECTION("Query 3") {
            shim.didReceiveMessage(testMessage("anshu", "samebot: remember all this stuff"));
        }
        SECTION("Query 4") {
            shim.didReceiveMessage(testMessage("anshu", "samebot: remember everything these people just said"));
        }
        SECTION("Query 5") {
            shim.didReceiveMessage(testMessage("anshu", "samebot: remember all of the things that were said"));
        }

        CHECK(dataStorePointer->retrieveRandomMemory()->memory() == "<user2> message12");
    }

    SECTION("Ambiguous, long history") {
        shim.didReceiveMessage(testMessage("user1", "message1"));
        shim.didReceiveMessage(testMessage("user2", "message2"));
        shim.didReceiveMessage(testMessage("user1", "message3"));
        shim.didReceiveMessage(testMessage("user2", "this"));
        shim.didReceiveMessage(testMessage("user2", "message5"));
        shim.didReceiveMessage(testMessage("user1", "message6"));
        shim.didReceiveMessage(testMessage("user1", "message7"));
        shim.didReceiveMessage(testMessage("that", "message8"));
        shim.didReceiveMessage(testMessage("user2", "message9"));
        shim.didReceiveMessage(testMessage("user1", "message10"));
        shim.didReceiveMessage(testMessage("user2", "message11"));
        shim.didReceiveMessage(testMessage("user2", "message12"));
        shim.didReceiveMessage(testMessage("user1", "message13"));
        shim.didReceiveMessage(testMessage("user1", "message14"));
        shim.didReceiveMessage(testMessage("user1", "message15"));
        shim.didReceiveMessage(testMessage("user2", "things were said"));
        shim.didReceiveMessage(testMessage("user2", "message17"));
        shim.didReceiveMessage(testMessage("user1", "message18"));
        shim.didReceiveMessage(testMessage("user1", "message19"));

        SECTION("Message matches") {
            SECTION("1") {
                shim.didReceiveMessage(testMessage("anshu", "samebot: remember this"));
                CHECK(dataStorePointer->retrieveRandomMemory()->memory() == "<user2> this");
            }
            SECTION("2") {
                shim.didReceiveMessage(testMessage("anshu", "samebot: remember that things were said"));
                CHECK(dataStorePointer->retrieveRandomMemory()->memory() == "<user2> things were said");
            }
        }

        SECTION("Nickname matches") {
            shim.didReceiveMessage(testMessage("anshu", "samebot: remember all of the things that were said"));
            CHECK(dataStorePointer->retrieveRandomMemory()->memory() == "<that> message8");
        }

        SECTION("Semantic matches") {
            SECTION("1") {
                shim.didReceiveMessage(testMessage("anshu", "samebot: remember all of this"));
            }
            SECTION("2") {
                shim.didReceiveMessage(testMessage("anshu", "samebot: remember all this stuff"));
            }
            SECTION("3") {
                shim.didReceiveMessage(testMessage("anshu", "samebot: remember everything these people just said"));
            }
            CHECK(dataStorePointer->retrieveRandomMemory()->memory() == "<user2> message12");
        }
    }
}

TEST_CASE("ScrapbookModule placeholder nicknames") {
    PREPARE_TEST_CONTEXT();

    shim.didReceiveMessage(testMessage("user1", "message1"));
    shim.didReceiveMessage(testMessage("user2", "message2"));
    shim.didReceiveMessage(testMessage("user1", "message3"));
    shim.didReceiveMessage(testMessage("user2", "i"));
    shim.didReceiveMessage(testMessage("samebot", "message5"));
    shim.didReceiveMessage(testMessage("user2", "message6"));
    shim.didReceiveMessage(testMessage("user1", "message7"));

    SECTION("I") {
        SECTION("Uppercase") {
            shim.didReceiveMessage(testMessage("user1", "samebot: remember what I said"));
        }
        SECTION("Lowercase") {
            shim.didReceiveMessage(testMessage("user1", "samebot: remember what i said"));
        }
        CHECK(dataStorePointer->retrieveRandomMemory()->memory() == "<user1> message7");
    }

    SECTION("You") {
        SECTION("Uppercase") {
            shim.didReceiveMessage(testMessage("user1", "samebot: remember what YOU said"));
        }
        SECTION("Lowercase") {
            shim.didReceiveMessage(testMessage("user1", "samebot: remember what you said"));
        }
        SECTION("Weird") {
            shim.didReceiveMessage(testMessage("user1", "samebot: remember what YoU said"));
        }
        CHECK(dataStorePointer->retrieveRandomMemory()->memory() == "<samebot> message5");
    }
}

TEST_CASE("ScrapbookModule::isRequestForRandomMemory") {
    PREPARE_TEST_CONTEXT();

    REQUIRE(shim.isRequestForRandomMemory("memory"));
    REQUIRE(shim.isRequestForRandomMemory("story"));
    REQUIRE(shim.isRequestForRandomMemory("random story"));
    REQUIRE(shim.isRequestForRandomMemory("random memory"));
    REQUIRE(shim.isRequestForRandomMemory("tell me a story"));
    REQUIRE(shim.isRequestForRandomMemory("give me a scrapbook story"));
    REQUIRE(shim.isRequestForRandomMemory("samebot: memory"));
    REQUIRE(shim.isRequestForRandomMemory("samebot: story"));
    REQUIRE(shim.isRequestForRandomMemory("samebot: random story"));
    REQUIRE(shim.isRequestForRandomMemory("samebot: random memory"));
    REQUIRE(shim.isRequestForRandomMemory("samebot: tell me a story"));
    REQUIRE(shim.isRequestForRandomMemory("samebot: give me a scrapbook story"));
    REQUIRE(shim.isRequestForRandomMemory("samebot: random memory from scrapbook"));
    REQUIRE(shim.isRequestForRandomMemory("samebot: scrapbook memory"));
    REQUIRE(shim.isRequestForRandomMemory("samebot: what do you remember?"));
    REQUIRE(shim.isRequestForRandomMemory("samebot: tell me something you remember"));
    REQUIRE(shim.isRequestForRandomMemory("samebot: what have you remembered?"));
    REQUIRE(shim.isRequestForRandomMemory("samebot: something from the scrapbook"));
    REQUIRE(shim.isRequestForRandomMemory("samebot: can you tell me a story?"));
    REQUIRE_FALSE(shim.isRequestForRandomMemory("samebot: scrapbook me something"));
    REQUIRE_FALSE(shim.isRequestForRandomMemory("samebot: memory is important to a computer"));
    REQUIRE_FALSE(shim.isRequestForRandomMemory("samebot: hello"));
    REQUIRE_FALSE(shim.isRequestForRandomMemory("samebot: "));
    REQUIRE_FALSE(shim.isRequestForRandomMemory("samebot:"));
    REQUIRE_FALSE(shim.isRequestForRandomMemory(""));
}

