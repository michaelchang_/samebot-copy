#include "Configuration.h"

#include "Assertions.h"
#include "StringUtilities.h"
#include <fstream>
#include <sstream>
#include <string>

using namespace samebot;

std::unique_ptr<Configuration> Configuration::create(std::string filePath)
{
    std::map<std::string, std::string> map;
    std::ifstream inputStream(filePath);
    if (inputStream.fail()) {
        LOG_ERROR("Cannot read configuration file at %s", filePath.c_str());
        return nullptr;
    }

    std::string line;
    while (std::getline(inputStream, line)) {
        line = stringByTrimmingString(line);

        // Ignore empty lines or comments.
        if (!line.length() || line[0] == '#') {
            continue;
        }

        auto positionOfAssignmentOperator = line.find('=');
        if (positionOfAssignmentOperator == std::string::npos) {
            LOG_ERROR("Configuration file contains invalid line: %s", line.c_str());
            return nullptr;
        }

        auto key = line.substr(0, positionOfAssignmentOperator);
        auto value = line.substr(positionOfAssignmentOperator + 1);
        if (!key.length()) {
            LOG_ERROR("Configuration file contains line without key: %s", line.c_str());
            return nullptr;
        }

        map[key] = value;
    }

    return std::unique_ptr<Configuration>(new Configuration(map));
}

Configuration::Configuration(std::map<std::string, std::string> map)
    : m_map(map)
{
}

int Configuration::getInt(std::string key) const
{
    std::istringstream stream(get(key));
    int i;
    stream >> i;
    if (stream.fail()) {
        LOG_ERROR("Non-integral value for key %s: %s", key.c_str(), get(key).c_str());
        return 0;
    }

    return i;
}

std::string Configuration::get(std::string key) const {
    if (m_map.find(key) != m_map.end()) {
        return m_map.at(key);
    }
    return "";
}
