#include "Assertions.h"

static bool debugLoggingEnabled = true;
static bool errorLoggingEnabled = true;

bool isDebugLoggingEnabled() { return debugLoggingEnabled; }
void disableDebugLogging() { debugLoggingEnabled = false; }
void enableDebugLogging() { debugLoggingEnabled = true; }

bool isErrorLoggingEnabled() { return errorLoggingEnabled; }
void disableErrorLogging() { errorLoggingEnabled = false; }
void enableErrorLogging() { errorLoggingEnabled = true; }

