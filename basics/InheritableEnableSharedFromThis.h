#ifndef __INHERITABLE_ENABLE_SHARED_FROM_THIS_H
#define __INHERITABLE_ENABLE_SHARED_FROM_THIS_H

#include <memory>

class _InheritableEnableSharedFromThis : public std::enable_shared_from_this<_InheritableEnableSharedFromThis> {
public:
    virtual ~_InheritableEnableSharedFromThis() {}
};

template <class T>
class InheritableEnableSharedFromThis : virtual public _InheritableEnableSharedFromThis {
public:
    template <class Derived>
    std::shared_ptr<Derived> cast_shared_from_this() {
        return std::dynamic_pointer_cast<Derived>(_InheritableEnableSharedFromThis::shared_from_this());
    }

    std::shared_ptr<T> shared_from_this() {
        return cast_shared_from_this<T>();
    }
};

#endif // __INHERITABLE_ENABLE_SHARED_FROM_THIS_H

