#ifndef __EITHER_H
#define __EITHER_H

#include "basics/Assertions.h"
#include <memory>

namespace samebot {

template <typename L, typename R>
class Either;

template <typename L, typename R, typename T>
struct _EitherSpecializableWrapper {
    bool check(Either<L, R>& either) = delete;
    T& cast(Either<L, R>& either) = delete;
};

template <typename L, typename R>
class Either {
public:
    typedef L Left;
    typedef R Right;

    Either(L left)
        : m_isLeft(true)
    {
        new (&m_data.left) std::unique_ptr<L>(new L(left));
    }

    Either(R right)
        : m_isLeft(false)
    {
        new (&m_data.right) std::unique_ptr<R>(new R(right));
    }

    ~Either() {
        if (m_isLeft) {
            m_data.left.~unique_ptr<L>();
        } else {
            m_data.right.~unique_ptr<R>();
        }
    }

    template <typename T>
    bool is() {
        // If this attempts to use a deleted function, you are providing T that is neither L nor R, and thus this statement
        // is vacuously false. You should remove whatever check you're making.
        return _EitherSpecializableWrapper<L, R, T>().check(*this);
    }

    template <typename T>
    T& as() {
        // If this attempts to use a deleted function, you are providing T that is neither L nor R, which would make this
        // a guaranteed crash / assertion failure at runtime.
        return _EitherSpecializableWrapper<L, R, T>().cast(*this);
    }

private:
    L* left() { return m_isLeft ? m_data.left.get() : nullptr; }
    R* right() { return !m_isLeft ? m_data.right.get() : nullptr; }

    union U {
        std::unique_ptr<L> left;
        std::unique_ptr<R> right;

        U() {}
        ~U() {}
    } m_data;

    bool m_isLeft;

    friend struct _EitherSpecializableWrapper<L, R, L>;
    friend struct _EitherSpecializableWrapper<L, R, R>;
};

template <typename T>
class Either<T, T> {
public:
    Either(T t) {
        static_assert(!std::is_same<T, T>(), "Either must be specialized with different types to be useful");
    }
};

template <typename L, typename R>
struct _EitherSpecializableWrapper<L, R, L> {
    bool check(Either<L, R>& either) {
        return !!either.left();
    }

    L& cast(Either<L, R>& either) {
        ASSERT(either.template is<L>());
        return *either.left();
    }
};

template <typename L, typename R>
struct _EitherSpecializableWrapper<L, R, R> {
    bool check(Either<L, R>& either) {
        return !!either.right();
    }

    R& cast(Either<L, R>& either) {
        ASSERT(either.template is<R>());
        return *either.right();
    }
};

} // namespace

#endif // __EITHER_H
