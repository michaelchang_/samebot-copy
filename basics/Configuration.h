#ifndef __CONFIGURATION_H
#define __CONFIGURATION_H

#include <map>
#include <memory>
#include <string>

namespace samebot {

class Configuration {
public:
    static std::unique_ptr<Configuration> create(std::string filePath);

    std::string host() const { return get("Host"); }
    int port() const { return getInt("Port"); }
    std::string nickname() const { return get("Nickname"); }
    std::string channel() const { return get("Channel"); }

    std::string lyftClientID() const { return get("LyftClientID"); }
    std::string lyftClientSecret() const { return get("LyftClientSecret"); }

    std::string merriamWebsterAPIKey() const { return get("MerriamWebsterAPIKey"); }
    std::string forecastAPIKey() const { return get("ForecastAPIKey"); }
    std::string memeAPIKey() const { return get("MemeAPIKey"); }
    std::string memeAPIID() const { return get("MemeAPIID"); }
    std::string googleAPIKey() const { return get("GoogleAPIKey"); }
    std::string googleAPIID() const { return get("GoogleAPIID"); }

private:
    Configuration(std::map<std::string, std::string>);

    std::string get(std::string key) const;
    int getInt(std::string key) const;

    std::map<std::string, std::string> m_map;
};

} // namespace

#endif // __CONFIGURATION_H
