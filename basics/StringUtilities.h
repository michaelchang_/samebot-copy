#ifndef __STRING_UTILITIES_H
#define __STRING_UTILITIES_H

#include <memory>
#include <string>
#include <vector>

namespace samebot {

std::vector<std::string> splitString(std::string string, char delimiter = ' ');
bool stringHasPrefix(const std::string& string, const std::string& prefix);
bool stringHasSuffix(const std::string& string, const std::string& suffix);
std::string lowercaseString(std::string string);
std::string stringByTrimmingString(std::string string);
std::string stringByRemovingSpecialCharacters(std::string);
std::string stringByCompressingIntrawordSpacesInString(std::string);
std::string removeFirstStringFromString(std::string, std::string);
bool isExactMatch(std::string, std::string knownString);
bool isExactMatch(std::string, const std::vector<std::string>& knownStrings);
bool isElongatedMatch(std::string, std::string knownString);
bool isExactMatchWithElongatedWords(std::string, std::vector<std::string>);
bool hasElongatedPrefixMatch(std::string, std::vector<std::string>);
bool hasElongatedSuffixMatch(std::string, std::vector<std::string>);

std::string pluralizeWord(const std::string&, float number);

int levenshtein(std::string, std::string);
int levenshteinThresholdForString(std::string);
bool isFuzzyMatch(std::string string, std::string knownString);
bool isFuzzyMatch(std::string, std::vector<std::string> knownStrings);
bool hasFuzzyPrefixMatch(std::string, const std::vector<std::string>& prefixes);
bool hasFuzzySuffixMatch(std::string, const std::vector<std::string>& suffixes);
size_t findFirstFuzzyWordMatch(std::string string, std::string word, std::string* outMatchingWord = nullptr);
} // namespace

#endif // __STRING_UTILITIES_H

