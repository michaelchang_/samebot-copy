#ifndef __GEOCODER_H
#define __GEOCODER_H

#include <memory>
#include <string>

namespace samebot {

class Geocoder {
public:
    struct Coordinate {
        float latitude;
        float longitude;
    };

    struct Response {
        Coordinate coordinate;
        std::string formattedAddress;
    };

    static std::unique_ptr<Response> geocode(std::string address);
};

} // namespace

#endif // __GEOCODER_H
