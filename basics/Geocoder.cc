#include "Geocoder.h"

#include "basics/Assertions.h"
#include "basics/json.h"
#include "network/URLRequest.h"
#include "network/URLSession.h"

using namespace samebot;

std::unique_ptr<Geocoder::Response> Geocoder::geocode(std::string address) {
    URLRequest request("https://maps.googleapis.com/maps/api/geocode/json", URLRequest::Method::GET, {
        { "address", address }
    });

    auto response = URLSession::sendSynchronousRequest(request);
    try {
        auto json = nlohmann::json::parse(response);
        if (!json["results"].size()) {
            return nullptr;
        }

        auto result = json["results"].front();
        auto location = result["geometry"]["location"];
        auto response = std::make_unique<Response>();
        response->formattedAddress = result["formatted_address"];
        response->coordinate.latitude = location["lat"];
        response->coordinate.longitude = location["lng"];
        return response;
    } catch(const std::exception& exception) {
        LOG_ERROR("Failed to parse geocoder response %s: %s", response.c_str(), exception.what());
        return nullptr;
    }
}
