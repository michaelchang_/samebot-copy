#include "StringUtilities.h"

#include <algorithm>
#include <map>
#include <sstream>

namespace samebot {

std::vector<std::string> splitString(std::string string, char delimiter) {
    std::vector<std::string> elements;
    std::stringstream stream(string);
    std::string element;
    while (std::getline(stream, element, delimiter)) {
        elements.push_back(element);
    }

    return elements;
}

bool stringHasPrefix(const std::string& string, const std::string& prefix) {
    return std::mismatch(prefix.begin(), prefix.end(), string.begin()).first == prefix.end();
}

bool stringHasSuffix(const std::string& string, const std::string& suffix) {
    return std::mismatch(suffix.rbegin(), suffix.rend(), string.rbegin()).first == suffix.rend();
}

std::string lowercaseString(std::string string) {

    //The locale is set to utf8 to support wikipedia unicode.
    //Initialize a C locale to use isalpha as intended
    std::locale loc("C");
    std::transform(string.begin(), string.end(), string.begin(),
            [loc](char c) { return (isalpha(c, loc)? (char)tolower(c) : c); });
    return string;
}

std::string stringByTrimmingString(std::string string) {
    string.erase(string.begin(), std::find_if(string.begin(), string.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
    string.erase(std::find_if(string.rbegin(), string.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), string.end());
    return string;
}

std::string stringByRemovingSpecialCharacters(std::string string) {
    string.erase(std::remove_if(string.begin(), string.end(), [](auto c) {
        return !isalpha(c) && !isdigit(c);
    }), string.end());
    return string;
}

std::string stringByCompressingIntrawordSpacesInString(std::string string) {
    std::vector<std::string> words;
    std::string currentWord;
    for (const auto& word : splitString(string)) {
        if (word.length() == 1 && isalpha(word[0])) {
            currentWord += word;
            continue;
        }

        words.push_back(currentWord);
        words.push_back(word);
        currentWord = "";
    }

    words.push_back(currentWord);
    std::string result;
    for (const auto& word : words) {
        if (word.length()) {
            result += stringByTrimmingString(word) + " ";
        }
    }

    return stringByTrimmingString(result);
}

std::string removeFirstStringFromString(std::string string, std::string stringToRemove) {
    if (string.find(stringToRemove) == std::string::npos) return string;
    string.erase(string.find(stringToRemove), stringToRemove.length());
    return string;
}

bool isExactMatch(std::string string, std::string knownString) {
    if (string.compare(knownString) == 0) return true;
    return false;
}

bool isExactMatch(std::string string, const std::vector<std::string>& knownStrings) {
    for (const auto& knownString : knownStrings) {
        if (isExactMatch(string, knownString)) {
            return true;
        }
    }
    return false;
}

bool isElongatedMatch(std::string string, std::string knownString) {
    if (string.empty() || knownString.empty()) return false;
    if (string[0] != knownString[0]) return false;
    size_t knownStringIndex = 1;
    size_t stringIndex = 1;
    for (; stringIndex < string.length() && knownStringIndex < knownString.length();) {
        if (string[stringIndex] == knownString[knownStringIndex]) {
            knownStringIndex++;
            stringIndex++;
        } else if (string[stringIndex] == knownString[knownStringIndex-1]) {
            stringIndex++;
        } else {
            return false;
        }
    }
    return true;
}

bool isExactMatchWithElongatedWords(std::string string, std::vector<std::string> knownWords) {
    auto words = splitString(string);
    auto wordsSize = static_cast<int>(words.size());

    if (words.size() != knownWords.size()) {
        return false;
    }

    for (int i = 0; i < wordsSize; i++) {
        if (!isElongatedMatch(words[i], knownWords[i])) {
            return false;
        }
    }
    return true;
}

bool hasElongatedPrefixMatch(std::string string, std::vector<std::string> wordList) {
    auto words = splitString(string);
    auto wordListSize = static_cast<int>(wordList.size());
    if (words.size() < wordList.size()) {
        return false;
    }

    for (int i = 0; i < wordListSize; i++) {
        if (!isElongatedMatch(words[i], wordList[i])) {
            return false;
        }
    }
    return true;
}

bool hasElongatedSuffixMatch(std::string string, std::vector<std::string> wordList) {
    auto words = splitString(string);
    auto wordsSize = static_cast<int>(words.size());
    auto wordListSize = static_cast<int>(wordList.size());
    if (words.size() < wordList.size()) {
        return false;
    }

    int wordListIndex = wordListSize-1;
    int wordsIndex = wordsSize-1;
    for (; wordListIndex >= 0 && wordsIndex >= 0; wordListIndex--, wordsIndex--) {
        if (!isElongatedMatch(words[wordsIndex], wordList[wordListIndex])) {
            return false;
        }
    }
    return true;
}

std::string pluralizeWord(const std::string& word, float number)
{
    if (number == 1) {
        return word;
    }

    if (stringHasSuffix(word, "s")) {
        return word + "es";
    }

    return word + "s";
}

int levenshtein(std::string string1, std::string string2) {
    auto string1Length = static_cast<int>(string1.length());
    auto string2Length = static_cast<int>(string2.length());

    std::map<char, int> alphabet;
    int distances[string1Length + 1][string2Length + 1];
    distances[0][0] = 0;

    for (int i = 1; i <= string1Length; ++i) {
        distances[i][0] = i;
        alphabet[string1[i - 1]] = 0;
    }

    for (int i = 1; i <= string2Length; ++i) {
        distances[0][i] = i;
        alphabet[string2[i - 1]] = 0;
    }

    for (int i = 1; i <= string1Length; ++i) {
        int dB = 0;
        for (int j = 1; j <= string2Length; ++j) {
            int i1 = alphabet[string2[j - 1]];
            int j1 = dB;
            int cost = 0;

            if (string1[i - 1] == string2[j - 1]) {
                dB = j;
            } else {
                cost = 1;
            }

            distances[i][j] = std::min({
                distances[i - 1][j] + 1,
                distances[i][j - 1] + 1,
                distances[i - 1][j - 1] + cost,
            });

            // Damerau-Levenshtein extension: account for transpositions.
            if (i1 > 0 && j1 > 0) {
                distances[i][j] = std::min<int>(distances[i][j], distances[i1 - 1][j1 - 1] + (i - i1 - 1) + (j - j1 - 1) + 1);
            }
        }

        alphabet[string1[i - 1]] = i;
    }

    return distances[string1Length][string2Length];
}

int levenshteinThresholdForString(std::string string) {
    // Very small strings should require exact matches.
    if (string.length() <= 2) {
        return 0;
    }

    // The longer a string, the more mistakes we allow it to have.
    return static_cast<int>(string.length()) / 3;
}

bool isFuzzyMatch(std::string string, std::string knownString) {
    auto preprocess = [](auto string) {
        return lowercaseString(stringByRemovingSpecialCharacters(stringByCompressingIntrawordSpacesInString(string)));
    };

    string = preprocess(string);
    knownString = preprocess(knownString);
    return levenshtein(string, knownString) <= levenshteinThresholdForString(knownString);
}

bool isFuzzyMatch(std::string string, std::vector<std::string> knownStrings)
{
    for (const auto& knownString : knownStrings) {
        if (isFuzzyMatch(string, knownString)) {
            return true;
        }
    }

    return false;
}

bool hasFuzzyPrefixMatch(std::string string, const std::vector<std::string>& prefixes) {
    auto words = splitString(string);
    std::string substring;
    for (size_t i = 0; i < words.size(); ++i) {
        substring += words[i];
        for (const auto& prefix : prefixes) {
            if (isFuzzyMatch(substring, prefix)) {
                return true;
            }
        }

        substring += " ";
    }

    return false;
}

bool hasFuzzySuffixMatch(std::string string, const std::vector<std::string>& suffixes) {
    auto words = splitString(string);
    std::string substring;
    for (int i = static_cast<int>(words.size()) - 1; i >= 0; --i) {
        substring = words[i] + " " + substring;
        for (const auto& suffix : suffixes) {
            if (isFuzzyMatch(substring, suffix)) {
                return true;
            }
        }
    }

    return false;
}

size_t findFirstFuzzyWordMatch(std::string string, std::string word, std::string* outMatchingWord) {
    for (const auto& candidate : splitString(string)) {
        if (isFuzzyMatch(candidate, word)) {
            if (outMatchingWord) {
                *outMatchingWord = candidate;
            }

            return string.find(candidate);
        }
    }

    return std::string::npos;
}

} // namespace
