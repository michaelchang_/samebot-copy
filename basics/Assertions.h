#ifndef __ASSERTIONS_H
#define __ASSERTIONS_H

#include <cassert>

#define ASSERT(condition) \
    assert(condition)

#define ASSERT_NOT_REACHED() \
    assert(false);

template <typename T>
void _ignore(T&&) { }

#define UNUSED(variable) \
    _ignore(variable)

bool isErrorLoggingEnabled();
void disableErrorLogging();
void enableErrorLogging();

#define LOG_ERROR(error, ...) \
    do { \
        if (isErrorLoggingEnabled()) { \
            fprintf(stderr, "ERROR in %s() - %s:%d: ", __func__, __FILE__, __LINE__); \
            fprintf(stderr, error, ##__VA_ARGS__); \
            fprintf(stderr, "\n"); \
        } \
    } while (0)

#ifdef DEBUG
bool isDebugLoggingEnabled();
void disableDebugLogging();
void enableDebugLogging();
#define LOG_DEBUG(format, ...) \
    do { \
        if (isDebugLoggingEnabled()) { \
            printf(format, ##__VA_ARGS__); \
            printf("\n"); \
        }\
    } while (0)
#else
#define LOG_DEBUG(format, ...)
#endif

#endif // __ASSERTIONS_H

