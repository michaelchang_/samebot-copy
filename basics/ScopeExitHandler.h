#ifndef __SCOPE_EXIT_HANDLER_H
#define __SCOPE_EXIT_HANDLER_H

#include <functional>

namespace samebot {

class ScopeExitHandler {
public:
    ScopeExitHandler(std::function<void (void)> function) : m_function(function) {}
    ~ScopeExitHandler() { m_function(); }

private:
    std::function<void (void)> m_function;
};

} // namespace

#endif // __SCOPE_EXIT_HANDLER_H

