#include "IRCBot.h"

#include "basics/Assertions.h"
#include "network/irc/IRCMessage.h"

using namespace samebot;

IRCBot::IRCBot(std::string host, int port, std::string name)
    : m_client(host, port)
    , m_name(name)
{
}

void IRCBot::joinChannel(std::string channel) {
    ensureConnected();
    m_client.joinChannel(channel);
}

void IRCBot::runForever() {
    ensureConnected();
    m_client.beginProcessingMessages();
}

#pragma mark - IRCClientDelegate

void IRCBot::handleError(IRCClient& client, const IRCMessage& message) {
    LOG_ERROR("Server error: %s", message.toString().c_str());
    client.disconnect();
}

void IRCBot::handlePing(IRCClient& client, const IRCMessage& message) {
    if (message.parameters().size() != 1) {
        LOG_ERROR("Received malformed PING (wrong number of parameters); ignoring");
        return;
    }

    LOG_DEBUG("Received PING; responding with PONG");
    client.pong(message.parameters().front());
}

void IRCBot::handleMessage(IRCClient& client, const IRCMessage& message) {

    if (message.parameters().size() != 2) {
        LOG_ERROR("Received malformed PRIVMSG (wrong number of parameters); ignoring");
        return;
    }

    didReceiveMessage(message);
}

#pragma mark - Internals

bool IRCBot::ensureConnected() {
    m_client.setDelegate(shared_from_this());
    if (m_client.isConnected()) {
        return true;
    }

    m_client.connect();
    if (m_client.isConnected()) {
        m_client.logIn(m_name, m_name, "samebotd");
    }

    return m_client.isConnected();
}

