#ifndef __IRC_BOT_H
#define __IRC_BOT_H

#include "basics/InheritableEnableSharedFromThis.h"
#include "network/irc/IRCClient.h"
#include <memory>
#include <string>

namespace samebot {

class IRCBot : public IRCClientDelegate, public InheritableEnableSharedFromThis<IRCBot> {
public:
    IRCBot(std::string host, int port, std::string name);

    void joinChannel(std::string);
    virtual void runForever();

protected:
    IRCClient& client() { return m_client; }
    virtual void didReceiveMessage(const IRCMessage&) = 0;

private:
    virtual void handlePing(IRCClient&, const IRCMessage&) override;
    virtual void handleError(IRCClient&, const IRCMessage&) override;
    virtual void handleMessage(IRCClient&, const IRCMessage&) override;

    bool ensureConnected();

    IRCClient m_client;
    std::string m_name;
};

};

#endif // __IRC_BOT_H

