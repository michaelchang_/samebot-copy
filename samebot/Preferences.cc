#include "Preferences.h"

#include <atomic>
#include <memory>

using namespace samebot;

static std::string standardPreferencesDatabasePath = "preferences.db";

Preferences& Preferences::standardPreferences() {
    static auto preferences = std::make_unique<Preferences>(standardPreferencesDatabasePath);
    static std::atomic<Preferences*> atomicPreferences(preferences.get());
    return *atomicPreferences;
}

Preferences::Preferences(std::string databasePath)
    : m_database(databasePath)
{
    m_database.executeSQLQuery("SELECT key, value FROM preferences", [this](const SQLiteResultRow& row) {
        m_cache[row.get<std::string>(0)] = row.get<std::string>(1);
    });
}

void Preferences::set(std::string key, std::string value) {
    std::lock_guard<std::mutex> lock(m_accessMutex);
    if (m_cache.find(key) != m_cache.end() && m_cache[key] == value) {
        return;
    }

    m_cache[key] = value;

    initializeSchemaIfNecessary();
    if (value.length()) {
        m_database.executeSQL("INSERT OR REPLACE INTO preferences VALUES (?, ?)", key, value);
    } else {
        m_database.executeSQL("DELETE FROM preferences WHERE key = ?", key);
    }
}

std::string Preferences::get(std::string key) {
    // FIXME: Implement in-memory caching.
    std::lock_guard<std::mutex> lock(m_accessMutex);
    return m_cache[key];
}

#pragma mark - Unit test support

#ifdef DEBUG
int Preferences::test_numberOfRowsInDatabase() {
    std::lock_guard<std::mutex> lock(m_accessMutex);
    int rows;
    if (m_database.executeSQLQuery(&rows, "SELECT COUNT(*) FROM preferences")) {
        return rows;
    }
    return -1;
}
#endif // DEBUG

#pragma mark - Internals

void Preferences::initializeSchemaIfNecessary() {
    // It is assumed that the caller has the lock already.

    if (m_databaseReady) {
        return;
    }

    int version;
    if (m_database.executeSQLQuery(&version, "PRAGMA user_version") && version == 1) {
        m_databaseReady = true;
        return;
    }

    m_database.destroyData();
    m_database.executeSQL("CREATE TABLE preferences (key TEXT PRIMARY KEY, value TEXT)");
    m_database.executeSQL("PRAGMA user_version = 1");
    m_databaseReady = true;
}

