#include "SameBot.h"

#include "basics/Configuration.h"
#include "network/irc/IRCMessage.h"

using namespace samebot;

SameBot::SameBot(const Configuration& configuration)
    : IRCBot(configuration.host(), configuration.port(), configuration.nickname())
    , m_configuration(configuration)
{
}

void SameBot::runForever() {
    if (m_switchboard) {
        m_switchboardProcessingThread = std::thread([this] {
            m_switchboard->startProcessingOutgoingMessages();
        });
    }

    IRCBot::runForever();
}

void SameBot::didReceiveMessage(const IRCMessage& message) {
    m_switchboard->dispatchMessageToModules(std::make_shared<IRCMessage>(message));
}

#pragma mark - Module support

void SameBot::addModule(std::unique_ptr<Module> module) {
    if (!m_switchboard) {
        m_switchboard = std::make_shared<ModuleSwitchboard>();
        m_switchboard->setMessageSender(cast_shared_from_this<SameBot>());
    }

    module->setConfiguration(&configuration());
    m_switchboard->registerModule(std::move(module));
}

#pragma mark ModuleSwitchboardMessageSender

void SameBot::sendMessageForModuleSwitchboard(ModuleSwitchboard&, std::shared_ptr<IRCMessage> message) {
    message = client().attachPrefixToMessageIfNecessary(message);
    client().sendMessage(*message);
    m_switchboard->notifyModulesMessageWasSent(message);
}

