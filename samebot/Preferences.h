#ifndef __PREFERENCES_H
#define __PREFERENCES_H

#include "database/SQLiteDatabase.h"
#include <map>
#include <mutex>

namespace samebot {

class Preferences {
public:
    static Preferences& standardPreferences();
    Preferences(std::string databasePath);

    void set(std::string key, std::string value);
    std::string get(std::string key);

#ifdef DEBUG
    int test_numberOfRowsInDatabase();
#endif

private:
    void initializeSchemaIfNecessary();

    SQLiteDatabase m_database;
    std::mutex m_accessMutex;
    std::unique_lock<std::mutex> m_accessLock;
    bool m_databaseReady = false;
    std::map<std::string, std::string> m_cache;
};

} // namespace

#endif // __PREFERENCES_H

