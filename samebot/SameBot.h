#ifndef __SAME_BOT_H
#define __SAME_BOT_H

#include "bot/IRCBot.h"

#include "modules/ModuleSwitchboard.h"

namespace samebot {

class Configuration;

class SameBot : public IRCBot, public ModuleSwitchboardMessageSender {
public:
    SameBot(const Configuration&);

    const Configuration& configuration() const { return m_configuration; }

    void addModule(std::unique_ptr<Module>);

    template <typename ModuleType, typename... Args>
    void createModule(Args... args) {
        addModule(std::make_unique<ModuleType>(args...));
    }

    virtual void runForever() override;

protected:
    virtual void didReceiveMessage(const IRCMessage&) override;

private:
    virtual void sendMessageForModuleSwitchboard(ModuleSwitchboard&, std::shared_ptr<IRCMessage>) override;

    const Configuration& m_configuration;
    std::thread m_switchboardProcessingThread;
    std::shared_ptr<ModuleSwitchboard> m_switchboard;
};

} // namespace

#endif // __SAME_BOT_H

