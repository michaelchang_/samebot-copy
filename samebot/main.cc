#include "SameBot.h"
#include "basics/Assertions.h"
#include "basics/Configuration.h"
#include "modules/control_center/ControlCenterModule.h"
#include "modules/dank/DankModule.h"
#include "modules/emoticon/EmoticonModule.h"
#include "modules/reverse/ReverseModule.h"
#include "modules/same/SameModule.h"
#include "modules/scrapbook/ScrapbookModule.h"
#include "modules/lookup/LookUpModule.h"
#include "modules/lyft/LyftModule.h"
#include "network/URLSession.h"
#include <sstream>

using namespace samebot;

int main(int argc, char** argv) {
    if (argc != 2) {
        printf("USAGE: %s <configuration file>\n", argv[0]);
        return EXIT_SUCCESS;
    }

    std::string configurationFilePath(argv[1]);
    auto configuration = Configuration::create(configurationFilePath);
    if (!configuration) {
        LOG_ERROR("Failed to read configuration file");
        return EXIT_FAILURE;
    }

    auto host = configuration->host();
    auto port = configuration->port();
    auto nickname = configuration->nickname();
    auto channel = configuration->channel();

    if (!host.length() || !nickname.length() || !channel.length() || !port) {
        LOG_ERROR("Invalid parameters");
        return EXIT_FAILURE;
    }

    URLSession::initialize();

    auto samebot = std::make_shared<SameBot>(*configuration);

    samebot->createModule<ControlCenterModule>();
    samebot->createModule<DankModule>();
    samebot->createModule<EmoticonModule>();
    samebot->createModule<LookUpModule>();
    samebot->createModule<LyftModule>();
    samebot->createModule<ReverseModule>();
    samebot->createModule<SameModule>();
    samebot->createModule<ScrapbookModule>();

    samebot->joinChannel(channel);
    samebot->runForever();

    URLSession::deinitialize();
    return EXIT_SUCCESS;
}

