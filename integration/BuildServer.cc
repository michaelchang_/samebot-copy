#include "BuildServer.h"

#include "basics/Assertions.h"
#include <cstdarg>
#include <functional>
#include <memory>
#include <unistd.h>
#include <vector>

#define SERVER_LOG(message, ...)        log(LogLevel::Standard, message, ##__VA_ARGS__)
#define SERVER_LOG_ERROR(message, ...)  log(LogLevel::Error, message, ##__VA_ARGS__)

using namespace samebot::integrator;

BuildServer::BuildServer(int port, std::string repositoryPath, std::unique_ptr<BuildBot> bot)
    : m_socketServer(port)
    , m_repositoryPath(repositoryPath)
    , m_bot(std::move(bot))
{
}

void BuildServer::serveForever() {
    if (m_bot) {
        m_botThread = std::thread([this] { m_bot->runForever(); });
    }

    ensureWorkingDirectoryIsProjectRoot();
    SERVER_LOG("samebotintegratord is online (revision %s)", currentGitCommitHash().c_str());

    m_socketServer.setDelegate(shared_from_this());
    m_socketServer.listenForConnections();
}

#pragma mark - SocketServerDelegate

void BuildServer::socketServerDidConnectClient(SocketServer&) {
    SERVER_LOG("BuildServer received build request!");

    m_socketServer.sendToClient("Status: 200 OK\r\n");
    m_socketServer.disconnectFromClient();

    ensureWorkingDirectoryIsProjectRoot();
    if (!pull()) {
        return;
    }

    static std::vector<std::string> targets { "samebotd", "samebotintegratord" };
    for (std::string target : targets) {
        if (integrate(target) == IntegrationResult::Success) {
            deploy(target);
        }
    }
}

#pragma mark - Internals

bool BuildServer::runCommand(std::string command, ShouldLog shouldLog) {
    if (shouldLog == ShouldLog::No) {
        return system(command.c_str()) == 0;
    }

    SERVER_LOG("$ %s", command.c_str());
    bool success;
    std::string output = outputForCommand(command.c_str(), ShouldLog::No, &success);
    if (output.length()) {
        SERVER_LOG("%s", output.c_str());
    }

    return success;
}

std::string BuildServer::outputForCommand(std::string command, ShouldLog shouldLog, bool* outSuccess) {
    if (shouldLog == ShouldLog::Yes) {
        SERVER_LOG("$ %s", command.c_str());
    }

    // Redirect stderr to stdout so we can capture it.
    command += " 2>&1";
    std::unique_ptr<FILE, std::function<void (FILE*)>> pipe(popen(command.c_str(), "r"), [&outSuccess] (FILE* file) {
        int exitCode = pclose(file);
        if (outSuccess) {
            *outSuccess = !exitCode;
        }
    });

    if (!pipe) {
        SERVER_LOG_ERROR("Could not execute command: %s", command.c_str());
        return "";
    }

    char *buffer = nullptr;
    std::string result;
    while (!feof(pipe.get())) {
        size_t length;
        if (getline(&buffer, &length, pipe.get()) != -1) {
            result += buffer;
            if (shouldLog == ShouldLog::Yes) {
                SERVER_LOG("%s", buffer);
            }
        }
    }

    if (buffer) {
        free(buffer);
    }

    return result;
}

std::string BuildServer::currentGitCommitHash() {
    return outputForCommand("git log --pretty=format:'%h' -n 1", ShouldLog::No);
}

bool BuildServer::ensureWorkingDirectoryIsProjectRoot() {
    LOG_DEBUG("Working directory: %s", m_repositoryPath.c_str());
    if (chdir(m_repositoryPath.c_str()) == 0) {
        return true;
    }

    LOG_ERROR("Could not navigate to repository path '%s'", m_repositoryPath.c_str());
    return false;
}

bool BuildServer::pull() {
    if (runCommand("git pull")) {
        return true;
    }

    LOG_ERROR("Could not synchronize repository");
    return false;
}

bool BuildServer::targetIsUpToDate(std::string target) {
    return runCommand("make -q " + target, ShouldLog::No);
}

BuildServer::IntegrationResult BuildServer::integrate(std::string target) {
    if (targetIsUpToDate(target)) {
        SERVER_LOG("Target '%s' is up-to-date", target.c_str());
        return IntegrationResult::NoChange;
    }

    if (runCommand("make " + target)) {
        SERVER_LOG("Built target '%s'", target.c_str());
        return IntegrationResult::Success;
    }

    std::string commitHash = currentGitCommitHash();
    std::string author = outputForCommand("git log --pretty=format:'%aN' -n 1", ShouldLog::No);
    SERVER_LOG_ERROR("Failed to build '%s' (commit: %s, author: %s)", target.c_str(), commitHash.c_str(), author.c_str());
    return IntegrationResult::Failure;
}

void BuildServer::deploy(std::string target) {
    runCommand("systemctl --user restart " + target);
}

void BuildServer::log(LogLevel level, const char* format, ...) {
    va_list arguments;
    va_start(arguments, format);
    // Add 1 for the null terminator.
    size_t size = vsnprintf(nullptr, 0, format, arguments) + 1;
    va_end(arguments);

    va_start(arguments, format);
    std::unique_ptr<char[]> buffer(new char[size]);
    vsnprintf(buffer.get(), size, format, arguments);
    va_end(arguments);

    std::string message(buffer.get());
    switch (level) {
        case LogLevel::Standard:
            LOG_DEBUG("%s", message.c_str());
            if (m_bot) {
                m_bot->log(message);
            }
            break;

        case LogLevel::Error:
            LOG_ERROR("%s", message.c_str());
            if (m_bot) {
                m_bot->notify(message);
                m_bot->log(message);
            }
            break;
    }
}

