#ifndef __SOCKET_SERVER_H
#define __SOCKET_SERVER_H

#include <memory>
#include <string>

namespace samebot {
namespace integrator {

class SocketServerDelegate;

class SocketServer {
public:
    SocketServer(int port);

    void setDelegate(std::shared_ptr<SocketServerDelegate> delegate) { m_delegate = delegate; }
    void listenForConnections();
    void sendToClient(std::string);
    void disconnectFromClient();

private:
    void setUp();
    void tearDown();

    int m_port;
    int m_socketDescriptor = 0;
    int m_currentClientSocketDescriptor = 0;
    std::weak_ptr<SocketServerDelegate> m_delegate;
};

class SocketServerDelegate {
public:
    virtual void socketServerDidConnectClient(SocketServer&) {}
};

} // namespace
} // namespace

#endif // __SOCKET_SERVER_H

