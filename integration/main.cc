#include "BuildServer.h"
#include "basics/Assertions.h"
#include <iostream>
#include <sstream>

using namespace samebot::integrator;

static bool parseInt(std::string string, int& outInt) {
    std::istringstream stream(string);
    stream >> outInt;
    return !stream.fail();
}

int main(int argc, char** argv) {
    if (argc != 3 && argc != 6) {
        printf("USAGE: %s <port> <repository path> [<irc host> <irc port> <notification channel>] \r\n", argv[0]);
        return EXIT_SUCCESS;
    }

    std::string portString(argv[1]);
    std::string repositoryPath(argv[2]);

    std::unique_ptr<BuildBot> bot;
    if (argc == 6) {
        std::string ircHost(argv[3]);
        std::string ircPortString(argv[4]);
        std::string notificationChannel(argv[5]);
        int ircPort;
        if (!parseInt(ircPortString, ircPort)) {
            LOG_ERROR("Invalid IRC port: %s", ircPortString.c_str());
            return EXIT_FAILURE;
        }

        if (!ircHost.length() || !notificationChannel.length()) {
            LOG_ERROR("Invalid bot parameters");
            return EXIT_FAILURE;
        }

        bot = std::unique_ptr<BuildBot>(new BuildBot(ircHost, ircPort, notificationChannel));
    }

    if (!repositoryPath.length()) {
        LOG_ERROR("Invalid repository path.");
        return EXIT_FAILURE;
    }

    int port;
    if (!parseInt(portString, port)) {
        LOG_ERROR("Invalid port: %s", portString.c_str());
        return EXIT_FAILURE;
    }

    auto server = std::make_shared<BuildServer>(port, repositoryPath, std::move(bot));
    server->serveForever();
    return EXIT_SUCCESS;
}

