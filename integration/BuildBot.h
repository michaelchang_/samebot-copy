#ifndef __BUILD_BOT_H
#define __BUILD_BOT_H

#include "IRCNotifier.h"
#include "bot/IRCBot.h"

namespace samebot {
namespace integrator {

class BuildBot : public IRCBot {
public:
    BuildBot(std::string host, int port, std::string notificationChannel);

    void log(std::string);
    void notify(std::string);

private:
    virtual void didReceiveMessage(const IRCMessage&) override {}

    bool m_hasJoinedIntegrationChannel = false;
    IRCNotifier m_notifier;
};

} // namespace
} // namespace

#endif // __BUILD_BOT_H

