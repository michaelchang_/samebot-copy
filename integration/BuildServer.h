#ifndef __BUILD_SERVER_H
#define __BUILD_SERVER_H

#include "BuildBot.h"
#include "SocketServer.h"
#include <thread>

namespace samebot {
namespace integrator {

class BuildServer : public SocketServerDelegate, public std::enable_shared_from_this<BuildServer> {
public:
    BuildServer(int port, std::string repositoryPath, std::unique_ptr<BuildBot>);
    void serveForever();

    virtual void socketServerDidConnectClient(SocketServer&) override;

private:
    enum class ShouldLog {
        No,
        Yes,
    };

    bool runCommand(std::string, ShouldLog = ShouldLog::Yes);
    std::string outputForCommand(std::string, ShouldLog = ShouldLog::Yes, bool* outSuccess = nullptr);

    std::string currentGitCommitHash();

    bool ensureWorkingDirectoryIsProjectRoot();
    bool pull();

    bool targetIsUpToDate(std::string target);

    enum class IntegrationResult {
        Failure,
        Success,
        NoChange,
    };

    IntegrationResult integrate(std::string target);
    void deploy(std::string target);

    enum class LogLevel {
        Standard,
        Error,
    };

    void log(LogLevel, const char* format, ...) __attribute__ ((format (printf, 3, 4)));

    SocketServer m_socketServer;
    std::string m_repositoryPath;
    std::shared_ptr<BuildBot> m_bot;
    std::thread m_botThread;
};

} // namespace
} // namespace

#endif // __BUILD_SERVER_H

