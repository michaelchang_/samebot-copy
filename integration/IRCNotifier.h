#ifndef __IRC_NOTIFIER_H
#define __IRC_NOTIFIER_H

#include "network/irc/IRCClient.h"
#include <string>

namespace samebot {
namespace integrator {

class IRCNotifier {
public:
    IRCNotifier(std::string host, int port, std::string name, std::string channel);
    void notify(std::string message);

private:
    IRCClient m_client;
    std::string m_name;
    std::string m_channel;
};

} // namespace
} // namespace

#endif // __IRC_NOTIFIER_H

