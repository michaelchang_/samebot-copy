#include "BuildBot.h"

using namespace samebot::integrator;

static const std::string buildBotChannel = "#samebot-integration";

BuildBot::BuildBot(std::string host, int port, std::string notificationChannel)
    : IRCBot(host, port, "buildbot")
    , m_notifier(host, port, "notifybot", notificationChannel)
{
}

void BuildBot::log(std::string message) {
    if (!m_hasJoinedIntegrationChannel) {
        joinChannel(buildBotChannel);
        m_hasJoinedIntegrationChannel = true;
    }

    client().sendMessage(buildBotChannel, message);
}

void BuildBot::notify(std::string message) {
    m_notifier.notify("NOTICE: " + message);
}

