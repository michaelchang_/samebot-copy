#include "IRCNotifier.h"

using namespace samebot::integrator;

IRCNotifier::IRCNotifier(std::string host, int port, std::string name, std::string channel)
    : m_client(host, port)
    , m_name(name)
    , m_channel(channel)
{
}

void IRCNotifier::notify(std::string message) {
    if (!m_client.isConnected()) {
        m_client.connect();
        if (!m_client.isConnected()) {
            return;
        }
    }

    m_client.logIn(m_name, m_name, "samebotintegratord");
    m_client.joinChannel(m_channel);
    m_client.sendMessage(m_channel, message);
    m_client.quit();
}

