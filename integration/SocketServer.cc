#include "SocketServer.h"

#include "basics/Assertions.h"
#include <netdb.h>
#include <netinet/in.h>
#include <unistd.h>

using namespace samebot::integrator;

SocketServer::SocketServer(int port)
    : m_port(port)
{
}

void SocketServer::listenForConnections() {
    if (!m_socketDescriptor) {
        setUp();
    }

    if (!m_socketDescriptor) {
        return;
    }

    listen(m_socketDescriptor, 5);
    while (true) {
        sockaddr_in address;
        unsigned int addressLength = sizeof(address);
        m_currentClientSocketDescriptor = accept(m_socketDescriptor, (sockaddr*)&address, &addressLength);
        if (m_currentClientSocketDescriptor < 0) {
            LOG_ERROR("Failed to connect client");
            m_currentClientSocketDescriptor = 0;
            continue;
        }

        LOG_DEBUG("Connected client");
        if (auto delegate = m_delegate.lock()) {
            delegate->socketServerDidConnectClient(*this);
        }
    }
}

void SocketServer::sendToClient(std::string message) {
    ASSERT(m_socketDescriptor);
    ASSERT(m_currentClientSocketDescriptor);

    write(m_currentClientSocketDescriptor, message.c_str(), message.length());
}

void SocketServer::disconnectFromClient() {
    ASSERT(m_socketDescriptor);
    ASSERT(m_currentClientSocketDescriptor);

    shutdown(m_currentClientSocketDescriptor, SHUT_RDWR);
    close(m_currentClientSocketDescriptor);
    m_currentClientSocketDescriptor = 0;
}

void SocketServer::setUp() {
    ASSERT(!m_socketDescriptor);

    m_socketDescriptor = socket(AF_INET, SOCK_STREAM, 0);
    if (m_socketDescriptor < 0) {
        LOG_ERROR("Could not set up socket");
        m_socketDescriptor = 0;
        return;
    }

    sockaddr_in address = {};
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(m_port);

    if (bind(m_socketDescriptor, (sockaddr*)&address, sizeof(address)) < 0) {
        LOG_ERROR("Could not bind socket");
        tearDown();
    }
}

void SocketServer::tearDown() {
    ASSERT(m_socketDescriptor);

    close(m_socketDescriptor);
    m_socketDescriptor = 0;
}

