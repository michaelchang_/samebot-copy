PROJECT_PATH = $(CURDIR)
CXXFLAGS = -c -O2 -Wall -Werror -std=c++14 -I$(PROJECT_PATH) -Wno-unknown-pragmas
LDFLAGS = -lpthread -lsqlite3 -lcurl
DEBUG_CXXFLAGS = -O0 -g3 -DDEBUG

SOURCES = $(shell find . -name '*.cc' -not -path "./test/*" -not -path "./integration/*")
OBJECTS = $(patsubst %.cc,%.o,$(SOURCES))
TARGET = samebotd

TEST_SOURCES = $(shell find . -name '*.cc' -not -path "./samebot/main.cc" -not -path "./integration/*")
TEST_OBJECTS = $(patsubst %.cc,%.o,$(TEST_SOURCES))
TEST_TARGET = samebot-tests

CI_SERVER_SOURCES = $(shell find integration network basics bot -name '*.cc')
CI_SERVER_OBJECTS = $(patsubst %.cc,%.o,$(CI_SERVER_SOURCES))
CI_SERVER_TARGET = samebotintegratord

DEPENDENCIES = $(shell find . -name "*.d")

debug: CXXFLAGS += $(DEBUG_CXXFLAGS)
debug: $(TARGET) $(CI_SERVER_TARGET) test

-include $(DEPENDENCIES)

all: $(TARGET) $(CI_SERVER_TARGET)

clean:
	$(RM) $(DEPENDENCIES)
	$(RM) $(TARGET) $(OBJECTS)
	$(RM) $(TEST_TARGET) $(TEST_OBJECTS)
	$(RM) $(CI_SERVER_TARGET) $(CI_SERVER_OBJECTS)

test: CXXFLAGS += $(DEBUG_CXXFLAGS)
test: $(TEST_TARGET)
	./$(TEST_TARGET)

%.o: %.cc
	$(CXX) $(CXXFLAGS) -MMD $< -o $(patsubst %.cc,%.o,$<)

$(TEST_TARGET): $(TEST_OBJECTS)
	$(CXX) -o $(TEST_TARGET) $(TEST_OBJECTS) $(LDFLAGS)

$(CI_SERVER_TARGET): $(CI_SERVER_OBJECTS)
	$(CXX) -o $(CI_SERVER_TARGET) $(CI_SERVER_OBJECTS) $(LDFLAGS)

$(TARGET): $(OBJECTS)
	$(CXX) -o $(TARGET) $(OBJECTS) $(LDFLAGS)

.PHONY: all debug clean test

