# samebot #

samebot is an IRC bot that will eventually support a number of inane features.

## Overview ##

samebot consists of two primary components:

* **samebotd:** A daemon that maintains a persistent connection to an IRC server and runs the bot logic.

* **samebotintegratord:** A daemon that listens on a port. When it sees activity on that port, it automatically pulls down the
  latest changes, integrates them, and deploys new builds of samebotd and samebotintegratord. (That's right, it builds and deploys
  *itself.*)

This could be accomplished by a single daemon, but I chose to split responsibility across two daemons so that they don't have to
be deployed in lockstep. The build server can remain available even if samebotd changes are made. Similarly, samebotd can remain
online while the build server is being redeployed.

samebot has various embodiments on the IRC server:

* **samebot:** The bot that most users interact with.

* **buildbot:** A bot that lives on its own #samebot-integration channel. It currently just logs a lot of information about the
  build server's activities, but will eventually provide a more complete interface to the build server (allowing developers to
  kick off new builds, run tests, investigate build failures, etc.).

* **notifybot:** A transient bot that samebotintegratord uses to notify users on public channels (outside of
  #samebot-integration). For example, this is the mechanism used to report build breakages.

## Features ##

samebotd is currently just a proof-of-concept, mostly to demonstrate that the underlying IRC framework is functional. It reverses
any string that begins with "samebot: " and that's about it.

samebotintegratord is a lot more functional. It can:

* automatically pull new changes when they're pushed to the repository.

* build samebotd and itself.

* interact with systemd to deploy new versions of samebotd and itself.

* notify users on IRC when they break the build.

* log a lot of information on its own #samebot-integration channel.
