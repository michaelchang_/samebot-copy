#ifndef __USER_INTELLIGENCE_CLIENT_H
#define __USER_INTELLIGENCE_CLIENT_H

#include "UserInformationRequest.h"
#include <chrono>
#include <functional>
#include <map>
#include <string>
#include <queue>

namespace samebot {

class IRCMessage;
class UserIntelligenceClientMessageSender;
class UserIntelligenceServer;

class UserIntelligenceClient {
public:
    UserIntelligenceClient(UserIntelligenceServer&, UserIntelligenceClientMessageSender&);

    std::string userDataForKey(const std::string& username, const std::string& key);
    void setUserDataForKey(const std::string& username, const std::string& key, const std::string& value);

    void requestInformationFromUser(const UserInformationRequest&, std::function<void (const std::string& data)> completionHandler);
    void processPrivateMessage(const IRCMessage& message);

private:
    struct UserInformationRequestContext {
        std::string userInformationKey;
        std::chrono::steady_clock::time_point requestCreationTimePoint;
        std::function<void (const std::string&)> completionHandler;

        bool isExpired() const;
    };

    struct DeferredRequest {
        UserInformationRequest informationRequest;
        std::function<void (const std::string&)> completionHandler;
    };

    bool hasActiveRequestForUser(const std::string& username) const;
    void performNextRequestForUser(const std::string& username);

    void deferRequest(const UserInformationRequest&, std::function<void (const std::string&)> completionHandler);
    void performRequest(const UserInformationRequest&, std::function<void (const std::string&)> completionHandler);

    UserIntelligenceServer& m_server;
    UserIntelligenceClientMessageSender& m_messageSender;
    std::map<std::string, std::queue<DeferredRequest>> m_usernamesToDeferredRequestQueues;
    std::map<std::string, UserInformationRequestContext> m_usernamesToActiveRequestContexts;
};

class UserIntelligenceClientMessageSender {
public:
    virtual void sendPublicMessageForUserIntelligenceClient(UserIntelligenceClient&, const std::string& message) = 0;
    virtual void sendPrivateMessageForUserIntelligenceClient(UserIntelligenceClient&, const std::string& recipient, const std::string& message) = 0;
};

} // namespace

#endif // __USER_INTELLIGENCE_CLIENT_H
