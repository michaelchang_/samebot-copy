#ifndef __USER_INFORMATION_SQLITE_DATA_STORE_H
#define __USER_INFORMATION_SQLITE_DATA_STORE_H

#include "database/SQLiteDataStore.h"
#include <string>

namespace samebot {

class UserInformationSQLiteDataStore : public SQLiteDataStore {
public:
    using SQLiteDataStore::SQLiteDataStore;

    std::string dataForUser(const std::string& username, const std::string& key);
    void setDataForUser(const std::string& username, const std::string& key, const std::string& value);

private:
    virtual int currentSchemaVersion() const override { return 1; }
    virtual bool createSchema(SQLiteDatabase&) override;
};

} // namespace

#endif // __USER_INFORMATION_SQLITE_DATA_STORE_H
