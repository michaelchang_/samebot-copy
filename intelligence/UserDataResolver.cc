#include "UserDataResolver.h"

#include "UserIntelligenceClient.h"
#include "basics/Assertions.h"
#include "network/irc/IRCMessage.h"

using namespace samebot;

UserDataResolver::UserDataResolver(UserIntelligenceClient& client)
    : m_client(client)
{
}

void UserDataResolver::resolve(const std::string& data, const IRCMessage& message, std::function<void (const std::string&)> completionHandler) {
    ASSERT(message.prefix());
    ASSERT(message.prefix()->type() == IRCMessagePrefix::Type::User);

    auto inResponseToPrivateMessage = message.parameters().front()[0] == '#' ? InResponseToPrivateMessage::No : InResponseToPrivateMessage::Yes;
    auto username = message.prefix()->user().username;
    auto nickname = message.prefix()->user().nickname;
    resolve(data, username, nickname, inResponseToPrivateMessage, completionHandler);
}

void UserDataResolver::resolve(const std::string& data, const std::string& username, const std::string& nickname, InResponseToPrivateMessage inResponseToPrivateMessage, std::function<void (const std::string&)> completionHandler) {
    auto key = userInformationKeyForData(data);
    if (!key.length()) {
        // Nothing to resolve, so just return the data unmodified.
        completionHandler(data);
        return;
    }

    auto resolvedData = m_client.userDataForKey(username, key);
    if (resolvedData.length()) {
        // We already have the necessary data, so we can return it immediately.
        completionHandler(resolvedData);
        return;
    }

    // This is user data but we don't already know it, so we need to ask for it.
    auto response = responseToUserRequestInvolvingDataWithKey(key);
    auto prompt = privatePromptForInformationWithKey(key);
    UserInformationRequest request(username, nickname, key, response, prompt);
    m_client.requestInformationFromUser(request, completionHandler);
}
