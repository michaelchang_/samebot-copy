#ifndef __USER_ADDRESS_RESOLVER_H
#define __USER_ADDRESS_RESOLVER_H

#include "UserDataResolver.h"

namespace samebot {

class UserAddressResolver : public UserDataResolver {
public:
    using UserDataResolver::UserDataResolver;

private:
    virtual std::string userInformationKeyForData(const std::string&) override;
    virtual std::string responseToUserRequestInvolvingDataWithKey(const std::string& key) override;
    virtual std::string privatePromptForInformationWithKey(const std::string& key) override;
};

} // namespace

#endif // __USER_ADDRESS_RESOLVER_H
