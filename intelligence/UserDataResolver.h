#ifndef __USER_DATA_RESOLVER_H
#define __USER_DATA_RESOLVER_H

#include "UserInformationRequest.h"
#include <functional>
#include <string>

namespace samebot {

class IRCMessage;
class UserIntelligenceClient;

class UserDataResolver {
public:
    UserDataResolver(UserIntelligenceClient&);

    void resolve(const std::string& data, const IRCMessage&, std::function<void (const std::string&)> completionHandler);
    void resolve(const std::string& data, const std::string& username, const std::string& nickname, InResponseToPrivateMessage, std::function<void (const std::string&)> completionHandler);

protected:
    virtual std::string userInformationKeyForData(const std::string&) = 0;
    virtual std::string responseToUserRequestInvolvingDataWithKey(const std::string& key) = 0;
    virtual std::string privatePromptForInformationWithKey(const std::string& key) = 0;

private:
    UserIntelligenceClient& m_client;
};

} // namespace

#endif // __USER_DATA_RESOLVER_H