#include "UserInformationSQLiteDataStore.h"

#include "basics/StringUtilities.h"

using namespace samebot;

bool UserInformationSQLiteDataStore::createSchema(SQLiteDatabase& database) {
    bool result = database.executeSQL(
        "CREATE TABLE users ("
            "id INTEGER PRIMARY KEY,"
            "username VARCHAR(255) NOT NULL UNIQUE)");
    if (!result) {
        return false;
    }

    result = database.executeSQL(
        "CREATE TABLE user_data_entries ("
            "id INTEGER PRIMARY KEY,"
            "key VARCHAR(255) NOT NULL,"
            "value TEXT NOT NULL,"
            "user_id INTEGER NOT NULL,"
            "FOREIGN KEY(user_id) REFERENCES users(id),"
            "UNIQUE(user_id, key))");
    if (!result) {
        return false;
    }

    return true;
}

static auto canonicalizeUsername(const std::string& username) {
    return lowercaseString(stringByTrimmingString(username));
}

std::string UserInformationSQLiteDataStore::dataForUser(const std::string& username, const std::string& key) {
    if (!setUpSchemaIfNeeded()) {
        return "";
    }

    std::string result;
    if (!database().executeSQLQuery(&result, "SELECT value FROM user_data_entries E INNER JOIN users U ON E.user_id = U.id WHERE U.username = ? AND E.key = ?", canonicalizeUsername(username), key)) {
        return "";
    }

    return result;
}

void UserInformationSQLiteDataStore::setDataForUser(const std::string& username, const std::string& key, const std::string& value) {
    if (!setUpSchemaIfNeeded()) {
        return;
    }

    int id;
    database().executeSQL("INSERT OR IGNORE INTO users VALUES (NULL, ?)", canonicalizeUsername(username));
    if (!database().executeSQLQuery(&id, "SELECT id FROM users WHERE username = ?", canonicalizeUsername(username))) {
        return;
    }

    if (!value.length()) {
        database().executeSQL("DELETE FROM user_data_entries WHERE key = ? AND user_id = ?", key, id);
        return;
    }

    database().executeSQL("UPDATE user_data_entries SET value = ? WHERE key = ? AND user_id = ?", value, key, id);
    database().executeSQL("INSERT OR IGNORE INTO user_data_entries VALUES (NULL, ?, ?, ?)", key, value, id);
    if (database().hasError()) {
        // Don't log the data as it might be sensitive.
        LOG_ERROR("UserInformationSQLiteDataStore: Could not insert user data for user %s (id %d), key %s", username.c_str(), id, key.c_str());
    }
}
