#include "UserInformationKeys.h"

namespace samebot {

namespace UserInformationKeys {
    std::string homeAddressKey = "commonUserData.homeAddress";
    std::string workAddressKey = "commonUserData.workAddress";
}

} // namespace
