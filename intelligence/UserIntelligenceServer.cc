#include "UserIntelligenceServer.h"

#include "UserIntelligenceClient.h"

using namespace samebot;

UserIntelligenceServer& UserIntelligenceServer::shared() {
    static auto server = std::make_unique<UserIntelligenceServer>("user_information.db");
    return *server;
}

UserIntelligenceServer::UserIntelligenceServer(const std::string& databasePath)
    : m_userInformationDataStore(databasePath)
{
}

std::unique_ptr<UserIntelligenceClient> UserIntelligenceServer::createClient(UserIntelligenceClientMessageSender& messageSender) {
    return std::make_unique<UserIntelligenceClient>(*this, messageSender);
}

#pragma mark - Exposed to UserIntelligenceClient

std::string UserIntelligenceServer::dataForUser(const std::string& username, const std::string& key) {
    std::lock_guard<std::mutex> lock(m_mutex);
    return m_userInformationDataStore.dataForUser(username, key);
}

void UserIntelligenceServer::setDataForUser(const std::string& username, const std::string& key, const std::string& value) {
    std::lock_guard<std::mutex> lock(m_mutex);
    m_userInformationDataStore.setDataForUser(username, key, value);
}
