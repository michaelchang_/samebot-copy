#include "UserIntelligenceClient.h"

#include "UserInformationRequest.h"
#include "UserIntelligenceServer.h"
#include "basics/StringUtilities.h"
#include "network/irc/IRCMessage.h"
#include "network/irc/IRCMessagePrefix.h"

using namespace samebot;

UserIntelligenceClient::UserIntelligenceClient(UserIntelligenceServer& server, UserIntelligenceClientMessageSender& messageSender)
    : m_server(server)
    , m_messageSender(messageSender)
{
}

std::string UserIntelligenceClient::userDataForKey(const std::string& username, const std::string& key) {
    return m_server.dataForUser(username, key);
}

void UserIntelligenceClient::setUserDataForKey(const std::string& username, const std::string& key, const std::string& value) {
    m_server.setDataForUser(username, key, value);
}

void UserIntelligenceClient::requestInformationFromUser(const UserInformationRequest& request, std::function<void (const std::string&)> completionHandler) {
    ASSERT(request.username().length());
    ASSERT(request.userInformationKey().length());

    auto data = userDataForKey(request.username(), request.userInformationKey());
    if (data.length()) {
        completionHandler(data);
        return;
    }

    // There must be some user-visible prompt for the request, or the user won't know that we're expecting something from them.
    ASSERT(request.responseToUserRequestInvolvingData().length() || request.privatePromptForInformation().length());

    if (request.responseToUserRequestInvolvingData().length()) {
        if (request.isInResponseToPrivateMessage()) {
            m_messageSender.sendPrivateMessageForUserIntelligenceClient(*this, request.nickname(), request.responseToUserRequestInvolvingData());
        } else {
            m_messageSender.sendPublicMessageForUserIntelligenceClient(*this, request.nickname() + ": " + request.responseToUserRequestInvolvingData());
        }
    }

    if (hasActiveRequestForUser(request.username())) {
        deferRequest(request, completionHandler);
    } else {
        performRequest(request, completionHandler);
    }
}

void UserIntelligenceClient::processPrivateMessage(const IRCMessage& message) {
    if (!message.prefix()) {
        return;
    }

    auto username = lowercaseString(stringByTrimmingString(message.prefix()->user().username));
    if (!hasActiveRequestForUser(username)) {
        return;
    }

    auto& context = m_usernamesToActiveRequestContexts[username];
    setUserDataForKey(username, context.userInformationKey, message.parameters().back());
    context.completionHandler(message.parameters().back());
    m_usernamesToActiveRequestContexts.erase(username);

    performNextRequestForUser(username);
}

#pragma mark - Internals

static std::string canonicalizeUsername(const std::string& username) {
    return lowercaseString(stringByTrimmingString(username));
}

bool UserIntelligenceClient::UserInformationRequestContext::isExpired() const {
    // At the moment, we consider a request expired after one minute.
    auto elapsedTime = std::chrono::steady_clock::now() - requestCreationTimePoint;
    return std::chrono::duration_cast<std::chrono::minutes>(elapsedTime).count() > 1;
}

bool UserIntelligenceClient::hasActiveRequestForUser(const std::string& username) const {
    auto canonicalizedUsername = canonicalizeUsername(username);
    if (m_usernamesToActiveRequestContexts.find(username) == m_usernamesToActiveRequestContexts.end()) {
        return false;
    }

    const auto& context = m_usernamesToActiveRequestContexts.at(username);
    return !context.isExpired();
}

void UserIntelligenceClient::performNextRequestForUser(const std::string& username) {
    auto canonicalizedUsername = canonicalizeUsername(username);
    if (m_usernamesToDeferredRequestQueues.find(canonicalizedUsername) == m_usernamesToDeferredRequestQueues.end()) {
        return;
    }

    auto& deferredRequestQueue = m_usernamesToDeferredRequestQueues.at(canonicalizedUsername);
    if (!deferredRequestQueue.size()) {
        return;
    }

    auto& deferredRequest = deferredRequestQueue.front();
    auto& request = deferredRequest.informationRequest;
    performRequest(request, deferredRequest.completionHandler);
    deferredRequestQueue.pop();
}

void UserIntelligenceClient::deferRequest(const UserInformationRequest& request, std::function<void (const std::string&)> completionHandler) {
    auto username = canonicalizeUsername(request.username());
    m_usernamesToDeferredRequestQueues[username].push({
        .informationRequest = request,
        .completionHandler = completionHandler,
    });
}

void UserIntelligenceClient::performRequest(const UserInformationRequest& request, std::function<void (const std::string&)> completionHandler) {
    auto username = canonicalizeUsername(request.username());
    ASSERT(!hasActiveRequestForUser(username));

    if (request.privatePromptForInformation().length()) {
        m_messageSender.sendPrivateMessageForUserIntelligenceClient(*this, request.nickname(), request.privatePromptForInformation());
    }

    m_usernamesToActiveRequestContexts[username] = {
        .userInformationKey = request.userInformationKey(),
        .requestCreationTimePoint = std::chrono::steady_clock::now(),
        .completionHandler = completionHandler,
    };
}
