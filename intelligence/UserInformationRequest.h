#ifndef __USER_INFORMATION_REQUEST_H
#define __USER_INFORMATION_REQUEST_H

#include <string>

namespace samebot {

enum class InResponseToPrivateMessage {
    No,
    Yes
};

class UserInformationRequest {
public:
    UserInformationRequest(const std::string& username,
        const std::string& nickname,
        const std::string& userInformationKey,
        const std::string& responseToUserRequestInvolvingData,
        const std::string& privatePromptForInformation,
        InResponseToPrivateMessage inResponseToPrivateMessage = InResponseToPrivateMessage::No);

    const std::string& username() const { return m_username; }
    const std::string& nickname() const { return m_nickname; }
    const std::string& userInformationKey() const { return m_userInformationKey; }
    const std::string& responseToUserRequestInvolvingData() const { return m_responseToUserRequestInvolvingData; }
    const std::string& privatePromptForInformation() const { return m_privatePromptForInformation; }
    bool isInResponseToPrivateMessage() const { return m_inResponseToPrivateMessage == InResponseToPrivateMessage::Yes; }

private:
    std::string m_username;
    std::string m_nickname;
    std::string m_userInformationKey;
    std::string m_responseToUserRequestInvolvingData;
    std::string m_privatePromptForInformation;
    InResponseToPrivateMessage m_inResponseToPrivateMessage;
};

} // namespace

#endif // __USER_INFORMATION_REQUEST_H
