#include "UserInformationRequest.h"

using namespace samebot;

UserInformationRequest::UserInformationRequest(const std::string& username,
    const std::string& nickname,
    const std::string& userInformationKey,
    const std::string& responseToUserRequestInvolvingData,
    const std::string& privatePromptForInformation,
    InResponseToPrivateMessage inResponseToPrivateMessage)
    : m_username(username)
    , m_nickname(nickname)
    , m_userInformationKey(userInformationKey)
    , m_responseToUserRequestInvolvingData(responseToUserRequestInvolvingData)
    , m_privatePromptForInformation(privatePromptForInformation)
    , m_inResponseToPrivateMessage(inResponseToPrivateMessage)
{
}
