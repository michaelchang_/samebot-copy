#ifndef __USER_INFORMATION_KEYS
#define __USER_INFORMATION_KEYS

#include <string>

namespace samebot {

// These values are persisted to disk and must not be changed.
namespace UserInformationKeys {
    extern std::string homeAddressKey;
    extern std::string workAddressKey;
}

} // namespace

#endif // __USER_INFORMATION_KEYS
