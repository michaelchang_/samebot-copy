#include "UserAddressResolver.h"

#include "UserInformationKeys.h"
#include "basics/Assertions.h"
#include "basics/StringUtilities.h"
#include "basics/format.h"

using namespace samebot;

std::string UserAddressResolver::userInformationKeyForData(const std::string& data) {
    static std::vector<std::string> homeAddressTriggers {
        "home",
        "house",
        "my home",
        "my house",
        "my place",
        "the house",
        "the apartment",
    };

    if (isFuzzyMatch(data, homeAddressTriggers)) {
        return UserInformationKeys::homeAddressKey;
    }

    static std::vector<std::string> workAddressTriggers {
        "work",
        "my work",
        "office",
        "the office",
        "my office",
        "workplace",
        "the workplace",
    };

    if (isFuzzyMatch(data, workAddressTriggers)) {
        return UserInformationKeys::workAddressKey;
    }

    return "";
}

static std::string userVisibleRepresentationOfKey(const std::string& key) {
    if (key == UserInformationKeys::homeAddressKey) {
        return "home";
    }

    if (key == UserInformationKeys::workAddressKey) {
        return "work";
    }

    ASSERT_NOT_REACHED();
    return "";
}

std::string UserAddressResolver::responseToUserRequestInvolvingDataWithKey(const std::string& key) {
    auto formatString = "i need to know your {} address to tell you that. i'll follow up with you via PM";
    return fmt::format(formatString, userVisibleRepresentationOfKey(key));
}

std::string UserAddressResolver::privatePromptForInformationWithKey(const std::string& key) {
    auto formatString = "hey! please PM me your {} address";
    return fmt::format(formatString, userVisibleRepresentationOfKey(key));
}
