#ifndef __USER_INTELLIGENCE_SERVER_H
#define __USER_INTELLIGENCE_SERVER_H

#include "UserInformationSQLiteDataStore.h"

#include <memory>
#include <mutex>

namespace samebot {

class UserIntelligenceClient;
class UserIntelligenceClientMessageSender;

class UserIntelligenceServer {
public:
    static UserIntelligenceServer& shared();

    UserIntelligenceServer(const std::string& databasePath);

    std::unique_ptr<UserIntelligenceClient> createClient(UserIntelligenceClientMessageSender&);

private:
    // These are not meant to be accessed directly. You must request access via a UserIntelligenceClient to
    // actually access user data.
    std::string dataForUser(const std::string& username, const std::string& key);
    void setDataForUser(const std::string& username, const std::string& key, const std::string& value);

    UserInformationSQLiteDataStore m_userInformationDataStore;
    std::mutex m_mutex;

    friend class UserIntelligenceClient;
};

} // namespace

#endif // __USER_INTELLIGENCE_SERVER_H
