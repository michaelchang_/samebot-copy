#include "BagOfWordsSQLiteDataStore.h"

#include "basics/StringUtilities.h"

using namespace samebot;

void BagOfWordsSQLiteDataStore::countInterestingWord(std::string word) {
    addToCountsForWord(word, 1, 0);
    ++m_totalInterestingWords;
}

void BagOfWordsSQLiteDataStore::countUninterestingWord(std::string word) {
    addToCountsForWord(word, 0, 1);
    ++m_totalUninterestingWords;
}

BagOfWordsSQLiteDataStore::WordCounts BagOfWordsSQLiteDataStore::countsForWord(std::string word) {
    if (!setUpSchemaIfNeeded()) {
        return {};
    }

    canonicalizeWord(word);
    WordCounts counts {};
    if (auto enumerator = database().rowEnumeratorForSQLQuery("SELECT interesting_count, uninteresting_count FROM bag_of_words WHERE word = ?", word)) {
        (*enumerator)([&counts](const SQLiteResultRow& row) {
            counts.interestingCount = row.get<int>(0);
            counts.uninterestingCount = row.get<int>(1);
        });
    }

    return counts;
}

int BagOfWordsSQLiteDataStore::totalInterestingWords() {
    countWordsIfNecessary();
    return m_totalInterestingWords;
}

int BagOfWordsSQLiteDataStore::totalUninterestingWords() {
    countWordsIfNecessary();
    return m_totalUninterestingWords;
}

#pragma mark - Internals

void BagOfWordsSQLiteDataStore::canonicalizeWord(std::string& word) const {
    word = lowercaseString(word);
    word = stringByTrimmingString(word);
    word = stringByRemovingSpecialCharacters(word);
}

void BagOfWordsSQLiteDataStore::addToCountsForWord(std::string word, int addToInterestingCount, int addToUninterestingCount) {
    if (!setUpSchemaIfNeeded()) {
        return;
    }

    canonicalizeWord(word);
    database().executeSQL("UPDATE bag_of_words SET interesting_count = interesting_count + ?, uninteresting_count = uninteresting_count + ? WHERE word = ?", addToInterestingCount, addToUninterestingCount, word);
    database().executeSQL("INSERT OR IGNORE INTO bag_of_words VALUES (?, ?, ?)", word, addToInterestingCount, addToUninterestingCount);
    if (database().hasError()) {
        LOG_ERROR("BagOfWordsSQLiteDataStore: Could not increment (%d, %d) for word '%s': %s", addToInterestingCount, addToUninterestingCount, word.c_str(), database().errorMessage().c_str());
    }
}

void BagOfWordsSQLiteDataStore::countWordsIfNecessary() {
    if (!setUpSchemaIfNeeded()) {
        return;
    }

    if (m_totalInterestingWords || m_totalUninterestingWords) {
        return;
    }

    database().executeSQLQuery("SELECT SUM(interesting_count), SUM(uninteresting_count) FROM bag_of_words", [this](const SQLiteResultRow& row) {
        ASSERT(!m_totalInterestingWords);
        ASSERT(!m_totalUninterestingWords);

        m_totalInterestingWords = row.get<int>(0);
        m_totalUninterestingWords = row.get<int>(1);
    });
}

#pragma mark - SQLiteDataStore overrides

bool BagOfWordsSQLiteDataStore::createSchema(SQLiteDatabase& database) {
    if (!database.executeSQL("CREATE TABLE bag_of_words (word VARCHAR(255) PRIMARY KEY, interesting_count INTEGER, uninteresting_count INTEGER)")) {
        LOG_ERROR("BagOfWordsSQLiteDataStore: Could not create bag_of_words table: %s", database.errorMessage().c_str());
        return false;
    }

    return true;
}
