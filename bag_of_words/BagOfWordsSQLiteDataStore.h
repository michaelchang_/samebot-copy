#ifndef __BAG_OF_WORDS_SQLITE_DATA_STORE_H
#define __BAG_OF_WORDS_SQLITE_DATA_STORE_H

#include "BagOfWordsDataStore.h"
#include "database/SQLiteDataStore.h"

namespace samebot {

class BagOfWordsSQLiteDataStore : public SQLiteDataStore, public BagOfWordsDataStore {
public:
    using SQLiteDataStore::SQLiteDataStore;

    virtual void countInterestingWord(std::string) override;
    virtual void countUninterestingWord(std::string) override;

    virtual BagOfWordsDataStore::WordCounts countsForWord(std::string) override;
    virtual int totalInterestingWords() override;
    virtual int totalUninterestingWords() override;

private:
    void canonicalizeWord(std::string&) const;
    void addToCountsForWord(std::string word, int addToInterestingCount, int addToUninterestingCount);
    void countWordsIfNecessary();

    virtual int currentSchemaVersion() const override { return 1; }
    virtual bool createSchema(SQLiteDatabase&) override;

    // FIXME: Implement caching.
    int m_totalInterestingWords = 0;
    int m_totalUninterestingWords = 0;
};

} // namespace

#endif // __BAG_OF_WORDS_SQLITE_DATA_STORE_H

