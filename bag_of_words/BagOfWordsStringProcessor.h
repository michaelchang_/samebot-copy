#ifndef __BAG_OF_WORDS_STRING_PROCESSOR_H
#define __BAG_OF_WORDS_STRING_PROCESSOR_H

#include <functional>
#include <string>
#include <vector>

namespace samebot {

class BagOfWordsDataStore;

class BagOfWordsStringProcessor {
public:
    BagOfWordsStringProcessor(BagOfWordsDataStore&);

    void noteStringWasInteresting(const std::string&);
    void noteStringWasUninteresting(const std::string&);
    double probabilityOfStringBeingInteresting(const std::string&);
    double combinedProbabilityOfStringsBeingInteresting(const std::vector<std::string>&);

private:
    bool shouldIncludeWord(const std::string&);
    void enumerateWordsInString(const std::string&, std::function<void (const std::string& word)>);
    double combinedProbability(const std::vector<double>& probabilities);

    BagOfWordsDataStore& m_dataStore;
};

} // namespace

#endif // __BAG_OF_WORDS_STRING_PROCESSOR_H

