#include "BagOfWordsStringProcessor.h"

#include "BagOfWordsDataStore.h"
#include "basics/StringUtilities.h"
#include <cmath>

using namespace samebot;

BagOfWordsStringProcessor::BagOfWordsStringProcessor(BagOfWordsDataStore& dataStore)
    : m_dataStore(dataStore)
{
}

void BagOfWordsStringProcessor::noteStringWasInteresting(const std::string& string) {
    enumerateWordsInString(string, [this](const std::string& word) {
        m_dataStore.countInterestingWord(word);
    });
}

void BagOfWordsStringProcessor::noteStringWasUninteresting(const std::string& string) {
    enumerateWordsInString(string, [this](const std::string& word) {
        m_dataStore.countUninterestingWord(word);
    });
}

double BagOfWordsStringProcessor::probabilityOfStringBeingInteresting(const std::string& string) {
    std::vector<double> probabilities;
    int totalInterestingWords = m_dataStore.totalInterestingWords();
    int totalUninterestingWords = m_dataStore.totalUninterestingWords();
    for (auto word : splitString(string)) {
        if (!shouldIncludeWord(word)) {
            continue;
        }

        auto counts = m_dataStore.countsForWord(word);
        double probabilityOfWordGivenInteresting = 0;
        double probabilityOfWordGivenUninteresting = 0;
        if (totalInterestingWords) {
            probabilityOfWordGivenInteresting = static_cast<double>(counts.interestingCount) / totalInterestingWords;
        }
        if (totalUninterestingWords) {
            probabilityOfWordGivenUninteresting = static_cast<double>(counts.uninterestingCount) / totalUninterestingWords;
        }
        if (probabilityOfWordGivenInteresting || probabilityOfWordGivenUninteresting) {
            auto probabilityOfInterestingGivenWord = probabilityOfWordGivenInteresting / (probabilityOfWordGivenInteresting + probabilityOfWordGivenUninteresting);
            probabilities.push_back(probabilityOfInterestingGivenWord);
        }
    }

    return combinedProbability(probabilities);
}

double BagOfWordsStringProcessor::combinedProbabilityOfStringsBeingInteresting(const std::vector<std::string>& strings) {
    std::vector<double> probabilities;
    for (const auto& string : strings) {
        probabilities.push_back(probabilityOfStringBeingInteresting(string));
    }

    return combinedProbability(probabilities);
}

#pragma mark - Internals

bool BagOfWordsStringProcessor::shouldIncludeWord(const std::string& word) {
    // Ignore really short words as they're likely not worth considering.
    if (word.length() <= 3) {
        return false;
    }

    // Ignore words ending with colons as they're likely mentions.
    if (word[word.length() - 1] == ':') {
        return false;
    }

    // Ignore words beginning and ending with angle brackets as they're likely samebot memory quotes.
    if (word[0] == '<' && word[word.length() - 1] == '>') {
        return false;
    }

    // Ignore words with apostrophes in them as many common words and contractions that we'd want to ignore contain apostrophes.
    // Obviously this isn't always correct, but it should be correct enough of the time.
    if (word.find('\'') != std::string::npos) {
        return false;
    }

    return true;
}

void BagOfWordsStringProcessor::enumerateWordsInString(const std::string& string, std::function<void (const std::string&)> function) {
    for (auto line : splitString(string, '\n')) {
        for (auto word : splitString(line)) {
            if (shouldIncludeWord(word)) {
                function(word);
            }
        }
    }
}

double BagOfWordsStringProcessor::combinedProbability(const std::vector<double>& probabilities) {
    // Do this in the log domain to avoid floating point underflow.
    double sumOfLogProbabilityDifferences = 0;
    for (auto probability : probabilities) {
        if (!probability) {
            return 0;
        }
        sumOfLogProbabilityDifferences += log(1 - probability) - log(probability);
    }
    return 1 / (1 + exp(sumOfLogProbabilityDifferences));
}

