#ifndef __BAG_OF_WORDS_DATA_STORE_H
#define __BAG_OF_WORDS_DATA_STORE_H

#include <string>

namespace samebot {

class BagOfWordsDataStore {
public:
    virtual void countInterestingWord(std::string) = 0;
    virtual void countUninterestingWord(std::string) = 0;

    struct WordCounts {
        int interestingCount;
        int uninterestingCount;
    };

    virtual WordCounts countsForWord(std::string word) = 0;

    virtual int totalInterestingWords() = 0;
    virtual int totalUninterestingWords() = 0;
};

} // namespace

#endif // __BAG_OF_WORDS_DATA_STORE_H

